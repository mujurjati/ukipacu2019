var Site = {
	init: function() {
		Site.ChangePassword();
		Site.OnchangeCategory();
		Site.ChangeStatusOrder();
		Site.AddColorStock();
		Site.editCategory();
		Site.allBtnDelete();
		Site.OnchageContentCategory();
	},

	ChangePassword: function(){
		var check = $('#ganti-password');
		if (!check.length) return;

		check.on('click', function(){
			$('input[name="userPassword"]').css({"display":"block"});
		});
	},

	OnchangeCategory: function(){
		var check = $('#p_cat_id_parent');
		if (!check.length) return;

		check.change(function(){
			$('#p_cat_id_2and').empty();
			$('#p_cat_id_2and').prepend('<option value="">Pilih</option>');
			$.get("/caterin-panel/product/getChildCat/"+check.val(), function(data){
				$.each(data.data, function(key, value){
					$('#p_cat_id_2and').append('<option value="' + value.p_cat_id + '">' + value.p_cat_name + '</option>');
				});
			});
		});
		$('#p_cat_id_2and').change(function(){
			$('#p_cat_id_child').empty();
			$('#p_cat_id_child').prepend('<option value="">Pilih</option>');
			$.get("/caterin-panel/product/getLastCat/"+$('#p_cat_id_2and').val(), function(data){
				$.each(data.data, function(key, value){
					$('#p_cat_id_child').append('<option value="' + value.p_cat_id + '">' + value.p_cat_name + '</option>');
				});
			});
		});
		$('#p_cat_id_child').change(function(){
			$.get("/caterin-panel/product/getAttribut/"+$('#p_cat_id_child').val(), function(data){
				if (data.data != null) {$('#attribut').show();}
				$.each(data.data, function(key, value){
					$('#showAttribute').append(
						'<div class="form-group row">'+
						'<label class="col-sm-4 col-form-label">'+value.p_attribute_name+'</label>'+
						'<div class="col-sm-8">'+
						'<input class="form-control" placeholder="'+value.p_attribute_alias+'" name="atribut['+value.p_attribute_alias+']" type="text" value="">'+
						'</div>'+
						'</div>'
					);
				});
			});
		});
	},

	OnchageContentCategory: function(){
		var check = $('#c_cat_id_parent');
		if (!check.length) return;

		check.change(function(){
			$('#c_cat_id_2and').empty();
			$('#c_cat_id_2and').prepend('<option value="">Pilih</option>');
			$.get("/caterin-panel/content/contentCatChild/"+check.val(), function(data){
				$.each(data.data, function(key, value){
					$('#c_cat_id_2and').append('<option value="' + value.c_cat_id + '">' + value.c_cat_name + '</option>');
				});
			});
		});

		$('#c_cat_id_2and').change(function(){
			$('#c_cat_id_child').empty();
			$('#c_cat_id_child').prepend('<option value="">Pilih</option>');
			$.get("/caterin-panel/content/ContentCat/"+$('#c_cat_id_2and').val(), function(data){
				$.each(data.data, function(key, value){
					$('#c_cat_id_child').append('<option value="' + value.c_cat_id + '">' + value.c_cat_name + '</option>');
				});
			});
		});
	},

	AddColorStock: function(){
		var check = $('#addStock');

		if (!check.length) return;

		check.on('click', function(){
			$('#formColor').append(
				'<div class="no" hidden></div>'
                +'<div class="form-group row rowColor list-'+$('.no').length+'">'+
                    '<label class="col-sm-4 col-form-label"></label>'+
                    '<div class="col-sm-3">'+
                    	'<input class="form-control" placeholder="stock option" name="p_stock_option_new['+$('.no').length+']" type="text" value="">'+
                    '</div>'+
                    '<div class="col-sm-3">'+
                    	'<input class="form-control" placeholder="stok" name="p_stock_stock_new['+$('.no').length+']" type="text" value="">'+
                    '</div>'+
					'<div class="col-sm-2 mt-1">'+
						'<a href="javascript:;" class="text-primary text-left" onclick="delstock('+$('.no').length+')"><i class="mdi mdi-delete-forever h3"></i></a>'+
					'</div>'+
                '</div>');
			return false;

		});
	},

	editCategory: function(){
		var check = $('#btnEditCat');
		if (!check.length) return;

		check.on('click', function(){
			alertify.confirm("Apakah Anda Yakin ingin mengganti Cateogri?",
				function(){
					$('#categoryName').hide();
					$('.editCategory').show();
					$('#btnEditCat').hide();
					$('#showAttribute div').remove();
					$('#attribut').hide();
				},
				function(){
					return false
				});
		});
	},

	ChangeStatusOrder: function(){
		var check = $('.statusOrder');
		if (!check.length) return;
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		check.change(function(){
			$.get("/caterin-panel/order/changeStatus/"+$(this).data('id')+"/"+$(this).val(), function(data){
				alertify.confirm("Success",
				function(){
					return false;
				},
				function(){
					return false
				});
			});
		});
	},

	allBtnDelete: function(){
		var check = $('.btnDelete');
		if (!check.length) return;

		check.on('click', function(){
			var dataId = $(this).data('id');
			var dataName = 'akan menghapus '+$(this).data('title');

		alertify.confirm("Apakah Anda yakin " + dataName+"?",
				function(){
					$('#deleteData'+dataId).submit();
				},
				function(){
					return false
				});
		});

	}
};

$(document).ready(function() {
	Site.init();

	$(document).on('keyup change keypress', '.hitung-weight', function () {
        var length  =parseInt($('#panjang').val());
        var width   =parseInt($('#lebar').val());
        var height  =parseInt($('#tinggi').val());
        var weight=(length * width * height)/ 4000;
        var weightOnKG=weight;
        if (length>0 & width>0 & height>0) {
            $("#berat").val(weightOnKG);
        }
    });

	$(document).on('keyup', '.hitung-hargaJual', function () {
		var harga 	= ($('.harga').val() == '') ? 0 : $('.harga').val().split('.').join('');
		var diskon 	= ($('.discount').val() == '') ? 0 : $('.discount').val().split('.').join('');

		var hargaJual = formatRupiah(harga-diskon);

		$('.harga-jual').val(hargaJual);
    });

	$(".rupiah").maskMoney({thousands:'.', decimal:',', allowZero:true, precision: 0});

    function formatRupiah(angka){
        var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return rupiah;
    }
});

function delstock(id) {
	$('.list-'+id).remove();
}

function delstockRow(id, name, url) {
    alertify.confirm("Anda yakin akan menghapus data stok "+name+" ?", function(asc) {
        var formData = {
            "id": id
        };
        if (asc) {
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': _token
                },
                success: function (response) {
                	console.log(response);
                    if(response.success) {
                        $('.row-stok-'+response.id).remove();
                    }
                }
            })
        }
    });
}