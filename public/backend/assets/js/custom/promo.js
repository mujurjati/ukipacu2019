$(function () {
    $("#datepicker1").datepicker({
        autoclose: !0,
        todayHighlight: !0,
        format: 'yyyy-mm-dd',
    });

    $("#datepicker2").datepicker({
        autoclose: !0,
        todayHighlight: !0,
        format: 'yyyy-mm-dd',
    });

    $(".rupiah").maskMoney({thousands:'.', decimal:',', allowZero:true, precision: 0});

    if ($('.cek-tipe').val() === 'price') {
        $('.mak-diskon').hide();
        $('.value-price').show();
        $('.value-percent').hide();
    } else {
        $('.value-percent').show();
        $('.value-price').hide();
    }
});

$(document).on('change', '.cek-tipe', function () {
    var value = $(this).val();
    if (value == 'percent') {
        $('.mak-diskon').show();
        $('.value-price').hide();
        $('.value-percent').show();
    }
    else {
        $('.mak-diskon').hide();
        $('.value-percent').hide();
        $('.value-price').show();
    }
});

$(document).on('keyup', '.validasi-percent', function () {
    var val = $(this).val();
    var valreplace = val.replace(".", "");
    if ( valreplace > 100){
        $(this).val('100');
    }
});
