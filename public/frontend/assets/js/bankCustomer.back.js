$(function () {
    $(".del-bankCus").on("click", function() {
        var id      = $(this).data('no');

        swal({
            title: "Anda yakin akan menghapus bank ini?",
            text: name,
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true,
        }).then(function(willDelete) {
            if(willDelete) {
                var formData = {
                    "id": id
                };

                ajax_bank_del(formData);
            }
        });
    });
});

function ajax_bank_del(data) {
    $.ajax({
        url: base_url+'customers/bank-del',
        type: 'POST',
        data: data,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': _token
        },
        success: function (response) {
            if(response.success) {
                $('.row'+response.id).remove();
            }
        }
    })
}