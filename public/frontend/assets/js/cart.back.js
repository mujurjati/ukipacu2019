$(function () {
    $(".act-item-remove").on("click", function() {
        var id      = $(this).parents('tr').data('id');
        var name    = $(this).parents('tr').data('name');

        swal({
            title: "Anda yakin akan menghapus produk ini ?",
            text: name,
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true,
        }).then(function(willDelete) {
            if(willDelete) {
                var formData = {
                    "type": "remove",
                    "id": id
                };

                ajax_cart(formData);

                $(".cart-list[data-id='"+id+"']").remove();
            }
        })
    });

    $(".act-item-qty").on("change", function () {
        var id  = $(this).parents('tr').data('id');
        var qty = $(this).val();

        var formData = {
            "type": "qty",
            "id": id,
            "qty": qty
        };

        ajax_cart(formData);
    });

    $(".act-clear-cart").on("click", function () {
        swal({
            title: "Clear Shopping Cart",
            text: 'Anda yakin akan mengkosongkan keranjang belanja ?',
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true,
        }).then(function(willDelete) {
            if(willDelete) {
                var formData = {
                    "type": "clear"
                };

                ajax_cart(formData);
            }
        })
    });

    if($('#selectAddress').length > 0) {
        $('#selectAddress').select2({
            theme: "bootstrap",
            placeholder: 'Pilih Kota atau Kecamatan',
            dropdownAutoWidth: true,
            minimumInputLength: 3,
            ajax: {
                url: $("#selectAddress").data('url'),
                type: 'post',
                headers: { 'X-CSRF-TOKEN': _token },
                dataType: 'json',
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data,
                        pagination: {
                            more: (params.page * 10) < data.total
                        }
                    };
                }
            }
        });
    }

    $("#addAddress").on("click", function () {
        $.preloader.start({
            modal: true,
            src : 'sprites.png'
        });

        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            headers: { 'X-CSRF-TOKEN': _token },
            data: $("#formAddAddr").serializeArray(),
            dataType: 'json',
            success: function (res) {
                $.preloader.stop();

                if(res.success) {
                    $('#cusAddr').modal("hide");

                    location.reload();
                }
            }
        });
    });

    $(".act-select-addr").on("click", function() {
        $.preloader.start({
            modal: true,
            src : 'sprites.png'
        });

        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            headers: { 'X-CSRF-TOKEN': _token },
            dataType: 'json',
            success: function (res) {
                $.preloader.stop();

                if(res.success)
                    location.reload();
            }
        });
    });

    $("#selectCourier").on("change", function () {
        $.preloader.start({
            modal: true,
            src : 'sprites.png'
        });

        var courier = $("#selectCourier option:selected").val();

        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            data: { courier: courier },
            headers: { 'X-CSRF-TOKEN': _token },
            dataType: 'json',
            success: function (res) {
                $.preloader.stop();

                var text = '';

                $.each(res, function(key, item) {
                    text += '<tr>';
                    text += '    <td width="20">';
                    text += '       <div class="custom-control custom-radio custom-control-inline">';
                    text += '           <input type="radio" name="courierPackage" value="'+item.price+'" data-courier="'+courier+'" data-service="'+item.service+'" data-etd="'+item.etd+'" id="customRadioInline'+key+'" class="custom-control-input act-select-package">';
                    text += '           <label class="custom-control-label" for="customRadioInline'+key+'"></label>';
                    text += '       </div>';
                    text += '    </td>';
                    text += '    <td><strong>'+item.service+' ('+item.name+')</strong></td>';
                    text += '    <td>'+item.etd+'</td>';
                    text += '    <td>'+item.note+'</td>';
                    text += '    <td align="right"><strong>Rp '+formatNumber(item.price)+'</strong></td>';
                    text += '</tr>';
                });

                if(text == '') {
                    text += '<tr>';
                    text += '   <td>';
                    text += '       <div class="alert alert-warning text-center">Tidak terdapat data service pengiriman untuk Kurir yang anda pilih</div>';
                    text += '   </td>';
                    text += '</tr>';

                    $("#toPayment").attr('disabled', true);
                }

                $("#wrapShipping").html(text);
            }
        })
    });

    $(document).on("change", ".act-select-package", function() {
        $.preloader.start({
            modal: true,
            src : 'sprites.png'
        });

        $.ajax({
            url: $("#wrapShipping").data('url'),
            type: 'post',
            data: {
                courier: $(this).data('courier'),
                service: $(this).data('service'),
                etd: $(this).data('etd'),
                price: $(this).val()
            },
            headers: { 'X-CSRF-TOKEN': _token },
            dataType: 'json',
            success: function (res) {
                $.preloader.stop();

                if(res.success) {
                    $(".con-shipping").html('Rp '+res.shipping);
                    $(".cart-total").html('Rp '+res.total);

                    $("#toPayment").removeAttr('disabled');
                }
            }
        });
    });

    $("#checkVoucher").on("click", function () {
        $.preloader.start({
            modal: true,
            src : 'sprites.png'
        });

        var voucher = $("#cartVoucher").val();

        if(voucher == '') {
             swal("Maaf !", "Silahkan masukan kode voucher Anda", "error");
        }
        else {
            $.ajax({
                url: $(this).data('url'),
                type: 'post',
                data: { voucher: voucher },
                headers: { 'X-CSRF-TOKEN': _token },
                dataType: 'json',
                success: function (res) {
                    $.preloader.stop();

                    if(res.success) {
                        swal("Hore !", res.message, "success");

                        $(".con-diskon").html(res.discount);
                        $(".cart-total").html(res.total);
                    }
                    else {
                        swal("Maaf !", res.message, "error");
                    }
                }
            });
        }
    });

    $("#toPayment").on("click", function () {
        $.preloader.start({
            modal: true,
            src : 'sprites.png'
        });

        var url = $(this).data('url');

        $.ajax({
            url: url+'/check',
            type: 'post',
            headers: { 'X-CSRF-TOKEN': _token },
            dataType: 'json',
            success: function (res) {
                $.preloader.stop();

                if(res.success) {
                    location.href=url;
                }
                else {
                    swal("Maaf !", res.message, "error");
                }
            }
        });
    });

    $('.act-select-bank').on("change", function () {
         if($(".act-select-bank:checked").val().length > 0)
             $("#savePayment").removeAttr('disabled');
    });

    $("#savePayment").on("click", function () {
        if($(".act-select-bank:checked").val().length > 0) {
            $.preloader.start({
                modal: true,
                src : 'sprites.png'
            });

            $.ajax({
                url: $(this).data('url'),
                type: 'post',
                data: { bank_id: $(".act-select-bank:checked").val() },
                headers: { 'X-CSRF-TOKEN': _token },
                dataType: 'json',
                success: function (res) {
                    $.preloader.stop();

                    if(res.success) {
                        swal("Hore !", res.message, "success")
                            .then(function(willOk) {
                                if(willOk)
                                    location.href=res.url;
                            });
                    }
                    else {
                        swal("Maaf !", res.message, "error");
                    }
                }
            })
        }
        else {
            swal("Maaf !", "Silahkan pilih Metode Pembayaran terlebih dahulu", "error");
        }
    });
});

function ajax_cart(data) {
    $.ajax({
        url: base_url+'ajax/cart/edit',
        type: 'POST',
        data: data,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': _token
        },
        success: function (response) {
            if(response.success) {
                if(response.subtotal != undefined) {
                    $(".cart-list[data-id='"+response.id+"'] .item-subtotal").html(response.subtotal);
                }

                if(response.cart != undefined) {
                    $.each(response.cart, function (key, val) {
                        $("."+key).html(val);
                    });
                }

                if(response.type == 'clear') {
                    location.href=base_url;
                }
            }
        }
    })
}