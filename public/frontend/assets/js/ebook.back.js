$('.act-select-bank').on("change", function () {
    if($(".act-select-bank:checked").val().length > 0)
        $("#savePaymentEbook").removeAttr('disabled');
});

$("#savePaymentEbook").on("click", function () {
    var valBank = $(".act-select-bank:checked").val();
    if(valBank.length > 0) {
        $.preloader.start({
            modal: true,
            src : 'sprites.png'
        });

        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            data: {"id_bank": valBank},
            headers: { 'X-CSRF-TOKEN': _token },
            dataType: 'json',
            success: function (res) {
                $.preloader.stop();

                if(res.success) {
                    location.href=res.url;
                }
                else {
                    swal("Maaf !", res.message, "error");
                }
            }
        })
    }
    else {
        swal("Maaf !", "Silahkan pilih Metode Pembayaran terlebih dahulu", "error");
    }
});