$(function () {
    $(".update-langganan").on("click", function() {
        var id      = $(this).data('no');
        var title   = $(this).data('title');
        swal({
            title: "Anda yakin akan membatalkan langganan "+title+" ini?",
            text: name,
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true,
        }).then(function(willDelete) {
            if(willDelete) {
                var formData = {
                    "id": id
                };

                ajax_update_subs(formData);
            }
        });
    });
});
function ajax_update_subs(data) {
    $.ajax({
        url: base_url+'customers/cancel-subscribe',
        type: 'POST',
        data: data,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': _token
        },
        success: function (response) {
            console.log(response);
            if(response.success) {
                location.href = response.url;
            }
        }
    })
}
$(".act-select-bank").on("change", function () {
    if ($(".act-select-bank:checked").val().length > 0) {
        $("#savePaymentEbook").removeAttr("disabled");
    }
});
$("#savePaymentEbook").on("click", function () {
    var valBank = $(".act-select-bank:checked").val();
    if (valBank.length > 0) {
        $.preloader.start({modal: true, src: "sprites.png"});
        $.ajax({
            url: $(this).data("url"),
            type: "post",
            data: {id_bank: valBank},
            headers: {"X-CSRF-TOKEN": _token},
            dataType: "json",
            success: function (res) {
                $.preloader.stop();
                if (res.success) {
                    location.href = res.url;
                } else {
                    swal("Maaf !", res.message, "error");
                }
            }
        });
    } else {
        swal("Maaf !", "Silahkan pilih Metode Pembayaran terlebih dahulu", "error");
    }
});