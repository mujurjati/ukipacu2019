$(function () {
    $(".act-cancel-order").on("click", function () {
        var id  = $(this).data('id');
        var url = $(this).data('url');

        swal({
            title: "Batalkan pesanan",
            text: 'Anda yakin akan membatalkan pesanan '+id+' ?',
            icon: "warning",
            buttons: ["Tidak", "Batalkan"],
            dangerMode: true
        }).then(function(willCancel) {
            if(willCancel) {
                $.preloader.start({
                    modal: true,
                    src : 'sprites.png'
                });

                $.ajax({
                    url: url,
                    type: 'post',
                    data: { order_number: id },
                    headers: { 'X-CSRF-TOKEN': _token },
                    dataType: 'json',
                    success: function (res) {
                        $.preloader.stop();

                        if(res.success) {
                            swal("Berhasil !", res.message, "success")
                                .then(function(willOk) {
                                    if(willOk)
                                        location.reload();
                                });
                        }
                        else {
                            swal("Maaf !", res.message, "error");
                        }
                    }
                })
            }
        });
    });

    $(".act-receive-order").on("click", function () {
        var id  = $(this).data('id');
        var url = $(this).data('url');

        swal({
            title: "Terima barang",
            text: 'Anda yakin akan sudah menerima semua barang dari pesanan ini ?',
            icon: "warning",
            buttons: ["Tidak", "Terima"],
            dangerMode: true
        }).then(function(willCancel) {
            if(willCancel) {
                $.preloader.start({
                    modal: true,
                    src : 'sprites.png'
                });

                $.ajax({
                    url: url,
                    type: 'post',
                    data: { order_number: id },
                    headers: { 'X-CSRF-TOKEN': _token },
                    dataType: 'json',
                    success: function (res) {
                        $.preloader.stop();

                        if(res.success) {
                            swal("Berhasil !", res.message, "success")
                                .then(function(willOk) {
                                    if(willOk)
                                        location.reload();
                                });
                        }
                        else {
                            swal("Maaf !", res.message, "error");
                        }
                    }
                })
            }
        });
    });
});