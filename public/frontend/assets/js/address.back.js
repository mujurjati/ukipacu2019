$(function () {
    $(".update-add").on("click", function() {
        var id      = $(this).val();
        swal({
            title: "Anda yakin akan mengganti alamat pengiriman ?",
            text: name,
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true
        }).then(function(willDelete) {
            if(willDelete) {
                var formData = {
                    "id": id
                };

                ajax_addr(formData);
            }
        });
    });

    $(".del-addrCus").on("click", function() {
        var id      = $(this).data('no');

        swal({
            title: "Anda yakin akan menghapus alamat pengiriman ini?",
            text: name,
            icon: "warning",
            buttons: ["Tidak", "Ya"],
            dangerMode: true,
        }).then(function(willDelete) {
            if(willDelete) {
                var formData = {
                    "id": id
                };

                ajax_addr_del(formData);
            }
        });
    });
});

function ajax_addr(data) {
    $.ajax({
        url: base_url+'customers/address-update-primary',
        type: 'POST',
        data: data,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': _token
        },
        success: function (response) {
            console.log(response);
        }
    })
}

function ajax_addr_del(data) {
    $.ajax({
        url: base_url+'customers/address-del',
        type: 'POST',
        data: data,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': _token
        },
        success: function (response) {
            if(response.success) {
                $('.row'+response.id).remove();
            }
        }
    })
}
