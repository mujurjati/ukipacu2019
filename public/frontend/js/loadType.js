$(document).ready(function () {
    $(document).on('click', '.load-lunchBox', function (e) {
        console.log($(this).data('url'));
        e.preventDefault();

        var id = $(this).data('id');
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {id: id, sub: _submenu, _token: _token},
            dataType: "json",
            success: function (res) {

                if (res.data != '' && res.type == 'lunch-box') {
                    $('.lunch-box').remove();
                    $('.Lunch-box').append(res.data);

                }
                else if(res.data != '' && res.type == 'sharedmeals')
                {
                    $('.sharedmeals').remove();
                    $('.Sharedmeals').append(res.data);
                }
                else if(res.data != '' && res.type == 'drinks')
                {
                    $('.drinks').remove();
                    $('.Drinks').append(res.data);
                }
                console.log(res);
            }
        });
    });

    /*$(document).on('click','.load-more-galery',function(e){
        e.preventDefault();

        var id = $(this).data('id');
        $.ajax({
            url : $(this).data('url'),
            type: 'POST',
            data : {id:id, _token:_token},
            dataType : "json",
            success : function (res)
            {
                if(res.data != '')
                {
                    $('#remove-row-galery').remove();
                    $('.parent-galery').append(res.data);
                }
                else
                {
                    $('#btn-more').html("No Data");
                }
                var count = $('.sections-wrapper section').length;
                var vWidth = $(window).width();
                var vheight = $(window).height();
                $('.sections-wrapper > section').css('width', vWidth);
                $('.sections-wrapper').css('width', vWidth * count).css('height', $('.sections-wrapper section.active').outerHeight());
            }
        });
    });*/
});