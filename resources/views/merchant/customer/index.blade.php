@extends('merchant.layouts.app')
@section('title', 'Daftar Pelanggan')
@section('content')
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">

            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Daftar Member</li>
                    </ol>
                </div>
            </div>
        </div>
        @include('backend.layouts.info')
        @if(count($data) > 0)
            <div class="row">
                <!-- column -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="">Daftar Member</h4>
{{--                            <a href="{{route('cus.add')}}" class="btn btn-success float-right" style="margin-bottom: 10px; color: white">Filter</a>--}}
                            <div class="table-responsive">
                                <table id="example" class="display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Member</th>
                                        <th>Atasan</th>
                                        <th>Email</th>
                                        <th>Profit</th>
                                        <th>Status</th>
                                        <th>Created Date</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $key  => $value)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$value->customer_name}}</td>
                                            <td>{{($value->cusAtasan != null ? $value->cusAtasan->customer_name : 'Tidak Punya Atasan')}}</td>
                                            <td>{{$value->customer_email}}</td>
                                            <td>{{profit($value->customer_count)}}</td>
                                            <td>{{label_status($value->customer_status)}}</td>
                                            <td>{{date_indo($value->customer_create_date,'datetime')}}</td>
                                            <td>
                                                <a href="{{route('cus.detail', ['id' => $value->customer_id])}}"><span class="label label-info">Detail</span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        @else
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="">Daftar Pelanggan</h4>
                            <a href="{{route('cus.add')}}" class="btn btn-success float-right" style="margin-bottom: 10px; color: white">Tambah</a>
                            <div class="table-responsive text-center">
                                <hr>
                                <h5>Data Member Kosong</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });

    </script>

@endsection
