@extends('merchant.layouts.app')
@section('title', 'Detail Member')
@section('content')

    <style>
        .custom-control {
            padding-left: 0;
            margin-top: 0;
            margin-bottom: 0;
            min-height: 2.5rem;
        }
    </style>

    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">

            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Detail Member</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 style="text-decoration: underline">Data Member</h4>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Atasan</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{($detail->cusAtasan != null ? $detail->cusAtasan->customer_name : 'Tidak Punya Atasan')}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Nama Lengkap</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->customer_name}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Email</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->customer_email}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->customer_birth_date}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">No. KTP</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->customer_nik}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Alamat</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->customer_address}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Kota atau Kecamatan</label>
                                <div class="col-sm-9">
{{--                                    <select name="city" id="city" class="select-address">--}}
{{--                                        <option value="">======= Silahkan Pilih Alamat ===================</option>--}}
{{--                                    </select>--}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">NPWP</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->customer_npwp}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Phone / WA</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->customer_phone}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <h4 style="text-decoration: underline">Data Penungjang</h4>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Pendidikan Terakhir</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->penunjang->c_pendidikan}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Pekerjaan</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->penunjang->c_pekerjaan}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Agama</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->penunjang->c_agama}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Nama Ibu Kandung</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->penunjang->c_ibu_kandung}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Nama Ahli Waris</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->penunjang->c_ahli_waris}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Hubungan dengan Ahli Waris</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->penunjang->c_hub_waris}}</p>
                                </div>
                            </div>

                            <h4 style="text-decoration: underline">Data Bank</h4>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Nama Bank</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->bank->s_bank_name}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Cabang</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->bank->c_bank_cabang}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Nama Nasabah</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->bank->c_bank_nasabah}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-sm-3 col-form-label">Nomor Rekening</label>
                                <label  class="col-sm-1 col-form-label">:</label>
                                <div class="col-sm-8 col-form-label">
                                    <p>{{$detail->bank->c_bank_rek}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row " style="text-align: right">
                        <div class="col-sm-12" style="margin-top: 20px">
                            <a href="{{route('cus.list')}}" class="btn btn-info waves-effect waves-light">Kembali</a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
