@extends('merchant.layouts.app')
@section('title', 'Daftar Pelanggan')
@section('content')

    <style>
        .custom-control {
            padding-left: 0;
            margin-top: 0;
            margin-bottom: 0;
            min-height: 2.5rem;
        }
    </style>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">

            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Tambah Pelanggan</li>
                    </ol>
                </div>
            </div>
        </div>
                    <div class="card">
                        <div class="card-body">
                            <form style="margin-top: 10px" action="{{ route('cus.add.act') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                            <div class="col-lg-6">
                            <h4 style="text-decoration: underline">Data Member</h4>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_name" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_email" type="email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" placeholder="Misalkan : 16 Desember 1994" name="customer_tlg" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">No. KTP</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_no_ktp" type="number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="customer_address" type="text" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Kota atau Kecamatan</label>
                                    <div class="col-sm-9">
                                        <select name="city" id="city" class="select-address">
                                            <option value="">======= Silahkan Pilih Alamat ===================</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">NPWP</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_npwp" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Phone / WA</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_phone" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Password</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_password" type="password">
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-6">
                                <h4 style="text-decoration: underline">Data Penungjang</h4>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Pendidikan Terakhir</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_pendidikan" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_perkerjaan" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Agama</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_agama" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Ibu Kandung</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_ibu" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Ahli Waris</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_waris" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Hubungan dengan Ahli Waris</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_hub_waris" type="text">
                                    </div>
                                </div>

                                <h4 style="text-decoration: underline">Data Bank</h4>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Bank</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_bank_name" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Cabang</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_bank_cabang" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Nasabah</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_bank_nasabah" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nomor Rekening</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="customer_bank_rek" type="number">
                                    </div>
                                </div>
                            </div>
                        </div>
{{--                    </div>--}}
                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12" style="margin-top: 20px">
                                        <button class="btn btn-success waves-effect waves-light" name="addCusByMer" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
{{--    <script src="{{ asset_url('/frontend/assets/js/address.js') }}"></script>--}}
    <script>
        var _token = '{{ csrf_token() }}';
        $(document).ready(function () {
            $('.select-address').select2({
                theme: "bootstrap",
                placeholder: 'Pilih Kota atau Kecamatan',
                dropdownAutoWidth: true,
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('ajax.get-address') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });
        });

        $(document).ready(function () {
            $('.select-atasan').select2({
                theme: "bootstrap",
                placeholder: 'Pilih Nama Atasan',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('ajax.get-customer') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 7) < data.total
                            }
                        };
                    }
                }
            });
        });
    </script>
@endsection
