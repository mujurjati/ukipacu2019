<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="{{ in_array(Request::url(), [route('merchant.home')]) ? 'active' : '' }}">
                    <a href="{{route('merchant.home')}}" >
                        <i class="icon-speedometer"></i>Dashboard </a>
                </li>
                <li class="{{ in_array(Request::url(), []) ? 'active' : '' }}">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-layout-grid2"></i><span class="hide-menu">Transaksi</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <!-- <li><a href="{{route('trx.list', 'new')}}">Pesanan Baru</a></li>
                        <li><a href="{{route('trx.list', 'process')}}">Pesanan Diproses</a></li>
                        <li><a href="{{route('trx.list', 'pickup')}}">Proses Pickup</a></li>
                        <li><a href="{{route('trx.list', 'shipment')}}">Proses Pengiriman</a></li>
                        <li><a href="{{route('trx.list', 'cancel')}}">Pesanan Dibatalkan</a></li>
                        <li><a href="{{route('trx.list', 'refund')}}">Pesanan Dikembalikan</a></li>
                        <li><a href="{{route('trx.list', 'finish')}}">Pesanan Selesai</a></li> -->
                    </ul>
                </li>
                <li class="{{ in_array(Request::url(), [route('cus.list'), route('cus.add')]) ? 'active' : '' }}">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-user"></i><span class="hide-menu">Member</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('cus.list')}}">List</a></li>
                        <li><a href="{{route('cus.add')}}">Tambah</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
