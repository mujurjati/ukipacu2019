<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset_url('/frontend/images/favicon.png')}}" type="image/x-icon"/>
    <title>@yield('title') - {{ config('app.name') }}</title>
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="{{asset_url('/merchant/assets/node_modules/morrisjs/morris.css')}}" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{asset_url('/merchant/assets/node_modules/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset_url('/merchant/assets/css/style.min.css')}}" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="{{asset_url('/merchant/assets/css/pages/dashboard1.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <script src="{{asset_url('/merchant/assets/node_modules/jquery/jquery-3.2.1.min.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>


</head>

<body class="horizontal-nav skin-megna fixed-layout">
    <div class="main-wrapper">
        {{--HEADER--}}
        @include('merchant.layouts.header')
        {{--END HEADER--}}

        {{--SIDEBAR--}}
        @include('merchant.layouts.sidebar')
        {{--END SIDEBAR--}}

        <div class="page-wrapper">
            @yield('content');
        </div>

        <footer class="footer">
            Copyrights © 2020 All Rights Reserved by Ukipacu.
        </footer>
    </div>

    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap popper Core JavaScript -->
    <script src="{{asset_url('/merchant/assets/node_modules/popper/popper.min.js')}}"></script>
    <script src="{{asset_url('/merchant/assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset_url('/merchant/assets/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset_url('/merchant/assets/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset_url('/merchant/assets/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset_url('/merchant/assets/js/custom.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="{{asset_url('/merchant/assets/node_modules/raphael/raphael-min.js')}}"></script>
    <script src="{{asset_url('/merchant/assets/node_modules/morrisjs/morris.min.js')}}"></script>
    <script src="{{asset_url('/merchant/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- Popup message jquery -->
    <script src="{{asset_url('/merchant/assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <!-- Chart JS -->
    <script src="{{asset_url('/merchant/js/dashboard1.js')}}"></script>
    <script src="{{asset_url('/merchant/assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>
        $(function(){
            $('#chat, #msg, #comment, #todo').perfectScrollbar();
        });
    </script>
</body>
</html>
