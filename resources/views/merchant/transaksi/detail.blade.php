@extends('merchant.layouts.app')
@section('title', 'Detail Transaksi')
@section('content')
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Detail Transaksi {{$detailTrx->order_number}}</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('merchant.home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Detail Transaksi</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-body printableArea">
                    <h3><b>INVOICE</b> <span class="pull-right">#{{$detailTrx->order_number}}</span></h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <address>
                                    <h3>Kepada,</h3>
                                    <h4 class="font-bold">{{$detailTrx->customer->customer_name}}</h4>
                                    <p class="text-muted m-l-30">{{$detailTrx->order_shipment_address}}</p>
                                    <p class="m-t-30"><b>Tanggal Pengiriman :</b> <i
                                                class="icon-calender"></i> {{date_indo($detailTrx->order_shipment_date, 'datetime')}}
                                    </p>
                                </address>
                                <address>
                                    <h5>Driver : <strong>
                                        {{($detailTrx->detail[0]->driver == null ? '-' : $detailTrx->detail[0]->driver->driver_name)}}</h5>
                                    </strong>
                                </address>
                                <address>
                                    <h5>Note : {{($detailTrx->order_note)}}</h5>
                                </address>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive m-t-40" style="clear: both;">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Produk</th>
                                        <th class="text-right">Harga</th>
                                        <th class="text-right">Jumlah</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($detailTrx->detail[0]->detail_product as $key => $val)
                                        <tr>
                                            <td class="text-center">{{$key+1}}</td>
                                            <td>{{$val->od_product_name}}</td>
                                            <td class="text-right">Rp {{price_format($val->od_product_hpp)}}</td>
                                            <td class="text-right"> {{$val->od_product_qty}}</td>
                                            <td class="text-right">Rp {{price_format($val->od_product_subtotal)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="pull-right m-t-30 text-right">
                                <h3><b>Total :</b> Rp {{price_format($detailTrx->o_detail_subtotal_hpp)}}</h3>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <div class="text-right">
                                <a href="{{route('trx.list', $status)}}" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection