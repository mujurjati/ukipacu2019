@extends('merchant.layouts.app')
@section('title', 'Daftar Transaksi')
@section('content')
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Transaksi {{statusTrx($status)}}</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Transaksi {{statusTrx($status)}}</li>
                    </ol>
                </div>
            </div>
        </div>
        @if(count($dataTrx) > 0)
            <div class="row">
                <!-- column -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Daftar Transaksi {{statusTrx($status)}}</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No. Pesanan</th>
                                        <th>Pelanggan</th>
                                        <th>Driver</th>
                                        <th>Waktu Pengiriman</th>
                                        <th>Total Trx</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataTrx as $key => $val)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$val->order_number}}</td>
                                            <td>{{$val->customer->customer_name}}</td>
                                            <td>{{($val->detail[0]->driver == null ? '-' : $val->detail[0]->driver->driver_name)}}</td>
                                            <td>{{date_indo($val->order_shipment_date, 'datetime')}}</td>
                                            <td>Rp. {{price_format($val->detail[0]->o_detail_subtotal_hpp)}}</td>
                                            <td><a href="{{route('trx.detail', ['id' => $val->order_id, 'status' => $val->detail[0]->o_detail_status])}}"><span class="label label-info">Detail</span></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body text-center">
                            Tidak ada data dengan status transaksi {{statusTrx($status)}}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection