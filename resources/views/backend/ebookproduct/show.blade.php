@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Edisi'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.ebookproduct') }}">Daftar Edisi</a></li>
                    <li class="breadcrumb-item active">Detail Edisi</li>
                @endslot
                @slot('btn')
                    
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                {{ Form::label('e_product_name', 'Nama', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{$ebook->e_product_name}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_product_image', 'Gambar', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-3">
                                        @component('backend.libs.image.show', ['path' => '/images/ebook_product/', 'class' => 'd-flex mr-3  thumb-md'])
                                            {{ $ebook->e_product_image }}
                                        @endcomponent
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_product_edition', 'Edisi', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{$ebook->e_product_edition}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_product_shortdesc', 'Deskripsi Singkat', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{$ebook->e_product_shortdesc}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_product_desc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{!! $ebook->e_product_desc !!}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_type', 'Tipe', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{ucwords($ebook->e_product_type)}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_product_status', 'Status', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{label_status($ebook->e_product_status)}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_product_status', 'Tanggal Dibuat', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{date_indo($ebook->e_product_publish_date, 'datetime')}}</p>
                                </div>
                            </div>
                            <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <a href="{{ route('admin.ebookproduct') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Kembali</a>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection