@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb')
                @slot('title')
                    Edit Edisi
                @endslot
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.ebookproduct') }}">Daftar Edisi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Edisi</li>
                @endslot
            @endcomponent
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @include('backend.layouts.info')
                            {{ Form::open(['route' => ['admin.ebook.product.update', $id], 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                            <div class="form-group row">
                                {{ Form::label('e_product_name', 'Judul', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('e_product_name', $ebook->e_product_name, ['class' => 'form-control']) }}
                                    </div>
                            </div>
                            <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input', [
                                            'name'      => 'ebookProductImage',
                                            'accept'    => ['image/jpeg', 'image/png'],
                                            'src'       => (!empty($ebook->e_product_image)) ? asset_url('/images/ebook_product/'.$ebook->e_product_image) : ''
                                        ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent
                                    </div>
                                </div>
                            <div class="form-group row">
                                {{ Form::label('e_product_edition', 'Nomor Edisi', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('e_product_edition', $ebook->e_product_edition, ['class' => 'form-control edisi']) }}
                                    </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_product_shortdesc', 'Deskripsi Singkat', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::textarea('e_product_shortdesc', $ebook->e_product_shortdesc, ['class' => 'form-control', 'rows' => '4']) }}
                                    </div>
                            </div>
                            <div class="form-group row">
                                    {{ Form::label('e_product_desc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        @component('backend.libs.form.editor', ['name' => 'e_product_desc'])
                                            {{ $ebook->e_product_desc }}
                                        @endcomponent
                                    </div>
                            </div>
                            <div class="form-group row">
                                    {{ Form::label('e_product_file', 'File', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <input type="file" name="e_product_file" class="filestyle" data-buttonname="btn-primary">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <code>(*) Kosongkan file jika tidak ingin di ganti</code><br>
                                        <span>{{$ebook->e_product_file}}</span>
                                    </div>
                            </div>
                            <div class="form-group row">
                                    {{ Form::label('e_product_type', ' Tipe', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::select('e_product_type',\App\Enums\Enums::ebookProType(), $ebook->e_product_type,array('class' => 'form-control')) }}
                                    </div>
                            </div>
                            <div class="form-group row">
                                    {{ Form::label('publish_date', 'Tanggal Publish', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        <input type="text" id="datepicker1" name="e_product_publish_date" class="form-control" value="{{date('Y-m-d', strtotime($ebook->e_product_publish_date))}}">
                                    </div>
                            </div>
                            <div class="form-group row">
                                    {{ Form::label('status', 'status', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary active">
                                                {{ Form::radio('e_product_status', 'active', $ebook->e_product_status == 'active' ? true : '') }} Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary">
                                                {{ Form::radio('e_product_status', 'non-active', $ebook->e_product_status == 'non-active' ? true : '') }} Non-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <button class="btn btn-lg btn-primary waves-effect waves-light" name="addProduct" value="1" type="submit">Simpan</button>
                                        <a href="{{ route('admin.product-cat') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            {{--val({{$ebook->e_product_file}})--}}
            $(".bootstrap-filestyle :input").removeAttr('disabled');
            $(".edisi").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $("#datepicker1").datepicker({
                autoclose: !0,
                todayHighlight: !0,
                format: 'yyyy-mm-dd',
            });
        });
    </script>
@endsection

@section('load-script')
    @include('backend.libs.image.image_js')
@stop