@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Edisi'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.ebookproduct') }}">Daftar Edisi</a></li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.ebookproduct.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah Edisi</a>
                @endslot
            @endcomponent

        <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Edisi</h4>
                            <diiv class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Gambar</th>
                                            <th data-priority="3">Tipe</th>
                                            <th data-priority="1">Nama Produk</th>
                                            <th data-priority="3">Edisi</th>
                                            <th data-priority="6">File</th>
                                            <th data-priority="6">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ebook as $data  => $value)
                                            <tr>
                                                <td>{{$data+1}}</td>
                                                <td>
                                                    @component('backend.libs.image.show', ['path' => '/images/ebook_product/', 'class' => 'd-flex mr-3  thumb-md'])
                                                        {{ $value->e_product_image }}
                                                    @endcomponent
                                                </td>
                                                <td>{{ ucwords($value->e_product_type) }}</td>
                                                <td>{{ $value->e_product_name }}</td>
                                                <td>{{ $value->e_product_edition }}</td>
                                                <td>{{ $value->e_product_file }}</td>
                                                <td>{{ label_status($value->e_product_status) }}</td>
                                                <td>
                                                    <a href="{{ route('admin.ebook.product.detail', ['id' => $value->e_product_id])}}"><i class="mdi mdi-eye h4 text-primary"></i></a>&nbsp
                                                    <a href="{{ route('admin.ebook.product.edit', ['id' => $value->e_product_id]) }}"><i class="mdi mdi-pencil h4 text-primary"></i></a>&nbsp
                                                    <a href="" class="btnDelete" data-id="{{$value->e_product_id}}" data-title="{{ $value->e_product_name }}" data-toggle="modal"><i class="mdi mdi-delete-forever h4 text-primary"></i></a>
                                                    {{ Form::open(['route' => ['admin.ebook.product.delete', $value->e_product_id], 'method' => 'post', 'id' => 'deleteData'.$value->e_product_id]) }}

                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $ebook->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop