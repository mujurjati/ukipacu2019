@extends('backend.layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/map/jquery-gmaps-latlon-picker.css') }}">
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Video'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.video') }}">Daftar Video</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Video</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Edit Video</h4>

                            @include('backend.layouts.info')

                            <form action="{{ route('admin.video.update', ['id' => $video->video_id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Video Name</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{ $video->video_name }}" name="video_name" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Video File</label>
                                    <div class="col-sm-10">
                                        <p>{{  $video->video_file }}</p>
                                        <input type="file" name="video_file" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Deskripsi Singkat</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="video_sortdesc" type="text" rows="4">{{ $video->video_sortdesc }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Deskripsi </label>
                                    <div class="col-sm-10">
                                        @component('backend.libs.form.editor', ['name' => 'video_desc'])
                                            {{$video->video_desc}}
                                        @endcomponent
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary {{ $video->video_status == 'publish' ? 'active' : ' ' }}">
                                                <input type="radio" name="video_status" id="option1" value="publish" {{ $video->video_status == 'publish' ? 'checked="checked"' : '' }}> Publish
                                            </label>
                                            <label class=" btn btn-sm btn-primary {{ $video->video_status == 'draft' ? 'active' : '' }}">
                                                <input type="radio" name="video_status" id="option2" value="draft" {{ $video->video_status == 'draft' ? 'checked="checked"' : '' }}> Draft
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12" style="margin-top: 20px">
                                        <button class="btn btn-primary waves-effect waves-light" name="editVideo" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div>
    </div>

@endsection
@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@stop
@section('load-script')
    @include('backend.libs.image.image_js')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}&callback=initMap">
    </script>
    <script src="{{ asset_url('/backend/assets/map/jquery-gmaps-latlon-picker.js') }}"></script>
    {{--    <script src="{{ asset_url('/backend/assets/map/jquery-2.1.1.min.js') }}"></script>--}}

@stop