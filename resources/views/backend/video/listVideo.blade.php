@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8"><h4 class="page-title mb-0">Management Video</h4>
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item">Dashboard</li>
                                    <li class="breadcrumb-item active">>Daftar Video</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-md-6"><h4 class="header-title">Daftar konten video</h4></div>
                                <div class="col-md-6 text-right"> <a href="{{ route('admin.video.create') }}" class="btn btn-success btn-fw" style="background-color: #1d9e54; color: white"><i style="color: white" class="icon-magnifier-add"></i>Tambah Video</a></div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <form role="form" method="get" action="{{ route('admin.video') }}">
                                        <div class="card-body row">
                                            <div class="col-sm-3">
                                                <input class="form-control" type="text" value="{{ Request::input('keyword') }}" name="keyword" placeholder="Masukkan kata kunci..">
                                            </div>
                                            <div class="col-sm-2">
                                                {{
                                                    Form::select('status', [
                                                        ''          => '-- Semua Status --',
                                                        'publish'   => 'Publish',
                                                        'draft'     => 'Draft'
                                                        ], Request::input('status'), ['class' => 'form-control'])
                                                }}
                                            </div>
                                            <button type="submit" class="btn btn-success waves-effect waves-light mr-2">Cari</button>
                                            <a href="{{ route('admin.video') }}" name="reset"  class="btn btn-warning">Reset</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="row">
                                @foreach($video as $items)

                                    <div class="col-lg-4 grid-margin stretch-card" >
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title" style="font-size: 14px;margin-bottom: 0.5rem">{{ $items->video_name }}</h4>

                                                <video width="100%" controls>
                                                    <source src="{{ asset_url('/videos/'.$items->video_file)}}" type="video/mp4">
                                                </video>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="ftext-left text-left"> <p class="card-description"> {{ label_status($items->video_status) }}  </p></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="float-right text-right"> <p class="card-description"> {{ $items->video_hits }} x tayang </p></div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label  class="col-sm-12 col-form-label">Deskripsi</label>
                                                    <div class="col-sm-12">
                                                        <p class="mt-2 mb-0 font-14 text-muted">{{ $items->video_sortdesc }}</p>
                                                        <p class="mt-2 mb-0 font-14 text-muted">{!! $items->video_desc !!}</p>
                                                    </div>
                                                </div>


                                                <div class="btn-group full-width" role="group" aria-label="Basic example" style="width: 100%">
                                                    <a href="{{ route('admin.video.edit', ['id' => $items->video_id]) }}"  class="btn btn-warning" style="width: 100%">Edit</a>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <ul class="pagination">
                                {{ $video->links("pagination::bootstrap-4") }}
                            </ul>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection