@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Store Corner'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.storecorner') }}">Daftar Store Corner</a></li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.storecorner.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah Baru</a>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Store Corner</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Gambar</th>
                                            <th data-priority="3">Kode</th>
                                            <th data-priority="1">Nama</th>
                                            <th data-priority="3">Telepon</th>
                                            <th data-priority="6">Beroperasi</th>
                                            <th data-priority="6">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($store as $key  => $data)
                                            <tr>
                                                <th>{{$key+1}}</th>
                                                <td><img class="d-flex mr-3" src="{{get_image('storecorner/'.$data->s_corner_logo)}}" alt="users" style="width: 50px"></td>
                                                <td>{{$data->s_corner_code}}</td>
                                                <td>{{$data->s_corner_name}}</td>
                                                <td>{{$data->s_corner_phone}}</td>
                                                <td>{{date('H:i',strtotime($data['s_corner_open']))}} - {{date('H:i',strtotime($data['s_corner_close']))}}</td>
                                                <td>{{label_status($data->s_corner_status)}}</td>

                                                <td>
                                                    <a href="{{ route('admin.storecorner.detail', ['id' => $data->s_corner_id]) }}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                                    <a href="{{ route('admin.storecorner.edit', ['id' => $data->s_corner_id]) }}" class="text-primary"><i class="mdi mdi-pencil h4"></i></a>&nbsp
                                                    <a href="javascript: delete_attr('{{ $data->s_corner_id}}', '{{ $data->s_corner_name }}')" data-toggle="modal"  class="text-primary"><i class="mdi mdi-delete-forever h4"></i></a>
                                                    {{ Form::open(['route' => ['admin.storecorner.hapus', $data->s_corner_id], 'method' => 'post', 'id' => 'delAttr-'.$data->s_corner_id]) }}
                                                    {{ Form::token() }}
                                                    {{ Form::hidden('idStore', $data->s_corner_id) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $store->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->
    <script>
        function delete_attr(id, name) {
            alertify.confirm("Anda yakin akan menghapus data store corner "+name+" ?", function() {
                $("#delAttr-"+id).submit();
            });
        }
    </script>
@endsection