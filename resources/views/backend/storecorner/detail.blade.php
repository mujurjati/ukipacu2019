@extends('backend.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Store Corner'])
                @slot('ul')
                    <li class="breadcrumb-item "><a href="{{ route('admin.storecorner') }}">Daftar Store Corner</a></li>
                    <li class="breadcrumb-item active">Detail Store Corner</li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.storecorner.edit', ['id' => $store->s_corner_id]) }}" class="btn btn-warning btn-rounded"><i class="ti-pencil mr-1"></i> Edit</a>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-lg-4 center">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title"></h4>
                            <div class="row">
                                <div class="col-6">
                                    <img class="img-fluid d-block mt-1" src="{{get_image('storecorner/'.$store->s_corner_logo)}}" alt="logo" width="145">
                                </div>
                                <div class="col-6">
                                    <h5 class="mt-0 font-14 m-b-15 text-muted">{{$store->s_corner_name}}</h5>
                                    <p class="mt-0 font-14 m-b-15 text-muted">{{$store->s_corner_code}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{$store->s_corner_phone}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">Open: {{date('H:i',strtotime($store['s_corner_open']))}} - {{date('H:i',strtotime($store['s_corner_close']))}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{label_status($store->s_corner_status)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 center">
                    <div class="card">
                        <div class="card-body pb-0"><h4 class="mt-0 header-title">Alamat</h4>
                            <div class="row">
                                <div class="col-12">
                                    <p class="mt-0 font-14 m-b-15 text-muted">{{$store->s_corner_address}} <br>
                                        {{ $store->subdistrict->s_subdistrict_name }},
                                        {{ $store->subdistrict->city->s_city_name }},
                                        {{ $store->subdistrict->city->province->s_province_name }}.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0"><h4 class="mt-0 header-title">Deskripsi</h4>
                            <div class="row">
                                <div class="col-12">
                                    <p class="mt-2 mb-0 font-14 text-muted">{!! $store->s_corner_desc !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div><!-- container-fluid -->
    </div><!-- content -->
@endsection