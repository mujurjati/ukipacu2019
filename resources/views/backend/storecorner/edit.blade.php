@extends('backend.layouts.app')

@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <style>
        .select2-container .select2-selection--single .select2-selection__rendered{
            line-height: 22px;
        }
    </style>
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Store Corner'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.storecorner') }}">Daftar Store Corner</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Store Corner</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Edit Store Corner</h4>

                            @include('backend.layouts.info')

                            <form action="{{ route('admin.storecorner.update', ['id' => $store->s_corner_id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{ $store->s_corner_name }}" name="storeName" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Kode</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{ $store->s_corner_code }}" name="storeCode" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input', [
                                            'name'      => 'storeImage',
                                            'accept'    => ['image/jpeg', 'image/png'],
                                            'src'       => (!empty($store->s_corner_logo)) ? asset_url('/images/storecorner/'.$store->s_corner_logo) : ''
                                        ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('storeDesc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        @component('backend.libs.form.editor', ['name' => 'storeDesc'])
                                            {{ $store->s_corner_desc }}
                                        @endcomponent
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Telepon</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{ $store->s_corner_phone }}" type="text" name="storePhone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kota atau Kecamatan</label>
                                    <div class="col-sm-10">
                                        <div class="form-group row text-left">
                                            <div class="col-sm-12">
                                                <select name="cityStore" class="select-address">
                                                    <option value="{{$store->s_subdistrict_id}}">
                                                        {{
                                                        $store->subdistrict->s_subdistrict_name.', '.
                                                        $store->subdistrict->city->s_city_name.', '.
                                                        $store->subdistrict->city->province->s_province_name.', '.
                                                        $store->subdistrict->city->s_city_postcode}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" type="text" name="storeAddress" rows="6">{{ $store->s_corner_address }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jam Beroperasi</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="openStore" type="time" value="{{ $store->s_corner_open }}" id="example-time-input">
                                        <span class="font-13 text-muted">Buka</span>
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="closeStore" type="time" value="{{ $store->s_corner_close }}" id="example-time-input">
                                        <span class="font-13 text-muted">Tutup</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary {{ $store->s_corner_status == 'active' ? 'active' : ' ' }}">
                                                <input type="radio" name="storeStatus" id="option1" value="active" {{ $store->s_corner_status == 'active' ? 'checked="checked"' : '' }}> Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary {{ $store->s_corner_status == 'non-active' ? 'active' : '' }}">
                                                <input type="radio" name="storeStatus" id="option2" value="non-active" {{ $store->s_corner_status == 'non-active' ? 'checked="checked"' : '' }}> No-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary waves-effect waves-light" name="editStore" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        var _token = '{{ csrf_token() }}';
        $(document).ready(function () {
            $('.select-address').select2({
                theme: "bootstrap",
                placeholder: 'Pilih Kota atau Kecamatan',
                dropdownAutoWidth: true,
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('ajax.get-address') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });
        });
    </script>
@endsection

@section('load-script')
    @include('backend.libs.image.image_js')
@stop