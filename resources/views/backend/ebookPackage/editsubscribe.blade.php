@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb')
                @slot('title')
                    Edit Subscribe Paket {{$subs->package->e_package_name}}
                @endslot
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.ebookpackage') }}">Daftar Subscribe</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Subscribe</li>
                @endslot
            @endcomponent
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @include('backend.layouts.info')
                            {{ Form::open(['route' => ['admin.ebookpackage.subscribe.update', $id], 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                            <div class="form-group row">
                                {{ Form::label('e_subscribe_expired_date', 'masa Berlaku', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::date('e_subscribe_expired_date', $subs->e_subscribe_expired_date, ['class' => 'form-control', 'id' => 'example-date-input', 'value' => $subs->e_subscribe_expired_date->toDateString()]) }}
                                    </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('e_subscribe_status', 'Status', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::select('e_subscribe_status',\App\Enums\Enums::subscribeStatus(), $subs->e_subscribe_status,array('class' => 'form-control')) }}
                                    </div>
                            </div>
                            <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <button class="btn btn-lg btn-primary waves-effect waves-light" name="editSubscribe" value="1" type="submit">Simpan</button>
                                        <a href="{{ route('admin.ebookpackage') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
