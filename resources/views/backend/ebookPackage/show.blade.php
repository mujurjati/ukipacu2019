@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Paket Langganan'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.product-list') }}">List Paket Langganan</a></li>
                    <li class="breadcrumb-item active">Detail Paket Langganan</li>
                @endslot
                @slot('btn')
                    
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                {{ Form::label('ebook_name', 'Nama', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{$ebook->e_package_name}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('ebook_name', 'Gambar', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-3">
                                        @component('backend.libs.image.show', ['path' => '/images/ebook_package/', 'class' => 'd-flex mr-3  thumb-md'])
                                            {{ $ebook->e_package_image }}
                                        @endcomponent
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_shortdesc', 'Deskripsi Singkat', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{$ebook->e_package_shortdesc}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_desc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{!! $ebook->e_package_desc !!}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_time', 'Durasi', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{$ebook->e_package_time}} Bulan</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_type', 'Tipe', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{$ebook->e_package_type}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_price', 'Harga', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">Rp. {{number_format($ebook->e_package_price)}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_status', 'Status', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <p class="col-form-label">{{label_status($ebook->e_package_status)}}</p>
                                </div>
                            </div>
                            <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <a href="{{ route('admin.ebookpackage') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Kembali</a>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection