@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Subscribe'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.product-list') }}">Daftar Subscribe</a></li>
                @endslot
                @slot('btn')
                        <a href="{{ route('admin.ebookpackage') }}" class="btn btn-info btn-rounded" >Kembali</a>
                @endslot
            @endcomponent

        <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">daftar Subscribe</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Nama Paket</th>
                                            <th data-priority="3">Nama Pelanggan</th>
                                            <th data-priority="1">Durasi</th>
                                            <th data-priority="3">Tipe</th>
                                            <th data-priority="6">Harga</th>
                                            <th data-priority="6">Masa Berlaku</th>
                                            <th data-priority="1">Status</th>
                                            <th data-priority="1">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($subs as $data  => $value)
                                            <tr>
                                                <td>{{$data+1}}</td>
                                                <td>{{ $value->package->e_package_name }}</td>
                                                <td>{{ $value->customer->customer_name }}</td>
                                                <td>{{ $value->e_subscribe_time }} Bulan</td>
                                                <td>{{ $value->e_subscribe_type }}</td>
                                                <td>Rp. {{ number_format($value->e_subscribe_price) }}</td>
                                                <td>{{date('d/m/Y', strtotime($value->e_subscribe_expired_date))}}</td>
                                                <td>{{ label_status_ebook($value->e_subscribe_status) }}</td>
                                                <td>
                                                    <a href="{{ route('admin.ebookpackage.subscribe.edit', ['id' => $value->e_subscribe_id]) }}" class="text-primary"><i class="mdi mdi-pencil h5"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop