@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Paket Langganan'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.product-list') }}">Daftar Paket Langganan</a></li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.ebookpackage.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah Paket Langganan</a>
                @endslot
            @endcomponent

        <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Paket Langganan</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Gambar</th>
                                            <th data-priority="3">Nama</th>
                                            <th data-priority="1">Durasi</th>
                                            <th data-priority="3">Tipe</th>
                                            <th data-priority="6">Harga</th>
                                            <th data-priority="6">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ebook as $data  => $value)
                                            <tr>
                                                <td>{{$data+1}}</td>
                                                <td>
                                                    @component('backend.libs.image.show', ['path' => '/images/ebook_package/', 'class' => 'd-flex mr-3  thumb-md'])
                                                        {{ $value->e_package_image }}
                                                    @endcomponent
                                                </td>
                                                <td>{{ $value->e_package_name }}</td>
                                                <td>{{ $value->e_package_time }} Bulan</td>
                                                <td>{{ $value->e_package_type }}</td>
                                                <td>Rp {{ number_format($value->e_package_price) }}</td>
                                                <td>{{ label_status($value->e_package_status) }}</td>
                                                <td>
                                                    <a href="{{ route('admin.ebookpackage.detail', ['id' => $value->e_package_id]) }}" class="text-primary"><i class="mdi mdi-eye h5"></i></a>&nbsp
                                                    <a href="{{ route('admin.ebookpackage.edit', ['id' => $value->e_package_id]) }}" class="text-primary"><i class="mdi mdi-pencil h5"></i></a>&nbsp
                                                    <a href="" data-toggle="modal" class="btnDelete text-primary" data-id="{{$value->e_package_id}}" data-title="{{ $value->e_package_name }}"><i class="mdi mdi-delete h5"></i></a>
                                                    {{ Form::open(['route' => ['admin.ebookpackage.delete', $value->e_package_id], 'method' => 'post', 'id' => 'deleteData'.$value->e_package_id]) }}

                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $ebook->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop