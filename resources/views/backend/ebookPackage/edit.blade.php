@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb')
                @slot('title')
                    Edit Paket Langganan
                @endslot
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.ebookpackage') }}">Daftar Paket Langganan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Paket Langganan</li>
                @endslot
            @endcomponent
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @include('backend.layouts.info')
                            {{ Form::open(['route' => ['admin.ebookpackage.update', $id], 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                            <div class="form-group row">
                                {{ Form::label('e_package_name', 'Nama Paket', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('e_package_name', $ebook->e_package_name, ['class' => 'form-control']) }}
                                    </div>
                            </div>
                            <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input', [
                                            'name'      => 'ebookImage',
                                            'accept'    => ['image/jpeg', 'image/png'],
                                            'src'       => (!empty($ebook->e_package_image)) ? asset_url('/images/ebook_package/'.$ebook->e_package_image) : ''
                                        ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent
                                    </div>
                                </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_shortdesc', 'Deskripsi Singkat', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('e_package_shortdesc', $ebook->e_package_shortdesc, ['class' => 'form-control']) }}
                                    </div>
                            </div>
                            <div class="form-group row">
                                    {{ Form::label('e_package_desc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        @component('backend.libs.form.editor', ['name' => 'e_package_desc'])
                                            {{$ebook->e_package_desc}}
                                        @endcomponent
                                    </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_time', 'Durasi', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        {{ Form::number('e_package_time', $ebook->e_package_time, ['class' => 'form-control']) }}
                                        <span class="input-group-addon">Bulan</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                    {{ Form::label('e_package_type', 'Product Status', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::select('e_package_type',\App\Enums\Enums::ebookType(), $ebook->e_package_type,array('class' => 'form-control')) }}
                                    </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('e_package_price', 'Harga', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp</span>
                                        {{ Form::text('e_package_price', price_format($ebook->e_package_price), ['class' => 'form-control rupiah']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                    {{ Form::label('status', 'status', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary active">
                                                {{ Form::radio('e_package_status', 'active', $ebook->e_package_status == 'active' ? true : '') }} Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary">
                                                {{ Form::radio('e_package_status', 'non-active', $ebook->e_package_status == 'non-active' ? true : '') }} Non-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <button class="btn btn-lg btn-primary waves-effect waves-light" value="1" type="submit">Simpan</button>
                                        <a href="{{ route('admin.ebookpackage') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@endsection
@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@stop