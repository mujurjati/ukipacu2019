@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Konten Kategori'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.content') }}">Konten Kategori</a></li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">List Kategori</h4>
                            <table class="table table-striped">
                                <thead>
                                    <th>No</th>
                                    <th width="100">Gambar</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                @foreach($dataCat as $key => $item)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>
                                            @if(empty($item->c_cat_image))
                                                -
                                            @else
                                            <img src="{{get_image_all($item->c_cat_image, 'cat_content')}}" width="70%" alt="gambar kategori"></td>
                                            @endif
                                        <td>{{$item->c_cat_name}}</td>
                                        <td>{{label_status($item->c_cat_status)}}</td>
                                        <td>
                                            @if($item->c_cat_name=="Slider")
                                                <span class='badge badge-purple'>Data master</span>
                                            @else
                                                <a href="#edit-{{$item->c_cat_id}}" data-toggle="modal" class="text-primary" title="Edit"><i class="mdi mdi-pencil h5"></i></a>
                                                <a href="" class="text-primary btnDelete" data-id="{{$item->c_cat_id}}" data-title="{{$item->c_cat_name}}" data-toggle="modal" title="Hapus"><i class="mdi mdi-delete h5"></i></a>
                                            @endif
                                            @component('backend.layouts.components.modal', [
                                                'id'    => 'edit-'.$item->c_cat_id,
                                                'title' => 'Edit Kategori',
                                                'route' => route('admin.content.catEdit', $item->c_cat_id)
                                            ])
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        {{ Form::label('cat-image', 'Gambar') }}
                                                        @component('backend.libs.image.input', [
                                                                'selector'  => 'img-selector'.$item->c_cat_id,
                                                                'asset'     => ($key > 0 ? false : true),
                                                                'name'      => 'gambar_kategori',
                                                                'accept'    => ['image/jpeg', 'image/png'],
                                                                'src'       => (!empty($item->c_cat_image) ? get_image_all($item->c_cat_image, 'cat_content') : '')
                                                            ])
                                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                                        @endcomponent
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('cat-name', 'Nama') }}
                                                    <div>
                                                        {{ Form::text('nama_kategori', $item->c_cat_name, ['class' => 'form-control']) }}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('cat-desc', 'Deskripsi') }}
                                                    <div>
                                                        {{ Form::textarea('deskripsi_kategori', $item->c_cat_desc, ['class' => 'form-control','rows'=>'3']) }}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('cat-status', 'Status') }}
                                                    <div class="col-sm-10">
                                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-sm btn-primary active">
                                                                <input type="radio" name="status_kategori" id="option1" value="1" {{$item->c_cat_status == '1' ? 'checked' : ''}}> Active
                                                            </label>
                                                            <label class=" btn btn-sm btn-primary">
                                                                <input type="radio" name="status_kategori" id="option2" value="0" {{$item->c_cat_status == '0' ? 'checked' : ''}}> No-Active
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                @slot('btn')
                                                    <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light">Update</button>
                                                @endslot
                                            @endcomponent

                                            {{ Form::open(['route' => ['admin.content.catDelete', $item->c_cat_id], 'method' => 'post', 'id' => 'deleteData'.$item->c_cat_id]) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- end col -->
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Tambah Kategori</h4>
                            <hr>
                            @include('backend.layouts.info')
                            {{ Form::open(['route' => 'admin.content.catAdd', 'method' => 'post']) }}
                                <div class="form-group">
                                    <div class="col-md-6">
                                        {{ Form::label('cat-image', 'Gambar') }}
                                        @component('backend.libs.image.input', [
                                                'name'      => 'gambar_kategori',
                                                'accept'    => ['image/jpeg', 'image/png']
                                            ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('cat-name', 'Nama') }}
                                    <div>
                                        {{ Form::text('nama_kategori', '', ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('cat-desc', 'Deskripsi') }}
                                    <div>
                                        {{ Form::textarea('deskripsi_kategori', '', ['class' => 'form-control','rows'=>'3']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('cat-status', 'Status') }}
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary active">
                                                <input type="radio" name="status_kategori" id="option1" value="1" checked="checked"> Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary">
                                                <input type="radio" name="status_kategori" id="option2" value="0"> No-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light">Simpan
                                        </button>
                                        <button type="reset" class="btn btn-sm btn-secondary waves-effect m-l-5">Reset
                                        </button>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div><!-- end col -->
            </div>

        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop
