@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Detail Transaksi Paket'])
                @slot('ul')
                    <li class="breadcrumb-item active">
                        <a href="{{ route('admin.product-list') }}">Transaksi Paket</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                @endslot
            @endcomponent

            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <strong>Status Pesanan</strong><br>
                                    {{ label_status_ebook($data->e_subscribe_status) }} <br>
                                    <small style="font-size: 11px" class="text-muted">
                                        Tgl. {{ date_indo($data->e_subscribe_create_date, 'datetime') }}</small>
                                </div>
                                <div class="col-md-4 text-center">
                                    <strong>Tanggal Aktif Paket</strong><br>
                                    <a href="{{ route('admin.customer.detail', [$data->customer_id]) }}">
                                        <strong class="text-primary">
                                            {{ date_indo(date('Y-m-d H:i:s'), 'datetime') }}
                                        </strong>
                                        <br>Sampai</br>
                                        <strong class="text-primary">
                                            {{ date_indo(date('Y-m-d H:i:s', strtotime('+'.$data->e_subscribe_time.' months')), 'datetime') }}
                                        </strong>
                                    </a>
                                </div>
                                <div class="col-md-4 text-right">
                                    <strong>Data Pelanggan</strong><br>
                                    <a href="{{ route('admin.customer.detail', [$data->customer_id]) }}">
                                        <strong class="text-primary">
                                            {{ strtoupper($data->customer->customer_name) }}
                                        </strong>
                                        <br>
                                        <small class="text-primary">({{ $data->customer->customer_email }})</small>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <h3 class="panel-title font-16"><strong>Rincian Transaksi</strong></h3>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>
                                            <strong>Bank</strong>
                                        </td>
                                        <td class="text-right">
                                            <strong>Nama Rekening</strong>
                                        </td>
                                        <td class="text-center">
                                            <strong>Nomor Rekening</strong>
                                        </td>
                                        <td class="text-center">
                                            <strong>Cabang</strong>
                                        </td>
                                        <td class="text-right">
                                            <strong>Total</strong>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{ $data->e_bank_to }}</td>
                                        <td class="text-right">{{ $data->e_bank_to_account }}</td>
                                        <td class="text-center">{{ $data->e_bank_to_number }}</td>
                                        <td class="text-center">{{ $data->e_bank_to_branch }}</td>
                                        <td class="text-right">Rp. {{ number_format($data->e_subscribe_price) }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="d-print-none m-t-15">
                                <div class="pull-right text-right">
                                    @if($data->e_subscribe_status == 'pending')
                                        {{ Form::close() }}
                                        {{ Form::open(['route' => ['admin.ebooktrx.update', $data->e_subscribe_id], 'id' => 'change-cancel', 'method' => 'post']) }}
                                        {{ Form::hidden('status', 'cancel') }}
                                        {{ Form::hidden('start', date('Y-m-d H:i:s')) }}
                                        {{ Form::hidden('end', date('Y-m-d H:i:s', strtotime('+'.$data->e_subscribe_time.' months'))) }}
                                        {{ Form::close() }}
                                        {{ Form::open(['route' => ['admin.ebooktrx.update', $data->e_subscribe_id], 'id' => 'change-success', 'method' => 'post']) }}
                                        {{ Form::hidden('status', 'success') }}
                                        {{ Form::hidden('start', date('Y-m-d H:i:s')) }}
                                        {{ Form::hidden('end', date('Y-m-d H:i:s', strtotime('+'.$data->e_subscribe_time.' months'))) }}
                                        {{ Form::close() }}
                                        <button type="button" class="btn btn-primary act-status-order"
                                                data-status="cancel">Batalkan
                                        </button>
                                        <button type="button" class="btn btn-purple act-status-order"
                                                data-status="success">Terbayar
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <script>
        $(function () {
            $('.act-status-order').on("click", function () {
                var status = $(this).data('status');

                if (status == 'success') {
                    var textConfirm = 'Anda yakin akan menandai transaksi ini SUDAH TERBAYAR ?';
                }
                else if (status == 'cancel') {
                    var textConfirm = 'Anda yakin akan menandai transaksi ini TELAH DIBATALKAN ?';
                }
                console.log(status);
                alertify.confirm(textConfirm, function () {
                    $("#change-" + status).submit();
                });
            });
        });
    </script>
@endsection