@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Store Corner'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.ebooktrx') }}">Daftar Transaksi Paket</a></li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Transaksi Paket</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Pelanggan</th>
                                            <th data-priority="3">Paket</th>
                                            <th data-priority="1">Tipe</th>
                                            <th data-priority="3">Durasi</th>
                                            <th data-priority="6">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($data as $key  => $item)
                                            <tr>
                                                <th>{{$key+1}}</th>
                                                <td>{{$item->customer->customer_name}}</td>
                                                <td>{{$item->package->e_package_name}}</td>
                                                <td>{{$item->e_subscribe_type}}</td>
                                                <td>{{$item->e_subscribe_time}} Bulan</td>
                                                <td>{{ label_status_ebook($item->e_subscribe_status) }}</td>
                                                <td>
                                                    <a href="{{ route('admin.ebooktrx.detail', ['id' => $item->e_subscribe_id]) }}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $data->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->
    <script>
        function delete_attr(id, name) {
            alertify.confirm("Anda yakin akan menghapus data store corner "+name+" ?", function() {
                $("#delAttr-"+id).submit();
            });
        }
    </script>
@endsection