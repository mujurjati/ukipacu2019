@extends('backend.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Promo'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.product-list') }}">Daftar Promo</a></li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.promo.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah Promo</a>
                @endslot
            @endcomponent

        <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Promo</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Nama</th>
                                            <th data-priority="3">Minimal Transaksi</th>
                                            <th data-priority="1">Maksimal Diskon</th>
                                            <th data-priority="6">Nilai</th>
                                            <th data-priority="6">Kode</th>
                                            <th data-priority="6">Jumlah</th>
                                            <th data-priority="6" class="text-center">Masa berlaku</th>
                                            <th data-priority="6" class="text-center">Status</th>
                                            <th class="text-center">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($promo as $data  => $value)
                                            <tr>
                                                <td>{{$data+1}}</td>
                                                <td>{{ $value->promo_name }}</td>
                                                <td>Rp. {{ number_format($value->promo_min_payment) }}</td>
                                                <td>Rp. {{ number_format($value->promo_max_discount) }}</td>
                                                <td>{{ ($value->promo_type == 'price' ? 'Rp. '.number_format($value->promo_value) : $value->promo_value.'%') }}</td>
                                                <td>{{ $value->promo_code }}</td>
                                                <td>{{ $value->promo_used }} / {{ $value->promo_qty }}</td>
                                                <td><small>{{ date_indo($value->promo_start_date) }} - {{ date_indo($value->promo_end_date) }}</small></td>
                                                <td>{{ label_status($value->promo_status)}}</td>
                                                <td>
                                                    <a href="{{ route('admin.promo.detail', ['id' => $value->promo_id]) }}" class="text-primary" title="Detail"><i class="mdi mdi-eye h5"></i></a>&nbsp
                                                    <a href="{{ route('admin.promo.edit', ['id' => $value->promo_id]) }}" class="text-primary" title="Edit"><i class="mdi mdi-pencil h5"></i></a>&nbsp
                                                    <a href="" class="text-primary btnDelete" data-id="{{$value->promo_id}}" data-title="{{ $value->promo_name }}" data-toggle="modal" title="Status"><i class="mdi mdi-delete h5"></i></a>&nbsp
                                                    <a href="{{ route('admin.promo.used', ['id' => $value->promo_id]) }}" class="text-primary" title="List Promo Used"><i class="mdi mdi-format-list-numbers h5"></i></a>
                                                    {{ Form::open(['route' => ['admin.promo.delete', $value->promo_id], 'method' => 'post', 'id' => 'deleteData'.$value->promo_id]) }}

                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop