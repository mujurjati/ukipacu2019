@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Promo'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.product-list') }}">Daftar Promo Yang Terpakai</a></li>
                @endslot
                @slot('btn')
                @endslot
            @endcomponent

        <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Promo Yang Terpakai</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Nama Voucher</th>
                                            <th data-priority="3">Nama Pelanggan</th>
                                            <th data-priority="1">Nomor Pesanan</th>
                                            <th data-priority="3">Jumlah Pesanan</th>
                                            <th data-priority="6">Nominal Diskon</th>
                                            <th data-priority="6">Tanggal Digunakan</th>
                                            <th data-priority="6">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($promo as $data  => $value)
                                            <tr>
                                                <td>{{ $data + 1 }}</td>
                                                <td>{{ $value->promo->promo_name }}</td>
                                                <td>{{ $value->customer->customer_name }}</td>
                                                <td>{{ $value->order->order_number }}</td>
                                                <td>{{ $value->order->order_subtotal }}</td>
                                                <td>{{ $value->promo->promo_value }}</td>
                                                <td>{{ $value->promo_used_create_date }}</td>
                                                <td>{{ $value->promo_used_status }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop