@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Promo'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.product-list') }}">Daftar Promo</a></li>
                    <li class="breadcrumb-item active">Detail Promo</li>
                @endslot
                @slot('btn')
                        <a href="{{ route('admin.promo') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Kembali</a>
                @endslot
            @endcomponent
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped mb-0">
                                <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{$promo->promo_name}}</td>
                                </tr>
                                <tr>
                                    <td>Deskripsi</td>
                                    <td>{!! $promo->promo_desc !!}</td>
                                </tr>
                                <tr>
                                    <td>Minimal Transaksi</td>
                                    <td>Rp. {{number_format($promo->promo_min_payment)}}</td>
                                </tr>
                                <tr>
                                    <td>Maksimal Diskon</td>
                                    <td>Rp. {{number_format($promo->promo_max_discount)}}</td>
                                </tr>
                                <tr>
                                    <td>Tipe</td>
                                    <td>{{$promo->promo_type}}</td>
                                </tr>
                                <tr>
                                    <td>Nilai</td>
                                    <td>{{($promo->promo_type == 'price' ? 'Rp. '.number_format($promo->promo_value) : $promo->promo_value.' %')}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped mb-0">
                                <tbody>
                                <tr>
                                    <td>Kode Promo</td>
                                    <td>{{$promo->promo_code}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah</td>
                                    <td>{{$promo->promo_qty}}</td>
                                </tr>
                                <tr>
                                    <td>Promo Terpakai</td>
                                    <td>{{$promo->promo_used}}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Mulai</td>
                                    <td>{{date('Y-m-d', strtotime($promo->promo_start_date))}}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Berakhir</td>
                                    <td>{{date('Y-m-d', strtotime($promo->promo_end_date))}}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>{{label_status($promo->promo_status)}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection