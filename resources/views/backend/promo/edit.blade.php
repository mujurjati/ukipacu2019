@extends('backend.layouts.app')
@section('content')
@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@endsection
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb')
                @slot('title')
                    Tambah Promo
                @endslot
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.promo') }}">Daftar Promo</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Promo</li>
                @endslot
            @endcomponent
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @include('backend.layouts.info')
                            {{ Form::open(['route' => ['admin.promo.update', $id], 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                            <div class="form-group row">
                                <div class="col-md-6 row">
                                    {{ Form::label('promo_name', 'Nama', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8">
                                        {{ Form::text('promo_name', $promo->promo_name, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 row">
                                    {{ Form::label('promo_min_payment', 'Minimal Pembayaran', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp</span>
                                            {{ Form::text('promo_min_payment', $promo->promo_min_payment, ['class' => 'form-control rupiah']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 row text-right mak-diskon">
                                    {{ Form::label('promo_max_discount', 'Maksimal Diskon', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp</span>
                                            {{ Form::text('promo_max_discount', $promo->promo_max_discount, ['class' => 'form-control rupiah']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 row">
                                    {{ Form::label('promo_type', 'Tipe Voucher', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8">
                                        {{ Form::select('promo_type',\App\Enums\Enums::promoType(), $promo->promo_type,array('class' => 'form-control cek-tipe')) }}
                                    </div>
                                </div>
                                <div class="col-md-6 row text-right">
                                    {{ Form::label('promo_value', 'Nilai', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8 value-price">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp</span>
                                            {{ Form::text('promo_value_price', $promo->promo_value, ['class' => 'form-control rupiah']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 value-percent">
                                        <div class="input-group">
                                            {{ Form::text('promo_value_percent', $promo->promo_value, ['class' => 'form-control validasi-percent rupiah']) }}
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 row">
                                    {{ Form::label('promo_code', 'Kode Voucher', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8">
                                        {{ Form::text('promo_code', $promo->promo_code, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="col-md-6 row text-right">
                                    {{ Form::label('promo_qty', 'Jumlah Voucher', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8">
                                        {{ Form::text('promo_qty', $promo->promo_qty, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 row">
                                    {{ Form::label('promo_start_date', 'Tanggal Mulai', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            {{ Form::text('promo_start_date', $promo->promo_start_date, ['class' => 'form-control', 'id' => 'datepicker1']) }}
                                            <span class="input-group-addon"><i class="mdi mdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 row text-right">
                                    {{ Form::label('promo_end_date', 'Tanggal Berakhir', ['class' => 'col-sm-4 col-form-label']) }}
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            {{ Form::text('promo_end_date', $promo->promo_end_date, ['class' => 'form-control', 'id' => 'datepicker2']) }}
                                            <span class="input-group-addon"><i class="mdi mdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('promo_desc', 'Deksripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    @component('backend.libs.form.editor', ['name' => 'promo_desc'])
                                        {{$promo->promo_desc}}
                                    @endcomponent
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('promo_status', 'Status', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-sm btn-primary active">
                                            {{ Form::radio('promo_status', 'active', $promo->promo_status == 'active' ? true : '') }} Active
                                        </label>
                                        <label class=" btn btn-sm btn-primary">
                                            {{ Form::radio('promo_status', 'non-active', $promo->promo_status == 'non-active' ? true : '') }} Non-Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row text-right">
                                <div class="col-sm-12">
                                    <button class="btn btn-lg btn-primary waves-effect waves-light" name="addProduct"
                                            value="1" type="submit">Simpan
                                    </button>
                                    <a href="{{ route('admin.product-cat') }}"
                                       class="btn btn-lg btn-secondary waves-effect waves-light">Batal</a>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/promo.js') }}"></script>
    @include('backend.libs.image.image_js')
@stop