@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        @component('backend.layouts.components.breadcrumb', ['title' => 'paket'])
                            @slot('ul')
                                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                            @endslot
                            @slot('ul')
                                <li class="breadcrumb-item active"><a href="{{ route('admin.paket') }}">Daftar Paket</a></li>
                            @endslot
                            @slot('btn')
                                <button type="button" class="btn btn-success btn-rounded" data-toggle="modal" data-target="#addPaket"><i class="ti-plus mr-1"></i> Tambah</button>

                                    @component('backend.layouts.components.modal', [
                                        'id'    => 'addPaket',
                                        'title' => 'Tambah Paket',
                                        'route' => route('admin.paket.create.save')
                                    ])
                                    @slot('slot')
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Nama Paket</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="paket_name" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Gambar</label>
                                        <div class="col-sm-5">
                                            @component('backend.libs.image.input', [
                                                'name'      => 'paket_image',
                                                'accept'    => ['image/jpeg', 'image/png']
                                            ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent

                                        </div>
                                    </div>
                                    @endslot
                                        
                                        @slot('btn')
                                            <button type="submit" name="savePaket" value="1"  class="btn btn-sm btn-primary waves-effect">Simpan</button>
                                        @endslot
                                    @endcomponent
                            @endslot
                        @endcomponent
                    </div>
                </div>
            </div><!-- end row -->
            @include('backend.layouts.info')
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title">Daftar Paket </h4>
                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Gambar</th>
                                            <th data-priority="2">Paket</th>
                                            <th data-priority="3">Tanggal Upload</th>
                                            <th data-priority="4">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($pakets) > 0)
                                            @foreach($pakets as $key  => $paket)
                                                <tr>
                                                    <th>{{$key+1}}</th>
                                                    <td>
                                                    @component('backend.libs.image.show', ['path' => '/images/paket/', 'class' => 'd-flex mr-3  thumb-md'])
                                                        {{ $paket->paket_image}}
                                                    @endcomponent
                                                    </td>
                                                    <td>
                                                        {{$paket->paket_name}}
                                                    </td>
                                                    <td>{{date_indo($paket->paket_created_date, 'datetime')}}</td>
                                                    <td>
                                                        <a href="{{ route('admin.paket.delete', ['id' => $paket->paket_id]) }}"><i class="mdi mdi-closed h4 text-primary"></i></a>&nbsp
                                                        <a href="{{ route('admin.paket.detail', ['id' => $paket->paket_id]) }}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" class="text-center"> <label class="text-danger">Data yang anda cari tidak di temukan !</label> </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $pakets->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection
