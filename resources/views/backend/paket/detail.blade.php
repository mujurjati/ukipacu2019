@extends('backend.layouts.app')
@section('content')
    <link href="{{ asset_url('/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/css/select2-bootstrap.min.css') }}" rel="stylesheet" />
    <style>
        .select2-container .select2-selection--single .select2-selection__rendered{
            line-height: 22px;
        }
    </style>
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'paket'])
                @slot('ul')
                    <li class="breadcrumb-item "><a href="{{ route('admin.reward') }}">Daftar Paket</a></li>
                    <li class="breadcrumb-item active">Detail Reward</li>
                @endslot
            @endcomponent
            @include('backend.layouts.info')
            <div class="row">
                <div class="col-lg-12 center">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title">Detail</h4>
                            <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th data-priority="1">Nama Paket</th>
                                        <th data-priority="6">Tanggal Upload</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                {{$paket->paket_name}}
                                            </td>
                                            <td>
                                                {{date_indo($paket->paket_created_date, 'datetime')}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12 center">
                    <div class="card">
                    <div class="card-body"><h4 class="mt-0 header-title">Daftar Produk </h4>
                            <form role="form" method="post" action="{{ route('admin.paket.add.produk', ['id' => $paket->paket_id]) }}">
                            {{ csrf_field() }}
                                <div class="card-body row">
                                    <div class="col-sm-5">
                                        <select name="product_id" class="select-address">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <button type="submit" value="1" class="btn btn-success waves-effect waves-light mr-2">Simpan</button>
                                </div>
                            </form>
                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Nama Produk</th>
                                            <th data-priority="3">Harga</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($produks) > 0)
                                            @foreach($produks as $key  => $pr)
                                                <tr>
                                                    <th>{{$key+1}}</th>
                                                    <td>
                                                        {{$pr->paketDetailProduk->product_name}}
                                                    </td>
                                                    <td>Rp {{rupiah($pr->paketDetailProduk->product_price)}}</td>
                                                    <td>
                                                        
                                                    <a href="" data-id="{{$pr->pd_id}}" data-toggle="modal" data-title="{{ $pr->paketDetailProduk->product_name }}" class="text-info btnDelete"><i class="mdi mdi-delete-forever h4"></i></a>
                                                    {{ Form::open(['route' => ['admin.paket.delete', $pr->pd_id, $paket->paket_id], 'method' => 'post', 'id' => 'deleteData'.$pr->pd_id]) }}

                                                    {{ Form::close() }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" class="text-center"> <label class="text-danger">Data produk tidak di temukan !</label> </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- content -->
    </div>
    <script src="{{ asset_url('/js/select2.min.js') }}"></script>
    <script>
        $(".select2").select2();
        var _token = '{{ csrf_token() }}';
        $(document).ready(function () {
            $('.select-address').select2({
                theme: "bootstrap",
                placeholder: 'Pilih Produk',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('ajax.get-produk') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });

        });

    </script>
@endsection
@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop

