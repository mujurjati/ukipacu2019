@extends('backend.layouts.app')
@section('content')
    <style>

        #map {
            height: 400px;
            width: 100%;
        }
    </style>
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'merchant'])
                @slot('ul')
                    <li class="breadcrumb-item "><a href="{{ route('admin.merchant') }}">Daftar Merchant</a></li>
                    <li class="breadcrumb-item active">Detail Merchant</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-lg-6 center">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title"></h4>
                            <div class="row">
                                <div class="col-4">
                                    @component('backend.libs.image.show', ['path' => '/images/merchant/', 'class' => 'd-flex mr-3  thumb-md'])
                                        {{ $merchant->merchant_image }}
                                    @endcomponent
                                </div>
                                <div class="col-8">

                                    <div class="form-group row">
                                        <label  class="col-sm-12 col-form-label">NPWP</label>
                                        <div class="col-sm-12">
                                            <p class="mt-2 mb-0 font-14 text-muted">{{$merchant->merchant_npwp}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-12 col-form-label">Nama Merchant</label>
                                        <div class="col-sm-12">
                                            <p class="mt-2 mb-0 font-14 text-muted">{{$merchant->merchant_name}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-12 col-form-label">Merchant Email</label>
                                        <div class="col-sm-12">
                                            <p class="mt-2 mb-0 font-14 text-muted">{{$merchant->merchant_email}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-12 col-form-label">Merchant Phone</label>
                                        <div class="col-sm-12">
                                            <p class="mt-2 mb-0 font-14 text-muted">{{$merchant->merchant_phone}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-12 col-form-label">Alamat</label>
                                        <div class="col-sm-12">
                                            <p class="mt-2 mb-0 font-14 text-muted">{{$merchant->merchant_address}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row mt-2">
                                        <label  class="col-sm-12 col-form-label">Daftar</label>
                                        <div class="col-sm-12">
                                            <p class="mt-2 mb-0 font-14 text-muted">{{date_indo($merchant->merchant_create_date, 'datetime')}}</p>
                                        </div>
                                    </div>

                                    <div class="form-group row mt-2">
                                        <label  class="col-sm-12 col-form-label">Status</label>
                                        <div class="col-sm-12">
                                            <p class="mt-2 mb-0 font-14 text-muted">{{label_status($merchant->merchant_status)}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div id="map" style="height: 530px"></div>
                </div>

                <input id="lat" hidden type="text" value={{ $merchant->merchant_lat }}>
                <input id="lng" hidden type="text" value={{ $merchant->merchant_lng }}>
            </div><!-- container-fluid -->
        </div><!-- content -->
        <script>

            function initMap() {
                var lat = document.getElementById("lat").value;
                var lng = document.getElementById("lng").value;

                var uluru = {lat: parseFloat(lat), lng: parseFloat(lng)};
                // var uluru = {lat: -25.344, lng: 131.036};

                var map = new google.maps.Map(
                    document.getElementById('map'), {zoom: 18, center: uluru});

                var marker = new google.maps.Marker({position: uluru, map: map});
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}&callback=initMap">
        </script>
@endsection

