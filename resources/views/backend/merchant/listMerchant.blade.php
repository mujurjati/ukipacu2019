@extends('backend.layouts.app')

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">

                    @component('backend.layouts.components.breadcrumb', ['title' => 'marketing'])
                        @slot('ul')
                            <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard/a></li>
                        @endslot
                            @slot('ul')
                            <li class="breadcrumb-item active"><a href="{{ route('admin.merchant') }}">Daftar Marketing</a></li>
                        @endslot
                        @slot('btn')
                            <a href="{{ route('admin.merchant.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah</a>
                        @endslot
                    @endcomponent
                </div>
            </div>
        </div><!-- end row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body"><h4 class="mt-0 header-title">Daftar Marketing </h4>
                        <form role="form" method="get" action="{{ route('admin.merchant') }}">
                            <div class="card-body row">
                                <div class="col-sm-3">
                                    <input class="form-control" type="text" value="{{ Request::input('keyword') }}" name="keyword" placeholder="Masukkan kata kunci..">
                                </div>
                                <div class="col-sm-2">
                                    {{
                                    Form::select('status', [
                                    ''          => '-- Semua Status --',
                                    'new'       => 'New',
                                    'active'    => 'Active',
                                    'block'     => 'Block',
                                    'delete'    => 'Delete'
                                    ], Request::input('status'), ['class' => 'form-control'])
                                    }}
                                </div>
                                <button type="submit" class="btn btn-success waves-effect waves-light mr-2">Cari</button>
                                <a href="{{route('admin.merchant')}}" name="reset"  class="btn btn-warning">Reset</a>
                            </div>
                        </form>

                        <div class="table-rep-plugin">
                            <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th data-priority="1">Gambar</th>
                                        <th data-priority="3">Nama</th>
                                        <th data-priority="1">Email</th>
                                        <th data-priority="3">Telepon</th>
                                        {{--<th data-priority="3">Alamat</th>--}}
                                        <th data-priority="6">Status</th>
                                        <th data-priority="6">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($merchants) > 0)
                                    @foreach($merchants as $key  => $merchant)
                                    <tr>
                                        <th>{{$key+1}}</th>
                                        <td>
                                            @component('backend.libs.image.show', ['path' => '/images/merchant/', 'class' => 'd-flex mr-3  thumb-md'])
                                            {{ $merchant->merchant_image }}
                                            @endcomponent
                                        </td>
                                        <td>{{$merchant->merchant_name}}</td>
                                        <td>{{$merchant->merchant_email}}</td>
                                        <td>{{$merchant->merchant_phone}}</td>
                                        {{--<td>--}}
                                            {{--{{--}}
                                            {{--@$merchant--}}
                                            {{--->address_primary--}}
                                            {{--->subdistrict--}}
                                            {{--->city--}}
                                            {{--->province->s_province_name--}}
                                            {{--}}--}}
                                        {{--</td>--}}
                                        <td>{{label_status($merchant->merchant_status)}}</td>
                                        <td>
                                            <a href="{{ route('admin.merchant.edit', ['id' => $merchant->merchant_id]) }}"><i class="mdi mdi-pencil h4 text-primary"></i></a>&nbsp
                                            <a href="{{ route('admin.merchant.detail', ['id' => $merchant->merchant_id]) }}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                            @if($merchant->merchant_status !='block')
                                            <a href="" data-target="#block-{{$merchant->merchant_id}}" data-toggle="modal"  class="text-primary"> <i class="mdi mdi-lock h4"></i></a>
                                            @else
                                            <a href="" data-target="#open-{{$merchant->merchant_id}}" data-toggle="modal"  class="text-danger"> <i class="mdi mdi-lock-open h4"></i></a>
                                            @endif
                                        </td>
                                    </tr>

                                    @component('backend.layouts.components.modal', [
                                    'id'    => 'block-'.$merchant->merchant_id,
                                    'title' => 'Konfirmasi',
                                    'route' => route('admin.merchant.block', ['id' => $merchant->merchant_id])
                                    ])
                                    Anda yakin ingin blokir merchant <b>{{ $merchant->merchant_name }}</b> ?
                                    @slot('btn')
                                    <button type="submit" name="updateblock" value="1"  class="btn btn-sm btn-primary waves-effect">Blokir</button>
                                    @endslot
                                    @endcomponent

                                    @component('backend.layouts.components.modal', [
                                    'id'    => 'open-'.$merchant->merchant_id,
                                    'title' => 'Konfirmasi',
                                    'route' => route('admin.merchant.open', ['id' => $merchant->merchant_id])
                                    ])
                                    Anda yakin ingin buka blokir merchant <b>{{ $merchant->merchant_name }}</b> ?
                                    @slot('btn')
                                    <button type="submit" name="updateopen" value="1" class="btn btn-sm btn-success waves-effect">Buka Blokir</button>
                                    @endslot
                                    @endcomponent

                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="8" class="text-center"> <label class="text-danger">Data yang anda cari tidak di temukan !</label> </td>
                                    </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <nav class="toolbox toolbox-pagination mt-10">
                                    {{ $merchants->links("pagination::bootstrap-4") }}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- container-fluid -->
</div><!-- content -->

@endsection