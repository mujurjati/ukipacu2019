@extends('backend.layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/map/jquery-gmaps-latlon-picker.css') }}">
{{--    <link rel="stylesheet" href="{{ asset_url('/backend/assets/map/demo.css') }}">--}}
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'marketing'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.merchant') }}">Daftar Marketing</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tambah Baru</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 mb-10 header-title">Tambah Marketing</h4>

                            @include('backend.layouts.info')

                            <form style="margin-top: 10px" action="{{ route('admin.merchant.create.save') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <!-- <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">NPWP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="merchant_npwp" type="text">
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="merchant_name" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="merchant_phone" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input_crop', ['selector' => '.imageupload', 'name' => 'merchant_image'])
                                            <input type="file" class="imageupload"
                                                   accept="image/jpeg, image/jpg, image/png"
                                                   data-ratio="100/100"
                                                   data-max-file-size="2MB">
                                        @endcomponent

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="merchant_email" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="merchant_password" type="password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="merchant_address" type="text" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary active">
                                                <input type="radio" name="merchant_status" id="option1" value="active" checked="checked"> Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary">
                                                <input type="radio" name="merchant_status" id="option2" value="block"> No-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- <label class="col-sm-2 col-form-label">Pilih lokasi merchant</label>
                                <fieldset class="gllpLatlonPicker">
                                    <div class="gllpMap">Google Maps</div>
                                    <input name="merchant_lat" type="hidden" class="gllpLatitude" value="-6.920315284273143"/>
                                    <input name="merchant_lng" type="hidden" class="gllpLongitude" value="107.61946507177731"/>
                                    <input type="hidden" class="gllpZoom" value="14"/>
                                </fieldset> -->



                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12" style="margin-top: 20px">
                                        <button class="btn btn-primary waves-effect waves-light" name="addMerchant" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div>
    </div>

@endsection
@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@stop
@section('load-script')
    @include('backend.libs.image.image_js')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}&callback=initMap">
    </script>
    <script src="{{ asset_url('/backend/assets/map/jquery-gmaps-latlon-picker.js') }}"></script>
{{--    <script src="{{ asset_url('/backend/assets/map/jquery-2.1.1.min.js') }}"></script>--}}
    <script>
        $(document).ready(function() {
            // Copy the init code from "jquery-gmaps-latlon-picker.js" and extend it here
            $(".gllpLatlonPicker").each(function() {
                $obj = $(document).gMapsLatLonPicker();

                $obj.params.strings.markerText = "Drag this Marker (example edit)";

                $obj.params.displayError = function(message) {
                    console.log("MAPS ERROR: " + message); // instead of alert()
                };

                $obj.init( $(this) );
            });
        });
    </script>
@stop