@extends('backend.layouts.app')

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">

                    @component('backend.layouts.components.breadcrumb', ['title' => 'Management Driver'])
                    @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard/a></li>
                    @endslot
                    @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.driver') }}">Daftar Driver</a></li>
                    @endslot
                    @slot('btn')
                    <a href="{{ route('admin.driver.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah Baru</a>
                    @endslot
                    @endcomponent
                </div>
            </div>
        </div><!-- end row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body"><h4 class="mt-0 header-title">Daftar Driver </h4>
                        <form role="form" method="get" action="{{ route('admin.driver') }}">
                            <div class="card-body row">
                                <div class="col-sm-3">
                                    <input class="form-control" type="text" value="{{ Request::input('keyword') }}" name="keyword" placeholder="Masukkan kata kunci..">
                                </div>
                                <div class="col-sm-2">
                                    {{
                                    Form::select('status', [
                                    ''          => '-- Semua Status --',
                                    'new'       => 'New',
                                    'active'    => 'Active',
                                    'block'     => 'Block',
                                    'delete'    => 'Delete'
                                    ], Request::input('status'), ['class' => 'form-control'])
                                    }}
                                </div>
                                <button type="submit" class="btn btn-success waves-effect waves-light mr-2">Cari</button>
                                <a href="{{route('admin.driver')}}" name="reset"  class="btn btn-warning">Reset</a>
                            </div>
                        </form>

                        <div class="table-rep-plugin">
                            <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th data-priority="1">Gambar</th>
                                        <th data-priority="3">Nama</th>
                                        <th data-priority="1">Email</th>
                                        <th data-priority="3">Telepon</th>
                                        {{--<th data-priority="3">Alamat</th>--}}
                                        <th data-priority="6">Status</th>
                                        <th data-priority="6">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($drivers) > 0)
                                    @foreach($drivers as $key  => $driver)
                                    <tr>
                                        <th>{{$key+1}}</th>
                                        <td>
                                            @component('backend.libs.image.show', ['path' => '/images/driver/', 'class' => 'd-flex mr-3  thumb-md'])
                                            {{ $driver->driver_image }}
                                            @endcomponent
                                        </td>
                                        <td>{{$driver->driver_name}}</td>
                                        <td>{{$driver->driver_email}}</td>
                                        <td>{{$driver->driver_phone}}</td>
                                        {{--<td>--}}
                                            {{--{{--}}
                                            {{--@$driver--}}
                                            {{--->address_primary--}}
                                            {{--->subdistrict--}}
                                            {{--->city--}}
                                            {{--->province->s_province_name--}}
                                            {{--}}--}}
                                        {{--</td>--}}
                                        <td>{{label_status($driver->driver_status)}}</td>
                                        <td>
                                            <a href="{{ route('admin.driver.edit', ['id' => $driver->driver_id]) }}"><i class="mdi mdi-pencil h4 text-primary"></i></a>&nbsp
                                            <a href="{{ route('admin.driver.detail', ['id' => $driver->driver_id]) }}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                            @if($driver->driver_status !='block')
                                            <a href="" data-target="#block-{{$driver->driver_id}}" data-toggle="modal"  class="text-primary"> <i class="mdi mdi-lock h4"></i></a>
                                            @else
                                            <a href="" data-target="#open-{{$driver->driver_id}}" data-toggle="modal"  class="text-danger"> <i class="mdi mdi-lock-open h4"></i></a>
                                            @endif
                                        </td>
                                    </tr>

                                    @component('backend.layouts.components.modal', [
                                    'id'    => 'block-'.$driver->driver_id,
                                    'title' => 'Konfirmasi',
                                    'route' => route('admin.driver.block', ['id' => $driver->driver_id])
                                    ])
                                    Anda yakin ingin blokir driver <b>{{ $driver->driver_name }}</b> ?
                                    @slot('btn')
                                    <button type="submit" name="updateblock" value="1"  class="btn btn-sm btn-primary waves-effect">Blokir</button>
                                    @endslot
                                    @endcomponent

                                    @component('backend.layouts.components.modal', [
                                    'id'    => 'open-'.$driver->driver_id,
                                    'title' => 'Konfirmasi',
                                    'route' => route('admin.driver.open', ['id' => $driver->driver_id])
                                    ])
                                    Anda yakin ingin buka blokir driver <b>{{ $driver->driver_name }}</b> ?
                                    @slot('btn')
                                    <button type="submit" name="updateopen" value="1" class="btn btn-sm btn-success waves-effect">Buka Blokir</button>
                                    @endslot
                                    @endcomponent

                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="8" class="text-center"> <label class="text-danger">Data yang anda cari tidak di temukan !</label> </td>
                                    </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <nav class="toolbox toolbox-pagination mt-10">
                                    {{ $drivers->links("pagination::bootstrap-4") }}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- container-fluid -->
</div><!-- content -->

@endsection