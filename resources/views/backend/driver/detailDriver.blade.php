@extends('backend.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Driver'])
                @slot('ul')
                    <li class="breadcrumb-item "><a href="{{ route('admin.driver') }}">Daftar Driver</a></li>
                    <li class="breadcrumb-item active">Detail Driver</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-lg-5 center">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title"></h4>
                            <div class="row">
                                <div class="col-4">
                                    @component('backend.libs.image.show', ['path' => '/images/driver/', 'class' => 'd-flex mr-3  thumb-md'])
                                        {{ $driver->driver_image }}
                                    @endcomponent
                                </div>
                                <div class="col-8">
                                    <h5 class="mt-0 font-14 m-b-15 text-muted">{{$driver->driver_name}}</h5>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{$driver->driver_email}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{$driver->driver_phone}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">Daftar: {{date_indo($driver->driver_create_date, 'datetime')}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{label_status($driver->driver_status)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 center">
                    <div class="card">
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-12">
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Jenis Kelamin : {{ jenis_kelamin($driver->driver_gender) }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Tempat Lahir : {{ $driver->driver_birth_place }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Tanggal Lahir : {{ date_indo($driver->driver_birth_date) }}
                                    </p>
                                    {{--<p class="mt-0 font-14 m-b-15 text-muted">--}}
                                        {{--Alamat :--}}
                                        {{--{{--}}
                                            {{--@$driver--}}
                                                {{--->address_primary--}}
                                                {{--->subdistrict--}}
                                                {{--->city--}}
                                                {{--->province->s_province_name--}}
                                        {{--}}--}}
                                    {{--</p>--}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- container-fluid -->
        </div><!-- content -->
@endsection