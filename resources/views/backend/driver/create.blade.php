@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Driver'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.driver') }}">Daftar Driver</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tambah Baru</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Tambah Driver</h4>

                            @include('backend.layouts.info')

                            <form action="{{ route('admin.driver.create.save') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="driver_name" type="text">
                                    </div>
                                </div>
                                {{--<div class="form-group row">--}}
                                {{--<label class="col-sm-2 col-form-label">Gender</label>--}}
                                {{--<div class="col-sm-10">--}}
                                {{--<select class="form-control" name="driver_gender">--}}
                                {{--<option value="m">Laki-laki</option>--}}
                                {{--<option value="p">Perempuan</option>--}}
                                {{--</select>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="driver_phone" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input_crop', ['selector' => '.imageupload', 'name' => 'driver_image'])
                                            <input type="file" class="imageupload"
                                                   accept="image/jpeg, image/jpg, image/png"
                                                   data-ratio="100/100"
                                                   data-max-file-size="2MB">
                                        @endcomponent

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="driver_email" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="driver_password" type="password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary active">
                                                <input type="radio" name="driver_status" id="option1" value="active" checked="checked"> Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary">
                                                <input type="radio" name="driver_status" id="option2" value="block"> No-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary waves-effect waves-light" name="addDriver" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div>
    </div>

@endsection
@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@stop
@section('load-script')
    @include('backend.libs.image.image_js')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop