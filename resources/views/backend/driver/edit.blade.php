@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Driver'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.driver') }}">Daftar Driver</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Driver</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Edit Driver</h4>

                            @include('backend.layouts.info')

                            <form action="{{ route('admin.driver.update', ['id' => $driver->driver_id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{ $driver->driver_name }}" name="driver_name" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{ $driver->driver_phone }}" type="text" name="driver_phone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input_crop', [
                                               'selector'  => '.imageupload',
                                               'name'      => 'driver_image',
                                               'src'       => get_image($driver->driver_image, 'driver'),
                                               ])
                                            <input type="file" class="imageupload"
                                                   accept="image/jpeg, image/jpg, image/png"
                                                   data-ratio="100/100"
                                                   data-max-file-size="10MB">
                                        @endcomponent

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{ $driver->driver_email }}"  name="driver_email" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="password" name="userPassword" style="display: none">
                                        <a href="#" class="btn btn-sm" id="ganti-password">Ganti Password</a>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary {{ $driver->driver_status == 'active' ? 'active' : ' ' }}">
                                                <input type="radio" name="driver_status" id="option1" value="active" {{ $driver->driver_status == 'active' ? 'checked="checked"' : '' }}> Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary {{ $driver->driver_status == 'block' ? 'active' : '' }}">
                                                <input type="radio" name="driver_status" id="option2" value="block" {{ $driver->driver_status == 'block' ? 'checked="checked"' : '' }}> No-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary waves-effect waves-light" name="editDriver" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div>
    </div>

@endsection
@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@stop
@section('load-script')
    @include('backend.libs.image.image_js')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop