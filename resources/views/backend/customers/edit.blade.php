@extends('backend.layouts.app')

@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Member'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.customer') }}">Daftar Member</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Member</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        @include('backend.layouts.info')
                            <form style="margin-top: 10px" action="{{ route('admin.customer.edit.save', ['id' => $cus->customer_id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                            <div class="col-lg-6">
                            <h4 style="text-decoration: underline">Data Member</h4>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Gambar</label>
                                    <div class="col-sm-5">
                                        @component('backend.libs.image.input_crop', [
                                                   'selector'  => '.imageupload',
                                                   'name'      => 'customer_image',
                                                   'src'       => get_image('customer/'.$cus->customer_image),
                                                   ])
                                                <input type="file" class="imageupload"
                                                       accept="image/jpeg, image/jpg, image/png"
                                                       data-ratio="100/100"
                                                       data-max-file-size="10MB">
                                            @endcomponent

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->customer_name }}" name="customer_name" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->customer_email }}" name="customer_email" type="email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->customer_birth_date }}" placeholder="Misalkan : 16 Desember 1994" name="customer_tlg" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">No. KTP</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->customer_nik }}" name="customer_nik" type="number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="customer_address" type="text" rows="4">
                                        {{ $cus->customer_address }}
                                        </textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">NPWP</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->customer_npwp }}" name="customer_npwp" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Phone / WA</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->customer_phone }}" name="customer_phone" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Password</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" style="display:none" name="userPassword" type="password">
                                        <a href="#" class="btn btn-sm" id="ganti-password">Ganti Password</a>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-6">
                                <h4 style="text-decoration: underline">Data Penungjang</h4>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Pendidikan Terakhir</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->penunjang->c_pendidikan }}" name="customer_pendidikan" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->penunjang->c_pekerjaan }}" name="customer_perkerjaan" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Agama</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->penunjang->c_agama }}" name="customer_agama" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Ibu Kandung</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->penunjang->c_ibu_kandung }}" name="customer_ibu" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Ahli Waris</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->penunjang->c_ahli_waris }}" name="customer_waris" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Hubungan dengan Ahli Waris</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->penunjang->c_hub_waris }}" name="customer_hub_waris" type="text">
                                    </div>
                                </div>

                                <h4 style="text-decoration: underline">Data Bank</h4>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Bank</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->bank->s_bank_name }}" name="customer_bank_name" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Cabang</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->bank->c_bank_cabang }}" name="customer_bank_cabang" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nama Nasabah</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->bank->c_bank_nasabah }}" name="customer_bank_nasabah" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-3 col-form-label">Nomor Rekening</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" value="{{ $cus->bank->c_bank_rek }}" name="customer_bank_rek" type="number">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row right" style="text-align: center">
                            <div class="col-sm-12" style="margin-top: 20px">
                                <button class="btn btn-success waves-effect waves-light" name="editCusByAdm" value="1" type="submit">Simpan</button>
                                <a href="{{ route('admin.customer') }}" class="btn btn-secondary waves-effect waves-light">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@stop
@section('load-script')
    @include('backend.libs.image.image_js')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        var _token = '{{ csrf_token() }}';
        $(document).ready(function () {
            $('.select-address').select2({
                theme: "bootstrap",
                placeholder: 'Pilih Kota atau Kecamatan',
                selectOnClose: true,
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('ajax.get-address') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });
        });
    </script>
@stop