@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                    @component('backend.layouts.components.breadcrumb', ['title' => 'member'])
                            @slot('ul')
                                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                            @endslot
                            @slot('ul')
                                <li class="breadcrumb-item active"><a href="{{ route('admin.customer') }}">Daftar Member</a></li>
                            @endslot
                            @slot('btn')
                                <a href="{{ route('admin.customer.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah</a>
                            @endslot
                        @endcomponent
                    </div>
                </div>
            </div><!-- end row -->
            @include('backend.layouts.info')
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title">Daftar Member </h4>
                            <form role="form" method="get" action="{{ route('admin.customer') }}">
                                <div class="card-body row">
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" value="{{ request()->get('keyword') }}" name="keyword" placeholder="Masukkan kata kunci..">
                                    </div>
                                    <div class="col-sm-3">
                                    {{
                                        Form::select('status', [
                                            ''          => '-- Semua Status --',
                                            'new'       => 'New',
                                            'active'    => 'Active',
                                            'block'     => 'Block',
                                            'delete'    => 'Delete'
                                        ], Request::input('status'), ['class' => 'form-control'])
                                    }}
                                    </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light mr-2">Cari</button>
                                    <a href="{{route('admin.customer')}}" name="reset"  class="btn btn-warning">Reset</a>
                                </div>
                            </form>

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Gambar</th>
                                            <th data-priority="2">Nama</th>
                                            <th data-priority="3">Bank</th>
                                            <th data-priority="5">Alamat</th>
                                            <th data-priority="6">Profit</th>
                                            <th data-priority="7">Status</th>
                                            <th data-priority="8">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    @if(count($customers) > 0)
                                        @foreach($customers as $key  => $customer)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>
                                                    @component('backend.libs.image.show', ['path' => '/images/customer/', 'class' => 'd-flex mr-3  thumb-md'])
                                                        {{ $customer->customer_image }}
                                                    @endcomponent
                                                </td>
                                                <td><strong> {{$customer->customer_name}} </strong>
                                                    <br> Email : {{$customer->customer_email}}
                                                    <br> Telp : {{$customer->customer_phone}}
                                                </td>
                                                <td> @if($customer->bank != '')
                                                    {{$customer->bank->s_bank_name}} <br> Rek : {{$customer->bank->c_bank_rek}}
                                                    @else
                                                    Data Bank Kosong
                                                    @endif
                                                </td>
                                                <td>
                                                    {{$customer->customer_address}}
                                                </td>
                                                <td>
                                                    {{profit($customer->customer_count)}}
                                                </td>
                                                <td align="center">
                                                    {{label_status($customer->customer_status)}} <br>

                                                    <!-- @if($customer->customer_status == 'new')
                                                        <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#confirm-{{ $customer->customer_id }}">Konfirmasi</button>

                                                        @component('backend.layouts.components.modal', [
                                                           'id'    => 'confirm-'.$customer->customer_id,
                                                           'title' => 'Konfirmasi',
                                                           'route' => route('admin.customer.open', ['id' => $customer->customer_id])
                                                       ])
                                                            Anda yakin ingin konfirmasi customer <b>{{ $customer->customer_name }}</b> ?
                                                            @slot('btn')
                                                                <button type="submit" name="updateopen" value="1"  class="btn btn-sm btn-primary waves-effect">Konfirmasi</button>
                                                            @endslot
                                                        @endcomponent
                                                    @endif -->
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.customer.detail', ['id' => $customer->customer_id]) }}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                                    <a href="{{ route('admin.customer.edit', ['id' => $customer->customer_id]) }}" class="text-primary"><i class="mdi mdi-pencil h4"></i></a>&nbsp
                                                    @if($customer->customer_status !='block')
                                                        <a href="" data-target="#block-{{$customer->customer_id}}" data-toggle="modal"  class="text-danger"> <i class="mdi mdi-lock h4"></i></a>
                                                    @endif
                                                </td>
                                            </tr>

                                            @component('backend.layouts.components.modal', [
                                               'id'    => 'block-'.$customer->customer_id,
                                               'title' => 'Konfirmasi',
                                               'route' => route('admin.customer.block', ['id' => $customer->customer_id])
                                           ])
                                                Anda yakin ingin blokir customer <b>{{ $customer->customer_name }}</b> ?
                                                @slot('btn')
                                                    <button type="submit" name="updateblock" value="1"  class="btn btn-sm btn-primary waves-effect">Blokir</button>
                                                @endslot
                                            @endcomponent

                                            @component('backend.layouts.components.modal', [
                                               'id'    => 'open-'.$customer->customer_id,
                                               'title' => 'Konfirmasi',
                                               'route' => route('admin.customer.open', ['id' => $customer->customer_id])
                                           ])
                                                Anda yakin ingin buka blokir customer <b>{{ $customer->customer_name }}</b> ?
                                                @slot('btn')
                                                    <button type="submit" name="updateopen" value="1" class="btn btn-sm btn-success waves-effect">Buka Blokir</button>
                                                @endslot
                                            @endcomponent

                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9" class="text-center"> <label class="text-danger">Data yang anda cari tidak di temukan !</label> </td>
                                        </tr>
                                    @endif
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $customers->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection
