@extends('backend.layouts.app')
@section('content')

<link href="{{ asset_url('/css/orgchart.css') }}" rel="stylesheet" type="text/css">
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Pelanggan'])
                @slot('ul')
                    <li class="breadcrumb-item "><a href="{{ route('admin.customer') }}">Daftar Pelanggan</a></li>
                    <li class="breadcrumb-item active">Detail Pelanggan</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-lg-5 center">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title">Utama</h4><hr>
                            <div class="row">
                                <div class="col-4">
                                    @component('backend.libs.image.show', ['path' => '/images/customer/', 'class' => 'd-flex mr-3  thumb-lg'])
                                        {{ $customer->customer_image }}
                                    @endcomponent
                                </div>
                                <div class="col-8">
                                    <h5 class="mt-0 font-14 m-b-15 text-muted">{{$customer->customer_name}}</h5>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{$customer->customer_email}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{$customer->customer_phone}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">Daftar: {{date_indo($customer->customer_create_date, 'datetime')}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{label_status($customer->customer_status)}}</p>
                                <hr>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        KTP :
                                       <strong> {{ $customer->customer_nik }} </strong>
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        NPWP : {{ $customer->customer_npwp }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Tanggal Lahir : {{ $customer->customer_birth_date }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Alamat :
                                        {{ $customer->customer_address }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Kota / Kecamatan :
                                        {{ $customer->customer_address }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 center">
                    <div class="card">
                        <div class="card-body pb-0">
                        <h4 class="mt-0 header-title">Bank</h4><hr>
                            <div class="row">
                                <div class="col-12">
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Bank :
                                        {{ $customer->bank->s_bank_name }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Cabang :
                                        {{ $customer->bank->c_bank_cabang }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Atas Nama :
                                        {{ $customer->bank->c_bank_nasabah }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        No. Rek :
                                        {{ $customer->bank->c_bank_rek }}
                                    </p>
                                    <hr>
                                    <h4 class="mt-0 header-title">Penunjang</h4>
                                    <hr>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Pendidikan :
                                        {{ $customer->penunjang->c_pendidikan }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Pekerjaan :
                                        {{ $customer->penunjang->c_pekerjaan }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Agama :
                                        {{ $customer->penunjang->c_agama }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Ibu Kandung :
                                        {{ $customer->penunjang->c_ibu_kandung }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Ahli Waris :
                                        {{ $customer->penunjang->c_ahli_waris }}
                                    </p>
                                    <p class="mt-0 font-14 m-b-15 text-muted">
                                        Hubungan Ahli Waris :
                                        {{ $customer->penunjang->c_hub_waris }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- container-fluid -->
            
            <div class="row">
                <div class="col-lg-12 center">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title">Informasi Transaksi Bawahan Bulan {{$bulan}}</h4>
                            <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th data-priority="1">No</th>
                                        <th data-priority="2">Nama</th>
                                        <th data-priority="3">Status Transaksi</th>
                                        <th data-priority="4">No Transaksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($bawahan) > 0)
                                            @foreach($bawahan as $key  => $bw)
                                                <tr>
                                                    <th>{{$key+1}}</th>
                                                    <td>
                                                        {{$bw->customer_name}}
                                                    </td>
                                                    <td>{!!($bw->order_status == '' ? order_status_label($x = 'belum_trx') : order_status_label($bw->order_status))!!}</td>
                                                    <td>
                                                        @if($bw->order_number != '')
                                                            <a href="{{ route('admin.order.detail', ['id' => $bw->order_id])}}">
                                                                <span class="text-primary">#{{ $bw->order_number }}</span>
                                                            </a> 
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5" class="text-center"> <label class="text-danger">Data bawahan tidak di temukan!</label> </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- content -->
    </div>
        <!-- <script src="//code.jquery.com/jquery-1.11.3.min.js"></script> -->
        <script src="{{ asset_url('/js/orgchart.js') }}"></script>
        <script>
		$(document).ready(function () {
			// create a tree
			$("#tree-data").jOrgChart({
				chartElement: $("#tree-view"), 
				nodeClicked: nodeClicked
			});
			
			// lighting a node in the selection
			function nodeClicked(node, type) {
				node = node || $(this);
				$('.jOrgChart .selected').removeClass('selected');
				node.addClass('selected');
			}
		});
		</script>
@endsection
