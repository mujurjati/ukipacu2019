@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                    @component('backend.layouts.components.breadcrumb', ['title' => 'laporan'])
                            @slot('ul')
                                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                            @endslot
                            @slot('ul')
                                <li class="breadcrumb-item active"><a href="{{ route('admin.report') }}">Laporan</a></li>
                            @endslot
                        @endcomponent
                    </div>
                </div>
            </div><!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="mt-0 header-title">Total Keuntungan : Rp {{($total == 0 ? 0 : price_format($total))}}</h4>
                            <form role="form" method="get" action="{{ route('admin.report') }}">
                                <div class="card-body row">
                                    <!-- <div class="col-sm-3">
                                        <input class="form-control" type="text" value="{{ request()->get('keyword') }}" name="keyword" placeholder="Masukkan kata kunci..">
                                    </div> -->
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control"
                                                   name="filter_date_awal"
                                                   value="{{ Request::input('filter_date_awal') }}"
                                                   placeholder="Tanggal Mulai"
                                                   id="datepicker-autoclose-1">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="mdi mdi-calendar"></i>
                                                </span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control"
                                                   name="filter_date_akhir"
                                                   value="{{ Request::input('filter_date_akhir') }}"
                                                   placeholder="Tanggal Akhir"
                                                   id="datepicker-autoclose-2">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="mdi mdi-calendar"></i>
                                                </span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light mr-2">Cari</button>
                                    <a href="{{route('admin.report')}}" name="reset"  class="btn btn-warning">Reset</a>
                                </div>
                            </form>

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Member</th>
                                            <th data-priority="2">Tipe Transaksi</th>
                                            <th data-priority="2">Code Transaksi</th>
                                            <th data-priority="3">Jumlah</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    @if(count($reports) > 0)
                                        @foreach($reports as $key  => $report)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$report->customer->customer_name}}</td>
                                                <td>{{$report->detailIndex->od_type}}</td>
                                                <td>
                                                    <a href="{{ route('admin.order.detail', ['id' => $report->order_id])}}">
                                                        <span class="text-primary">#{{ $report->order_number }}</span>
                                                    </a>
                                                </td>
                                                <td> Rp
                                                    {{ price_format($report->order_total-100000) }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="text-center"> <label class="text-danger">Data yang anda cari tidak di temukan !</label> </td>
                                        </tr>
                                    @endif
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $reports->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->
   
@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $("#datepicker-autoclose-1").datepicker({
            autoclose: !0,
            todayHighlight: !0,
            format: "dd-mm-yyyy",
        });
        $("#datepicker-autoclose-2").datepicker({
            autoclose: !0,
            todayHighlight: !0,
            format: "dd-mm-yyyy",
        });
    </script>
@endsection
