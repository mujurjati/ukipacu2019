@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8"><h4 class="page-title mb-0">Dashboard</h4>
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="#">Ukipacu</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">

                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">{{$totalProduk}}</h5>
                                <h6 class="mt-0 mb-3">Produk</h6>
                                <p class="text-muted mb-0">Total Jumlah Produk</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">

                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">{{$totalCustomer}}</h5>
                                <h6 class="mt-0 mb-3">Customer</h6>
                                <p class="text-muted mb-0">Total Jumlah Customer</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">

                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">{{$totalMerchant}}</h5>
                                <h6 class="mt-0 mb-3">Marketing</h6>
                                <p class="text-muted mb-0">Total Marketing Aktif</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">
                            
                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">{{$totalTrxSuccess}}</h5>
                                <h6 class="mt-0 mb-3">Transaksi Sukses</h6>
                                <p class="text-muted mb-0">Total Transaksi Sukses</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <canvas id="pieChartCatPro" height="200"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <canvas id="barChartStatus" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Total Jumlah Transaksi Bulan {{$bulanSekarang}}</h4>
                            <canvas id="barChartBulan" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        var keyStatus = <?= json_encode($dataKeyStatus) ?>;
        var valStatus = <?= str_replace('"', '', json_encode($dataTotStatus)) ?>;
        var dataKeyPro = <?= json_encode($dataKeyPro) ?>;
        var dataTotPro = <?= str_replace('"', '', json_encode($dataTotPro)) ?>;
        var dataDayTotTRXBulanIni = <?= json_encode($dataDayTotTRXBulanIni) ?>;
        var dataValTotTRXBulanIni = <?= str_replace('"', '', json_encode($dataValTotTRXBulanIni)) ?>;
    </script>
    <script src="{{ asset_url('/backend/assets/js/chart.min.js') }}"></script>
    <script>
        //Berdasarkan Status
        new Chart(document.getElementById("barChartStatus"), {
            type: 'bar',
            data: {
            labels: keyStatus,
            datasets: [{
                            label: "Total Trx",
                            fill: !0,
                            lineTension: .5,
                            backgroundColor: [
                                "#66ffff",
                                "#FF6384",
                                "#d24dff",
                                "#8463FF",
                                "#6384FF"
                            ],
                            borderColor: "#e2595f",
                            borderCapStyle: "butt",
                            borderDash: [],
                            borderDashOffset: 0,
                            borderJoinStyle: "miter",
                            pointBorderColor: "#e2595f",
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "#e2595f",
                            pointHoverBorderColor: "#eef0f2",
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            data: valStatus
                        }]
            },
            options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Total Transaksi Sukses Berdasarkan Status'
            },
            scales: {yAxes: [{ticks: {min: 0, stepSize : 500000}}]}
            }
        });

    //Produk Terlaris
        new Chart(document.getElementById("pieChartCatPro"), {
            type: 'pie',
            data: {
            labels: dataKeyPro,
            datasets: [
                    {
                        data: dataTotPro,
                        backgroundColor: [
                            "#FF6384",
                            "#66ffff",
                            "#d24dff",
                            "#8463FF",
                            "#6384FF"
                        ]
                    }]
            },
            options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Produk Terlaris'
            },
            scales: {yAxes: [{ticks: {min: 0, stepSize : 200}}]}
            }
        });

        // bulan ini
        new Chart(document.getElementById("barChartBulan"), {
            type: 'bar',
            data: {
            labels: dataDayTotTRXBulanIni,
            datasets: [{
                            label: "Total Trx",
                            fill: !0,
                            lineTension: .5,
                            backgroundColor: "rgba(150, 238, 167, 1)",
                            borderColor: "#79D1CF",
                            borderCapStyle: "butt",
                            borderDash: [],
                            borderDashOffset: 0,
                            borderJoinStyle: "miter",
                            pointBorderColor: "#79D1CF",
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "#79D1CF",
                            pointHoverBorderColor: "#79D1CF",
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            data: dataValTotTRXBulanIni
                        }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                title: {
                    display: false,
                    text: ''
                },
                scales: {
                    yAxes: [{ticks: {min: 0, stepSize : 750000}}]
                },
                "animation": {
                "duration": 1,
              "onComplete": function() {
                var chartInstance = this.chart,
                  ctx = chartInstance.ctx;
 
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';
 
                this.data.datasets.forEach(function(dataset, i) {
                  var meta = chartInstance.controller.getDatasetMeta(i);
                  meta.data.forEach(function(bar, index) {
                    var data = dataset.data[index];
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                  });
                });
              }
            },
            }
        });
    </script>

@endsection