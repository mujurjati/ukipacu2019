@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Produk'])
                @slot('ul')
                    <li class="breadcrumb-item "><a href="{{ route('admin.product-list') }}">Daftar Produk</a></li>
                    <li class="breadcrumb-item active"><a href="#">Detail Produk</a></li>
                @endslot
                @slot('btn')

                @endslot
            @endcomponent

            <div class="row">
                <div class="col-md-12 col-xl-12">
                    <div class="row">

                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body"><h4 class="card-title font-20 mt-0"></h4>
                                        <img src="{{ asset_url('/images/products/'.$product->product_image)}}" width="120">

                                    </div>
                                    <div class="card-body"><h4 class="card-title font-20 mt-0">{{$product->product_name}}</h4>
                                        <table width="100%">
                                            <tr>
                                                <td>Harga</td>
                                                <td>:</td>
                                                <td>Rp {{number_format($product->product_price)}}</td>
                                            </tr>
                                            <tr>
                                                <td width="100"><strong>Stok</strong></td>
                                                <td width="10">:</td>
                                                <td><strong>{{$product->product_stock}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="100"><strong>Tags</strong></td>
                                                <td width="10">:</td>
                                                <td>
                                                    @foreach(explode(',',$product->product_tags) as $item)
                                                        <small class="badge badge-secondary">{{ucwords(str_replace('_',' ',$item))}}</small>
                                                    @endforeach
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="card">

                                    <div class="card-body">

                                        <ul class="list-group list-group-flush">

                                            <li class="list-group-item">
                                                <strong>Deskripsi Lengkap :</strong>
                                                {!! $product->product_desc !!}
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection