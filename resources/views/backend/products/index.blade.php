@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Produk'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.product-list') }}">Daftar Produk</a></li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.product.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah Produk</a>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Produk</h4>
                            <form role="form" method="get" action="{{ route('admin.product-list') }}">
                                <div class="card-body row">
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" value="{{ Request::input('keyword') }}" name="keyword" placeholder="Masukkan kata kunci..">
                                    </div>
                                    <div class="col-sm-2">
                                        {{
                                            Form::select('category', ['' => '-- Semua Kategori --']+$category, Request::input('category'), ['class' => 'form-control'])
                                        }}
                                    </div>
                                    <div class="col-sm-2">
                                        {{
                                            Form::select('status', [
                                                ''          => '-- Semua Status --',
                                                'publish'       => 'Publish',
                                                'draft'    => 'Draft',
                                                'block'     => 'Block',
                                            ], Request::input('status'), ['class' => 'form-control'])
                                        }}
                                    </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light mr-2">Cari</button>
                                    <a href="{{route('admin.product-list')}}" name="reset"  class="btn btn-warning">Reset</a>
                                </div>
                            </form>
                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Image</th>
                                            <th data-priority="3">Mechant</th>
                                            <th data-priority="3">Nama Produk</th>
{{--                                            <th data-priority="1" style="text-align: right">Hpp </th>--}}
{{--                                            <th data-priority="1" style="text-align: right">Harga </th>--}}
                                            <th data-priority="3" class="text-center">Stok</th>
                                            <th data-priority="3">Status</th>
                                            <th data-priority="3">Tgl Upload</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($products) > 0)
                                            @foreach($products as $data  => $product)
                                            <tr>
                                                <td>{{ $data + 1 }}</td>
                                                <td>
                                                    @component('backend.libs.image.show', ['path' => '/images/products/', 'class' => 'd-flex mr-3  thumb-md'])
                                                        {{ $product->product_image}}
                                                    @endcomponent
                                                </td>
                                                <td>
                                                    -
                                                </td>
                                                <td>
                                                    {{ $product->product_name }} <br>
                                                    <small class="badge badge-primary">{{ @$product->category->p_cat_name }}</small>
                                                    @foreach(explode(',',$product->product_tags) as $item)
                                                        <small class="badge badge-secondary">{{ucwords(str_replace('_',' ',$item))}}</small>
                                                    @endforeach
                                                </td>
{{--                                                <td class="text-right">Rp {{ number_format($product->product_hpp) }}</td>--}}
{{--                                                <td class="text-right">Rp {{ number_format($product->product_price-$product->product_discount) }}</td>--}}
                                                <td class="text-center">{{ $product->product_stock }}</td>
                                                <td>{{ label_status($product->product_status)}}</td>
                                                <td class="text-left">{{ date_indo($product->product_create_date)}}</td>
                                                <td>
                                                    <a href="{{ route('admin.product.show', ['id' => $product->product_id])}}"><i class="mdi mdi-eye h4 text-primary"></i></a>&nbsp
                                                    <a href="{{ route('admin.product.edit', ['id' => $product->product_id]) }}"><i class="mdi mdi-pencil h4 text-primary"></i></a>&nbsp
                                                    <a href="javascript:" class="btnDelete" data-id="{{ $product->product_id }}" data-title="produk <b>{{ $product->product_name }}</b>">
                                                        <i class="mdi mdi-delete-forever h4 text-primary"></i>
                                                    </a>
                                                    {{ Form::open(['route' => ['admin.product.delete', $product->product_id], 'method' => 'post', 'id' => 'deleteData'.$product->product_id]) }}

                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" class="text-center"> <label class="text-danger">Data yang anda cari tidak di temukan !</label> </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $products->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop