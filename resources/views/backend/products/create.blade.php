@extends('backend.layouts.app')

@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <style>
        .select2-container .select2-selection--single .select2-selection__rendered{
            line-height: 22px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/css/custom.css') }}">
{{--    <link rel="stylesheet" href="{{ asset_url('/backend/assets/js/custom/select2.css') }}">--}}
    <div class="content">
        <div class="container-fluid">
                @component('backend.layouts.components.breadcrumb', ['title' => 'Tambah Produk'])
                    @slot('ul')
                        <li class="breadcrumb-item"><a href="{{ route('admin.product-list') }}">Daftar Produk</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Produk</li>
                    @endslot
                @endcomponent
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @include('backend.layouts.info')
                            {{ Form::open(['route' => 'admin.product.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                                <!-- <div class="form-group row">
                                    {{ Form::label('merchant_id', 'Merchant', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        <select name="merchant_id" class="select-address">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    {{ Form::label('product_name', 'Nama Produk *', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('product_name', '', ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar *</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input_crop', ['selector' => '.imageupload', 'name' => 'product_image'])
                                            <input type="file" class="imageupload"
                                                   accept="image/jpeg, image/jpg, image/png"
                                                   data-ratio="300/300"
                                                   data-max-file-size="2MB">
                                        @endcomponent

                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('catDesc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        @component('backend.libs.form.editor', ['name' => 'product_desc'])

                                        @endcomponent
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('Stock', 'Stok *', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::number('product_stock', '', ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('Price', 'Harga *', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp</span>
                                            {{ Form::text('product_price', '', ['class' => 'form-control hitung-hargaJual harga rupiah']) }}
                                        </div>
                                    </div>
                                </div>

                            <div class="form-group row">
                                {{ Form::label('Tags', 'Tags', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    <select name="product_tags[]" class="select2 form-control select2-multiple" multiple="multiple"
                                            data-placeholder="Choose ...">
                                        <optgroup label="Pilih Tags">
                                            <option value="minggu_ini">Menu Minggu Ini</option>
                                            <option value="promo">Menu Promo</option>
                                            <option value="bisnis">Menu Bisnis</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('product_status', 'Status', ['class' => 'col-sm-2 col-form-label']) }}
                                <div class="col-sm-10">
                                    {{ Form::select('product_status',\App\Enums\Enums::productStatus(), '',array('class' => 'form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success waves-effect waves-light" name="addProduct" value="1" type="submit">Simpan</button>
                                        <a href="{{ route('admin.product-cat') }}" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>

                            </div>
                            <!-- Modal -->
                            
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(".select2").select2();
        var _token = '{{ csrf_token() }}';
        $(document).ready(function () {
            $('.select-address').select2({
                theme: "bootstrap",
                placeholder: 'Pilih Merchant',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('ajax.get-merchant') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });

        });

    </script>

@endsection

@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@stop

@section('load-script')
    @include('backend.libs.image.image_js')
    <script src="{{ asset_url('/backend/assets/js/custom/select2.js') }}"></script>

    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>

    <script>
        $(".ingredients-save").on("click", function () {
            var file = $(".ingredients-img");
            var name = $(".ingredients-name").val();

            var reader = new FileReader();

            reader.onloadend = function() {
                var html = '<div class="col-md-2" style="margin-bottom: 10px; text-align: center;">';
                html += '   <a href="javascript:;" class="ingredients-del" style="position: absolute; top: 5px; right: 20px; color: #fff">';
                html += '       <i class="dripicons-trash"></i>';
                html += '   </a>';
                html += '   <img style="width: 100%;height: 100px;object-fit: contain" src="'+reader.result+'">';
                html += '   <span style="margin-top: 10px">'+name+'</span>';
                html += '   <input type="hidden" name="ingredients_image[]" value="'+reader.result+'">';
                html += '   <input type="hidden" name="ingredients_image_name[]" value="'+file.val()+'">';
                html += '   <input type="hidden" name="ingredients_name[]" value="'+name+'">';
                html += '</div>';

                $(".ingredients-list").append(html);

                $(".ingredients-img").val("");
                $(".ingredients-name").val("");

                $("#ingredients-add").modal('hide');
            }

            reader.readAsDataURL(file[0].files[0]);
        });

        $(document).on("click", ".ingredients-del", function() {
            $(this).parent().remove();
        });
    </script>
@stop