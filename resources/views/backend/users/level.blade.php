{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: admin--}}
 {{--* Date: 2019-01-30--}}
 {{--* Time: 23:07--}}
 {{--*/--}}

@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'User Level'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.user.level') }}">Daftar User Level</a></li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">List User Level</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="3">User Level</th>
                                            <th data-priority="1">Status</th>
                                            <th data-priority="1">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($level as $data  => $user)

                                            <tr>
                                                <th>{{$data+1}}</th>
                                                <td>{{$user->u_level_name}}</td>
                                                <td>{{label_status($user->u_level_status)}}</td>
                                                <td>
                                                    <a href="{{ route('admin.user.editlevel', ['id' => $user->u_level_id]) }}" class="text-primary"><i class="mdi mdi-pencil h4"></i></a>&nbsp
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection
