@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen User'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.user') }}">Daftar User</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Edit User</h4>

                            @include('backend.layouts.info')

                            <form action="{{ route('admin.user.update', ['id' => $user->user_id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Level</label>
                                    <div class="col-sm-10">
                                        {{ Form::select('userLevel',['' => '-- Choose --'] + \App\Enums\Enums::userLevel(), $user->u_level_id, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input', [
                                            'name'      => 'userImage',
                                            'accept'    => ['image/jpeg', 'image/png'],
                                            'src'       => (!empty($user->user_photo)) ? asset_url('/images/'.$user->user_photo) : ''
                                        ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="userName" value="{{ $user->user_name }}" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="userEmail" value="{{ $user->user_email }}" type="text"  >
                                    </div>
                                </div>
                                <div class="form-group row" id="user-password">
                                    <label class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="password" name="userPassword" style="display: none">
                                        <a href="#" class="btn btn-sm" id="ganti-password">Ganti Password</a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Telepon</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="userPhone" value="{{ $user->user_phone }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" type="text" name="userAddress" rows="6">{{ $user->user_address }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary {{ $user->user_status == 'active' ? 'active' : ' ' }}">
                                                <input type="radio" name="userStatus" id="option1" value="active" {{ $user->user_status == 'active' ? 'checked="checked"' : '' }}> Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary {{ $user->user_status == 'non-active' ? 'active' : '' }}">
                                                <input type="radio" name="userStatus" id="option2" value="non-active" {{ $user->user_status == 'non-active' ? 'checked="checked"' : ' ' }}> No-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                </div>
                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary waves-effect waves-light" name="editUser" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop