@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen User'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.user') }}">Daftar User</a></li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.user.add') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah User</a>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar User</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Gambar</th>
                                            <th data-priority="3">Nama</th>
                                            <th data-priority="6">Level</th>
                                            <th data-priority="1">Email</th>
                                            <th data-priority="3">Telepon</th>
                                            <th data-priority="6">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $data  => $user)

                                            <tr>
                                                <th>{{$data+1}}</th>
                                                <td>
                                                    @component('backend.libs.image.show', ['path' => '/images/', 'class' => 'd-flex mr-3 rounded-circle thumb-md'])
                                                        {{ $user->user_photo }}
                                                    @endcomponent
                                                </td>
                                                <td>{{$user->user_name}}</td>
                                                <td>{{$user->level->u_level_name}}</td>
                                                <td>{{$user->user_email}}</td>
                                                <td>{{$user->user_phone}}</td>
                                                <td>{{label_status($user->user_status)}}</td>
                                                <td>
                                                    <a href="#detail-{{$user->user_id}}" data-toggle="modal" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                                    <a href="{{ route('admin.user.edit', ['id' => $user->user_id]) }}" class="text-primary"><i class="mdi mdi-pencil h4"></i></a>&nbsp
                                                    <a href="" data-id="{{$user->user_id}}" data-toggle="modal" data-title="{{$user->user_name}}" class="text-primary btnDelete"><i class="mdi mdi-delete-forever h4"></i></a>
                                                    {{ Form::open(['route' => ['admin.user.hapus', $user->user_id], 'method' => 'post', 'id' => 'deleteData'.$user->user_id]) }}

                                                    {{ Form::close() }}
                                                </td>
                                            </tr>

                                            @component('backend.layouts.components.modal', [
                                                'id'    => 'detail-'.$user->user_id,
                                                'title' => 'Detail User'
                                            ])
                                                <div class="media-body">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="card directory-card">
                                                            <div class="p-4 directory-content">
                                                                <div class="float-right ">
                                                                    {{label_status($user->user_status)}}
                                                                </div>
                                                                <div class="media">
                                                                    <img class="d-flex mr-3 rounded-circle thumb-md" src="{{ asset_url('/backend/assets/images/users/avatar-6.jpg') }}" alt="user">
                                                                    <div class="media-body text-white">
                                                                        <h5 class="mt-0 font-20 mb-10">{{$user->user_name}}</h5>
                                                                        <p class="font-600"><i class="mdi mdi-email"></i> {{$user->user_email}}</p>
                                                                        <p class="font-600"><i class="mdi mdi-phone"></i> {{$user->user_phone}}</p>
                                                                        <p class="font-600"><i class="mdi mdi-home-map-marker"></i> {{$user->user_address}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <ul class="social-links text-center list-inline mb-0 p-3">
                                                                    <li class="list-inline-item">
                                                                        <h4>{{$user->level->u_level_name}}</h4>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcomponent

                                            @component('backend.layouts.components.modal', [
                                                'id'    => 'del-'.$user->user_id,
                                                'title' => 'Konfirmasi Hapus User',
                                                'route' => route('admin.user.hapus', ['user_id' => $user->user_id])
                                            ])
                                                Anda yakin ingin mneghapus user <b>{{ $user->user_name }}</b> ?

                                                <input type="hidden" name="_method" value="delete" />

                                                @slot('btn')
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Delete</button>
                                                @endslot
                                            @endcomponent

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop