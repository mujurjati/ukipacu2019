<div class="row">
    <div class="col-md-5">
        <div class="card">
            <h6 class="card-header mt-0">Tambah Atribut</h6>
            <div class="card-body">
                {{ Form::open(['route' => ['admin.product-attr.'.(!empty($dataAttr) ? 'edit' : 'add'), (!empty($dataAttr) ? $dataAttr->p_attribute_id : $data->p_cat_id)], 'method' => 'post']) }}
                {{ Form::token() }}
                    <div class="form-group row">
                        {{ Form::label('attrName', 'Nama', ['class' => 'col-sm-3 col-form-label']) }}
                        <div class="col-sm-9">
                            {{ Form::text('attrName', (!empty($dataAttr) ? $dataAttr->p_attribute_name : ''), ['class' => 'form-control', 'placeholder' => 'Nama Atribut']) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('attrType', 'Tipe', ['class' => 'col-sm-3 col-form-label']) }}
                        <div class="col-sm-9">
                            {{ Form::select('attrType', ['' => '- Pilih Tipe -', 'text' => 'Text', 'textarea' => 'Long Text', 'number' => 'Number'], (!empty($dataAttr) ? $dataAttr->p_attribute_form_type : ''), ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('attrOption', 'Option', ['class' => 'col-sm-3 col-form-label']) }}
                        <div class="col-sm-9">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="attrOption" class="custom-control-input" id="optAttr" {{ (!empty($dataAttr) && $dataAttr->p_attribute_filter == '1' ? 'checked' : '') }}>
                                {{ Form::label('optAttr', 'Option Filter', ['class' => 'custom-control-label']) }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('attrStatus', 'Status', ['class' => 'col-sm-3 col-form-label']) }}
                        <div class="col-sm-9">
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-sm btn-primary {{ (!empty($dataAttr) && $dataAttr->p_attribute_status == 'active' ? 'active' : '') }}">
                                    {{ Form::radio('attrStatus', 'active', (!empty($dataAttr) && $dataAttr->p_attribute_status == 'active')) }} Active
                                </label>
                                <label class=" btn btn-sm btn-primary {{ (!empty($dataAttr) && $dataAttr->p_attribute_status == 'non-active' ? 'active' : '') }}">
                                    {{ Form::radio('attrStatus', 'non-active', (!empty($dataAttr) && $dataAttr->p_attribute_status == 'non-active')) }} Non-Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-warning">{{ (!empty($dataAttr) ? 'Edit' : 'Tambah') }} Atribut</button>
                        @if(!empty($dataAttr))
                            <button type="button" onclick="location.href='{{ route('admin.product-cat.edit', ['attribute', $data->p_cat_id]) }}'" class="btn btn-default">
                                Batal
                            </button>
                        @endif
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>

        <div class="card">
            <h6 class="card-header mt-0">List Atribut</h6>
            <div class="card-body">
                <ul class="list-group">
                @foreach($data->attribute as $item)
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-8">
                                {{ $item->p_attribute_name }}
                            </div>
                            <div class="col-md-4 text-right">
                                {{ label_status($item->p_attribute_status) }} &nbsp;&nbsp;
                            @if($item->p_attribute_create == '0')
                                <a href="{{ route('admin.product-cat.edit', ['attribute', $item->p_cat_id, $item->p_attribute_id]) }}">
                                    <i class="dripicons-document-edit"></i>
                                </a>
                                <a href="javascript: delete_attr('{{ $item->p_attribute_id }}', '{{ $item->p_attribute_name }}');">
                                    <i class="dripicons-trash"></i>
                                </a>
                                {{ Form::open(['route' => ['admin.product-attr.delete', $item->p_attribute_id], 'method' => 'post', 'id' => 'delAttr-'.$item->p_attribute_id]) }}
                                    {{ Form::token() }}
                                    {{ Form::hidden('catId', $data->p_cat_id) }}
                                {{ Form::close() }}
                            @endif
                            </div>
                        </div>
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="card">
            <h6 class="card-header mt-0">Preview Atribut</h6>
            <div class="card-body">
            @foreach($data->attribute as $item)
                <div class="form-group row">
                    {{ Form::label('', $item->p_attribute_name, ['class' => 'col-sm-3 col-form-label']) }}
                    <div class="col-sm-9">

                    @if($item->p_attribute_form_type == 'text')
                        {{ Form::text('', '', ['class' => 'form-control']) }}
                    @elseif($item->p_attribute_form_type == 'textarea')
                        {{ Form::textarea('', '', ['class' => 'form-control']) }}
                    @elseif($item->p_attribute_form_type == 'number')
                        {{ Form::number('', '', ['class' => 'form-control']) }}
                    @endif

                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <strong>Table :</strong> <strong class="text-info"><i>_product_attr_{{ $data->p_cat_alias }}</i></strong>
                    </div>
                    <div class="col-md-12 text-center mt-3">
                    {{ Form::open(['route' => ['admin.product-attr.table', $data->p_cat_id], 'method' => 'post']) }}
                        {{ Form::token() }}
                        {{ Form::hidden('name', '_product_attr_'.$data->p_cat_alias) }}

                        @if($countCreate == 0)
                            <button class="btn btn-primary" value="1" name="createTable">
                                Create Table
                            </button>
                        @elseif($countUpdate > 0)
                            <button class="btn btn-warning" value="1" name="updateTable">
                                Update Table
                            </button>
                        @endif
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function delete_attr(id, name) {
        alertify.confirm("Anda yakin akan menghapus data atribut "+name+" ?", function() {
            $("#delAttr-"+id).submit();
        });
    }
</script>