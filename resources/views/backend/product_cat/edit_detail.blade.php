{{ Form::open(['route' => ['admin.product-cat.edit.detail', $data->p_cat_id], 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
{{ Form::token() }}

<div class="form-group row">
    {{ Form::label('gambar', 'Gambar', ['class' => 'col-sm-2 col-form-label']) }}
    <div class="col-sm-3">
        @component('backend.libs.image.input', [
            'name'      => 'gambar',
            'accept'    => ['image/jpeg', 'image/png'],
            'src'       => (!empty($data->p_cat_image)) ? asset_url('/images/product_cat/'.$data->p_cat_image) : ''
        ])
            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
        @endcomponent
    </div>
</div>
<div class="form-group row">
    {{ Form::label('nama', 'Nama', ['class' => 'col-sm-2 col-form-label']) }}
    <div class="col-sm-10">
        {{ Form::text('nama', $data->p_cat_name, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('catDesc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
    <div class="col-sm-10">
        @component('backend.libs.form.editor', ['name' => 'deskripsi'])
            {{ $data->p_cat_desc }}
        @endcomponent
    </div>
</div>
<div class="form-group row">
    {{ Form::label('status', 'Status', ['class' => 'col-sm-2 col-form-label']) }}
    <div class="col-sm-10">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-sm btn-primary {{ ($data->p_cat_status == 'active' ? 'active' : '') }}">
                {{ Form::radio('status', 'active', ($data->p_cat_status == 'active')) }} Active
            </label>
            <label class=" btn btn-sm btn-primary {{ ($data->p_cat_status == 'non-active' ? 'active' : '') }}">
                {{ Form::radio('status', 'non-active', ($data->p_cat_status == 'non-active')) }} Non-Active
            </label>
        </div>
    </div>
</div>
<div class="form-group row">
</div>
<div class="form-group row text-right">
    <div class="col-sm-12">
        <button class="btn btn-lg btn-primary waves-effect waves-light" value="1" type="submit">Simpan</button>
        <a href="{{ route('admin.product-cat') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Batal</a>
    </div>
</div>
{{ Form::close() }}