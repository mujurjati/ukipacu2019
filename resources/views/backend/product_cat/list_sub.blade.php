@php
    $class = (!empty($class)) ? $class : '';
@endphp

<ul class="{{ $class }}">
    @foreach($data as $item)
        <li class="has-children">
            @if($item->children->count() > 0)
                <input type="checkbox" name ="cat-{{ $item->p_cat_id }}" id="cat-{{ $item->p_cat_id }}" checked>
                <label for="cat-{{ $item->p_cat_id }}">
                    <div class="row">
                        <div class="col-md-8">
                            @component('backend.libs.image.show', ['path' => '/images/product_cat/'])
                                {{ $item->p_cat_image }}
                            @endcomponent
                            {{ $item->p_cat_name }}
                        </div>
                        <div class="col-md-4 text-right">
                            {{ label_status($item->p_cat_status) }} &nbsp;&nbsp;&nbsp;
                            <button class="btn btn-sm btn-primary" onclick="location.href='{{ route('admin.product-cat.edit', ['section' => 'detail', 'id' => $item->p_cat_id]) }}'">
                                <i class="dripicons-document-edit"></i>
                            </button>
                            <button class="btn btn-sm btn-warning" onclick="delete_cat('{{ $item->p_cat_id }}', '{{ $item->p_cat_level }}');">
                                <i class="dripicons-trash"></i>
                            </button>
                        </div>
                    </div>
                </label>
                @component('backend.product_cat.list_sub', ['data' => $item->children])
                @endcomponent
            @else
                <a href="#0" class="cat-list">
                    <div class="row">
                        <div class="col-md-8">
                            @component('backend.libs.image.show', ['path' => '/images/product_cat/'])
                                {{ $item->p_cat_image }}
                            @endcomponent
                            {{ $item->p_cat_name }}
                        </div>
                        <div class="col-md-4 text-right">
                            {{ label_status($item->p_cat_status) }} &nbsp;&nbsp;&nbsp;
                            <button class="btn btn-sm btn-primary" onclick="location.href='{{ route('admin.product-cat.edit', ['section' => 'detail', 'id' => $item->p_cat_id]) }}'">
                                <i class="dripicons-document-edit"></i>
                            </button>
                            <button class="btn btn-sm btn-warning" onclick="delete_cat('{{ $item->p_cat_id }}', '{{ $item->p_cat_level }}');">
                                <i class="dripicons-trash"></i>
                            </button>
                        </div>
                    </div>
                </a>
            @endif

            {{ Form::open(['route' => ['admin.product-cat.add.delete', $item->p_cat_id], 'method' => 'post', 'id' => 'delCat-'.$item->p_cat_id]) }}
                {{ Form::token() }}
            {{ Form::close() }}
        </li>
    @endforeach
</ul>

<script>
    function delete_cat(id, level) {
        if(level < 3) {
            alertify.confirm("<small>Menghapus kategori ini juga akan menghapus data sub kategorinya</small> <br><br><strong>Anda yakin akan menghapus kategori ini ?</strong>", function () {
                $("#delCat-" + id).submit();
            });
        }
        else {
            alertify.confirm("<strong>Anda yakin akan menghapus kategori ini ?</strong>", function () {
                $("#delCat-" + id).submit();
            });
        }
    }
</script>