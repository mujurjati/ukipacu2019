@extends('backend.layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/css/custom.css') }}">

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb')
                @slot('title')
                    Edit Kategori Produk
                @endslot
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.product-cat') }}">Daftar Kategori</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Kategori</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link {{ ($section == 'detail' ? 'active' : '') }}" data-toggle="tab" href="#detail" role="tab">
                                        <span class="d-none d-md-block">Detail Kategori</span>
                                        <span class="d-block d-md-none"><i class="mdi mdi-home-variant h5"></i></span>
                                    </a>
                                </li>
                            @if($data->p_cat_level == '3')
                                <li class="nav-item">
                                    <a class="nav-link {{ ($section == 'attribute' ? 'active' : '') }}" data-toggle="tab" href="#attribute" role="tab">
                                        <span class="d-none d-md-block">Atribut Kategori</span>
                                        <span class="d-block d-md-none"><i class="mdi mdi-account h5"></i></span>
                                    </a>
                                </li>
                            @endif
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane {{ ($section == 'detail' ? 'active' : '') }} p-3" id="detail" role="tabpanel">
                                    @include('backend.layouts.info')

                                    @include('backend.product_cat.edit_detail')
                                </div>
                            @if($data->p_cat_level == '3')
                                <div class="tab-pane {{ ($section == 'attribute' ? 'active' : '') }} p-3" id="attribute" role="tabpanel">
                                    @include('backend.layouts.info')

                                    @include('backend.product_cat.edit_attribute')
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection