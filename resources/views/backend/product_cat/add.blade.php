@extends('backend.layouts.app')

@section('load-css')
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/css/custom.css') }}">
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb')
                @slot('title')
                    Tambah Kategori Produk
                @endslot
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.product-cat') }}">Daftar Kategori</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tambah Kategori</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @include('backend.layouts.info')

                            {{ Form::open(['route' => 'admin.product-cat.add.save', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                                {{ Form::token() }}

                                <div class="form-group row">
                                    {{ Form::label('tipe', 'Tipe Kategori', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary active" id="selectParent">
                                                {{ Form::radio('tipe', 'parent', true) }} Kategori Utama
                                            </label>
                                            <label class=" btn btn-sm btn-primary" id="selectSub">
                                                {{ Form::radio('tipe', 'sub') }} Sub Kategori
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" id="subWrap" style="display: none">
                                    {{ Form::label('sub', 'Sub Kategori', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('', '', ['class' => 'form-control', 'disabled' => true, 'id' => 'catName']) }}

                                        {{ Form::hidden('sub', '', ['id' => 'catSub']) }}
                                        {{ Form::hidden('level', '0', ['id' => 'catLevel']) }}
                                        {{ Form::hidden('root', '', ['id' => 'catRoot']) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('gambar', 'Gambar', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input', [
                                            'name'      => 'gambar',
                                            'accept'    => ['image/jpeg', 'image/png']
                                        ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('nama', 'Nama', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('nama', '', ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('catDesc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        @component('backend.libs.form.editor', ['name' => 'deskripsi'])

                                        @endcomponent
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('status', 'status', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary active">
                                                {{ Form::radio('status', 'active', true) }} Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary">
                                                {{ Form::radio('status', 'non-active') }} Non-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                </div>
                                <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <button class="btn btn-lg btn-primary waves-effect waves-light" name="add" value="1" type="submit">Simpan</button>
                                        <a href="{{ route('admin.product-cat') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

    @component('backend.layouts.components.modal', [
        'id'    => 'showSubCat',
        'title' => 'Pilih Parent Kategori',
        'size'  => 'large'
    ])
        <div class="row">
        @foreach($data as $item)
            <div class="col-md-12 cat-level-1">
                <div class="custom-control custom-radio">
                    {{
                        Form::radio('select', $item->p_cat_id, false, [
                            'class' => 'custom-control-input sub-cat-opt',
                            'id' => 'catSelect-'.$item->p_cat_id,
                            'data-level' => $item->p_cat_level,
                            'data-root' => $item->p_cat_root,
                            'data-name' => $item->p_cat_name
                        ])
                    }}
                    {{ Form::label('catSelect-'.$item->p_cat_id, $item->p_cat_name, ['class' => 'custom-control-label']) }}
                </div>
            </div>
        @endforeach

        </div>

        @slot('btn')
            <button type="button" id="chooseSubCat" class="btn btn-primary waves-effect waves-light">Pilih</button>
        @endslot
    @endcomponent
@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $("#selectSub").on("click", function() {
                $("#showSubCat").modal('show');
            });

            $("#selectParent").on("click", function () {
                $("#catSub").val('');
                $("#catName").val('');
                $("#catRoot").val('');
                $("#catLevel").val('0');

                $("#subWrap").hide();
            });

            $("#chooseSubCat").on("click", function () {
                if($(".sub-cat-opt:checked").length) {
                    var sub     = $(".sub-cat-opt:checked").val();
                    var level   = $(".sub-cat-opt:checked").data('level');
                    var root    = $(".sub-cat-opt:checked").data('root');
                    var name    = $(".sub-cat-opt:checked").data('name');

                    $("#catSub").val(sub);
                    $("#catName").val(name);
                    $("#catRoot").val(root);
                    $("#catLevel").val(level);

                    $(".sub-cat-opt:checked").prop("checked", false);

                    $("#subWrap").show();

                    $("#showSubCat").modal('hide');
                }
                else {
                    alertify.alert("Pilih Parent Kategori terlebih dahulu");
                }
            });
        });
    </script>
@endsection