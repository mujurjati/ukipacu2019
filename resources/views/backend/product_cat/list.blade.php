@extends('backend.layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/css/collapse-list.css') }}">

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb')
                @slot('title')
                    Kategori Produk
                @endslot
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.product-cat') }}">Daftar Kategori</a></li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.product-cat.add') }}" class="btn btn-primary" ><i class="ti-plus mr-1"></i> Tambah Kategori</a>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Kategori</h4>

                            @include('backend.layouts.info')

                            @component('backend.product_cat.list_sub', ['data' => $data, 'class' => 'cd-accordion-menu animated'])
                            @endcomponent

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection