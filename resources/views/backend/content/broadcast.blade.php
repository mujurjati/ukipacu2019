@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Konten'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.content') }}">Broadcast Email</a></li>
                @endslot
                @slot('btn')
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Email Terkirim</h4>
                            <table class="table table-striped">
                                <thead>
                                    <th>No</th>
                                    <th>Pengirim</th>
                                    <th>Tanggal</th>
                                    <th>Pesan</th>
                                </thead>
                                <tbody>
                                @foreach($dataTerkirim as $key => $item)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$item->user->user_name}}</td>
                                        <td><small>{{date_indo($item->eb_create_date, 'datetime')}}</small></td>
                                        <td>
                                            <a href="#show-{{$item->eb_id}}" data-toggle="modal" class="btn btn-sm btn-purple btn-rounded" title="show">Lihat</a>
                                            @component('backend.layouts.components.modal', [
                                                'id'    => 'show-'.$item->eb_id,
                                                'title' => 'Pesan Broadcast',
                                                'size'  => 'large'
                                            ])
                                                <div class="form-group">
                                                    {{ Form::label('cat-name', 'Subject :') }}
                                                    <div>
                                                        <p>{{$item->eb_subject}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('cat-desc', 'Deskripsi :') }}
                                                    <div>
                                                        <p>{!! $item->eb_desc !!}</p>
                                                    </div>
                                                </div>
                                            @endcomponent
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- End Left sidebar --><!-- Right Sidebar -->
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            @include('backend.layouts.info')
                            {{ Form::open(['route' => 'admin.broadcast.act', 'method' => 'post']) }}
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subject" placeholder="Subject">
                                </div>
                                <div class="form-group">
                                    @component('backend.libs.form.editor', ['name' => 'deskripsi'])

                                    @endcomponent
                                </div>
                                <div class="btn-toolbar form-group m-b-0">
                                    <div class="">
                                        <button type="reset" class="btn btn-primary waves-effect waves-light m-r-5">
                                            <span>Reset</span> <i class="fa fa-refresh m-l-10"></i></button>
                                        <button type="submit" value="1" name="sendBroadcast" class="btn btn-success waves-effect waves-light">
                                            <span>Send</span> <i class="fa fa-send m-l-10"></i></button>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container-fluid -->
    </div>
    <!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop