@extends('backend.layouts.app')

@section('content')
@section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@endsection
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/css/custom.css') }}">

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb')
                @slot('title')
                    Tambah Content
                @endslot
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.content') }}">Daftar Konten</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tambah Konten</li>
                @endslot
            @endcomponent
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @include('backend.layouts.info')
                            {{ Form::open(['route' => 'admin.content.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                                <div class="form-group row">
                                    {{ Form::label('p_cat_id ', 'Kategori', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-3">
                                        {{ Form::select('c_cat_id', ['' => '-- Pilih Kategori --'] +$category,'', array('class' => 'form-control', 'id' => 'c_cat_id_parent')) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('content_name', 'Nama Konten', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('content_name', '', ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('content_type', 'Tipe Konten', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::select('content_type', ['' => '-- Pilih Tipe --'] +\App\Enums\Enums::contentType(),'', array('class' => 'form-control', 'id' => 'content_type')) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Gambar</label>
                                    <div class="col-sm-3">
                                        @component('backend.libs.image.input_crop', [
                                            'name'      => 'content_image',
                                            'accept'    => ['image/jpeg', 'image/png'],
                                            'cropWidth' => '1751',
                                            'cropHeight' => '720'
                                        ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{ Form::label('content_sortdesc', 'Deskripsi Singkat', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="content_sortdesc" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('content_desc', 'Deskripsi', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        @component('backend.libs.form.editor', ['name' => 'content_desc'])

                                        @endcomponent
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('content_tags', 'Tags', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::text('content_tags', '', ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{ Form::label('content_status', 'Konten Status', ['class' => 'col-sm-2 col-form-label']) }}
                                    <div class="col-sm-10">
                                        {{ Form::select('content_status', ['' => '-- Pilih Status --'] +\App\Enums\Enums::contentStatus(),'', array('class' => 'form-control', 'id' => 'content_status')) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                </div>
                                <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <button class="btn btn-lg btn-primary waves-effect waves-light" name="addContent" value="1" type="submit">Simpan</button>
                                        <a href="{{ route('admin.content') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                            <!-- Modal -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
    @include('backend.libs.image.image_js')
@stop