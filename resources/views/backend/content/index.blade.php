@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Konten'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.content') }}">Manajement Konten</a></li>
                @endslot
                @slot('btn')
                    <a href="{{ route('admin.content.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah Konten</a>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Konten</h4>

                            <form role="form" method="get" action="{{ route('admin.content') }}">
                                <div class="card-body row">
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" value="{{ Request::input('keyword') }}" name="keyword" placeholder="Masukkan kata kunci..">
                                    </div>
                                    <div class="col-sm-3">
                                        {{
                                            Form::select('cat', ['' => '-- Semua Kategori --']+$dataCat, Request::input('cat'), ['class' => 'form-control'])
                                        }}
                                    </div>
                                    <div class="col-sm-2">
                                        {{
                                            Form::select('status', [
                                                ''          => '-- Semua Status --',
                                                'publish'   => 'Publish',
                                                'draft'     => 'Draft'
                                                ], Request::input('status'), ['class' => 'form-control'])
                                        }}
                                    </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light mr-2">Cari</button>
                                    <a href="{{ route('admin.content') }}" name="reset"  class="btn btn-warning">Reset</a>
                                </div>
                            </form>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Gambar</th>
                                            <th data-priority="1">Judul</th>
                                            <th data-priority="1">Kategori</th>
                                            <th data-priority="3">Tgl Upload</th>
                                            <th data-priority="1">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($content as $key => $value)
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>
                                                        <img class="d-flex mr-3  thumb-md" src="{{asset_url('/images/content/'.$value->content_image)}}" alt="content">
                                                    </td>
                                                    <td>{{$value->content_name}}</td>
                                                    <td>

                                                        @if($value->category->c_cat_name=="Slider")
                                                            <span class='badge badge-purple'>{{$value->category->c_cat_name}}</span>
                                                        @else
                                                            {{$value->category->c_cat_name}}
                                                        @endif


                                                        </td>
                                                    <td><small>{{date_indo($value->content_publish_date,'datetime')}}</small></td>
                                                    <td>
                                                        {{label_status($value->content_status)}}
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('admin.content.detail', ['id' => $value->content_id])}}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                                        <a href="{{ route('admin.content.edit', ['id' => $value->content_id]) }}" class="text-primary"><i class="mdi mdi-pencil h4"></i></a>&nbsp
                                                        <a href="javascript:;" class="btnDelete text-primary" data-id="{{$value->content_id}}"><i class="mdi mdi-delete-forever h4"></i></a>
                                                        {{ Form::open(['route' => ['admin.content.delete', $value->content_id], 'method' => 'post', 'id' => 'deleteData'.$value->content_id]) }}

                                                        {{ Form::close() }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
@stop