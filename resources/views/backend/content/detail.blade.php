@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Konten'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.content') }}">Daftar Konten</a></li>
                    <li class="breadcrumb-item active">Detail Konten</li>
                @endslot
                @slot('btn')
                    
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <img class="d-flex" src="{{asset_url('/images/content/'.$content->content_image) }}" style="width: 100%;height: 100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <h4>{{$content->content_name}}</h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('content_name', 'Kategori Konten ', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {{$content->category->c_cat_name}}</p>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        {{ Form::label('user_id', 'Tipe Konten', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {{$content->content_type }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('e_package_shortdesc', 'Deskripsi Singkat', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {{$content->content_sortdesc}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('e_package_desc', 'Deskripsi', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {!! $content->content_desc !!}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('e_package_desc', 'Tags', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {!! $content->content_tags !!}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('e_package_desc', 'Hits', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {!! $content->content_hits !!}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('e_package_desc', 'Tgl Upload', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {!! date_indo($content->content_create_date,'datetime') !!}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('e_package_desc', 'Tgl Tayang', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {!! date_indo($content->content_publish_date,'datetime') !!}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('user_id', 'User Upload', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {{$content->user->user_name}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('e_package_desc', 'Status', ['class' => 'col-sm-3 col-form-label']) }}
                                        <div class="col-sm-9">
                                            <p class="col-form-label">: {!! label_status($content->content_status) !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row text-right">
                                    <div class="col-sm-12">
                                        <a href="{{ route('admin.content') }}" class="btn btn-lg btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection