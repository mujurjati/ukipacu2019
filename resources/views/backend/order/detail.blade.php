@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Order'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.order') }}">Manajemen Transaksi</a></li><li class="breadcrumb-item active" aria-current="page">Order Detail</li>
                @endslot
                @slot('btn')
                @endslot
            @endcomponent

            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="invoice-title">
                                <h4 class="pull-left font-16">
                                    <strong>Status Pesanan</strong><br>
                                    {!! order_status_label($order->order_status) !!} <br>
                                    <small style="font-size: 11px" class="text-muted">Tgl. {!! date_indo($order->{'order_'.$order->order_status.'_date'}, 'datetime') !!}</small>
                                </h4>
                                <h4 class="pull-right font-16 text-right">
                                    <small class="text-muted font-12">{{ date_indo($order->order_create_date, 'datetime') }}</small>
                                    <br>
                                    <strong class="text-primary">#{{ $order->order_number }}</strong>
                                </h4>
                                <div class="clearfix"></div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <address>
                                <strong>Data Pelanggan</strong><br>
                                <a href="{{ route('admin.customer.detail', [$order->customer_id]) }}">
                                    <strong class="text-info">
                                        {{ strtoupper($order->customer->customer_name) }}
                                    </strong>
                                    <br>
                                    <small class="text-info">({{ $order->customer->customer_email }})</small>
                                </a>
                            </address>
                            <address>
                                <strong>Metode Pembayaran</strong><br>
                                Bank Transfer <br>
                                {{ strtoupper($order->order_bank_to) }}
                            </address>
                        </div>
                        <div class="col-6 text-right">
                            <address>
                                <strong>Waktu Pembelian</strong><br>
                                {{ date_indo($order->order_create_date,"datetime") }}<br>
                            </address>
                            <address>
                                <strong>Jenis Pembelain</strong><br>
                                {{ $order->detail[0]->od_type }}<br>
                            </address>

                            <!-- <address>
                               <a class="btn btn-sm btn-dark text-white" data-toggle="modal" href="#maploc" title="Klik untuk membuka MAP">Lokasi Pengiriman</a>
                                @component('backend.layouts.components.modal', [
                                    'id'    => 'maploc',
                                    'size'  => 'large',
                                    'title' => 'Lokasi Pengiriman',

                                    ])
                                    <div id="map" style="height: 400px; width: 100%"></div>
                                @endcomponent
                            </address> -->
                            <address>
                                <strong>Notes</strong><br>
                                {{ $order->order_note }}<br>
                            </address>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card m-b-10">

                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive ">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>
                                            <strong>Produk</strong>
                                        </td>
                                        <td class="text-right">
                                            <strong>Harga</strong>
                                        </td>
                                        <td class="text-center">
                                            <strong>Qty</strong>
                                        </td>
                                        <td class="text-right">
                                            <strong>Total</strong>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($order->detail as $product)
                                        <tr>
                                            <td>
                                                {{ $product->o_detail_name }}
                                            </td>
                                            <td class="text-right">Rp {{ @price_format($product->o_detail_price) }}</td>
                                            <td class="text-center">{{ $product->o_detail_qty }}</td>
                                            <td class="text-right">Rp {{ @price_format($product->o_detail_price*$product->o_detail_qty) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" align="right">
                                                <strong class="font-16">Total</strong>
                                            </td>
                                            <td align="right">
                                                <strong class="font-18">Rp {{ price_format($order->order_total) }}</strong>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- end col -->
            <div class="card">
                <div class="card-header" >
                    <div class="row">
                        <!-- @if($order->detail[0]->od_type == 'paket')
                        <div class="col-md-12">
                            <h4 class="pull-left font-16">
                                <strong>Sub Total</strong>
                            </h4>
                            <h4 class="pull-right font-16 text-right">
                                Rp {{ price_format(125000-$order->order_total) }}
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        @endif -->
                        <!-- <div class="col-md-12">
                            <h4 class="pull-left font-16">
                                <strong>Diskon</strong>
                            </h4>
                            <h4 class="pull-right font-16 text-right">
                                Rp {{ price_format($order->order_discount_price) }}
                            </h4>
                            <div class="clearfix"></div>
                        </div> -->
                        <!-- <div class="col-md-12">
                            <h4 class="pull-left font-16">
                               <strong>Kode Unik</strong>
                            </h4>
                            <h4 class="pull-right font-16 text-right">
                                Rp {{ price_format($order->order_unique_code) }}
                            </h4>
                            <div class="clearfix"></div>
                        </div> -->
                        <div class="col-md-12">
                            <h4 class="pull-left font-16">
                                <strong>Total Pesanan</strong>
                            </h4>
                            <h4 class="pull-right font-17 text-right">
                                <strong>Rp {{ price_format($order->order_total) }}</strong>
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    @if(in_array(auth('web')->user()->u_level_id, [1]))
                        <div class="d-print-none m-t-15">
                            <div class="pull-right text-right">
                                @if($order->order_status == 'pending_payment')
                                    {{ Form::open(['route' => ['admin.order.updatestatus', $order->order_id], 'id' => 'change-cancel', 'method' => 'post']) }}
                                    {{ Form::hidden('status', 'cancel') }}
                                    {{ Form::close() }}
                                    {{ Form::open(['route' => ['admin.order.updatestatus', $order->order_id], 'id' => 'change-paid', 'method' => 'post']) }}
                                    {{ Form::hidden('status', 'paid') }}
                                    {{ Form::close() }}
                                    <button type="button" class="btn btn-primary act-status-order" data-status="cancel">Batalkan</button>
                                    <button type="button" class="btn btn-purple act-status-order" data-status="paid">Terbayar</button>
                                
                                @elseif($order->order_status == 'paid')
                                    {{ Form::open(['route' => ['admin.order.updatestatus', $order->order_id], 'id' => 'change-finish', 'method' => 'post']) }}
                                    {{ Form::hidden('status', 'finish') }}
                                    {{ Form::close() }}
                                    <button type="button" class="btn btn-success act-status-order" data-status="finish">Selesaikan Pesanan</button>
                                @endif
                            </div>
                        </div>
                </div>
                @endif
            </div>
        </div>
    </div>


@if(in_array(auth('web')->user()->u_level_id, [1]))
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(function () {
            $('.act-status-order').on("click", function () {
                var status = $(this).data('status');

                if(status == 'paid') {
                    var textConfirm = 'Anda yakin akan menandai pesanan ini SUDAH TERBAYAR ?';
                }
                else if(status == 'process') {
                    var textConfirm = 'Anda yakin akan menandai pesanan ini SEDANG DIPROSES ?';

                    var emptyDriver = false;

                    $(document).find('.select-driver').each(function(){
                        if($(this).val() == '')
                            emptyDriver = true;
                    });

                    if(emptyDriver) {
                        alertify.alert("Anda belum menentukan driver untuk mengirim pesanan ini ?");

                        return false;
                    }
                }
                else if(status == 'shipment') {
                    var textConfirm = 'Anda yakin akan menandai pesanan ini MENUNGGU PENGIRIMAN ?';
                }
                else if(status == 'shipping') {
                    var resi = $("#change-"+status+" .order-resi").val();

                    if(resi == '') {
                        alertify.alert("Anda wajib mengisikan NOMOR RESI terlebih dahulu ?");

                        return false;
                    }

                    var textConfirm = 'Anda yakin akan menandai pesanan ini DALAM PENGIRIMAN ?';
                }
                else if(status == 'received') {
                    var textConfirm = 'Anda yakin akan menandai pesanan ini TELAH DITERIMA ?';
                }
                else if(status == 'finish') {
                    var textConfirm = 'Anda yakin akan menandai pesanan ini TELAH SELESAI ?';
                }
                else if(status == 'cancel') {
                    var textConfirm = 'Anda yakin akan menandai pesanan ini TELAH DIBATALKAN ?';
                }

                alertify.confirm(textConfirm, function() {
                    $("#change-"+status).submit();
                });
            });

            $(".select-driver").on("change", function() {
                var o_detail_id = $(this).data('id');
                var driver_id   = $(this).val();

                $.ajax({
                    url: '{{ route('admin.order.updateDriver') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: { o_detail_id: o_detail_id, driver_id: driver_id },
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            swal("Berhasil !", res.message, "success");

                            $("#status-"+o_detail_id).html('{!! order_status_label("process") !!}')
                        }
                        else {
                            swal("Gagal !", res.message, "error");
                        }
                    }
                });
            });
        });
    </script>
@endif
    <script>
        function initMap() {
            var lat = '{{ $order->order_shipment_lat }}';
            var lng = '{{ $order->order_shipment_lng }}';

            var uluru = {lat: parseFloat(lat), lng: parseFloat(lng)};
            // var uluru = {lat: -25.344, lng: 131.036};

            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 18, center: uluru});

            var marker = new google.maps.Marker({position: uluru, map: map});
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}&callback=initMap">
    </script>
@endsection