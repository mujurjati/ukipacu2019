@extends('backend.layouts.app')
@section('content')
    <link href="{{ asset_url('/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/css/select2-bootstrap.min.css') }}" rel="stylesheet" />
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Manajemen Transaksi'])
                @slot('ul')
                    <li class="breadcrumb-item active">Manajemen Transaksi</li>
                @endslot
                @slot('btn')
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Transaksi</h4>
                            <div class="col-sm-3">
                                <button type="submit" id="hideshow" class="btn btn-warning waves-effect waves-light mr-2">Filter</button>
                            </div>
                            <form role="form" method="get" action="{{ route('admin.order') }}" id="content">
                                <div class="card-body row">
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" value="{{ Request::input('keyword') }}" name="keyword" placeholder="Masukkan kata kunci..">
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control"
                                                   name="filter_date"
                                                   value="{{ Request::input('filter_date') }}"
                                                   placeholder="Tanggal Pengiriman"
                                                   id="datepicker-autoclose">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="mdi mdi-calendar"></i>
                                                </span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>

                                    <div class="col-sm-2">
                                        {{
                                            Form::select('status', [
                                                ''                  => '-- Semua Status --',
                                                'pending_payment'   => 'Menunggu Pembayaran',
                                                'paid'              => 'Terbayar',
                                                'process'           => 'Sedang Diproses',
                                                'ready_shipment'    => 'Menunggu Pengiriman',
                                                'shipping_process'  => 'Dalam Pengiriman',
                                                'received'          => 'Terkirim',
                                                'complaint'         => 'Proses Komplain',
                                                'finish'            => 'Selesai',
                                                'cancel'            => 'Sudah Dibatalkan',
                                                'refund'            => 'Sudah Dibatalkan (Refund)'
                                            ], Request::input('status'), ['class' => 'form-control'])
                                        }}
                                    </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light mr-2">Cari</button>
                                    <a href="{{route('admin.order')}}" name="reset"  class="btn btn-warning">Reset</a>
                                </div>
                            </form>
                            <form role="form" method="get" action="{{ route('admin.order.add') }}">
                                <div class="card-body row">
                                    <div class="col-sm-3">
                                        {{
                                            Form::select('status', [
                                                ''                  => '- Tipe Transaksi -',
                                                'paket'   => 'Paket',
                                                'eceran'  => 'Eceran'
                                            ], Request::input('status'), ['class' => 'form-control tipe-trx'])
                                        }}
                                    </div>

                                    <div class="col-sm-3 member" style="display:none">
                                        <select name="customer_id" class="select-customer form-control" data-live-search="true" data-size="10">
                                            <option value="">Pilih Member</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-3 paket" style="display:none">
                                        <select name="paket_id" class="select-paket form-control" data-live-search="true" data-size="10" >
                                            <option value="">Pilih Paket</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="simpan btn btn-success waves-effect waves-light mr-2 text-right">Simpan</button>
                                </div> 
                                <div class="card-body row table-produk" style="display:none">
                                    <div class="col-sm-3 p-2">
                                        <button  type="button" id="btn_add" class="btn btn-info waves-effect waves-light mr-2 text-right">Tambah</button>
                                    </div>
                                    <div class="col-sm-12">
                                        <table id="tbldet" class="table table-striped table-hover" style="width:100%;margin:0 auto;">
                                            <thead>
                                                <tr>
                                                <th>Nama Barang</th>
                                                <th>Harga</th>
                                                <th>Qty</th>
                                                <th>Stok</th>
                                                <th>Total</th>
                                                <th style="width:50px">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tb">
                                            </tbody>
                                        </table>   
                                    </div>                            
                                </div>                               
                            </form>
                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th data-priority="1">Tanggal</th>
                                            <th data-priority="2">Nomor</th>
                                            <th data-priority="3">Pelanggan</th>
                                            <th data-priority="4" style="text-align: right">Tipe Transaksi</th>
                                            <th data-priority="5" style="text-align: right">Total Transaksi</th>
                                            <th data-priority="6" style="text-align: center">Status</th>
                                            <th data-priority="7" style="text-align: center">Detail</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($order) > 0)
                                            @foreach($order as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ date_indo($value->order_create_date, 'datetime') }}</td>
                                                <td>
                                                    <a href="{{ route('admin.order.detail', ['id' => $value->order_id])}}">
                                                        <span class="text-primary">#{{ $value->order_number }}</span>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.customer.detail', [$value->customer_id]) }}" target="_blank">
                                                        {{ strtoupper(@$value->customer->customer_name) }}
                                                    </a>
                                                </td>
                                                <td text-align="right">{{ $value->detailIndex->od_type }}</td>
                                                
                                                <td  text-align="right">Rp {{ price_format($value->order_total) }}</td>
                                                <td  text-align="center">{!! order_status_label($value->order_status)  !!}</td>
                                                <td  text-align="center">
                                                    <a href="{{ route('admin.order.detail', ['id' => $value->order_id])}}"><i class="mdi mdi-eye h4 text-primary"></i></a>&nbsp
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="9" class="text-center"> <label class="text-danger">Data yang anda cari tidak di temukan !</label> </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $order->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection

@section('load-script')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
    <script src="{{ asset_url('/backend/assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset_url('/js/select2.min.js') }}"></script>
    <script>
        var _token = '{{ csrf_token() }}';
    </script>
    <script >
        $("#datepicker-autoclose").datepicker({
            autoclose: !0,
            todayHighlight: !0,
            format: "dd-mm-yyyy",
        });
        $(document).ready(function(){
            $('#content').toggle('hide');
            $('#hideshow').on('click', function(event) {        
                $('#content').toggle('show');
            });
            var count_click = 0;
            $("#btn_add").on("click",function(){
                count_click +=1;
                $.ajax({
                    type:'POST',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    url:'{{ route('admin.order.addRow') }}',
                    data:{'count':count_click},
                    success: function(data){
                        $('#tb').append(data);
                        $('.selectpicker-pro-'+count_click).select2().on('change', function (e) {
                                var me = $(this);
                                var harga   = me.select2().find(":selected").data("harga");
                                var stok    = me.select2().find(":selected").data("stok");
                                $('.harga-'+count_click).html(rupiah(harga));
                                $('.stok-'+count_click).html(stok);
                                $('#qty-'+count_click).prop("disabled", false);
                        });
                    }
                });                
            });

            $('.simpan').attr('disabled', true);
            $('.select-customer').on('change', function () {
                var paket = $('.select-paket').val();
                if ($(this).val() != '' && paket != '') {
                    $('.simpan').attr('disabled', false);
                } else {
                    $('.simpan').attr('disabled', true);
                }
            });

            $('.select-paket').on('change', function () {
                var cus = $('.select-customer').val();
                if ($(this).val() != '' && cus != '') {
                    $('.simpan').attr('disabled', false);
                } else {
                    $('.simpan').attr('disabled', true);
                }
            });
        });

        function qtyFunction(x) {
            var qty = document.getElementById("qty-"+x).value;
            var stokinit = document.getElementById("stok-"+x).textContent;
            var stok = parseInt(stokinit);
            var hargaInit = document.getElementById("harga-"+x).textContent;
            var matches = hargaInit.match(/\d+/g);
            var harga = parseInt(matches[0]+matches[1]);
            if(qty > stok) {
                $("#qty-"+x).val(stok);
                var total = stok*harga;
                $('#total-'+x).val(rupiah(total));
            } else {
                $("#qty-"+x).val(qty);
                var total = qty*harga;
                $('#total-'+x).val(rupiah(total));
            }
            
        }

        function rupiah(param) {
            var	reverse = param.toString().split('').reverse().join(''),
                ribuan 	= reverse.match(/\d{1,3}/g);
                ribuan	= ribuan.join('.').split('').reverse().join('');

            return ribuan;
        }

        $(".tipe-trx").on("change", function () {
            if($(this).val() == 'eceran') {
                $('.member').css('display','block');
                $('.table-produk').css('display','block');
                $('.paket').css('display','none');	
            }
            else if($(this).val() == 'paket') {
                $('.member').css('display','block');	
                $('.paket').css('display','block');	
                $('.table-produk').css('display','none');
            } 
        });

        $(document).ready(function () {
            $('.select-customer').select2({
                // theme: "bootstrap",
                // placeholder: 'Pilih Member',
                // minimumInputLength: 3,
                ajax: {
                    url: '{{ route('admin.order.pilihMember') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });
        });

        // $(document).ready(function () {
        //     $('.select-eceran').select2({
        //         theme: "bootstrap",
        //         placeholder: 'Pilih Produk',
        //         minimumInputLength: 3,
        //         multiple: true,
        //         ajax: {
        //             url: '{{ route('admin.order.setType-eceran') }}',
        //             type: 'post',
        //             headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        //             dataType: 'json',
        //             processResults: function (data, params) {
        //                 params.page = params.page || 1;

        //                 return {
        //                     results: data.data,
        //                     pagination: {
        //                         more: (params.page * 10) < data.total
        //                     }
        //                 };
        //             }
        //         }
        //     });
        // });
        $(document).ready(function () {
            $('.select-paket').select2({
                // theme: "bootstrap",
                // placeholder: 'Pilih Paket',
                // minimumInputLength: 3,
                ajax: {
                    url: '{{ route('admin.order.setType-paket') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });
        });
    </script>
@stop