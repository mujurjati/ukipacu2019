<script src="{{ asset_url('/backend/assets/js/ckeditor/ckeditor.js') }}"></script>

{{ Form::textarea($name, $slot, ['class' => 'tinymce']) }}

<script>
    $(function () {
        var dd = 1;

        $(".tinymce").each(function() {
            $(this).removeAttr("id");
            $(this).attr("id", "editor"+dd);

            CKEDITOR.replace('editor'+dd, {
                'extraPlugins': 'imgbrowse',
                'filebrowserImageBrowseUrl': '{{ asset_url('/backend/assets/js/ckeditor/plugins/imgbrowse/imgbrowse.html') }}',
                'filebrowserImageUploadUrl': '{{ asset_url('/backend/assets/js/ckeditor/plugins/imgupload/imgupload.php') }}',
            });

            dd = dd+1;
        });
    });
</script>
