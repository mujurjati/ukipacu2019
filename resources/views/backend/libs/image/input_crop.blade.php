@php
    $selector   = (isset($selector) && !empty($selector)) ? $selector : 'inputFile';
    $src        = (isset($src) && !empty($src)) ? $src : '';
    $asset      = (isset($asset)) ? $asset : true;

    if(!empty($src)) {
        $filename   = explode('/', $src);
        $filename   = end($filename);
        $ext        = explode('.', $src);
        $ext        = end($ext);
    }
    else {
        $filename   = '';
        $ext        = '';
    }
@endphp

@if($asset)
    <link href="{{ asset_url('/backend/assets/js/filepond/css/filepond.css') }}" rel="stylesheet">
    <link href="{{ asset_url('/backend/assets/js/filepond/css/filepond-plugin-image-preview.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-image-edit/dist/filepond-plugin-image-edit.css" rel="stylesheet">
    <link href="{{ asset_url('/backend/assets/js/cropper/cropper.css') }}" rel="stylesheet">

    <style>
        .copper-container {
            /* Never limit the container height here */
            max-width: 100%;
        }

        .copper-container img {
            /* This is important */
            width: 100%;
        }
    </style>
@endif

<div id="pond-wrap">
    {{ $slot }}
</div>

@component('backend.layouts.components.modal', [
   'id'     => 'cropper',
   'title'  => 'Crop Image',
   'size'   => 'large',
   'close'  => false,
   'btn'    => '<button type="button" class="btn btn-danger file-crop">Crop</button>'
])
    <div class="copper-container"></div>
    <input type="hidden" name="{{ $name }}" class="filepond-input">
@endcomponent

    @section('image-crop-script')
    @if($asset)
        <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-file-encode.js') }}"></script>
        <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-file-validate-size.js') }}"></script>
        <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-file-validate-type.js') }}"></script>
        <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-image-exif-orientation.js') }}"></script>
        <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-image-preview.min.js') }}"></script>
        <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-image-transform.js') }}"></script>
        <script src="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.js"></script>
        <script src="https://unpkg.com/filepond-plugin-image-edit/dist/filepond-plugin-image-edit.js"></script>
        <script src="{{ asset_url('/backend/assets/js/filepond/filepond.min.js') }}"></script>
        <script src="{{ asset_url('/backend/assets/js/filepond/filepond.jquery.js') }}"></script>
        <script src="{{ asset_url('/backend/assets/js/cropper/cropper.js') }}"></script>

    @endif

    <script type="text/javascript">
        $(function(){
            var $selector   = '{{ $selector }}';

            var ratio = $($selector).data('ratio');

            if(typeof ratio !== typeof undefined && ratio !== false) {
                var ratioSp = ratio.split('/');

                var $ratio      = eval(ratioSp[0])/eval(ratioSp[1]);
                var $ratioInv   = eval(ratioSp[1])/eval(ratioSp[0]);
            }
            else {
                var $ratio      = 1;
                var $ratioInv   = 1;
            }

            $.fn.filepond.registerPlugin(
                FilePondPluginFileEncode,
                FilePondPluginFileValidateSize,
                FilePondPluginFileValidateType,
                FilePondPluginImageExifOrientation,
                FilePondPluginImagePreview,
                FilePondPluginImageTransform,
                FilePondPluginFilePoster,
                FilePondPluginImageEdit
            );

            var pond = FilePond.create(
                document.querySelector($selector),
                {
                    allowReplace: true,
                    instantUpload: false,
                    imageEditInstantEdit: true,
                    imageEditEditor: {
                        open: function(file) {
                            var reader = new FileReader();

                            reader.onloadend = function() {
                                $(".copper-container").html('<img src="'+reader.result+'" id="cropper-img"/>');

                                $("#cropper").modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });

                                $('#cropper').on('shown.bs.modal', function () {
                                    $("#cropper-img").cropper({ aspectRatio: $ratio });
                                }).on('hidden.bs.modal', function () {
                                    $("#cropper-img").data('cropper').destroy();
                                    $(".copper-container").html('');
                                });

                                $(".file-crop").on("click", function () {
                                    pond.setOptions({
                                        stylePanelAspectRatio: $ratioInv,
                                        styleItemPanelAspectRatio: $ratioInv,
                                        files: [
                                            {
                                                options: {
                                                    file: {
                                                        name: file.name
                                                    },
                                                    metadata: {
                                                        poster: $("#cropper-img").cropper('getCroppedCanvas').toDataURL()
                                                    }
                                                }
                                            }
                                        ]
                                    });

                                    $('.filepond-input').val($("#cropper-img").cropper('getCroppedCanvas').toDataURL());

                                    $("#cropper").modal('hide');
                                });
                            };

                            reader.readAsDataURL(file);
                        }
                    },
                }
            );

            @if(!empty($src))
            pond.setOptions({
                stylePanelAspectRatio: $ratioInv,
                styleItemPanelAspectRatio: $ratioInv,
                files: [
                    {
                        options: {
                            file: {
                                name: '{{ $filename }}'
                            },
                            metadata: {
                                poster: '{{ $src }}'
                            }
                        }
                    }
                ]
            });
            @endif
        });
    </script>
@endsection