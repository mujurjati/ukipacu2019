@php
    $class  = (!empty($class)) ? $class : '';
    $type   = (!empty($type)) ? $type : 'black';
@endphp

<img src="{{ asset_url($path.$slot) }}" class="{{ $class }}"/>