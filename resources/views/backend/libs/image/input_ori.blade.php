@php
    $selector   = (isset($selector) && !empty($selector)) ? $selector : 'inputFile';
    $name       = (isset($name) && !empty($name)) ? $name : 'fileimage[]';
    $accept     = (isset($accept) && !empty($accept)) ? implode(',', $accept) : 'image/*';
    $max_size   = (isset($max_size) && !empty($max_size)) ? $max_size : '2MB';
    $src        = (isset($src) && !empty($src)) ? $src : '';
    $asset      = (isset($asset)) ? $asset : true;
    $ratio      = (isset($ratio) && !empty($ratio)) ? $ratio : '8:5';

    if(!empty($src)) {
        $filename   = explode('/', $src);
        $filename   = end($filename);
        $ext        = explode('.', $src);
        $ext        = end($ext);
    }
    else {
        $filename   = '';
        $ext        = '';
    }
@endphp

@if($asset)
    <link href="{{ asset_url('/backend/assets/js/filepond/css/filepond.css') }}" rel="stylesheet">
    <link href="{{ asset_url('/backend/assets/js/filepond/css/filepond-plugin-image-preview.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css" rel="stylesheet">
@endif

<input type="file" name="{{$name}}" class="{{$selector}}" accept="{{$accept}}">

@if($asset)
    <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-file-encode.js') }}"></script>
    <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-file-validate-size.js') }}"></script>
    <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-file-validate-type.js') }}"></script>
    <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-image-exif-orientation.js') }}"></script>
    <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-image-preview.min.js') }}"></script>
    <script src="{{ asset_url('/backend/assets/js/filepond/dist/filepond-plugin-image-transform.js') }}"></script>
    <script src="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.js"></script>
    <script src="{{ asset_url('/backend/assets/js/filepond/filepond.min.js') }}"></script>
    <script src="{{ asset_url('/backend/assets/js/filepond/filepond.jquery.js') }}"></script>
@endif

<script type="text/javascript">
    $(function(){
        @if($asset)
            $.fn.filepond.registerPlugin(
                FilePondPluginFileEncode,
                FilePondPluginFileValidateSize,
                FilePondPluginFileValidateType,
                FilePondPluginImageExifOrientation,
                FilePondPluginImagePreview,
                FilePondPluginImageTransform,
                FilePondPluginFilePoster
            );
        @endif

        var pondConfig = {
            instantUpload: false,
            labelIdle: '{{ $slot }}',
            maxFileSize: '{{ $max_size }}'
        };

        var pond = FilePond.create(
            document.querySelector('.{{$selector}}'),
            pondConfig
        );

        @if(!empty($src))
            pond.setOptions({
                stylePanelAspectRatio: '{{ $ratio }}',
                styleItemPanelAspectRatio: '{{ $ratio }}',
                files: [
                    {
                        options: {
                            file: {
                                name: '{{ $filename }}'
                            },
                            metadata: {
                                poster: '{{ $src }}'
                            }
                        }
                    }
                ]
            });
        @endif
    });
</script>