@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        @component('backend.layouts.components.breadcrumb', ['title' => 'reward'])
                            @slot('ul')
                                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                            @endslot
                            @slot('ul')
                                <li class="breadcrumb-item active"><a href="{{ route('admin.reward') }}">Daftar Reward</a></li>
                            @endslot
                            @slot('btn')
                                <a href="{{ route('admin.reward.create') }}" class="btn btn-success btn-rounded" ><i class="ti-plus mr-1"></i> Tambah</a>
                            @endslot
                        @endcomponent
                    </div>
                </div>
            </div><!-- end row -->
            @include('backend.layouts.info')
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title">Daftar Reward </h4>
                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Posisi</th>
                                            <th data-priority="3">Member/Kons</th>
                                            <th data-priority="1">Reward/Hadiah</th>
                                            <th data-priority="3">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($rewards) > 0)
                                            @foreach($rewards as $key  => $reward)
                                                <tr>
                                                    <th>{{$key+1}}</th>
                                                    <td>
                                                        {{$reward->reward_position}}
                                                    </td>
                                                    <td>{{$reward->reward_member}}</td>
                                                    <td>{{$reward->reward}}</td>
                                                    <td>{{label_status($reward->reward_status)}}</td>
                                                    <td>
                                                        <a href="{{ route('admin.reward.edit', ['id' => $reward->reward_id]) }}"><i class="mdi mdi-pencil h4 text-primary"></i></a>&nbsp
                                                        <a href="{{ route('admin.reward.detail', ['id' => $reward->reward_id]) }}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" class="text-center"> <label class="text-danger">Data yang anda cari tidak di temukan !</label> </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $rewards->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection
