@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'reward'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.merchant') }}">Daftar Reward</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tambah Baru</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 mb-10 header-title">Tambah Reward</h4>

                            @include('backend.layouts.info')

                            <form style="margin-top: 10px" action="{{ route('admin.reward.create.save') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Posisi</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="reward_position" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Member / Kons</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="reward_member" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Reward</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="reward" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary active">
                                                <input type="radio" name="reward_status" id="option1" value="active" checked="checked"> Active
                                            </label>
                                            <label class=" btn btn-sm btn-warning">
                                                <input type="radio" name="reward_status" id="option2" value="draft"> Draft
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12" style="margin-top: 20px">
                                        <button class="btn btn-primary waves-effect waves-light" name="addReward" value="1" type="submit">Simpan</button>
                                        <a href="{{ route('admin.reward') }}" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection