@extends('backend.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'reward'])
                @slot('ul')
                    <li class="breadcrumb-item "><a href="{{ route('admin.reward') }}">Daftar Reward</a></li>
                    <li class="breadcrumb-item active">Detail Reward</li>
                @endslot
            @endcomponent
            @include('backend.layouts.info')
            <div class="row">
                <div class="col-lg-12 center">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title">Detail</h4>
                            <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th data-priority="1">Posisi</th>
                                            <th data-priority="3">Member/Kons</th>
                                            <th data-priority="1">Reward/Hadiah</th>
                                            <th data-priority="3">Status</th>
                                            <th data-priority="6">Tanggal Upload</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    {{$reward->reward_position}}
                                                </td>
                                                <td>{{$reward->reward_member}}</td>
                                                <td>{{$reward->reward}}</td>
                                                <td>{{label_status($reward->reward_status)}}</td>
                                                <td>
                                                {{date_indo($reward->reward_created_date, 'datetime')}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 center">
                    <div class="card">
                    <div class="card-body"><h4 class="mt-0 header-title">Daftar Member Penerima </h4>
                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Member</th>
                                            <th data-priority="3">Member/Kons</th>
                                            <th data-priority="6">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($rewardHistory) > 0)
                                            @foreach($rewardHistory as $key  => $cs)
                                                <tr>
                                                    <th>{{$key+1}}</th>
                                                    <td>
                                                        {{$cs->rewardHistory->customer_name}}
                                                    </td>
                                                    <td>{{$cs->rewardHistory->customer_count}}</td>
                                                    <td>{{label_status($cs->rh_status)}}</td>
                                                    <td>
                                                    @if($cs->rh_status == 'pending')
                                                    <a href="" data-target="#confirm-{{$cs->rh_id}}" data-toggle="modal"  class="text-success"> <i class="mdi mdi-pencil h4"></i></a>

                                                        @component('backend.layouts.components.modal', [
                                                           'id'    => 'confirm-'.$cs->rh_id,
                                                           'title' => 'Konfirmasi Approved',
                                                           'route' => route('admin.reward.approve', ['id' => $cs->rh_id, 'param'=>$reward->reward_id])
                                                       ])
                                                            Anda yakin ingin approved customer <b>{{ $cs->rewardHistory->customer_name }}</b> ?
                                                            @slot('btn')
                                                                <button type="submit" name="updateopen" value="1"  class="btn btn-sm btn-primary waves-effect">Konfirmasi</button>
                                                            @endslot
                                                        @endcomponent
                                                    @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" class="text-center"> <label class="text-danger">Data penerima reward tidak di temukan !</label> </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- content -->
    </div>
@endsection

