@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8"><h4 class="page-title mb-0">Kontak Tamu</h4>
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item active"><a href="{{ route('admin.contact') }}">Daftar Kontak Tamu</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title">Daftar Kontak Tamu </h4>
                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Tanggal</th>
                                            <th data-priority="3">Nama</th>
                                            <th data-priority="1">Email</th>
                                            <th data-priority="3">Subjek</th>
                                            <th data-priority="6">Pesan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($contact) > 0)
                                            @foreach($contact as $key  => $contacts)
                                                <tr>
                                                    <th>{{$key+1}}</th>
                                                    <td><small>{{date_indo($contacts->guest_created_date,'datetime') }}</small></td>
                                                    <td>{{ $contacts->guest_name }}</td>
                                                    <td>{{ $contacts->guest_email }}</td>
                                                    <td>{{ $contacts->guest_subject }}</td>
                                                    <td>
                                                        <a href="" data-target="#show-{{$contacts->guest_id}}" data-toggle="modal"  class="text-primary"> <i class="dripicons-mail h4"></i></a>
                                                    </td>
                                                </tr>

                                                @component('backend.layouts.components.modal', [
                                                   'id'    => 'show-'.$contacts->guest_id,
                                                   'title' => 'Pesan',

                                               ])
                                                    {{ $contacts->guest_message }}
                                                    @slot('btn')

                                                    @endslot
                                                @endcomponent

                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" class="text-center"> <label class="text-danger">Data Kontak Tamu Kosong !</label> </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <nav class="toolbox toolbox-pagination mt-10">
                                        {{ $contact->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection