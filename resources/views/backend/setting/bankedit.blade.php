
{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: admin--}}
 {{--* Date: 2019-01-29--}}
 {{--* Time: 23:57--}}
 {{--*/--}}

@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Bank'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.setting.bank') }}">Daftar Bank</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Bank</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Edit Bank</h4>

                            @include('backend.layouts.info')

                            <form action="{{ route('admin.setting.bankupdate', ['id' => $bank->bank_id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Nama Bank</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" readonly name="bankName" value="{{ $bank->bank_name }}" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-2 col-form-label">Nama Akun</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="bankAccount" value="{{ $bank->bank_account }}" type="text"  >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nomor Rekening</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="bankNumber" value="{{ $bank->bank_number }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Cabang</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="bankBranch" value="{{ $bank->bank_branch }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary {{ $bank->bank_status == 'active' ? 'active' : '' }}">
                                                <input type="radio" name="bankStatus" id="option1" value="active" {{ $bank->bank_status == 'active' ? 'checked="checked"' : '' }}> Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary {{ $bank->bank_status == 'non-active' ? 'active' : '' }}">
                                                <input type="radio" name="bankStatus" id="option2" value="non-active" {{ $bank->bank_status == 'non-active' ? 'checked="checked"' : '' }}> No-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                </div>
                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary waves-effect waves-light" name="editbank" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection
