
{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: admin--}}
 {{--* Date: 2019-01-29--}}
 {{--* Time: 23:19--}}
 {{--*/--}}

@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Bank'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.setting.bank') }}">Daftar Bank</a></li>
                @endslot

            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">List Bank</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="1">Logo</th>
                                            <th data-priority="1">Nama Bank</th>
                                            <th data-priority="3">Akun</th>
                                            <th data-priority="6">No Rekning</th>
                                            <th data-priority="6">Cabang</th>
                                            <th data-priority="6">Status</th>
                                            <th data-priority="6">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($bank as $key  => $data)
                                            <tr>
                                                <th>{{$key+1}}</th>
                                                <td><img class="d-flex mr-3" src="{{get_image('bank/'.$data->bank_image)}}" alt="bank" style="width: 50px"></td>
                                                <td>{{$data->bank_name}}</td>
                                                <td>{{$data->bank_account}}</td>
                                                <td>{{$data->bank_number}}</td>
                                                <td>{{$data->bank_branch}}</td>
                                                <td>{{label_status($data->bank_status)}}</td>
                                                <td>
                                                    <a href="{{ route('admin.setting.bankdetail', ['id' => $data->bank_id]) }}" class="text-primary"><i class="mdi mdi-eye h4"></i></a>&nbsp
                                                    <a href="{{ route('admin.setting.bankedit', ['id' => $data->bank_id]) }}" class="text-primary"><i class="mdi mdi-pencil h4"></i></a>&nbsp
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->
@endsection