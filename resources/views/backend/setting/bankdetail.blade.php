
{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: admin--}}
 {{--* Date: 2019-01-29--}}
 {{--* Time: 23:42--}}
 {{--*/--}}

@extends('backend.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Bank'])
                @slot('ul')
                    <li class="breadcrumb-item "><a href="{{ route('admin.setting.bank') }}">Daftar Bank</a></li>
                    <li class="breadcrumb-item active">Detail Bank</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-lg-12 center">
                    <div class="card">
                        <div class="card-body"><h4 class="mt-0 header-title"></h4>
                            <div class="row">
                                <div class="col-2">
                                    <img class="img-fluid d-block mt-1 center" src="{{get_image('bank/'.$bank->bank_image)}}" alt="logo" width="145">
                                </div>
                                <div class="col-10">
                                    <h5 class="mt-0 font-14 m-b-15 text-muted">{{$bank->bank_name}}</h5>
                                    <p class="mt-0 font-14 m-b-15 text-muted">{{$bank->bank_account}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{$bank->bank_number}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{$bank->bank_branch}}</p>
                                    <p class="mt-2 mb-0 font-14 text-muted">{{label_status($bank->bank_status)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- container-fluid -->
        </div><!-- content -->
@endsection