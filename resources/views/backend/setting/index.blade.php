@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Pengaturan Umum'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.setting') }}">Pengaturan Umum</a>
                    </li>
                @endslot
            @endcomponent
            @include('backend.layouts.info')
            <form action="{{ route('admin.setting.update') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-xl-4 col-md-6">
                    <div class="card messages">
                        <div class="card-body"><h4 class="mt-0 header-title">Umum</h4>
                            <nav class="mt-4">
                                <div class="nav nav-tabs latest-messages-tabs nav-justified" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-first-tab" data-toggle="tab"
                                            href="#nav-first"
                                            role="tab" aria-controls="nav-first" aria-selected="true">
                                        <p class="text-muted mb-0">Nama Website</p>
                                        <input class="form-control" value="{{$setting['General']['web_name']}}" type="text" name="form[web_name]">
                                    </a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane show active" id="nav-first" role="tabpanel"
                                     aria-labelledby="nav-first-tab">
                                    <div class="p-2 mt-2">
                                        <ul class="list-unstyled latest-message-list mb-0">
                                            <li class="message-list-item">
                                                <div class="form-group"><label>Email</label>
                                                    <input type="text" class="form-control" value="{{$setting['General']['email']}}" name="form[email]" required="" placeholder="Email" >
                                                </div>
                                            </li>
                                            <li class="message-list-item">
                                                <div class="form-group"><label>Telepon</label>
                                                    <input type="text" class="form-control" name="form[phone]" value="{{$setting['General']['phone']}}" required="" placeholder="Telepon" >
                                                </div>
                                            </li>
                                            <li class="message-list-item">
                                                <div class="form-group"><label>Alamat</label>
                                                    <textarea type="text" rows="7" class="form-control" name="form[address]">{{$setting['General']['address']}}</textarea>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-8 col-md-6">
                    <div class="card messages">
                        <div class="card-body"><h4 class="mt-0 header-title">Sosial Media</h4>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane show active" id="nav-first" role="tabpanel"
                                         aria-labelledby="nav-first-tab">
                                        <div class="p-2 mt-2">
                                            <ul class="list-unstyled latest-message-list mb-0">
                                                <div class="form-group"><label>Playstore URL</label>
                                                    <input value="{{$setting['General']['google_play_url']}}" type="text" class="form-control" name="form[google_play_url]" required="" placeholder="Playstore URL" >
                                                </div>
                                                <div class="form-group"><label>Facebook</label>
                                                    <input value="{{$setting['General']['facebook_id']}}" type="text" class="form-control" name="form[facebook_id]" required="" placeholder="Facebook" >
                                                </div>
                                                <div class="form-group"><label>Facebook URL</label>
                                                    <input value="{{$setting['General']['facebook_url']}}" type="text" class="form-control" name="form[facebook_url]" required="" placeholder="Facebook URL" >
                                                </div>
                                                <div class="form-group"><label>Instagram</label>
                                                    <input value="{{$setting['General']['instagram_id']}}" type="text" class="form-control" name="form[instagram_id]" required="" placeholder="Instagram" >
                                                </div>
                                                <div class="form-group"><label>Instagram URL</label>
                                                    <input  value="{{$setting['General']['instagram_url']}}"type="text" class="form-control" name="form[instagram_url]" required="" placeholder="Instagram URL" >
                                                </div>
                                                <div class="form-group"><label>Twitter</label>
                                                    <input  value="{{$setting['General']['twitter_id']}}"type="text" class="form-control" name="form[twitter_id]" required="" placeholder="Twitter" >
                                                </div>
                                                <div class="form-group"><label>Twitter URL</label>
                                                    <input  value="{{$setting['General']['twitter_url']}}" type="text" class="form-control" name="form[twitter_url]" required="" placeholder="Twitter URL" >
                                                </div>

                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-xl-12 col-md-6 text-center">
                    <div class="card messages">
                        <button type="submit" name="updateSetting" value="1" class="btn btn-success waves-effect waves-light">Simpan</button>
                    </div>
                </div>
            </div>

            </form>
        </div>
    </div>

@endsection