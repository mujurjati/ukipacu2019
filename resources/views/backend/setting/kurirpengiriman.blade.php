{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: admin--}}
{{--* Date: 2019-01-30--}}
{{--* Time: 23:07--}}
{{--*/--}}

@extends('backend.layouts.app')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Kurir Pengiriman'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="{{ route('admin.user.level') }}">Daftar Kurir Pengiriman</a></li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Daftar Kurir Pengiriman</h4>

                            @include('backend.layouts.info')

                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0 b-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th data-priority="3">Kode Kurir</th>
                                            <th data-priority="1">Nama Kurir</th>
                                            <th data-priority="1">Status</th>
                                            <th data-priority="1">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($kurir as $data  => $key)

                                            <tr>
                                                <th>{{$data+1}}</th>
                                                <td>{{$key->s_courier_code}}</td>
                                                <td>{{$key->s_courier_name}}</td>
                                                <td>{{label_status($key->s_courier_status)}}</td>
                                                <td>
                                                    <a href="{{ route('admin.setting.editkurir', ['id' => $key->s_courier_id]) }}" class="text-primary"><i class="mdi mdi-pencil h4"></i></a>&nbsp
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection
