@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Kebijakan Privasi'])
                @slot('ul')
                    <li class="breadcrumb-item active"><a href="#">Kebijakan Privasi</a>
                    </li>
                @endslot
            @endcomponent
            @include('backend.layouts.info')
            <form action="{{ route('admin.setting.kebijakanprivasiUpdate') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xl-12 col-md-6">
                        <div class="card messages">
                            <div class="card-body"><h4 class="mt-0 header-title">Kebijakan Privasi</h4>
                                <nav class="mt-4">
                                    <div class="nav nav-tabs latest-messages-tabs nav-justified" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-first-tab" data-toggle="tab"
                                           href="#nav-first"
                                           role="tab" aria-controls="nav-first" aria-selected="true">
                                            @component('backend.libs.form.editor', ['name' => 'form[kebijakan_privasi]'])
                                                {{$setting['General']['kebijakan_privasi']}}
                                            @endcomponent

                                        </a>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-6 text-center">
                        <div class="card messages">
                            <button type="submit" name="kebijakanprivasiUpdate" value="1" class="btn btn-success waves-effect waves-light">Simpan</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection