@extends('backend.layouts.app')

@section('content')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <style>
        .select2-container .select2-selection--single .select2-selection__rendered{
            line-height: 22px;
        }
    </style>
<div class="content">
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset_url('/backend/assets/map/jquery-gmaps-latlon-picker.css') }}">
    <div class="container-fluid">
        @component('backend.layouts.components.breadcrumb', ['title' => ' Pengiriman'])
        @slot('ul')
        <li class="breadcrumb-item active"><a href="#"> Pengiriman</a>
        </li>
        @endslot
        @endcomponent
        @include('backend.layouts.info')
        <form action="{{ route('admin.setting.alamatpengirimanUpdate') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-xl-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2">Alamat</label>
                                <div class="col-sm-10">
                                    <select name="form[alamat_pengiriman]" class="select-address">
                                        <option value="{{ $setting['General']['alamat_pengiriman'] }}">{{
                                            $alamat->subdistrict->s_subdistrict_name.', '.
                                            $alamat->subdistrict->city->s_city_name.', '.
                                            $alamat->subdistrict->city->province->s_province_name.', '.
                                            $alamat->subdistrict->city->s_city_postcode
                                        }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2">Biaya Kirim</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp</span>
{{--                                        {{ Form::text('form[biaya_kirim]', price_format($setting['General']['biaya_kirim']), ['class' => 'form-control harga rupiah']) }}--}}
                                        {{ Form::text('form[biaya_kirim]', $setting['General']['biaya_kirim'], ['class' => 'form-control harga rupiah']) }}
                                        <span class="input-group-addon">KM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Pilih lokasi kantor</label>
                            </div>
                            <fieldset class="gllpLatlonPicker">
                                <div class="gllpMap">Google Maps</div>
                                <input name="form[alamat_lat]" type="hidden" class="gllpLatitude" value="{{ $setting['General']['alamat_lat'] }}"/>
                                <input name="form[alamat_lng]" type="hidden" class="gllpLongitude"  value="{{ $setting['General']['alamat_lng'] }}"/>
                                <input type="hidden" class="gllpZoom" value="14"/>
                            </fieldset>
                        </div>
                    </div>
                </div>


                <div class="col-xl-12 col-md-6 text-center">
                    <div class="card messages">
                        <button type="submit" name="alamatpengirimanUpdate" value="1" class="btn btn-success waves-effect waves-light">Simpan</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    var _token = '{{ csrf_token() }}';
    $(document).ready(function () {
        $('.select-address').select2({
            theme: "bootstrap",
            dropdownAutoWidth: true,
            minimumInputLength: 3,
            ajax: {
                url: '{{ route('ajax.get-address') }}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                dataType: 'json',
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data,
                        pagination: {
                            more: (params.page * 10) < data.total
                        }
                    };
                }
            }
        });
    });
</script>
@endsection
@section('load-css')
@include('backend.libs.image.multiple.multiple_css')
@stop

@section('load-script')
    @include('backend.libs.image.image_js')
    <script src="{{ asset_url('/backend/assets/js/custom/custom.js') }}"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}&callback=initMap">
    </script>
    <script src="{{ asset_url('/backend/assets/map/jquery-gmaps-latlon-picker.js') }}"></script>
    {{--    <script src="{{ asset_url('/backend/assets/map/jquery-2.1.1.min.js') }}"></script>--}}
    <script>
        $(document).ready(function() {
            // Copy the init code from "jquery-gmaps-latlon-picker.js" and extend it here
            $(".gllpLatlonPicker").each(function() {
                $obj = $(document).gMapsLatLonPicker();

                $obj.params.strings.markerText = "Drag this Marker (example edit)";

                $obj.params.displayError = function(message) {
                    console.log("MAPS ERROR: " + message); // instead of alert()
                };

                $obj.init( $(this) );
            });
        });
    </script>
@stop