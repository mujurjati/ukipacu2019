@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            @component('backend.layouts.components.breadcrumb', ['title' => 'Kurir Pengiriman'])
                @slot('ul')
                    <li class="breadcrumb-item"><a href="{{ route('admin.setting.kurirpengiriman') }}">Daftar Kurir</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Kurir</li>
                @endslot
            @endcomponent

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Edit Kurir</h4>

                            @include('backend.layouts.info')

                            <form action="{{ route('admin.setting.updatekurir', ['id' => $kurir->s_courier_id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group row" style="padding-top: 20px;padding-bottom: 20px">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10" >
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-sm btn-primary {{ $kurir->s_courier_status == 'active' ? 'active' : ' ' }}">
                                                <input type="radio" name="kurirStatus" id="option1" value="active" {{ $kurir->s_courier_status == 'active' ? 'checked="checked' : '' }}"> Active
                                            </label>
                                            <label class=" btn btn-sm btn-primary {{ $kurir->s_courier_status == 'non-active' ? 'active' : ' ' }}">
                                                <input type="radio" name="kurirStatus" id="option2" value="non-active" {{ $kurir->s_courier_status == 'non-active' ? 'checked="checked"' : ' ' }}> No-Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row right" style="text-align: center">
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary waves-effect waves-light" name="editKurir" value="1" type="submit">Simpan</button>
                                        <a href="" class="btn btn-secondary waves-effect waves-light">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container-fluid -->
    </div><!-- content -->

@endsection