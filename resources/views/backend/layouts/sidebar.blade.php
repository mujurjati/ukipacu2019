<!-- Top Bar End --><!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll"><!--- Sidemenu -->
        <div id="sidebar-menu"><!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Main</li>
                <li>
                    <a href="{{ route('admin.home') }}" class="waves-effect">
                        <i class="dripicons-meter"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.contact') }}" class="waves-effect">
                        <i class="dripicons-mail"></i>
                        <span>Kontak Tamu</span>
                    </a>
                </li>

            @if(in_array(auth('web')->user()->u_level_id, [1,2]))
                
                <li>
                    <a href="{{ route('admin.customer') }}" class="waves-effect">
                        <i class="dripicons-user"></i>
                        <span>Member</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.merchant') }}" class="waves-effect">
                        <i class="dripicons-store"></i>
                        <span>Marketing</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.reward') }}" class="waves-effect">
                        <i class="dripicons-rocket"></i>
                        <span>Reward</span>
                    </a>
                </li>
            @endif

            @if(in_array(auth('web')->user()->u_level_id, [1,2]))
                <li class="menu-title">Management Produk</li>
                    <li>
                        <a href="javascript:void(0);" class="waves-effect">
                            <i class="dripicons-basket"></i>
                            <span>
                                Produk
                                <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span>
                            </span>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ route('admin.product-cat') }}">Kategori Produk</a></li>
                            <li><a href="{{ route('admin.product-list') }}">Daftar Produk</a></li>
                            <li><a href="{{ route('admin.product.create') }}">Upload Produk</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.order') }}" class="waves-effect">
                            <i class="dripicons-cart"></i>
                            <span>Transaksi </span>
                        </a>
                        <!-- <a href="{{ route('admin.promo') }}" class="waves-effect">
                            <i class="ti-settings"></i>
                            <span>Promo</span>
                        </a> -->
                    </li>
                    <li>
                    <a href="{{ route('admin.paket') }}" class="waves-effect">
                        <i class="dripicons-checklist"></i>
                        <span>Paket</span>
                    </a>
                    </li>
            @endif

                <!-- <li class="menu-title">Management Konten</li>
                <li>
                    <a href="{{ route('admin.content.broadcast') }}" class="waves-effect">
                        <i class="ti-announcement"></i>
                        <span>Broadcast Email</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.content.cat') }}" class="waves-effect">
                        <i class="dripicons-checklist"></i>
                        <span>Kategori Konten</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.content') }}" class="waves-effect">
                        <i class="dripicons-blog"></i>
                        <span>Konten & Slider</span>
                    </a>
                </li>
                    <li>
                    <a href="{{ route('admin.video') }}" class="waves-effect">
                        <i class="dripicons-media-play"></i>
                        <span>Konten Video</span>
                    </a>
                </li> -->

                <li class="menu-title">Pengaturan</li>
                <li>
                    <a href="{{ route('admin.setting') }}" class="waves-effect">
                        <i class="ti-settings"></i>
                        <span>Umum</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="dripicons-user-group"></i>
                        <span>
                            User
                            <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span>
                        </span>
                    </a>
                    <ul class="submenu">
                        <li><a href="{{ route('admin.user.level') }}" class="waves-effect"><span>User Level</span></a></li>
                        <li><a href="{{ route('admin.user') }}" class="waves-effect"><span>User</span></a></li>
                    </ul>
                </li>
                <!-- <li>
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="ti-settings"></i>
                        <span>
                            Halaman
                            <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span>
                        </span>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('admin.setting.about') }}" class="waves-effect">
                                <span>Tentang Kami</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.setting.terms') }}" class="waves-effect">
                                <span>Syarat & Ketentuan</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.setting.kebijakanprivasi') }}" class="waves-effect">
                                <span>Kebijakan Privasi</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.setting.panduankeamanan') }}" class="waves-effect">
                                <span>Panduan Keamanan</span>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <li class="menu-title">Laporan</li>
                <li>
                    <a href="{{ route('admin.report') }}" class="waves-effect">
                        <i class="dripicons-blog"></i>
                        <span>Laporan</span>
                    </a>
                </li>

            </ul>
        </div><!-- Sidebar -->
        <div class="clearfix"></div>
    </div><!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->
