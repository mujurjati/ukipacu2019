@if($errors->any())
    <div class="alert alert-danger" role="alert"> <i class="ti-user"></i>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif

@if(Session::has('success'))
    <div class="alert alert-success">
        {{ \Illuminate\Support\Facades\Session::get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif