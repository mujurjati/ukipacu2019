<!DOCTYPE html>
<html lang="en">
<head>
    @php
        $check = '0';
        if(isset($multiple)){
            $check = $multiple;
        }
    @endphp

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Ukipacu - Admin Panel</title>
    <meta content="Admin Dashboard" name="description">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="Themesbrand" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- App Icons -->
    <link rel="shortcut icon" href="{{ asset_url('/frontend/images/favicon.png') }}">
    <!--Morris Chart CSS -->
    <link href="{{ asset_url('/backend/assets/js/alertify/alertify.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/backend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/backend/assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/backend/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/backend/assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/backend/assets/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    @yield('load-css')

    @if($check == '0')
        <script src="{{ asset_url('/backend/assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset_url('/backend/assets/js/bootstrap.bundle.min.js') }}"></script>
    @endif
</head>
<body class="fixed-left"><!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner"></div>
    </div>
</div><!-- Begin page -->
<div id="wrapper"><!-- Top Bar Start -->
    <div class="topbar"><!-- LOGO -->
        <div class="topbar-left">
            <a href="{{ route('admin.home') }}" class="logo">
                <img src="{{ asset_url('/frontend/images/logoheader.png') }}" alt="" height="50" class="logo-large">
                <img src="{{ asset_url('/frontend/images/favicon.png') }}" alt="" height="40" class="logo-sm">
            </a>
        </div>

        {{--HEADER--}}
        @include('backend.layouts.header')
        {{--END HEADER--}}
    </div>
    {{--SIDEBAR--}}
        @include('backend.layouts.sidebar')
    {{--END SIDEBAR--}}
    <!-- ============================================================== --><!-- Start right Content here -->
    <!-- ============================================================== -->
    <!-- Start content -->
    <div class="content-page">
        @yield('content');
        <!-- content -->
        <footer class="footer">©  2020 All Rights Reserved <span class="d-none d-sm-inline-block">- PT. Ukipacu Indonesia</span></footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
</div>
<!-- END wrapper --><!-- jQuery  -->

@if($check == '1')
    <script src="{{ asset_url('/backend/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset_url('/backend/assets/js/bootstrap.bundle.min.js') }}"></script>
@endif
<script type="text/javascript">
        var base_url = '{{ url('/') }}';
    </script>
<script src="{{ asset_url('/backend/assets/js/modernizr.min.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/metisMenu.min.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/waves.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/alertify/alertify.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/jquery.maskMoney.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/bootstrap-filestyle.min.js') }}"></script>

@yield('image-crop-script')
@yield('load-script')

<!-- App js -->
<script src="{{ asset_url('/backend/assets/js/app.js') }}"></script>
</body>
</html>