<nav class="navbar-custom"><!-- Search input -->
    <div class="search-wrap" id="search-wrap">
        <div class="search-bar"><input class="search-input" type="search" placeholder="Search">
            <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                <i class="mdi mdi-close-circle"></i>
            </a>
        </div>
    </div>
    <ul class="navbar-right d-flex list-inline float-right mb-0">
        <li class="list-inline-item dropdown notification-list">
            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user"
               data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{ asset_url('/backend/assets/images/users/avatar-6.jpg') }}" alt="user" class="rounded-circle">
                <span class="d-none d-md-inline-block ml-1">
                    {{Auth::user()->user_name}}
                    <i class="mdi mdi-chevron-down"></i>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">

                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="dripicons-exit text-muted"></i> Keluar
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </li>
    </ul>
    <ul class="list-inline menu-left mb-0">
        <li class="float-left">
            <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>
        </li>
    </ul>
</nav>
<script src="{{ asset_url('/js/app.js') }}"></script>