<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Ukipacu - Login</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->
    <link rel="shortcut icon" href="{{ asset_url('/frontend/images/favicon.png') }}"><!-- Basic Css files -->
    <link href="{{ asset_url('/backend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/backend/assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/backend/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/backend/assets/css/style.css') }}" rel="stylesheet" type="text/css">
</head>
<body class="fixed-left"><!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner"></div>
    </div>
</div><!-- Begin page -->
<div class="accountbg"></div>
<div class="wrapper-page">
    <div class="card">
        @yield('content')
    </div>
</div><!-- end wrapper-page --><!-- jQuery  -->
<script src="{{ asset_url('/backend/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/modernizr.min.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/metisMenu.min.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset_url('/backend/assets/js/waves.js') }}"></script><!-- App js -->
<script src="{{ asset_url('/backend/assets/js/app.js') }}"></script>
</body>
</html>