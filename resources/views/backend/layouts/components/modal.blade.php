@php
    if(!isset($size))
        $modalsize = '';
    elseif($size == 'large')
        $modalsize = 'modal-lg';
    elseif($size == 'small')
        $modalsize = 'modal-sm';
    else
        $modalsize = '';

    if(!isset($close))
        $btnClose = true;
    else
        $btnClose = $close;

    if(!isset($center))
        $modalcenter = '';
    elseif($size == 'true')
        $modalcenter = 'modal-dialog-centered';
    else
        $modalcenter = '';
@endphp

<div id="{{ $id }}" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    @isset($route)
        {{--class="remove-record-model"--}}
        <form method="post"  action="{{ $route }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            @endisset
            <div class="modal-dialog {{ $modalsize }} {{ $modalcenter }}">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myModalLabel"><i></i>{{ $title }}</h5>
                        @if($btnClose)
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        @endif
                    </div>
                    <div class="modal-body">
                        {{ $slot }}
                    </div>
                    <div class="modal-footer">
                        @isset($btn)
                            {!! $btn !!}
                        @endisset
                        @if($btnClose)
                            <button type="button" class="btn btn-sm btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        @endif
                    </div>
                </div>
            </div>

            @isset($route)
        </form>
    @endisset

</div>