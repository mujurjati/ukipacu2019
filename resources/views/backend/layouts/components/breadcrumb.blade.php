<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-0">{{ $title }}</h4>
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                    @isset($ul)
                        {{ $ul }}
                    @endisset
                    </ol>
                </div>
                <div class="col-md-4">
                    <div class="float-right d-none d-md-block">
                    @isset($btn)
                        {{ $btn }}
                    @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- end row -->