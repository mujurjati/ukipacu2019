<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div style='padding: 15px; border-bottom: 3px solid #5cb85c;'>
    <img src='{{ asset_url('frontend/images/logo.png') }}' style='height: 50px;' />
</div>
<div style='padding: 15px;'>
    <p>{!! $desc !!}</p>
</div>
</body>
</html>