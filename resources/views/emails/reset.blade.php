<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div style='padding: 15px; border-bottom: 3px solid #5cb85c;'>
    <img src='{{ asset_url('frontend/images/logo.png') }}' style='height: 50px;' />
</div>
<div style='padding: 15px;'>
    <p>Hai {{ $data->customer_name }},</p>
    <p>Anda telah melakukan reset password.</p>
    <p>Silahkan konfirmasi reset password sebelum <b>{{ $expired  }}</b></p>
    <p>Berikut Detail Akun Anda :</p>

    <div style='padding:10px; background: #eeeeee'>
        <table border='0'>
            <tr><td>Nama</td><td>: {{  $data->customer_name }}</td></tr>
            <tr><td>Email</td><td>: {{ $data->customer_email }}</td></tr>
        </table>
    </div>

    <p>Konfirmasi reset password Anda di sini :</p>
    <p>
        <a href='{{ $link }}' style='padding: 10px 20px; background: #5cb85c;color: #ffffff;text-decoration: none'>
            RESET PASSWORD
        </a>
    </p>
    <p>Apabila tombol tidak berfungsi, Anda dapat mengakses link berikut :</p>
    <p>{{ $link }}</p>
</div>

</body>
</html>