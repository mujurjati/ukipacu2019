<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div style='padding: 15px; border-bottom: 3px solid #8cc63e;'>
    <img src='' style='height: 50px;' />
</div>
<div style='padding: 15px;'>
    <p>Hai {{ strtoupper(Auth::guard('member')->user()->customer_name) }},</p>
    <p>Kami telah menerima langganan Ebook anda.</p>
    <p>Berikut Detail Langganan Ebook :</p>

    <div style='padding:10px; background: #eeeeee'>
        <table border='0'>
            <tr><td>Nama</td><td>: {{  strtoupper(Auth::guard('member')->user()->customer_name) }}</td></tr>
            <tr><td>Email</td><td>: {{ strtoupper(Auth::guard('member')->user()->customer_email) }}</td></tr>
            <tr><td>Paket Ebook</td><td>: {{session()->get('dataEbook')->e_package_name}}</td></tr>
            <tr><td>Tipe Paket</td><td>: {{session()->get('dataEbook')->e_package_type}}</td></tr>
            <tr><td>Jumlah Tagihan</td><td>: Rp {{ price_format($data->e_subscribe_price) }}</td></tr>
            <tr><td>Status Transaksi</td><td>: Menunggu Pembayaran</td></tr>
        </table>
    </div>
</div>

<div style='padding: 15px;'>
    <p>Lakukan pembayaran sebelum <strong>{{ date('g:i A, d F Y', strtotime($expDate)) }}</strong> ke rekening berikut</p>
    <div style='padding:10px; background: #eeeeee'>
        <table border='0'>
            <tr><td>Bank</td><td>: {{ $data->e_bank_to }}</td></tr>
            <tr><td>Nomor Rekening</td><td>: {{ $data->e_bank_to_number }}</td></tr>
            <tr><td>Nama Rekening</td><td>: {{ $data->e_bank_to_account }}</td></tr>
        </table>
    </div>
</div>

</body>
</html>