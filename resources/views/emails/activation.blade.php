<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div style='padding: 15px; border-bottom: 3px solid #5cb85c;'>
    <img src='{{ asset_url('frontend/images/logo.png') }}' style='height: 50px;' />
</div>
<div style='padding: 15px;'>
    <p>Hai {{ $name }},</p>
    <p>Kami telah menerima pendaftaran akun Anda.</p>
    <p>Berikut Detail Akun Anda :</p>

    <div style='padding:10px; background: #eeeeee'>
        <table border='0'>
            <tr><td>Nama</td><td>: {{  $name }}</td></tr>
            <tr><td>Tipe</td><td>: {{  $type }}</td></tr>
            <tr><td>Email</td><td>: {{ $email }}</td></tr>
        </table>
    </div>

    <p>Segera konfirmasi email Anda di sini :</p>
    <p>
        <a href='{{ $link }}' style='padding: 10px 20px; background: #5cb85c;color: #ffffff;text-decoration: none'>
            KONFIRMASI EMAIL
        </a>
    </p>
    <p>Apabila tombol tidak berfungsi, Anda dapat mengakses link berikut :</p>
    <p>{{ $link }}</p>
</div>

</body>
</html>