<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div style='padding: 15px; border-bottom: 3px solid #5cb85c;'>
    <img src='{{ asset_url('frontend/images/logo.png') }}' style='height: 50px;' />
</div>
<div style='padding: 15px;'>
    <p>Hai {{ strtoupper($customer->customer_name) }},</p>

    @if($status == 'confirm')
        <p>Kami telah menerima transaksi Anda dengan nomor tagihan <strong>#{{ $data->order_number }}</strong>.</p>
    @elseif($status == 'paid')
        <p>Pembayaran transaksi Anda dengan nomor tagihan <strong>#{{ $data->order_number }}</strong> telah berhasil.</p>
    @elseif($status == 'cancel')
        <p>Sayang sekali, transaksi Anda dengan nomor tagihan <strong>#{{ $data->order_number }}</strong> telah dibatalkan.</p>
    @elseif($status == 'process')
        <p>Transaksi Anda dengan nomor <strong>#{{ $data->order_number }}</strong> telah diproses. Barang Anda sedang dalam proses packing.</p>
    @elseif($status == 'ready_shipment')
        <p>Transaksi Anda dengan nomor <strong>#{{ $data->order_number }}</strong> siap untuk dikirim.</p>
    @elseif($status == 'shipping_process')
        <p>Transaksi Anda dengan nomor <strong>#{{ $data->order_number }}</strong> sedang dalam proses pengiriman.</p>
    @endif

    <p>Berikut Detail Transaksi Anda :</p>

    <div style='padding:10px; background: #eeeeee'>
        <table style="width: 100%">
            <tr style="background: #f0f0f0">
                <td><strong>Produk</strong></td>
                <td><strong>Kuantitas</strong></td>
                <td style="text-align: right"><strong>Subtotal</strong></td>
            </tr>
            @foreach($data->detail as $item)
                <tr>
                    <td>{{ $item->o_detail_product_name }}</td>
                    <td align="center">x {{ $item->o_detail_qty }}</td>
                    <td width="170" align="right">
                        Rp {{ price_format($item->o_detail_subtotal) }}
                    </td>
                </tr>
            @endforeach
        </table>
        <table style="width: 100%">
            <tr>
                <td align="right">Total Barang</td>
                <td align="right" width="200">Rp {{ price_format($data->order_subtotal) }}</td>
            </tr>
            <tr>
                <td align="right">Kode Unik</td>
                <td align="right" width="200">Rp {{ price_format($data->order_unique_code) }}</td>
            </tr>
            <tr>
                <td align="right">Diskon Voucher</td>
                <td align="right" width="200">Rp {{ price_format($data->order_discount_price) }}</td>
            </tr>
            <tr>
                <td align="right">Biaya Pengiriman</td>
                <td align="right" width="200">Rp {{ price_format($data->order_shipment_price) }}</td>
            </tr>
            <tr>
                <td align="right">Pajak</td>
                <td align="right" width="200">Rp {{ price_format($data->order_tax) }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom: 1px solid #ddd"></td>
            </tr>
            <tr>
                <td align="right"><strong>TOTAL</strong></td>
                <td align="right" width="200"><strong>Rp {{ price_format($data->order_total) }}</strong></td>
            </tr>
        </table>
    </div>
</div>

@if($status == 'confirm')
    <div style='padding: 15px;'>
        <p>Lakukan pembayaran sebelum <strong>{{ date('g:i A, d F Y', strtotime($exp_date)) }}</strong> ke rekening berikut :</p>
        <div style='padding:10px; background: #eeeeee'>
            <table border='0'>
                <tr><td>Bank</td><td>: {{ $data->order_bank_to }}</td></tr>
                <tr><td>Nomor Rekening</td><td>: {{ $data->order_bank_to_number }}</td></tr>
                <tr><td>Nama Rekening</td><td>: {{ $data->order_bank_to_account }}</td></tr>
            </table>
        </div>
        <p>Pastikan untuk melakukan pembayaran sesuai dengan nominal tagihan sampai tiga digit terakhir.</p>
    </div>
@elseif($status == 'shipping_process')
    <div style='padding: 15px;'>
        <p>Berikut informasi pengiriman transaksi Anda :</p>
        <div style='padding:10px; background: #eeeeee'>
            <table border='0'>
                <tr>
                    <td>Jasa Pengiriman</td>
                    <td>: {{ $data->courier->s_courier_name }} &mdash; {{ $data->order_shipment_package }}</td>
                </tr>
                <tr>
                    <td>Nomor Resi</td>
                    <td>: {{ $data->order_shipment_resi }}</td>
                </tr>
            </table>
        </div>
    </div>
@endif

</body>
</html>