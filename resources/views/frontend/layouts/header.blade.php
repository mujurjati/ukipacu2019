<header id="header">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="{{url('/')}}" class="standard-logo" data-dark-logo="{{asset_url('/frontend/images/logo-dark.png')}}">
                    <img src="{{asset_url('/frontend/images/logoheader.png')}}" alt="Canvas Logo" width="140">
                </a>
                <!-- <a href="{{url('/')}}" class="retina-logo" data-dark-logo="{{asset_url('/frontend/images/logo-dark@2x.png')}}">
                    <img src="{{asset_url('/frontend/images/logo@2x.png')}}" alt="Canvas Logo">
                </a> -->
            </div><!-- #logo end -->

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu">

                <ul>
                    <li><a href="{{ url('/') }}"><div>Home</div></a>
                    </li>
                    <!-- <li class="sub-menu"><a href="#" class="sf-with-ul"><div>Menu <i class="icon-chevron-down"></i></div></a>
                        <ul style="display: none;">
                            <li class=""><a href="{{route('product.product_list')}}"><div>Menu hari ini</div></a></li>
                            <li class=""><a href="{{route('product.product_list')}}/promo"><div>Menu Promo</div></a></li>
                            <li class=""><a href="{{route('product.product_list')}}/bisnis"><div>Menu Bisnis</div></a></li>
                        </ul>
                    </li> -->
                    </li>
                    <li><a href="{{route('product.product_list')}}"><div>Paket</div></a>
                    </li>
                    <li><a href="{{route('blog.list')}}"><div>Blog</div></a>
                    </li>
                    <li><a href="{{route('about.about')}}">Tentang Kami</a></li>
                    <li><a href="{{route('contact.contact')}}">Kontak</a></li>

                    @if(auth('member')->check())
                        <li class="sub-menu"><a href="#" class="sf-with-ul">
                                <div>Hi,{{Auth::guard('member')->user()->customer_name}}</div>
                            </a>
                            <ul style="display: none;">
                                <li class=""><a href="{{route('customers.profil')}}">
                                        <div>Profile</div>
                                    </a></li>
                                <li class=""><a href="#">
                                        <div>My Orders</div>
                                    </a></li>
                                <li class=""><a href="{{ route('customers.logout') }}">
                                        <div>Log Out</div>
                                    </a></li>
                            </ul>
                        </li>
                    @else
                        <li><a href="{{route('customers.register')}}">
                                <div class="button button-small button-circle"
                                     style="margin-top: -10px; margin-bottom:-10px; background: #5cb85c;">Masuk / Daftar
                                </div>
                            </a>
                        </li>
                    @endif

                </ul>



                <!-- Top Cart
                ============================================= -->
                <!-- <div id="top-cart">
                    <a href="#" id="top-cart-trigger">
                        <i class="icon-inbox" style="font-size: 20px"></i>
                        <span id="headCartCount">{{ Cart::getContent()->count() }}</span>
                    </a>
                    <div class="top-cart-content">
                        <div class="top-cart-title">
                            <h4>hari ini {{date("Y/m/d")}}</h4>
                        </div>
                        <div class="top-cart-items" id="headCartItem">
                            @if(!Cart::isEmpty())
                                @foreach(Cart::getContent() as $item)
                                    <div class="top-cart-item clearfix head-cart" data-id="{{ $item->id }}">
                                        <div class="top-cart-item-image">
                                            <a href="#"><img src="{{ get_image_product($item->attributes->image) }}" alt="{{ $item->name }}"/></a>
                                        </div>
                                        <div class="top-cart-item-desc">
                                            <a href="#">{{ $item->name }}</a>
                                            <div class="quantity clearfix" style="margin-top: 10px;">
                                                <input type="button" value="-" class="head-minus">
                                                <input readonly type="text" step="1" min="1" name="quantity" value="{{ $item->quantity }}" title="Qty" class="qty" size="4">
                                                <input type="button" value="+" class="head-plus">
                                            </div>
                                            <span class="top-cart-item-price">Rp {{ price_format($item->price) }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="top-cart-action clearfix">
                            <span class="fleft">
                                <li><strong> TOTAL</strong></li>
                            </span>
                            <span class="fright">
                                <li><strong id="headCartTotal" class="cart-total">Rp {{ Cart::getTotal() }}</strong></li>
                            </span>

                            <a href="{{ route('customer.cart') }}" {{ (!Cart::isEmpty() ? '' : 'style="display:none"') }} id="headCartBtn" class="button button-circle nomargin btn-block">Go to Checkout</a>
                        </div>
                    </div>
                </div> -->
                <!-- #top-cart end -->

            </nav><!-- #primary-menu end -->

        </div>

    </div>
    <!-- Modal -->
    <div class="modal1 mfp-hide" id="myModal1">
        <div class="block divcenter" style="background-color: #FFF; max-width: 500px; border-radius: 20px;">
            <div class="center" style="padding: 30px;">
                <form role="form" method="POST" action="{{ route('customers.login.act') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1"><span class="c-title">Sign In or Register</span></label>
                        <div class="input-group mb-2">
                            <input type="text" name="login_email" class="form-control brd-orange border-cst"
                                   id="inlineFormInputGroup" placeholder="Email">
                        </div>
                        <div class="input-group mb-2">
                            <input type="password" name="login_password" class="form-control brd-orange border-cst"
                                   id="inlineFormInputGroup" placeholder="Password">
                        </div>
                        <a href="#myModal2" data-lightbox="inline"
                           style="margin-left: 10px;">Lupa Password <i class="icon-line-arrow-right"></i></a>
                    </div>
                    <button type="submit" class="button button-small button-circle"
                            style="margin-top: -10px; margin-bottom:-10px; background: #5cb85c;">Sign In
                    </button>
                    <div class="form-group mt-5">
                        <h5>Atau</h5>
                        <a href="{{route('customers.register')}}" class="button btn-lg btn-block button-circle bg-green">Daftar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal 2 -->
    <div class="modal2 mfp-hide" id="myModal2">
        <div class="block divcenter" style="background-color: #FFF; max-width: 500px; border-radius: 20px;">
            <button href="#myModal1" data-lightbox="inline" class="btn btn-link" style="margin-left: 10px;"><i
                        class="icon-line-arrow-left"></i>Back
            </button>
            <div class="center" style="padding: 30px;">
                {{ Form::open(['route' => 'customers.resetPass', 'method' => 'post']) }}
                {{ Form::token() }}
                <div class="form-group">
                    <label for="exampleInputEmail1"><span class="c-title">Reset Password</span></label>
                    <h5>Tambahkan email untuk proses reset password</h5>
                    <div class="input-group">
                        <input type="email" class="form-control brd-orange border-cst" id="inlineFormInputGroup"
                               placeholder="Mujurjati@gmail.com" name="email">
                    </div>
                    <div class="pad-sm">
                        <button type="submit" class="button btn-lg btn-block button-circle bg-orange">Kirim</button>

                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Modal email 1 -->
    <div class="myModalEmail1 mfp-hide" id="myModalEmail1">
        <div class="block divcenter" style="background-color: #FFF; max-width: 500px; border-radius: 20px;">
            <div class="center" style="padding: 30px;">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><span class="c-title">Enter your email</span></label>
                        <h5>You're just one step away from chef-made food.</h5>
                        <div class="input-group">
                            <input type="text" class="form-control brd-orange border-cst" id="inlineFormInputGroup"
                                   placeholder="Email Address">
                        </div>
                        <span class="alert-red">email not available</span>
                        <div class="pad-sm">
                            <a href="#myModalEmail2" data-lightbox="inline"
                               class="button btn-lg btn-block button-circle bg-orange">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal email 2 -->
    <div class="myModalEmail2 mfp-hide" id="myModalEmail2">
        <div class="block divcenter" style="background-color: #FFF; max-width: 500px; border-radius: 20px;">
            <button href="#myModalEmail1" data-lightbox="inline" class="btn btn-link" style="margin-left: 10px;"><i
                        class="icon-line-arrow-left"></i>Back
            </button>
            <div class="center" style="padding: 30px;">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><span class="c-title">Enter your password</span></label>
                        <h5>Enter your password to sign into your account</h5>
                        <div class="input-group">
                            <input type="password" class="form-control brd-orange border-cst" id="inlineFormInputGroup"
                                   placeholder="Password">
                        </div>
                        <span class="alert-red">password incorrect</span>
                        <div class="pad-sm">
                            <a href="#" class="button btn-lg btn-block button-circle bg-orange">Submit</a>
                        </div>
                        <a href="#myModalEmail3" data-lightbox="inline" class="btn btn-link">Forgot Password?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal email forgot password -->
    <div class="myModalEmail3 mfp-hide" id="myModalEmail3">
        <div class="block divcenter" style="background-color: #FFF; max-width: 500px; border-radius: 20px;">
            <button href="#myModalEmail2" data-lightbox="inline" class="btn btn-link" style="margin-left: 10px;"><i
                        class="icon-line-arrow-left"></i>Back
            </button>
            <div class="center" style="padding: 30px;">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><span class="c-title">Check your email</span></label>
                        <h5>We've sent instructions on how to change your password to your email address.</h5>
                        <div class="pad-sm">
                            <a href="#myModalEmail1" data-lightbox="inline"
                               class="button btn-lg btn-block button-circle bg-orange">Login</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</header><!-- #header end -->


<script type="text/javascript">
    var _token = '{{ csrf_token() }}';
</script>
<script>
    $(".head-plus").off( 'click' ).on( 'click', function(){
        var id = $(this).parents(".head-cart").data("id");

        var productQuantity = $(this).parents('.quantity').find('.qty').val(),
            quantityStep = $(this).parents('.quantity').find('.qty').attr('step'),
            intRegex = /^\d+$/;

        if( !quantityStep ) { quantityStep = 1; }

        if( intRegex.test( productQuantity ) ) {
            var productQuantityPlus = Number(productQuantity) + Number(quantityStep);
            $(this).parents('.quantity').find('.qty').val( productQuantityPlus );

            var formData = {type: "qty", id: id, qty: productQuantityPlus};
            ajax_cart_head(formData);
        } else {
            $(this).parents('.quantity').find('.qty').val( Number(quantityStep) );
        }

        return false;
    });

    $(".head-minus").off( 'click' ).on( 'click', function(){
        var id = $(this).parents(".head-cart").data("id");

        var productQuantity = $(this).parents('.quantity').find('.qty').val(),
            quantityStep = $(this).parents('.quantity').find('.qty').attr('step'),
            intRegex = /^\d+$/;

        if( !quantityStep ) { quantityStep = 1; }

        if( intRegex.test( productQuantity ) ) {
            if( Number(productQuantity) > 1 ) {
                var productQuantityMinus = Number(productQuantity) - Number(quantityStep);
                $(this).parents('.quantity').find('.qty').val( productQuantityMinus );

                var formData = {type: "qty", id: id, qty: productQuantityMinus};
                ajax_cart_head(formData);
            }
        } else {
            $(this).parents('.quantity').find('.qty').val( Number(quantityStep) );
        }

        return false;
    });

    function ajax_cart_head(data) {
        $.ajax({
            url: base_url + "ajax/cart/edit",
            type: "POST",
            data: data,
            dataType: "json",
            headers: {"X-CSRF-TOKEN": _token},
            success: function (response) {
                if (response.success) {
                    if (response.subtotal != undefined) {
                        $(".cart-list[data-id='" + response.id + "'] .item-subtotal").html(response.subtotal);
                    }
                    if (response.cart != undefined) {
                        $.each(response.cart, function  (key, val) {
                            $("." + key).html(val);
                        });
                    }
                    if (response.type == "clear") {
                        location.href = base_url;
                    }
                }
            }
        });
    }
</script>