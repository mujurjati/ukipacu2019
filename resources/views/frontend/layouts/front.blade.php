@extends('frontend.layouts.app')
@section('title', 'Home')
@section('content')

    <section>
        <div class="container">
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="hero-style" src="images/content/{{$slider->content_image}}" class="d-block w-100" alt="...">
                        <div class="carousel-caption cap-cust d-none d-md-block ">
                            <h2>{{$slider->content_name}}</h2>
                            <span class="capt-text">{{$slider->content_sortdesc}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section style="margin: 15px 0; padding: 15px 0;background: #f5f4f9;">
        <div class="container clearfix">
            <div class="fancy-title title-bottom-border">
                <h3 style="font-weight: normal; font-size: 27">Rekomendasi <span><strong>Minggu Ini</strong></span></h3>
            </div>
            <div id="shop" class="shop clearfix">
            @foreach($proMinggu as $item)
                <div class="product clearfix">
                    <div class="product-image">
                        <a href="#"><img src="{{ asset_url('/images/products/'.$item->product_image)}}" alt="Checked Short Dress"></a>
                    @if(!empty($item->product_image2))
                        <a href="#"><img src="{{ asset_url('/images/products/'.$item->product_image2)}}" alt="Checked Short Dress"></a>
                    @endif
                        <div class="product-overlay">
                            <a href="#"><span>Lihat Detail Menu</span></a>
                        </div>
                    </div>
                    <div class="product-desc">
                        <a href="#" class="btn add-cart-btn btn-block">
                            {{$item->product_name}}
                        </a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </section>
    <div class="paralax-home"></div>

    <section style="margin: 15px 0; padding: 15px 0;background: #f5f4f9;">
        <div class="container clearfix">
            <div class="fancy-title title-bottom-border">
                <h3 style="font-weight: normal; font-size: 27">Daftar <span><strong>Produk</strong></span></h3>
            </div>
            <div id="shop" class="shop clearfix">
            @foreach($proTerbaru as $item)
                <div class="product clearfix">
                    <div class="product-image">
                        <a href="#"><img src="{{ asset_url('/images/products/'.$item->product_image)}}" alt="Checked Short Dress"></a>
                        @if(!empty($item->product_image2))
                            <a href="#"><img src="{{ asset_url('/images/products/'.$item->product_image2)}}" alt="Checked Short Dress"></a>
                        @endif
                        <div class="product-overlay">
                            <a href="#"><span>Lihat Detail Menu</span></a>
                        </div>
                    </div>
                    <div class="product-desc">
                        <a href="#" class="btn add-cart-btn btn-block">
                            {{$item->product_name}}
                        </a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </section>
    <div class="paralax-home"></div>

    <section style="margin: 15px 0; padding: 15px 0">
        <div class="content-wrap" style="padding: 0">
            <div class="container clearfix">
                <div class="fancy-title title-bottom-border">
                    <h3 style="font-weight: normal; font-size: 27">Uki <span><strong>Paket</strong></span></h3>
                </div>
                <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="true" data-items-xs="1" data-items-sm="1" data-items-lg="2" data-items-xl="2">
                    @foreach($paket as $item)
                        <div class="oc-item">
                            <a href="#"><img class="rads-30" src="{{ asset_url('/images/paket/'.$item->paket_image) }}" alt="{{$item->paket_name}}"></a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <div class="notopborder nomargin" style="margin: 15px 0; padding: 15px 0;background: #f5f4f9;">
        <div class="content-wrap" style="padding: 15px 0">
            <div class="container clearfix">

                <div class="col_half nobottommargin">

                    <div class="heading-block topmargin-sm">
                        <h2>Toko Online</h2>
                        <span>Browser produk kami</span>
                    </div>

                    <p>
                        Ukipacu didirikan untuk meninggalkan kesan yang baik. Dalam usaha kami
                        terdapat beragam jenis kebutuhan pokok sehari-hari. Dikirim ke depan rumahmu atau datang ke toko kami.
                    </p>

                    <a href="{{route('product.product_list')}}" class="button button-circle bg-orange button-large">Kunjungi</a>

                </div>

                <div class="col_half nobottommargin col_last">
                    <img class="images-responsive" src="{{ asset_url('frontend/menu/service-online_wt.png') }}" alt="images responsive">
                </div>

            </div>
        </div>
    </div>

@endsection
