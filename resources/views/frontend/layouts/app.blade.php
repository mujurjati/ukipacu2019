<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link rel="icon" href="{{asset_url('/frontend/images/favicon.png')}}" type="image/x-icon"/>
    <!--<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">-->
    <link rel="stylesheet" href="{{asset_url('/frontend/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset_url('/frontend/style.css?v=21071')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset_url('/frontend/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset_url('/frontend/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset_url('/frontend/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset_url('/frontend/css/magnific-popup.css')}}" type="text/css" />
    
    <link rel="stylesheet" href="{{asset_url('/frontend/css/responsive.css')}}" type="text/css" />
    <script src="{{asset_url('/frontend/js/jquery.js')}}"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>@yield('title') - {{ config('app.name') }}</title>

    <style>

        .revo-slider-emphasis-text {
            font-size: 58px;
            font-weight: 700;
            letter-spacing: 1px;
            font-family: 'Raleway', sans-serif;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }

        .revo-slider-desc-text {
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }

        .revo-slider-caps-text {
            font-size: 16px;
            font-weight: 400;
            letter-spacing: 3px;
            font-family: 'Raleway', sans-serif;
        }

        .tp-video-play-button { display: none !important; }

        .tp-caption { white-space: nowrap; }

    </style>
    @yield('load-css')
    <script type="text/javascript">
        var base_url = '{{ url('/') }}';
    </script>

</head>

<body class="stretched">

<div id="wrapper" class="clearfix">

    @include('frontend.layouts.header')

    @yield('content')

    @include('frontend.layouts.footer')

</div>
<!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->

<script src="{{asset_url('/frontend/js/plugins.js')}}"></script>

<!-- Footer Scripts
============================================= -->
<script src="{{asset_url('/frontend/js/functions.js')}}"></script>
@yield('image-crop-script')
@yield('load-script')
</body>
</html>

