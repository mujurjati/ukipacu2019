@extends('frontend.layouts.app')
@section('title', 'Home')
@section('content')

    <section id="slider" class="slider-element slider-parallax swiper_wrapper full-screen clearfix">

        <div class="slider-parallax-inner">

            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" style="background-image: url('images/content/{{$slider->content_image}}');">
                        <div class="container clearfix">
                            <div class="slider-caption">
                                <div class="row">
                                    <div class="col-md-7">
                                        <h3 data-animate="fadeInUp">{{$slider->content_name}}</h3>
                                        <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">
                                            <small>{{$slider->content_sortdesc}}</small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

    <section id="content">

        <div class="pad-sm">
            <div class="container clearfix shaddow">
                <div class="col_one_third mrg-btm">
                    <div class="feature-box fbox-effect">
                        <div class="fbox-icon">
                            <img src="{{asset_url('/frontend/images/icon_1.svg')}}" alt="images">
                        </div>
                        <h5>Harga termasuk ongkir</h5>
                        <p>Tanpa biaya ongkir tambahan ke seluruh Jakarta dan Tangsel</p>
                    </div>
                </div>

                <div class="col_one_third mrg-btm">
                    <div class="feature-box fbox-effect">
                        <div class="fbox-icon">
                            <img src="{{asset_url('/frontend/images/icon_2.svg')}}" alt="images">
                        </div>
                        <h5>Makananmu bergaransi</h5>
                        <p>Setiap makananmu memiliki garansi kualitas dan jam pengantaran</p>
                    </div>
                </div>

                <div class="col_one_third col_last mrg-btm">
                    <div class="feature-box fbox-effect">
                        <div class="fbox-icon">
                            <img src="{{asset_url('/frontend/images/icon_3.svg')}}" alt="images">
                        </div>
                        <h5>Atur langganan sesuka hati</h5>
                        <p>Pilih tanggal, skip, dan cancel langgananmu dengan mudah</p>
                    </div>
                </div>
            </div>
        </div>


    </section>

    <section id="content">
        <div class="pad-sm">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-6 norightpadding" style="margin-bottom:15px;">
                        <a href="#"><img src="{{asset_url('/frontend/menu/service-catering.png')}}" style="border-radius: 20px; box-shadow: 1px 3px 9px 0px #cac3c3;"></a>
                    </div>

                    <div class="col-lg-6 norightpadding" style="margin-bottom:15px;">
                        <a href="{{route('product.product_list')}}"><img src="{{asset_url('/frontend/menu/service-online.png')}}" style="border-radius: 20px; box-shadow: 1px 3px 9px 0px #cac3c3;"></a>
                    </div>
                </div>

            </div>
        </div>

    </section>

    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">

                <div class="heading-block center bottommargin-lg">
                    <h2>Our Happy Custumer</h2>
                    <span>Lorem ipsum dollor sit amet.</span>
                </div>

                <ul class="clients-grid grid-6 nobottommargin clearfix">
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/1.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/2.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/3.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/4.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/5.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/6.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/7.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/8.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/9.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/10.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/11.png')}}" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{asset_url('/frontend/images/clients/logo/12.png')}}" alt="Clients"></a></li>
                </ul>

            </div>
        </div>
    </section>
@endsection