<footer id="footer" class="light">

    <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix">

            <div class="col_two_third">

                <div class="col_one_third">

                    <div class="widget clearfix">

                        <img src="images/footer-widget-logo.png" alt="" class="footer-logo">

                        <p>Order <strong>Simple</strong>, <strong>Cepat</strong> &amp; <strong>Flexible</strong> Dan Sehat.</p>

                        <div style="background: url('frontend/images/world-map.png') no-repeat center center; background-size: 100%;">
                            <address>
                                <strong>Ukipacu</strong><br>
                                {{$setting['General']['address']}}
                            </address>
                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> {{$setting['General']['phone']}}<br>
                            <abbr title="Email Address"><strong>Email:</strong></abbr> {{$setting['General']['email']}}
                        </div>

                    </div>

                </div>

                <div class="col_one_third">

                    <div class="widget widget_links clearfix">

                        <h4>Ukipacu</h4>

                        <ul>
                            <li><a href="{{ route('about.about')  }}">Tentang Kami</a></li>
                            <li><a href="{{ route('contact.contact')  }}">Kontak</a></li>
                            <li><a href="{{ route('customers.profil')  }}">Akun Saya</a></li>
                        </ul>

                    </div>

                </div>

                <div class="col_one_third col_last">

                    <div class="widget  widget_links clearfix">
                        <h4>Pesanan</h4>
                        <ul>
                            <li><a href="{{ route('customers.order') }}">Riwayat Belanja</a></li>
                            <li><a href="#">Pencarian Produk</a></li>
                            <li><a href="{{ route('customers.login')  }}">Masuk</a></li>
                        </ul>
                    </div>

                </div>

            </div>

            <div class="col_one_third col_last">

                <div class="widget  widget_links clearfix">
                    <h4>Ketentuan</h4>
                    <ul>
                        <li><a href="{{route('bantuan.terms')}}">Syarat Ketentuan</a></li>
                        <li><a href="{{route('bantuan.kebijakanprivasi')}}">Kebijakan Privasi</a></li>
                        <li><a href="#"> Hubungi Kami</a></li>
                        <li><a href="#"> Panduan Keamanan </a></li>
                    </ul>
                </div>
            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">

            <div class="col_half">
                Copyrights &copy; 2020 All Rights Reserved by PT. Ukipacu Indonesia.<br>
                <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
            </div>

            <div class="col_half col_last tright">

                <div><img src="{{ asset_url('/frontend/images/payment.png')}}" alt=""></div>
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer>
