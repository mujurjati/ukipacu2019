@extends('frontend.layouts.app')
@section('title', 'Daftar Video')
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-wrap text-center">
                        <nav aria-label="breadcrumb">
                            <h2>Daftar Video</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Daftar Video</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area end -->

    <!-- blog main wrapper start -->
    <div class="blog-main-wrapper pt-60 pb-60 pb-md-54 pb-sm-54">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 order-1 order-lg-2">
                    <div class="row">
                        @foreach($video as $item)
                            <div class="col-lg-4">
                                <div class="blog-item-wrapper blog-item-wrapper__blog-grid mb-40">
                                    <div class="blog-img">
                                        <div class="blog-gallery-slider slider-arrow-style slider-arrow-style__style-2">
                                            <div class="blog-single-slide">
                                                <video width="100%" height="250" controls preload="metadata">
                                                    <source src="{{asset_url('/videos/'.$item->video_file)}}"
                                                            type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blog-inner-content">
                                        <h3>{{$item->video_name}}</h3>
                                        <ul class="blog-meta">
                                            <li><strong>Upload
                                                    :</strong> {{ date_indo($item->video_create_date, 'datetime') }}
                                            </li>
                                        </ul>
                                        <p>{{$item->video_sortdesc}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- start pagination area -->
                {{ $video->links("pagination::bootstrap-4") }}
                <!-- end pagination area -->
                </div>
            </div>
        </div>
    </div>
    <!-- blog main wrapper end -->
@endsection