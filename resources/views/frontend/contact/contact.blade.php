@extends('frontend.layouts.app')
@section('title', 'Kontak Kami')
@section('content')

    <div class="login-register-wrapper topmargin-lg">
        <div class="container">
            <div class="member-area-from-wrap">
                <div class="row">
                    <!-- Login Content Start -->
                    <div class="col-lg-5">
                        <div class="login-reg-form-wrap  pr-lg-50">
                            <h3>Kirim Pesan</h3>
                            <form id="contact-form" action="{{route('contact.add')}}" method="post"
                                  class="contact-form">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input class="form-control" name="name" placeholder="Name *" type="text" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="email" placeholder="Email *" type="text" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="subject" placeholder="Subject *" type="text">
                                </div>
                                <div class="form-group">
                                    <textarea placeholder="Message *" name="message" class="form-control"
                                              required=""></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="button button-circle">Kirim</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Login Content End -->
                    <div class="col-lg-1">
                    </div>

                    <!-- Register Content Start -->
                    <div class="col-lg-6">
                        <div class="login-reg-form-wrap mt-md-60 mt-sm-60">
                            <h3>Kontak Kami</h3>
                            <p>Silahkan hubungi kami jika ada yang ingin anda sampaikan</p>
                            <table>
                                <tr>
                                    <td width="30px;"><i class="icon-compass"></i> </td>
                                    <td>{{$setting['General']['address']}}</td>
                                </tr>
                                <tr>
                                    <td><i class="icon-email"></i></td>
                                    <td>{{$setting['General']['email']}}</td>
                                </tr>
                                <tr>
                                    <td><i class="icon-phone"></i></td>
                                    <td>{{$setting['General']['phone']}}</td>
                                </tr>
                                <tr>
                                    <td><i class="icon-instagram"></i></td>
                                    <td>{{$setting['General']['instagram_id']}}</td>
                                </tr>
                                <tr>
                                    <td><i class="icon-facebook"></i></td>
                                    <td>{{$setting['General']['facebook_id']}}</td>
                                </tr>
                                <tr>
                                    <td><i class="icon-twitter"></i></td>
                                    <td>{{$setting['General']['twitter_id']}}</td>
                                </tr>
                            </table>
                            <div class="working-time">
                                <h3>Jam Kerja</h3>
                                <p><span>Senin – Sabtu:</span>08AM – 22PM</p>
                            </div>
                        </div>
                    </div>
                    <!-- Register Content End -->
                </div>
            </div>
        </div>
    </div>
    <!-- contact area start -->
    <!-- contact area end -->

    <!-- map area start -->
    <div class="map-area-wrapper">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3947.373902219725!2d116.15887701413695!3d-8.364830093983244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcddbbe4a807df5%3A0x4adca0a251d4df77!2sJl.%20Lading-Lading%2C%20Tanjung%2C%20Kabupaten%20Lombok%20Utara%2C%20Nusa%20Tenggara%20Bar.%2083352!5e0!3m2!1sid!2sid!4v1611811686123!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!-- map area end -->
@endsection