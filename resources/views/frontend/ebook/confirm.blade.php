@extends('frontend.layouts.app')
@section('title', 'Data Pembayaran Paket Langganan')
@section('content')
    <main class="main">
        <nav aria-label="breadcrumb" class="breadcrumb-nav">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/')  }}"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">Data Pembayaran Paket Langganan</li>
                </ol>
            </div><!-- End .container -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="alert alert-light" style="background: #fff2db; color: #555" role="alert">
                        <strong>Selalu waspada kepada pihak yang tidak bertanggungjawab</strong>
                        <ul class="mt-1 ml-5">
                            <li><i class="icon-ok"></i> Pastikan untuk menyelesaikan pembayaran sesuai dengan informasi yang tertera pada halaman ini.</li>
                            <li><i class="icon-ok"></i> Jangan melakukan pembayaran dengan nominal yang berbeda dengan yang tertera pada tagihan.</li>
                            <li><i class="icon-ok"></i> Jangan melakukan transfer di luar nomor rekening yang tertera pada halaman ini.</li>
                        </ul>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            Pembayaran Via Transfer Bank
                        </div><!-- End .card-header -->

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-center">Jumlah Tagihan</div>
                                <div class="col-md-12 text-center mt-1">
                                    <strong class="popover-show"
                                            data-content="Transfer tepat hingga 3 digit terakhir agar tidak menghambat proses verifikasi">
                                        <h2>Rp {{ price_format($nominal) }}</h2>
                                    </strong>
                                </div>
                            </div>
                        </div><!-- End .card-body -->
                    </div>

                    <div class="card">
                        <div class="card-body text-center">
                            Lakukan pembayaran sebelum <strong>{{ date('g:i A, d F Y', strtotime($expDate)) }}</strong> ke rekening berikut

                            <div class="row mt-3">
                                <div class="col-md-12 pr-5 pl-5">
                                    <div class="row mb-1">
                                        <div class="col-md-6 text-left">Bank</div>
                                        <div class="col-md-6 text-right"><h3>{{ $dataConfirm->e_bank_to }}</h3></div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-md-6 text-left">Nomor Rekening</div>
                                        <div class="col-md-6 text-right"><h3>{{ $dataConfirm->e_bank_to_number }}</h3></div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-md-6 text-left">Nama Rekening</div>
                                        <div class="col-md-6 text-right"><h3>{{ $dataConfirm->e_bank_to_account }}</h3></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="{{ url('/') }}" class="btn btn-block btn-primary btn-sm">SELESAI</a>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </main>

    <script>
        $(function () {
            $('.popover-show').popover({
                placement: 'bottom',
                template: '<div class="popover" role="tooltip" style="font-size: 13px;color: #fff"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body text-center" style="background: #ffa500;color: #fff"></div></div>'
            }).popover('show');
        });
    </script>
@endsection