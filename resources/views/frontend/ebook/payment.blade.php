@extends('frontend.layouts.app')
@section('title', 'Pembayaran Paket Langganan')
@section('content')
    <main class="main">
        <nav aria-label="breadcrumb" class="breadcrumb-nav">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/')  }}"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">Payment Paket Langganan</li>
                </ol>
            </div><!-- End .container -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h2 class="step-title">Metode Pembayaran</h2>

                    <div class="cart-table-container">
                        <table class="table table-cart">
                            <thead>
                            <tr>
                                <th class="text-left" style="background: #fafafa">
                                    <strong>TRANSFER BANK</strong>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-justify">
                                    <div class="row">
                                        @foreach($dataBank as $item)
                                            <div class="col-md-6 mb-3">
                                                <div class="custom-control custom-radio custom-control-inline"
                                                     style="margin: 0">
                                                    <input type="radio" name="paymentMethod"
                                                           value="{{ $item->bank_id }}" id="bank-{{ $item->bank_id }}"
                                                           class="custom-control-input act-select-bank"/>
                                                    <label class="custom-control-label" for="bank-{{ $item->bank_id }}">
                                                        <img src="{{ asset_url('/images/bank/'.$item->bank_image) }}"
                                                             style="height: 30px" alt="{{ $item->bank_name }}">
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <strong>Ketentuan Pembayaran :</strong>
                                            <ul>
                                                <li><i class="icon-ok"></i> Pembayaran dapat dilakukan ke rekening yang
                                                    Anda pilih di atas.
                                                </li>
                                                <li><i class="icon-ok"></i> Total belanja belum termasuk kode pembayaran
                                                    untuk keperluan proses verifikasi otomatis.
                                                </li>
                                                <li><i class="icon-ok"></i> Mohon segera transfer tepat sampai 3 digit
                                                    terakhir.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td class="clearfix">
                                    <div class="float-left">
                                        <a href="{{ route('ebook.detail', session()->get('dataEbook')->e_package_id) }}" class="btn btn-outline-dark">Halaman
                                            Sebelumnya</a>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="cart-summary">
                        @if(session()->has('dataEbook'))
                            <h3>Ringkasan Paket Langganan</h3>
                            <h4>
                                <a data-toggle="collapse" href="#desc-ebook" class="collapsed" role="button"
                                   aria-expanded="false" aria-controls="total-estimate-section">Deskripsi</a>
                            </h4>
                            <div class="collapse" id="desc-ebook" style="">
                                <div class="product-desc-content">
                                    <p class="text-justify">{!! session()->get('dataEbook')->e_package_shortdesc !!}</p>
                                </div>
                            </div>
                            <table class="table table-totals">
                                <tbody>
                                <tr>
                                    <td>Nama Paket</td>
                                    <td>{{session()->get('dataEbook')->e_package_name}}</td>
                                </tr>
                                <tr>
                                    <td>Tipe Paket</td>
                                    <td>{{ucwords(session()->get('dataEbook')->e_package_type)}}</td>
                                </tr>
                                <tr>
                                    <td>Durasi Paket</td>
                                    <td>{{session()->get('dataEbook')->e_package_time}}</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td>Order Total</td>
                                    <td class="cart-total">
                                        Rp {{ price_format(session()->get('dataEbook')->e_package_price) }}</td>
                                </tr>
                                </tfoot>

                            </table>
                            <div class="checkout-methods">
                                <button type="button" id="savePaymentEbook" class="btn btn-block btn-sm btn-primary" disabled="" data-url="{{ route('ebook.create_order') }}">
                                    Bayar
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-6"></div>
    </main>

    <script type="text/javascript">
        var _token = '{{ csrf_token() }}';
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/jquery.preloaders.min.js') }}"></script>
    <script src="{{ asset_url('/frontend/assets/js/ebook.js') }}"></script>
@endsection