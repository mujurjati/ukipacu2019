@extends('frontend.layouts.app')
@section('title', $dataEbook->e_package_name)
@section('content')
    <div class="container">
        @include('flashy::message')

        <script id="flashy-template" type="text/template">
            <div class="flashy flashy--success">
                <a href="#" class="flashy__body" target="_blank"></a>
            </div>
        </script>
    </div><!-- End .header-bottom -->

    <div class="container">
        <main class="main">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ route('ebook.list')  }}">Paket Langganan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$dataEbook->e_package_name}}</li>
                    </ol>
                </div><!-- End .container -->
            </nav>
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="product-single-container product-single-default">
                            <div class="row">
                                <div class="col-lg-7 col-md-6 product-single-gallery">
                                    <div class="product-slider-container product-item">
                                        <div class="product-single-carousel owl-carousel owl-theme owl-loaded owl-drag">
                                            <div class="owl-stage-outer">
                                                <div class="owl-stage"
                                                     style="transform: translate3d(-940px, 0px, 0px); transition: all 0.3s ease 0s; width: 3761px;">
                                                    <div class="owl-item active" style="width: 470.031px;">
                                                        <div class="product-item">
                                                            <img class="product-single-image"
                                                                 src="{{get_image_ebook($dataEbook->e_package_image)}}"
                                                                 data-zoom-image="{{get_image_ebook($dataEbook->e_package_image)}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="owl-nav">
                                                <button type="button" role="presentation" class="owl-prev"><i
                                                            class="icon-angle-left"></i></button>
                                                <button type="button" role="presentation" class="owl-next"><i
                                                            class="icon-angle-right"></i></button>
                                            </div>
                                        </div>
                                        <!-- End .product-single-carousel -->
                                        <span class="prod-full-screen">
                                            <i class="icon-plus"></i>
                                        </span>
                                    </div>
                                </div><!-- End .col-lg-7 -->

                                <div class="col-lg-5 col-md-6">
                                    <div class="product-single-details">
                                        <h1 class="product-title">{{$dataEbook->e_package_name}}</h1>

                                        <div class="ratings-container">
                                            <strong><i class="icon-tag"></i> Edisi {{ucwords($dataEbook->e_package_type)}}</strong>
                                        </div>
                                        <div class="ratings-container">
                                            <strong><i class="icon-calendar"></i> Durasi {{$dataEbook->e_package_time}} Bulan</strong>
                                        </div>


                                        <div class="price-box">
                                            <span class="product-price">Rp. {{price_format($dataEbook->e_package_price)}}</span>
                                        </div><!-- End .price-box -->

                                        <div class="product-desc">
                                            <p class="text-justify">{{$dataEbook->e_package_shortdesc}}</p>
                                        </div><!-- End .product-desc -->

                                        {{ Form::open(['route' => ['customer.cart.order', $dataEbook->product_alias], 'method' => 'post', 'id' => 'formAddCart']) }}
                                        <div class="product-action product-all-icons">
                                            <a href="{{ route('ebook.payment') }}" class="btn btn-sm btn-primary" style="margin: 0 1rem 1rem 0"
                                                    title="Beli Sekarang">
                                                <span>Berlangganan</span>
                                            </a>
                                        </div><!-- End .product-action -->
                                        {{ Form::close() }}
                                        <div class="product-single-share">
                                            {{-- <label>Share:</label>--}}

                                            <div class="addthis_inline_share_toolbox"></div>
                                        </div><!-- End .product single-share -->
                                    </div><!-- End .product-single-details -->
                                </div><!-- End .col-lg-5 -->
                            </div><!-- End .row -->
                        </div><!-- End .product-single-container -->

                        <div class="product-single-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="product-tab-desc" data-toggle="tab"
                                       href="#product-desc-content" role="tab" aria-controls="product-desc-content"
                                       aria-selected="true">Deskripsi</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel"
                                     aria-labelledby="product-tab-desc">
                                    <div class="product-desc-content">
                                        <p class="text-justify">{!! $dataEbook->e_package_desc !!}</p>
                                    </div><!-- End .product-desc-content -->
                                </div><!-- End .tab-pane -->
                            </div><!-- End .tab-content -->
                        </div><!-- End .product-single-tabs -->
                    </div><!-- End .col-lg-9 -->

                    <div class="sidebar-overlay"></div>
                    <div class="sidebar-toggle"><i class="icon-sliders"></i></div>
                    <aside class="sidebar-product col-lg-3 padding-left-lg mobile-sidebar">
                        <div class="pin-wrapper" style="height: 1034.28px;">
                            <div class="sidebar-wrapper sticky-active sticky-absolute sticky-transition"
                                 style="border-bottom: 0px none rgb(122, 125, 130); width: 250px;">


                                <div class="widget">
                                    <h4 class="widget-title">Paket Terbaru</h4>
                                    <ul class="simple-entry-list">
                                        @foreach($ebookTerbaru as $item)
                                            <li>
                                                <div class="entry-media">
                                                    <a href="">
                                                        <img src="{{ get_image_ebook($item->e_package_image) }}" alt="Post">
                                                    </a>
                                                </div><!-- End .entry-media -->
                                                <div class="entry-info">
                                                    <a href="{{route('ebook.detail', $item->e_package_id)}}">{{$item->e_package_name}}</a>
                                                    <div class="entry-meta">
                                                        {{date_indo($item->e_package_create_date, 'datetime')}}
                                                    </div><!-- End .entry-meta -->
                                                </div><!-- End .entry-info -->
                                            </li>
                                        @endforeach
                                    </ul>
                                </div><!-- End .widget -->


                            </div>
                        </div>
                    </aside><!-- End .col-md-3 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </main>
    </div><!-- End .header-bottom -->

    <script src="{{ asset_url('/frontend/assets/js/jquery.preloaders.min.js') }}"></script>
@endsection