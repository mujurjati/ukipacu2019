@extends('frontend.layouts.app')
@section('title', 'Paket Langganan')
@section('content')
    <main class="main">
        <nav aria-label="breadcrumb" class="breadcrumb-nav">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">Paket Langganan</li>
                </ol>
            </div><!-- End .container -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        @foreach($dataEbook as $item)
                            <div class="product p-md-3 text-center">
                                <figure class="product-image-container">
                                    <a href="{{route('ebook.detail', $item->e_package_id)}}" class="ebook-image">
                                        <img src="{{ get_image_ebook($item->e_package_image) }}"
                                             alt="ebook" style="height: 250px;">
                                    </a>
                                    <a href="javascript:" class="btn-hover-ebook">Edisi {{$item->e_package_type}}</a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <span>{{$item->e_package_time}} Bulan</span><!-- End .ratings -->
                                    </div><!-- End .product-container -->
                                    <h2 class="product-title">
                                        <a href=""></a>
                                    </h2>
                                    <div class="price-box">
                                        <a href="{{route('ebook.detail', $item->e_package_id)}}"><span class="product-price">{{$item->e_package_name}}</span></a>
                                    </div><!-- End .price-box -->
                                    <div class="product-action">
                                        <a href="javascript:" class="btn btn-sm btn-danger" title="Add to Cart">
                                            <span>Rp. {{price_format($item->e_package_price)}}</span>
                                        </a>
                                    </div>
                                </div><!-- End .product-details -->
                            </div>
                            <!-- End .product -->
                        @endforeach
                    </div>
                </div><!-- End .col-lg-9 -->

                <aside class="sidebar col-lg-3">
                    <div class="pin-wrapper" style="height: 968px;">
                        <div class="sidebar-wrapper sticky-active"
                             style="border-bottom: 0px none rgb(122, 125, 130); width: 270px;">
                            <div class="widget widget-search">
                                <form role="search" method="get" class="search-form" action="#">
                                    <input type="search" class="form-control" placeholder="Masukkan kata kunci..."
                                           name="s" required="">
                                    <button type="submit" class="search-submit" title="Cari">
                                        <i class="icon-search"></i>
                                        <span class="sr-only">Cari</span>
                                    </button>
                                </form>
                            </div><!-- End .widget -->

                            <div class="widget widget-categories">
                                <h4 class="widget-title">Tipe Paket</h4>

                                <ul class="list">
                                    <li><a href="{{route('ebook.list', 'ganjil')}}">Ganjil</a></li>
                                    <li><a href="{{route('ebook.list', 'genap')}}">Genap</a></li>
                                    <li><a href="{{route('ebook.list', 'full')}}">Full</a></li>
                                </ul>
                            </div><!-- End .widget -->

                            <div class="widget">
                                <h4 class="widget-title">Paket Terbaru</h4>
                                <ul class="simple-entry-list">
                                    @foreach($ebookTerbaru as $item)
                                        <li>
                                            <div class="entry-media">
                                                <a href="">
                                                    <img src="{{ get_image_ebook($item->e_package_image) }}" alt="Post">
                                                </a>
                                            </div><!-- End .entry-media -->
                                            <div class="entry-info">
                                                <a href="{{route('ebook.detail', $item->e_package_id)}}">{{$item->e_package_name}}</a>
                                                <div class="entry-meta">
                                                    {{date_indo($item->e_package_create_date, 'datetime')}}
                                                </div><!-- End .entry-meta -->
                                            </div><!-- End .entry-info -->
                                        </li>
                                    @endforeach
                                </ul>
                            </div><!-- End .widget -->
                        </div>
                    </div><!-- End .sidebar-wrapper -->
                </aside><!-- End .col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->

        <div class="mb-6"></div><!-- margin -->
    </main>
@endsection