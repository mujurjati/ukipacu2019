@extends('frontend.layouts.app')
@section('title', 'Store Corner')
@section('content')

    <main class="main">
        <div class="page-header page-header-bg" style="background-color: #efeeee">
            <div class="container">
                <h1><span>Store Corner</span>
                    {{$setting['General']['web_name']}}
                </h1>
            </div><!-- End .container -->
        </div><!-- End .page-header -->
        <div class="container mt-6 mb-5">
            <div class="row">
                @foreach($data as $item)
                <div class="col-sm-6 col-md-1 mt-1">
                    <div class="feature-box feature-box-simple text-left">
                        <i style="border: 0"><img src="{{get_image('storecorner/'.$item->s_corner_logo)}}" alt="Post" ></i>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 mt-1">
                    <div class="feature-box feature-box-simple text-left">
                        <div class="feature-box-content mt-1">
                            <h2>{{$item['s_corner_name']}}</h2>
                            <p style="max-width: none;text-align: justify">{!! $item['s_corner_desc'] !!}</p>
                            <h5><i class="icon-location " style="border: none;height: 0px;width: 0px;padding-right: 20px; padding-left: 15px;margin-bottom: 0px"></i> {{$item['s_corner_address']}}</h5>
                            <h5 style="margin-left: 35px">{{ $item->subdistrict->s_subdistrict_name }},
                                {{ $item->subdistrict->city->s_city_name }},
                                {{ $item->subdistrict->city->province->s_province_name }}.
                            </h5>
                            <h5><i class="icon-phone" style="border: none;height: 0px;width: 0px;padding-right: 20px; padding-left: 15px"></i>{{$item['s_corner_phone']}}</h5>
                            <h5> <i class="icon-clock" style="border: none;height: 0px;width: 0px;padding-right: 20px; padding-left: 15px"></i>{{date('H:i',strtotime($item['s_corner_open']))}} - {{date('H:i',strtotime($item['s_corner_close']))}}</h5>
                        </div>
                       <!-- End .feature-box-content -->
                    </div><!-- End .feature-box -->
                </div><!-- End .col-md-4 -->
                @endforeach

                <nav class="toolbox toolbox-pagination">
                    {{ $data->links("pagination::bootstrap-4") }}
                </nav>

            </div>
        </div>

    </main><!-- End .main -->
@endsection