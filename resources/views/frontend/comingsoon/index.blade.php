@extends('frontend.layouts.app')
@section('title', 'Coming soon')
@section('content')
    <section id="page-title" class="page-title-parallax page-title-dark page-title-right" style="padding: 250px 0; background-image: url('{{ asset_url('frontend/menu/comingsoon1.jpg') }}'); background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 440px;" data-top-bottom="background-position:0px -500px;">

        <div class="container clearfix">
            <h1>COMINGSOON</h1>
            <span>
                Halaman masih dalam perbaikan | Terimakasih atas kedatanganmu, tolong kembali lagi beberapa saat kemudian.
            </span>
        </div>

    </section><!-- #page-title end -->
@endsection