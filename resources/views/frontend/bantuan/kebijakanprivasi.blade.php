@extends('frontend.layouts.app')
@section('title', 'Kebijakan Privasi')
@section('content')

    <div class="login-register-wrapper topmargin-lg">
        <div class="container">
            <div class="member-area-from-wrap">
                <div class="row">
                    <!-- Login Content Start -->
                    <div class="col-lg-12">
                        <div class="login-reg-form-wrap  pr-lg-50">
                            <h3>Kebijakan Privasi</h3>
                            <p>{{$setting['General']['web_name']}}</p><br>
                            {!!$setting['General']['kebijakan_privasi'] !!}
                        </div>
                    </div>
                    <!-- Login Content End -->
                </div>
            </div>
        </div>
    </div>
@endsection