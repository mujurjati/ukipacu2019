@extends('frontend.layouts.app')
@section('title', 'Syarat & Ketentuan')
@section('content')

    <div class="login-register-wrapper topmargin-lg">
        <div class="container">
            <div class="member-area-from-wrap">
                <div class="row">
                    <!-- Login Content Start -->
                    <div class="col-lg-5">
                        <div class="login-reg-form-wrap  pr-lg-50">
                            <h3>Syarat & Ketentuan</h3>
                            <p>{{$setting['General']['web_name']}}</p><br>
                            {!!$setting['General']['terms'] !!}
                        </div>
                    </div>
                    <!-- Login Content End -->
                    <div class="col-lg-1">
                    </div>

                    <!-- Register Content Start -->
                    <div class="col-lg-6">
                        <div class="login-reg-form-wrap mt-md-60 mt-sm-60">
                            <img src="{{asset_url('/frontend/assets/img/about/about.jpg')}}" alt="About"/>
                        </div>
                    </div>
                    <!-- Register Content End -->
                </div>
            </div>
        </div>
    </div>

    <!-- about wrapper start -->

    <!-- about wrapper end -->
@endsection