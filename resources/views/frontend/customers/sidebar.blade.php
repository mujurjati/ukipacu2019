<ul class="tab-nav tab-nav2 clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header"
    role="tablist">
    <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab {{ Request::url()== route('customers.profil') ? 'ui-tabs-active ui-state-active' : '' }}">
        <a href="{{route('customers.profil')}}"><i
                    class="icon-user2"></i> Profile</a></li>
    <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab {{ Request::url()== route('customers.order') ? 'ui-tabs-active ui-state-active' : '' }}">
        <a href="#"><i
                    class="icon-inbox1"></i> Bonus</a></li>
    <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab">
        <a href="{{route('customers.logout')}}"><i
                    class="icon-chevron-left"></i> Keluar</a></li>
</ul>
