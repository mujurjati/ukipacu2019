@extends('frontend.customers.app')
@section('title', $myEbookDetail->e_product_name)
@section('main-content')
    <div class="col-lg-9 order-lg-last dashboard-content">
        <div class="row">
            <div class="col-md-10">
                <h2>Edisi Detail</h2>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-lg-12">
                <div class="product-single-container product-single-default">
                    <div class="row">
                        <div class="col-lg-7 col-md-6 product-single-gallery">
                            <div class="product-slider-container product-item">
                                <div class="product-single-carousel owl-carousel owl-theme owl-loaded owl-drag">
                                    <div class="owl-stage-outer">
                                        <div class="owl-stage"
                                             style="transform: translate3d(-940px, 0px, 0px); transition: all 0.3s ease 0s; width: 3761px;">
                                                <div class="owl-item active" style="width: 470.031px;">
                                                    <div class="product-item">
                                                        <img class="product-single-image"
                                                             src="{{ get_image_ebook_product($myEbookDetail->e_product_image) }}"
                                                             data-zoom-image="{{ get_image_ebook_product($myEbookDetail->e_product_image) }}">
                                                        <div class="zoomContainer"
                                                             style="-webkit-transform: translateZ(0);position:absolute;left:0px;top:0px;height:470.016px;width:470.016px;">
                                                            <div class="zoomWindowContainer" style="width: 400px;">
                                                                <div style="z-index: 999; overflow: hidden; margin-left: 0px; margin-top: 0px; background-position: 0px -193.914px; width: 470.016px; height: 470.016px; float: left; cursor: grab; background-repeat: no-repeat; position: absolute; background-image: url(&quot;{{asset_url('/frontend/assets/images/products/zoom/product-1-big.jpg')}}&quot;); top: 0px; left: 0px; display: none;"
                                                                     class="zoomWindow">&nbsp;
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="owl-nav">
                                        <button type="button" role="presentation" class="owl-prev"><i
                                                    class="icon-angle-left"></i></button>
                                        <button type="button" role="presentation" class="owl-next"><i
                                                    class="icon-angle-right"></i></button>
                                    </div>
                                </div>
                                <!-- End .product-single-carousel -->
                                <span class="prod-full-screen">
                                            <i class="icon-plus"></i>
                                        </span>
                            </div>
                        </div><!-- End .col-lg-7 -->

                        <div class="col-lg-5 col-md-6">
                            <div class="product-single-details">
                                <h1 class="product-title">{{$myEbookDetail->e_product_name}}</h1>

                                <div class="price-box">
                                    <span class="product-price">Edisi : {{$myEbookDetail->e_product_edition}}</span>
                                </div><!-- End .price-box -->

                                <div class="price-box">
                                    <span class="product-price">Tipe : {{$myEbookDetail->e_product_type}}</span>
                                </div><!-- End .price-box -->

                                <div class="product-single-share">
                                    {{-- <label>Share:</label>--}}

                                    <div class="addthis_inline_share_toolbox"></div>
                                </div><!-- End .product single-share -->
                            </div><!-- End .product-single-details -->
                        </div><!-- End .col-lg-5 -->
                    </div><!-- End .row -->
                </div><!-- End .product-single-container -->

                <div class="product-single-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="product-tab-desc" data-toggle="tab"
                               href="#product-desc-content" role="tab" aria-controls="product-desc-content"
                               aria-selected="true">Description</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel"
                             aria-labelledby="product-tab-desc">
                            <div class="product-desc-content">
                                <p>{!! $myEbookDetail->e_product_desc !!}</p>
                            </div><!-- End .product-desc-content -->
                        </div><!-- End .tab-pane -->
                    </div><!-- End .tab-content -->
                </div><!-- End .product-single-tabs -->

            @if(!empty($myEbookDetail->e_product_file))
                <div class="product-single-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="product-tab-desc" data-toggle="tab"
                               href="#product-desc-content" role="tab" aria-controls="product-desc-content"
                               aria-selected="true">Baca Majalah</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel"
                             aria-labelledby="product-tab-desc">
                            <div class="product-desc-content">
                                <iframe id="ebook-read" src="{{ asset_url('/frontend/assets/js/ViewerJS') }}/#../../../../document/{{ $myEbookDetail->e_product_file }}" width='100%' height='700' allowfullscreen webkitallowfullscreen>
                                </iframe>
                            </div><!-- End .product-desc-content -->
                        </div><!-- End .tab-pane -->
                    </div><!-- End .tab-content -->
                </div><!-- End .product-single-tabs -->
            @endif

            </div><!-- End .col-lg-9 -->

        </div>
        <!-- End .row -->
    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection