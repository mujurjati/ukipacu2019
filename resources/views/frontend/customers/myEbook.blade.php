@extends('frontend.customers.app')
@section('title', 'Edisi Saya')
@section('main-content')
    <div class="col-lg-9 order-lg-last dashboard-content">
        @if(count($myEbook) > 0)
        <div class="row">
            <div class="col-md-10">
                <h2>Edisi Saya</h2>
            </div>
        </div>

        <div class="row mt-2">
            @foreach($myEbook as $item)

                <div class="col-6 col-md-4">
                    <div class="product">
                        <figure class="product-image-container">
                            <a href="{{route('customers.myebook.detail', $item->e_product_id)}}" class="product-image">
                                @if(!empty($item->e_product_image))
                                    <img src="{{ get_image_ebook_product($item->e_product_image) }}" alt="my-ebook"
                                         style="width: 100%; height: 200px; object-fit: cover">
                                @else
                                    <img src="{{ get_image_product('default-black.png') }}" alt="my-ebook">
                                @endif
                            </a>
                        </figure>
                        <div class="product-details">
                            <div class="price-box">
                                <a href="{{route('customers.myebook.detail', $item->e_product_id)}}" class="product-price">{{$item->e_product_name}}</a>
                            </div>
                            <p>{{$item->e_product_shortdesc}}</p>
                            <div class="product-action">
                                <a href="{{route('customers.myebook.detail', $item->e_product_id)}}" class="btn btn-sm btn-danger" title="Add to Cart">
                                   Lihat
                                </a>
                            </div>
                        </div><!-- End .product-details -->
                    </div><!-- End .product -->
                </div><!-- End .col-md-4 -->
            @endforeach
        </div>
        <!-- End .row -->
        @else
            <div class="card">
                <div class="card-body text-center">
                    <p></p>
                    <h3>Belum ada ebook langganan</h3>
                    Setiap ebook langgananmu akan tersimpan di sini
                </div>
            </div>
        @endif
    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection