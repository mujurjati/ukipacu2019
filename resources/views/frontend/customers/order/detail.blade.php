@extends('frontend.customers.app')
@section('title', '#'.$dataOrder->order_number)
@section('main-content')
    <div class="card">
        <div class="card-header">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9">
                        <small class="text-muted">No Pesanan</small>
                        <br>
                        # {{ $dataOrder->order_number }}
                    </div>
                    <div class="col-md-3 text-right">
                        <small>Status Pesanan</small>
                        <br>
                        <strong>{{ order_status($dataOrder->order_status) }}</strong>
                        <br>
                        @if($dataOrder->order_status == 'shipping_process')
                            <button class="btn btn-sm btn-danger mt-1 act-receive-order"
                                    type="button"
                                    data-id="{{ $dataOrder->order_number }}"
                                    data-url="{{ route('customers.order.ajax.receive') }}"
                                    style="text-decoration: none; padding: 5px 10px; font-size: 10px">
                                Terima Barang
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body" style="background-color: #fefefe">
            <div class="row">
                <div class="col-md-6">
                    <small>ALAMAT PENGIRIMAN</small>
                    <br>
                    <strong class="text-dark">{{ $dataOrder->order_shipment_recipient }}</strong>
                    <br>
                    Telp. {{ $dataOrder->order_shipment_phone }}
                    <br>
                    {{ $dataOrder->order_shipment_address }},
                    {{-- {{ $dataOrder->subdistrict->s_subdistrict_name }},
                     {{ $dataOrder->subdistrict->city->s_city_name }},
                     {{ $dataOrder->subdistrict->city->province->s_province_name }},
                     {{ $dataOrder->subdistrict->city->s_city_postcode }}--}}
                </div>
                <div class="col-md-3">
                    <small>WAKTU PENGIRIMAN</small>
                    <br>
                    <strong>{{ date_indo($dataOrder->order_shipment_date,"datetime") }}</strong>
                    <br>
                    No Resi : {{ (( $dataOrder->order_shipment_resi ? $dataOrder->order_shipment_resi : '-' )) }}
                </div>
                <div class="col-md-3">
                    <small>METODE PEMBAYARAN</small>
                    <br>
                    <strong>TRANSFER BANK : {{ strtoupper($dataOrder->order_bank_to) }}</strong>
                    <address>
                        <a class="badge badge-success" data-toggle="modal" href="#maploc"
                           title="Klik untuk membuka MAP">Lokasi Pengiriman</a>
                        @component('backend.layouts.components.modal', [
                            'id'    => 'maploc',
                            'size'  => 'large',
                            'title' => 'Lokasi Pengiriman',

                            ])
                            <div id="map" style="height: 400px; width: 100%">
                                <iframe
                                        width="750"
                                        height="420"
                                        frameborder="0"
                                        scrolling="no"
                                        marginheight="0"
                                        marginwidth="0"
                                        src="https://maps.google.com/maps?q={{$dataOrder->order_shipment_lat}},{{$dataOrder->order_shipment_lng}}&hl=es;z=14&amp;output=embed"
                                >
                                </iframe>
                            </div>
                        @endcomponent
                    </address>
                </div>
            </div>
        </div>
    </div>

    @foreach($dataOrder->detail as $items)
        <div class="card mt-10">
            <div class="card-body" style="background-color: #f7f7f7">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9">
                            <h5>
                                <strong> {{ $items->merchant->merchant_name }} </strong><br>
                            </h5>
                        </div>
                        <div class="col-md-3 text-right">
                            <span id="status-{{ $items->o_detail_id }}">{!! order_status_label($items->o_detail_status) !!}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body" style="background-color: #fefefe">
                <table class="table table-bordered">
                    <tr style="background: #f0f0f0">
                        <td>Produk</td>
                        <td>Kuantitas</td>
                        <td class="text-right">Subtotal</td>
                    </tr>

                    @foreach($items->detail_product as $item)
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="{{ get_image_product($item->od_product_image) }}"
                                             alt="{{ $item->od_product_name }}" style="width: 80%">
                                    </div>
                                    <div class="col-md-10 pt-2">
                                        {{ $item->od_product_name }}
                                    </div>
                                </div>
                            </td>
                            <td align="center">x {{ $item->od_product_qty }}</td>
                            <td width="170" class="text-right">
                                Rp {{ price_format($item->od_product_subtotal) }}
                            </td>
                        </tr>
                    @endforeach

                </table>
                <table style="width: 100%">
                    <tr>
                        <td class="text-right" colspan="2">Subtotal</td>
                        <td class="text-right" width="200">Rp {{ price_format($items->o_detail_subtotal) }}</td>
                    </tr>
                    <tr>
                        <td class="text-right" colspan="2">Biaya Pengiriman</td>
                        <td class="text-right" width="200">Rp {{ price_format($items->o_detail_shipping_price) }}</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-bottom: 1px solid #ddd"></td>
                    </tr>
                    <tr>
                        <td class="text-left"><strong>Driver : {{ (!empty($items->driver) ? $items->driver->driver_name : '-') }}</strong><br>
                        @if(!empty($items->driver))
                            <a class="badge badge-primary" data-toggle="modal" href="#maplocDriver"
                               title="Klik untuk membuka MAP">Lokasi Driver</a>
                            @component('backend.layouts.components.modal', [
                            'id'    => 'maplocDriver',
                            'size'  => 'large',
                            'title' => 'Lokasi Driver',

                            ])
                                <div id="map" style="height: 400px; width: 100%">
                                    <iframe
                                            width="750"
                                            height="420"
                                            frameborder="0"
                                            scrolling="no"
                                            marginheight="0"
                                            marginwidth="0"
                                            src="https://maps.google.com/maps?q={{$items->driver->driver_location_lat}},{{$items->driver->driver_location_lng}}&hl=es;z=14&amp;output=embed"
                                    >
                                    </iframe>
                                </div>
                            @endcomponent
                        @endif
                        </td>
                        <td class="text-right"><strong class="product-price">Total</strong></td>
                        <td class="text-right" width="200"><strong
                                    class="product-price">Rp {{ price_format($items->o_detail_total) }}</strong></td>
                    </tr>
                </table>
            </div>

        </div>
    @endforeach
    <div class="card mt-4">
        <div class="card-body" style="background-color: #f7f7f7">
            <table style="width: 100%">
                <tr>
                    <td class="text-right">Total Barang</td>
                    <td class="text-right" width="200">Rp {{ price_format($dataOrder->order_subtotal) }}</td>
                </tr>
                <tr>
                    <td class="text-right">Kode Unik</td>
                    <td class="text-right" width="200">Rp {{ price_format($dataOrder->order_unique_code) }}</td>
                </tr>
                <tr>
                    <td class="text-right">Diskon Voucher</td>
                    <td class="text-right" width="200">Rp {{ price_format($dataOrder->order_discount_price) }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 1px solid #ddd"></td>
                </tr>
                <tr>
                    <td class="text-right"><strong class="product-price">Total Pesanan</strong></td>
                    <td class="text-right" width="200"><strong
                                class="product-price">Rp {{ price_format($dataOrder->order_total) }}</strong></td>
                </tr>
            </table>
        </div>
    </div>

    <script>
        var _token = '{{ csrf_token() }}'
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/jquery.preloaders.min.js') }}"></script>
    <script src="{{ asset_url('/frontend/assets/js/order.js') }}"></script>
@endsection