@extends('frontend.customers.app')
@section('title', 'Daftar Pembelian')
@section('main-content')
    <div id="download" role="tabpanel">
        <div class="myaccount-content">
            <div class="row bottommargin-sm">
                <div class="col-md-4">
                    {{
                        Form::select('', [
                            ""                  => "Semua Status",
                            "pending_payment"   => "Menunggu Pembayaran",
                            "paid"              => "Terbayar",
                            "process"           => "Sedang Diproses",
                            "ready_shipment"    => "Menunggu Pengiriman",
                            "shipping_process"  => "Dalam Pengiriman",
                            "received"          => "Terkirim",
                            "finish"            => "Selesai",
                            "cancel"            => "Sudah Dibatalkan"
                        ], $selectStatus, ['class' => 'form-control', 'id' => 'orderFilterStatus'])
                    }}
                </div>
            </div>

    @if($dataOrder->count() > 0)
        @foreach($dataOrder->get() as $item)
            <div class="card">
                <div class="card-body" style="background-color: #fafafa">
                    <div class="row mb-1">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12"><small>No Transaksi</small></div>
                                <div class="col-md-12">
                                    <a href="{{ route('customers.order.detail', [$item->order_number]) }}">
                                        <strong class="text-dark">{{ $item->order_number }}</strong>
                                    </a>
                                </div>
                                <div class="col-md-12">
                                    <small>
                                        {{ date_indo($item->order_create_date, 'dd FF YYYY') }}
                                        {{ date('H.i', strtotime($item->order_create_date)) }} WIB
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12"><small>Status Transaksi</small></div>
                                <div class="col-md-12"><strong class="text-dark">{{ order_status($item->order_status) }}</strong></div>
                            @if($item->order_status == 'pending_payment')
                                @php
                                    $expDate = date('Y-m-d H:i:s', strtotime('+1 Day', strtotime($item->order_create_date)));
                                @endphp
                                <div class="col-md-12">
                                    <small>
                                        Batas pembayaran :
                                        <strong>
                                            {{ date_indo($expDate) }}
                                            {{ date('H.i', strtotime($expDate)) }} WIB
                                        </strong>
                                    </small>
                                </div>
                            @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="row">
                                <div class="col-md-12"><small>Total Transaksi</small></div>
                                <div class="col-md-12"><strong class="text-dark">Rp {{ price_format($item->order_total) }}</strong></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <a href="{{ route('customers.order.detail', [$item->order_number]) }}" class="btn btn-sm btn-outline-danger" style="text-decoration: none; padding: 5px 10px; font-size: 10px">
                                        Lihat Detail
                                    </a>
                                @if($item->order_status == 'pending_payment')
                                    <button class="btn btn-sm btn-default mt-1 act-cancel-order"
                                        type="button"
                                        data-id="{{ $item->order_number }}"
                                        data-url="{{ route('customers.order.ajax.cancel') }}"
                                        style="text-decoration: none; padding: 5px 10px; font-size: 10px">
                                        Batalkan
                                    </button>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>

                @foreach($item->detail as $keys)
                    @foreach($keys->detail_product as $key)
                        <div class="row" style="background: #fff;border-top: 1px solid #eee">
                            <div class="col-md-1">
                                <img src="{{ get_image_product($key->od_product_image) }}" alt="{{ $key->od_product_name }}">
                            </div>
                            <div class="col-md-4 pt-2">
                                <a href="{{ route('product.detail', [@$key->product->product_alias]) }}">
                                    <small>{{ $key->od_product_name }}</small>
                                </a>
                            </div>
                            <div class="col-md-3 pt-2">
                                <small>Rp {{ price_format($key->od_product_price) }} x {{ $key->od_product_qty }}</small>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-2 pt-2 text-center">
                            @if(in_array($item->order_status, array('received', 'finish', 'cancel', 'refund')))
                                <a href="{{ route('product.detail', [@$key->product->product_alias]) }}" class="btn btn-sm" style="text-decoration: none; padding: 5px 10px; font-size: 10px">
                                    Beli Lagi
                                </a>
                            @endif
                            </div>
                        </div>
                    @endforeach
                @endforeach
                </div>
            </div>
        @endforeach
    @else
        <div class="card">
            <div class="card-body text-center">
                <p></p>
                <h3>Belum ada transaksi pembelian</h3>
                Setiap transaksi pembelianmu akan tersimpan di sini
            </div>
        </div>
    @endif
    </div>

    <script>
        var _token = '{{ csrf_token() }}'
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/jquery.preloaders.min.js') }}"></script>
    <script src="{{ asset_url('/frontend/assets/js/order.js') }}"></script>

    <script>
        $(function () {
            $("#orderFilterStatus").on("change", function () {
                var status = $(this).val();

                location.href = '{{ route('customers.order') }}/'+status;
            });
        });
    </script>
@endsection