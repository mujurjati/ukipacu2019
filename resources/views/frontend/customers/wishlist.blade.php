@extends('frontend.customers.app')

@section('title', 'Produk Favorit')

@section('main-content')
    <div class="col-lg-12 order-lg-last dashboard-content">
        @if(count($dataWishlist) > 0)
            <div class="row">
                <div class="col-md-10">
                    <h2>Produk Favorit</h2>
                </div>
            </div>

            <div class="row mt-2">
                @foreach($dataWishlist as $item)
                    <div class="col-6 col-md-3">
                        <div class="product">
                            <figure class="product-image-container">
                                <a href="{{route('product.detail', ['alias' => $item->product->product_alias])}}"
                                   class="product-image">
                                    @if(!empty($item->product_image))
                                        <img src="{{ asset_url('/images/products/'.$item->product->product_image)}}"
                                             alt="product" style="width: 100%; height: 200px; object-fit: cover">
                                    @else
                                        <img src="{{ asset_url('/images/products/default-black.png') }}"
                                             alt="product">
                                    @endif
                                </a>
                                <a href="{{route('product.detail', ['alias' => $item->product->product_alias])}}"
                                   class="btn-quickview">Detail Produk</a>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:{{$item->product->avgRating}}%"></span>
                                        <!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h5 class="product-title">
                                    <a href="{{route('product.detail', ['alias' => $item->product->product_alias])}}">{{$item->product->product_name}}</a>
                                </h5>
                                <div class="price-box">
                                    @if($item->product->product_discount > 0)
                                        <span class="old-price">Rp. {{ price_format($item->product->product_price) }}</span>
                                    @endif
                                    <span class="product-price">Rp. {{price_format($item->product->product_price-$item->product->product_discount)}}</span>
                                </div><!-- End .price-box -->
                                <div class="product-action">
                                    <button type="button" class="btn btn-sm btn-danger delWishlist" title="Hapus Produk"
                                        data-alias="{{ $item->product->product_alias }}"
                                        data-name="{{ $item->product->product_name }}" >
                                        Hapus
                                    </button>
                                </div>
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->
                    </div>
                @endforeach
            </div>
            <!-- End .row -->
            <div class="row">
                <div class="col-md-12">
                    <nav class="toolbox toolbox-pagination mt-10">
                        {{ $dataWishlist->links("pagination::bootstrap-4") }}
                    </nav>
                </div>
            </div>
        @else
            <div class="card">
                <div class="card-body text-center">
                    <p></p>
                    <h3>Belum ada produk favorit</h3>
                    Setiap produk favorit mu akan tersimpan di sini
                </div>
            </div>
        @endif
    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/jquery.preloaders.min.js') }}"></script>
    <script>
        $(function () {
            $(".delWishlist").on("click", function() {
                var alias = $(this).data('alias');
                var name = $(this).data('name');

                swal({
                    title: name,
                    text: "Anda yakin akan menghapus produk ini ?",
                    icon: "warning",
                    buttons: ["Tidak", "Hapus"],
                    dangerMode: true
                }).then(function(willDelete) {
                    if(willDelete) {
                        $.preloader.start({
                            modal: true,
                            src: 'sprites.png'
                        });

                        $.ajax({
                            url: '{{ route('customers.wishlist.delete') }}',
                            type: 'post',
                            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                            data: { alias: alias },
                            dataType: 'json',
                            success: function (res) {
                                $.preloader.stop();

                                if (res.success) {
                                    swal("Berhasil !", "Produk berhasil di hapus", "success")
                                        .then(function () {
                                            location.reload();
                                        });
                                }
                                else {
                                    swal("Gagal !", "Terjadi Kesalahan", "error");
                                }
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection