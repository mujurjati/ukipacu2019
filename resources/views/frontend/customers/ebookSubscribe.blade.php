@extends('frontend.customers.app')
@section('title', 'Paket Saya')
@section('main-content')
    <div class="col-lg-9 order-lg-last dashboard-content">
        @if(count($dataEbook) > 0)
        <div class="row">
            <div class="col-md-10">
                <h2>Paket Saya</h2>
            </div>
        </div>

        <div class="row mt-2">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Paket</th>
                    <th>Tipe</th>
                    <th>Durasi</th>
                    <th>Tanggal Langganan</th>
                    <th>Harga</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataEbook as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->package->e_package_name}}</td>
                        <td>{{ucwords($item->e_subscribe_type)}}</td>
                        <td>{{$item->e_subscribe_time}} Bulan</td>
                        <td>{{date_indo($item->e_subscribe_active_date, 'datetime')}}</td>
                        <td>Rp. {{price_format($item->e_subscribe_price)}}</td>
                        <td>{{label_status_ebook($item->e_subscribe_status)}}</td>
                        @if($item->e_subscribe_status == 'pending')
                        <td>
                            <a href="#" class="btn btn-sm btn-outline-danger update-langganan"
                               data-no="{{$item->e_subscribe_id}}" data-title="{{$item->package->e_package_name}}"
                                style="text-decoration: none; padding: 5px 10px; font-size: 10px"> Cancel
                            </a>
                        </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- End .row -->
        @else
            <div class="card">
                <div class="card-body text-center">
                    <p></p>
                    <h3>Belum ada paket langganan</h3>
                    Setiap paket langgananmu akan tersimpan di sini
                </div>
            </div>
        @endif
    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/ebook.js') }}"></script>
    <script>
        var _token = '{{ csrf_token() }}';
    </script>
@endsection