<html>
<head>
    <title>Ukipacu - Reset Password</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
        }
        input {
            border: 1px solid #ddd;
            padding: 5px 10px;
        }
        td {
            padding: 5px
        }
        button {
            background: #5cb85c;
            color: white;
            border: 1px solid #555;
            padding: 5px 10px;
            cursor: pointer;
        }
        h3 {
            font-size: 15px;
            color: #5cb85c;
            margin-bottom: 15px;
            text-align: left;
            padding-left: 5px;
        }
        .wrap {
            width: 70%;
            margin-top: 50px;
            text-align: left;
            border: 1px solid #ddd
        }
        .header {
            padding: 15px;
            border-bottom: 3px solid #5cb85c;
        }
        .logo {
            height: 70px;
        }
        .body {
            padding: 15px;
            text-align: center;
        }
        .body h1 {
            font-size: 23px;
            text-align: center;
        }
        .success {
            color: #5cb85c;
        }
        .error {
            color: #5cb85c;
        }
    </style>
</head>
<body>

<center>
    <div class="wrap">
        <div class="header">
            <img class="logo" src="{{asset_url('/frontend/images/logo.png')}}" />
        </div>
        <div class="body">
            <form method="post" action="{{route('customers.reset.act', $data)}}">
                {{ csrf_field() }}
                <table border="0">
                    <tr>
                        <td rowspan="2">Password Baru</td>
                        <td rowspan="2">:</td>
                        <td>
                            <input type="password" id="password" name="password" placeholder="Masukan Password Baru" />
                        </td>
                    </tr>
                    <tr>
                        <td><span id="error_password"><code>Masukan kombinasi huruf kecil, besar dan numerik. Minimal 8 karakter</code></span></td>
                    </tr>
                    <tr>
                        <td rowspan="2">Konfirmasi Password</td>
                        <td rowspan="2">:</td>
                        <td>
                            <input type="password" id="confirm_password" name="conf_password" placeholder="Ulangi Password" />
                        </td>
                    </tr>
                    <tr>
                        <td><span id="error_confirm"><code>Konfirmasi password baru</code></span></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right">
                            <button id="submit" name="resetPass">
                                SIMPAN
                            </button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</center>
</body>
</html>
<script src="{{ asset_url('/frontend/assets/js/jquery.min.js') }}"></script>
<script>
    var $password = $("#password");
    var $confirmPassword = $("#confirm_password");
    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;

    //Hide Hints
    $("tr td span").hide();

    function isPasswordValid() {
        return $password.val().match(passw);
    }

    function arePasswordsMatching () {
        return $password.val() === $confirmPassword.val();
    }

    function canSubmit() {
        return isPasswordValid() && arePasswordsMatching();
    }

    function passwordEvent() {
        if(isPasswordValid()) {
            $("#error_password").hide();
        } else {
            $("#error_password").show();
        }
    }

    function confirmPasswordEvent() {
        if(arePasswordsMatching()) {
            $("#error_confirm").hide();
        } else {
            $("#error_confirm").show();
        }
    }

    function enableSubmitEvent(){
        $("#submit").prop("disabled", !canSubmit());
    }

    $password.focus(passwordEvent).keyup(passwordEvent).keyup(confirmPasswordEvent).keyup(enableSubmitEvent);
    $confirmPassword.focus(confirmPasswordEvent).keyup(confirmPasswordEvent).keyup(enableSubmitEvent);

    enableSubmitEvent();
</script>