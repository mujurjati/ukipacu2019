@extends('frontend.customers.app')
@section('title', 'Alamat')
@section('main-content')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />

    @if(count($dataAdd) > 0)
    <div id="download" role="tabpanel">
        <div class="myaccount-content">
            <div class="row">
                <div class="col-md-8">
                    <h3>Alamat Pengiriman</h3>
                </div>
                <div class="col-md-2 text-right">
                    <a href="#addAdd" data-toggle="modal" class="btn btn__bg btn__sqr">Tambah Alamat</a>
                </div>
            </div>
            <div class="myaccount-table table-responsive text-center">
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th></th>
                        <th>Penerima</th>
                        <th>Alamat Pengiriman</th>
                        <th>Daerah pengiriman</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dataAdd as $key => $item)
                        <tr class="row{{$item->c_address_id}}">
                            <td class="text-center">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline{{$key}}" value="{{$item->c_address_id}}"
                                           name="pengiriman_id"
                                           class="update-add custom-control-input mb-2" {{$item->c_address_primary == 1 ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="customRadioInline{{$key}}"></label>
                                </div>
                            </td>
                            <td>
                            <span>
                                {{$item->c_address_name}}
                            </span><br>
                                <span>
                                {{$item->c_address_phone}}
                            </span>
                            </td>
                            <td>
                                <strong>{{$item->c_address_type}}</strong> <br>
                                <span>
                                {{$item->c_address_address}}
                            </span>
                            </td>
                            <td>
                            <span>
                                {{
                                    $item->subdistrict->s_subdistrict_name.', '.
                                    $item->subdistrict->city->s_city_name
                                }}
                            </span><br>
                                <span>
                                {{
                                    $item->subdistrict->city->province->s_province_name.', '.
                                    $item->subdistrict->city->s_city_postcode
                                }}
                            </span>
                            </td>
                            <td>
                                <div class="collapse show mt-2" id="widget-body-3">
                                    <div class="widget-body">
                                        <ul class="config-size-list">
                                            <li>
                                                <a href="#addEdit{{$key}}" data-toggle="modal" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="del-addrCus" data-no="{{$item->c_address_id}}"><i
                                                            class="fa fa-times" aria-hidden="true"></i></a>
                                            </li>
                                        </ul>
                                    </div><!-- End .widget-body -->
                                </div>
                                {{ Form::open(['route' => ['customers.addressUpdate', $item->c_address_id], 'method' => 'post']) }}
                                {{ Form::token() }}
                                {{ Form::hidden('c_address_primary', $item->c_address_primary) }}
                                @component('backend.layouts.components.modal', [
                                                    'id'    => 'addEdit'.$key,
                                                    'title' => 'Edit Alamat Pengiriman'
                                                ])
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                {{ Form::label('alamat', 'Nama Alamat', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                <div class="col-sm-12">
                                                    {{ Form::text('alamat', $item->c_address_name, ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh: Alamat Rumah, Kantor, Apartemen, Dropship', 'style' => 'max-width: 100%']) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        {{ Form::label('name', 'Nama Penerima', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                        <div class="col-sm-12">
                                                            {{ Form::text('name', $item->c_address_name, ['class' => 'form-control form-control-sm']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        {{ Form::label('phone', 'Nomor HP', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                        <div class="col-sm-12">
                                                            {{ Form::text('phone', $item->c_address_phone, ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh: 081234567890']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row text-left">
                                                {{ Form::label('city', 'Kota atau Kecamatan', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                <div class="col-sm-12">
                                                    <select name="city" class="select-address">
                                                        <option value="{{$item->s_subdistrict_id}}">
                                                            {{
                                                            $item->subdistrict->s_subdistrict_name.', '.
                                                            $item->subdistrict->city->s_city_name.', '.
                                                            $item->subdistrict->city->province->s_province_name.', '.
                                                            $item->subdistrict->city->s_city_postcode}}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                {{ Form::label('alamat_lengkap', 'Alamat', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                <div class="col-sm-12">
                                                    {{ Form::textarea('alamat_lengkap', $item->c_address_address, ['class' => 'form-control form-control-sm', 'placeholder' => 'Alamat Lengkap', 'style' => 'max-width: 100%']) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @slot('btn')
                                        <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                    @endslot
                                @endcomponent
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
        <div class="card">
            <div class="card-body text-center">
                <p></p>
                <h3>Belum ada data Alamat Pengiriman</h3>
                Setiap data alamatmu akan tersimpan di sini
            </div>
        </div>
    @endif

    {{--ADD ADDRESS MODAL--}}
    {{ Form::open(['route' => 'customers.addressSave', 'method' => 'post']) }}
    {{ Form::token() }}
    @component('backend.layouts.components.modal', [
                        'id'    => 'addAdd',
                        'title' => 'Tambah Alamat Pengiriman'
                    ])
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row">
                    {{ Form::label('alamat', 'Nama Alamat', ['class' => 'col-sm-12 col-form-label text-left']) }}
                    <div class="col-sm-12">
                        {{ Form::text('alamat', '', ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh: Alamat Rumah, Kantor, Apartemen, Dropship', 'style' => 'max-width: 100%']) }}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            {{ Form::label('name', 'Nama Penerima', ['class' => 'col-sm-12 col-form-label text-left']) }}
                            <div class="col-sm-12">
                                {{ Form::text('name', $dataCus->customer_name, ['class' => 'form-control form-control-sm']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            {{ Form::label('phone', 'Nomor HP', ['class' => 'col-sm-12 col-form-label text-left']) }}
                            <div class="col-sm-12">
                                {{ Form::text('phone', $dataCus->customer_phone, ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh: 081234567890']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group row text-left">
                    {{ Form::label('city', 'Kota atau Kecamatan', ['class' => 'col-sm-12 col-form-label text-left']) }}
                    <div class="col-sm-12">
                        <select name="city" id="city" class="select-address">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group row">
                    {{ Form::label('alamat_lengkap', 'Alamat', ['class' => 'col-sm-12 col-form-label text-left']) }}
                    <div class="col-sm-12">
                        {{ Form::textarea('alamat_lengkap', '', ['class' => 'form-control form-control-sm', 'placeholder' => 'Alamat Lengkap', 'style' => 'max-width: 100%']) }}
                    </div>
                </div>
            </div>
        </div>
        @slot('btn')
            <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
        @endslot
    @endcomponent
    {{ Form::close() }}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/address.js') }}"></script>
    <script>
        var _token = '{{ csrf_token() }}';
        $(document).ready(function () {
            $('.select-address').select2({
                theme: "bootstrap",
                placeholder: 'Pilih Kota atau Kecamatan',
                dropdownAutoWidth: true,
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('ajax.get-address') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });
        });
    </script>
@endsection