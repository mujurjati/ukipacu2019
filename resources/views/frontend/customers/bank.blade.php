@extends('frontend.customers.app')
@section('title', 'Bank')
@section('main-content')
    <div id="download" role="tabpanel">
        <div class="myaccount-content">
            <div class="row">
                <div class="col-md-9">
                    <h3>Akun Bank</h3>
                </div>
                <div class="col-md-2 text-right">
                    <a href="#addBank" data-toggle="modal" class="button button-circle">Tambah Bank</a>
                </div>
            </div>
            @if(count($dataBank) > 0)
                <div class="myaccount-table table-responsive text-center">
                    <table class="table table-bordered">
                        <thead class="thead-light">
                        <tr>
                            <th>No</th>
                            <th>Bank</th>
                            <th>Bank Akun</th>
                            <th>Nomor Rekening</th>
                            <th>Cabang</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dataBank as $key => $item)
                            <tr class="row{{$item->c_bank_id}}">
                                <td>{{$key+1}}</td>
                                <td>
                                    <img src="{{get_image_bank($item->bank->bank_image)}}" width="55px;" alt="img-bank">
                                </td>
                                <td>{{$item->c_bank_acc}}</td>
                                <td>{{$item->c_bank_number}}</td>
                                <td>{{$item->c_bank_branch}}</td>
                                <td>
                                    <a href="#bankEdit{{$key}}" data-toggle="modal"><i
                                                class="icon-edit" aria-hidden="true"></i></a>

                                    <a href="#" class="del-bankCus" data-no="{{$item->c_bank_id}}"><i
                                                class="icon-remove" aria-hidden="true"></i></a>

                                </td>
                                {{ Form::open(['route' => ['customers.bankUpdate', $item->c_bank_id], 'method' => 'post']) }}
                                {{ Form::token() }}
                                @component('backend.layouts.components.modal', [
                                                    'id'    => 'bankEdit'.$key,
                                                    'title' => 'Edit Bank'
                                                ])
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        {{ Form::label('c_bank_name', 'Bank', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                        <div class="col-sm-12">
                                                            <select name="c_bank_name" class="form-control">
                                                                @foreach($dataFromBank as $bank)
                                                                    <option value="{{$bank->bank_name}}" {{$item->c_bank_name == $bank->bank_name ? 'selected' : ''}}>{{$bank->bank_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        {{ Form::label('c_bank_branch', 'Bank Cabang', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                        <div class="col-sm-12">
                                                            {{ Form::text('c_bank_branch', $item->c_bank_branch, ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh : KCP Sleman']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        {{ Form::label('c_bank_acc', 'Bank Akun', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                        <div class="col-sm-12">
                                                            {{ Form::text('c_bank_acc', $item->c_bank_acc, ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh: Mujur jati']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        {{ Form::label('c_bank_number', 'Nomor Rekening', ['class' => 'col-sm-12 col-form-label text-left']) }}
                                                        <div class="col-sm-12">
                                                            {{ Form::text('c_bank_number', $item->c_bank_number, ['class' => 'form-control form-control-sm']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @slot('btn')
                                        <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                    @endslot
                                @endcomponent
                                {{ Form::close() }}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- End .row -->
        </div>
        @else
            <div class="card">
                <div class="card-body text-center">
                    <p></p>
                    <h3>Belum ada Data Bank</h3>
                    Setiap Data Bank akan tersimpan di sini
                </div>
            </div>
        @endif
    </div>

    {{--ADD ADDRESS MODAL--}}
    {{ Form::open(['route' => 'customers.bankAdd', 'method' => 'post']) }}
    {{ Form::token() }}
    @component('backend.layouts.components.modal', [
                        'id'    => 'addBank',
                        'title' => 'Tambah Akun Bank'
                    ])
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            {{ Form::label('c_bank_name', 'Bank', ['class' => 'col-sm-12 col-form-label text-left']) }}
                            <div class="col-sm-12">
                                <select name="c_bank_name" class="form-control">
                                    <option value=""> -- Pilih Bank --</option>
                                    @foreach($dataFromBank as $bank)
                                        <option value="{{$bank->bank_name}}">{{$bank->bank_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            {{ Form::label('c_bank_branch', 'Bank Cabang', ['class' => 'col-sm-12 col-form-label text-left']) }}
                            <div class="col-sm-12">
                                {{ Form::text('c_bank_branch', '', ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh : KCP Sleman']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            {{ Form::label('c_bank_acc', 'Bank Akun', ['class' => 'col-sm-12 col-form-label text-left']) }}
                            <div class="col-sm-12">
                                {{ Form::text('c_bank_acc', '', ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh: Mujur jati']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            {{ Form::label('c_bank_number', 'Nomor Rekening', ['class' => 'col-sm-12 col-form-label text-left']) }}
                            <div class="col-sm-12">
                                {{ Form::text('c_bank_number', '', ['class' => 'form-control form-control-sm']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @slot('btn')
            <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
        @endslot
    @endcomponent
    {{ Form::close() }}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/bankCustomer.js') }}"></script>
    <script>
        var _token = '{{ csrf_token() }}';
    </script>
@endsection