@extends('frontend.layouts.app')
@section('title', 'Login - Register')
@section('content')
    <link href="{{ asset_url('/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_url('/css/select2-bootstrap.min.css') }}" rel="stylesheet" />
    <!-- login register wrapper start -->
    <div class="login-register-wrapper topmargin-lg">
    @include('backend.layouts.info')
        <div class="container">
            <div class="member-area-from-wrap">
                <div class="row">
                <div class="col-12"><h3 for="exampleInputEmail1"><span class="">Login</span></h3></div>
                    <div class="col-6" style="margin-top: 10px;">
                        <form role="form" method="POST" action="{{ route('customers.login.act') }}">
                        {{ csrf_field() }}
                        <!-- <div class="form-group">
                            <div class="input-group mb-2">
                                <input type="text" name="login_email" class="form-control"
                                    id="inlineFormInputGroup" placeholder="Email">
                            </div>
                            <div class="input-group mb-2">
                                <input type="password" name="login_password" class="form-control"
                                    id="inlineFormInputGroup" placeholder="Password">
                            </div>
                            <a href="#myModal2" data-lightbox="inline"
                            style="margin-left: 10px;">Lupa Password <i class="icon-line-arrow-right"></i></a>
                        </div> -->
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" name="login_email" class="form-control"
                                    id="inlineFormInputGroup" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="password" name="login_password" class="form-control"
                                    id="inlineFormInputGroup" placeholder="Password">
                            </div>
                        </div>
                        <a href="#myModal2" data-lightbox="inline"
                            style="margin-left: 10px;">Lupa Password <i class="icon-line-arrow-right"></i></a>
                            &nbsp; /
                        <a href="#" id="hideshow"
                            style="margin-left: 10px; color: burlywood">Belum punya akun? Silahkan daftar !! <i class="icon-line-arrow-down"></i></a>
                        <div style="margin-bottom: 30px; float:right">
                            <button type="submit" class="button button-small button-circle"
                                    style="margin-top: -10px; margin-bottom:-10px; background: #5cb85c;">Masuk
                            </button>
                        </div>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="row" id="content">
                    <div class="col-12">
                        <h3 for="exampleInputEmail1"><span class="">Daftar</span></h3>
                    </div>
                    <div class="col-12">                    
                        <form style="margin-top: 10px" action="{{ route('customers.register.act') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-6">
                                <h4 for="exampleInputEmail1"><span class="">Data Member</span></h4>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Gambar</label>
                                        <div class="col-sm-5">
                                            @component('backend.libs.image.input', [
                                                'name'      => 'customer_image',
                                                'accept'    => ['image/jpeg', 'image/png']
                                            ])
                                            Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                        @endcomponent

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Nama Lengkap</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_name" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Email</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_email" type="email">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" placeholder="Misalkan : 16 Desember 1994" name="customer_tlg" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">No. KTP</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_no_ktp" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="customer_address" type="text" rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Kota atau Kecamatan</label>
                                        <div class="col-sm-9">
                                            <select name="city" id="city" class="form-control select-address">
                                                <option value="">======= Silahkan Pilih Alamat ===================</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">NPWP</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_npwp" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Phone / WA</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_phone" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_password" type="password">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                <h4 for="exampleInputEmail1"><span class="">Data Penungjang</span></h4>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Pendidikan Terakhir</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_pendidikan" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Pekerjaan</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_perkerjaan" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Agama</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_agama" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Nama Ibu Kandung</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_ibu" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Nama Ahli Waris</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_waris" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Hubungan dengan Ahli Waris</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_hub_waris" type="text">
                                        </div>
                                    </div>

                                    <h4 for="exampleInputEmail1"><span class="">Data Bank</span></h4>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Nama Bank</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_bank_name" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Cabang</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_bank_cabang" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Nama Nasabah</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_bank_nasabah" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  class="col-sm-3 col-form-label">Nomor Rekening</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="customer_bank_rek" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12" style="text-align: right">
                                    <button class="button button-small button-circle" name="addCusBySelf" value="1" type="submit" style="margin-top: 10px; margin-bottom:10px; background: #5cb85c;">Daftar</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <!-- login register wrapper end -->
    @include('flashy::message')
@endsection
<!-- @section('load-css')
    @include('backend.libs.image.multiple.multiple_css')
@stop -->
@section('load-script')
    <!-- @include('backend.libs.image.image_js') -->
    <script src="{{ asset_url('/js/select2.min.js') }}"></script>
    <script>
        var _token = '{{ csrf_token() }}';
        $(document).ready(function(){
            $('#content').toggle('hide');
            $('#hideshow').on('click', function(event) {        
                $('#content').toggle('show');
            });
        });

        $(document).ready(function () {
            $('.select-address').select2({
                theme: "bootstrap",
                placeholder: 'Pilih Kota atau Kecamatan',
                selectOnClose: true,
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('ajax.get-address') }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    dataType: 'json',
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    }
                }
            });
        });
    </script>
@stop