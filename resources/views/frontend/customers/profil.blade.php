@extends('frontend.customers.app')
@section('title', 'Informasi Akun')
@section('main-content')

    <div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content"
         aria-labelledby="ui-id-41" role="tabpanel" aria-hidden="false" style="">
        {{ Form::open(['route' => 'customers.profil.save', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        {{ Form::token() }}
            <div class="form-group col-md-3">
                {{ Form::label('foto', 'Foto', ['for' => 'acc-email']) }}
                @component('backend.libs.image.input', [
                    'name'      => 'gambar',
                    'accept'    => ['image/jpeg', 'image/png'],
                    'ratio'     => '1:1',
                    'src'       => (!empty($dataCus->customer_image)) ? asset_url('/images/customer/'.$dataCus->customer_image) : ''
                ])
                    Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                @endcomponent
            </div>
            <div class="form-group">
                {{ Form::label('', 'Nama Akun', ['for' => 'acc-name']) }}
                {{ Form::text('customer_name', $dataCus->customer_name, ['class' => 'form-control', 'id' => 'acc-name']) }}
            </div>
            <div class="form-group">
                {{ Form::label('tipe', 'Email', ['for' => 'acc-name']) }}
                {{ Form::text('email', $dataCus->customer_email, ['class' => 'form-control', 'id' => 'acc-name', 'readonly']) }}
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{ Form::label('tipe', 'Tempat lahir', ['for' => 'acc-name']) }}
                    {{ Form::text('customer_birth_place', $dataCus->customer_birth_place, ['class' => 'form-control', 'id' => 'acc-name']) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('tipe', 'Tanggal lahir', ['for' => 'acc-name']) }}
                    <input type="text" name="customer_birth_date" class="form-control"
                           placeholder="Contoh: 1994-12-16"
                           value="{{$dataCus->customer_birth_date}}">
                </div>
            </div>
            <div class="form-group">
                <label for="acc-password">Jenis Kelamin</label>
                <div class="custom-control custom-radio custom-control-inline ml-3">
                    <input type="radio" id="customRadioInline1" value="m" name="gender"
                           class="custom-control-input" {{($dataCus->customer_gender == 'm' ? 'checked' : '')}}>
                    <label class="custom-control-label" for="customRadioInline1">Pria</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline2" value="f" name="gender"
                           class="custom-control-input" {{($dataCus->customer_gender == 'f' ? 'checked' : '')}}>
                    <label class="custom-control-label" for="customRadioInline2">Wanita</label>
                </div>
            </div>
            <div class="form-row">
                <button type="submit" class="button button-circle">Simpan</button>
            </div>

        {{ Form::close() }}
    </div>

    <script type="text/javascript">
        $(function () {
            $(".pickadate").pickadate({format: "yyyy-mm-dd", formatSubmit: "yyyy-mm-dd", hiddenName: true});
        });
    </script>

    {{--<div id="account-info" role="tabpanel">
        <div class="myaccount-content">
            <h3>Informasi Akun</h3>
            <div class="account-details-form">
                {{ Form::open(['route' => 'customers.profil.save', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                {{ Form::token() }}
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group required-field">
                                {{ Form::label('foto', 'Foto', ['for' => 'acc-email']) }}
                                @component('backend.libs.image.input', [
                                    'name'      => 'gambar',
                                    'accept'    => ['image/jpeg', 'image/png'],
                                    'src'       => (!empty($dataCus->customer_image)) ? asset_url('/images/customer/'.$dataCus->customer_image) : ''
                                ])
                                    Drag & Drop your picture or <span class="filepond--label-action">Browse</span>
                                @endcomponent
                            </div><!-- End .row -->
                        </div><!-- End .col-sm-11 -->
                    </div><!-- End .row -->
                    <div class="single-input-item">
                        {{ Form::label('', 'Nama Akun', ['for' => 'acc-name']) }}
                        {{ Form::text('customer_name', $dataCus->customer_name, ['class' => 'form-control', 'id' => 'acc-name']) }}
                    </div>
                    <div class="single-input-item">
                        {{ Form::label('tipe', 'Email', ['for' => 'acc-name']) }}
                        {{ Form::text('email', $dataCus->customer_email, ['class' => 'form-control', 'id' => 'acc-name', 'readonly']) }}
                    </div>
                    <fieldset>
                        <legend>Detail Akun</legend>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    {{ Form::label('tipe', 'Tempat lahir', ['for' => 'acc-name']) }}
                                    {{ Form::text('customer_birth_place', $dataCus->customer_birth_place, ['class' => 'form-control', 'id' => 'acc-name']) }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    {{ Form::label('tipe', 'Tanggal lahir', ['for' => 'acc-name']) }}
                                    <input type="text" name="customer_birth_date" class="form-control"
                                           placeholder="Contoh: 1994-12-16"
                                           value="{{$dataCus->customer_birth_date}}">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="single-input-item">
                        <label for="acc-password">Jenis Kelamin</label>
                        <div class="custom-control custom-radio custom-control-inline ml-3">
                            <input type="radio" id="customRadioInline1" value="m" name="gender"
                                   class="custom-control-input" {{($dataCus->customer_gender == 'm' ? 'checked' : '')}}>
                            <label class="custom-control-label" for="customRadioInline1">Pria</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" value="f" name="gender"
                                   class="custom-control-input" {{($dataCus->customer_gender == 'f' ? 'checked' : '')}}>
                            <label class="custom-control-label" for="customRadioInline2">Wanita</label>
                        </div>
                    </div>
                    <div class="single-input-item">
                        <button type="submit" class="btn btn__bg btn__sqr">Simpan</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $(".pickadate").pickadate({format: "yyyy-mm-dd", formatSubmit: "yyyy-mm-dd", hiddenName: true});
        });
    </script>--}}
@endsection