@extends('frontend.customers.app')
@section('title', 'Dashboard')
@section('main-content')
    {{--<div class="tab-pane fade show active" id="dashboad" role="tabpanel">
        <div class="myaccount-content">
            <h3>Dashboard</h3>
            <div class="row">
                <div class="col-md-4">
                    <div class="card border-warning mx-sm-1 p-3">
                        <div class="text-info text-center mt-2"><h6>MENUNGGU PEMBAYARAN</h6></div>
                        <div class="row text-center mt-1">
                            <div class="col-md-6">
                                <h1>{{$data['pending_payment']}}</h1>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('customers.order', ['pending_payment']) }}" class="btn btn-block btn-sm btn-info">Lihat</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card border-success mx-sm-1 p-3">
                        <div class="text-success text-center mt-2"><h6>TERBAYAR</h6></div>
                        <div class="row text-center mt-1">
                            <div class="col-md-6">
                                <h1>{{$data['paid']}}</h1>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('customers.order', ['paid']) }}" class="btn btn-block btn-sm btn-success">Lihat</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card border-primary mx-sm-1 p-3">
                        <div class="text-info text-center mt-2"><h6>PROSES</h6></div>
                        <div class="row text-center mt-1">
                            <div class="col-md-6">
                                <h1>{{$data['process']}}</h1>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('customers.order', ['process']) }}" class="btn btn-block btn-sm btn-info">Lihat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-20">
                <div class="col-md-4">
                    <div class="card border-success mx-sm-1 p-3">
                        <div class="text-success text-center mt-2"><h6>SELESAI</h6></div>
                        <div class="row text-center mt-1">
                            <div class="col-md-6">
                                <h1>{{$data['finish']}}</h1>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('customers.order', ['finish']) }}" class="btn btn-block btn-sm btn-success">Lihat</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card border-danger mx-sm-1 p-3">
                        <div class="text-danger text-center mt-2"><h6>CANCEL</h6></div>
                        <div class="row text-center mt-1">
                            <div class="col-md-6">
                                <h1>{{$data['cancel']}}</h1>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('customers.order', ['cancel']) }}" class="btn btn-block btn-sm btn-danger">Lihat</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card border-warning mx-sm-1 p-3">
                        <div class="text-warning text-center mt-2"><h6>REFUND</h6></div>
                        <div class="row text-center mt-1">
                            <div class="col-md-6">
                                <h1>{{$data['refund']}}</h1>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('customers.order', ['refund']) }}" class="btn btn-block btn-sm btn-warning">Lihat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
@endsection
