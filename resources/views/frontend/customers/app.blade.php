@extends('frontend.layouts.app')
@section('title', 'Dashboard')
@section('content')

    <section id="page-title" style="padding: 100px;">

        <div class="container clearfix">
            <div class="row justify-content-center">
                <h2 style="color: white">Selamat Datang, {{Auth::guard('member')->user()->customer_name}}</h2>
            </div>
        </div>

    </section>

    <section style="#f2f2f2">

        <div class="container">
            <div class="row" style="margin-top: -100px">
                <div class="col-md-3">
                    <a href="#">
                        <img alt="100%x180" src="{{ (!empty(Auth::guard('member')->user()->customer_image)) ? asset_url('/images/customer/'.Auth::guard('member')->user()->customer_image) : asset_url('frontend/menu/avatar.jpg') }}" class="img-thumbnail"
                             style="height: 200px; width: 200px; display: block; border-radius: 100%;">
                    </a>
                </div>
            </div>
            <div class=" side-tabs nobottommargin clearfix ui-tabs ui-corner-all ui-widget ui-widget-content">
                @include('frontend.customers.sidebar')

                <div class="tab-container" style="padding-left: 50px;">
                    @yield('main-content')
                </div>

            </div>
        </div>
    </section>

    {{--<main class="main">

        <!-- my account wrapper start -->
        <div class="my-account-wrapper pt-60 pb-60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- My Account Page Start -->
                        <div class="myaccount-page-wrapper">
                            <!-- My Account Tab Menu Start -->
                            <div class="row">
                            @include('frontend.customers.sidebar')
                                <!-- My Account Tab Menu End -->

                                <!-- My Account Tab Content Start -->
                                <div class="col-lg-9 col-md-8">
                                    <div class="tab-content" id="myaccountContent">
                                        @yield('main-content')
                                    </div>
                                </div>
                                <!-- My Account Tab Content End -->
                            </div>
                        </div> <!-- My Account Page End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- my account wrapper end -->
    </main>--}}
@endsection