@extends('frontend.layouts.app')
@section('title', 'Checkout')
@section('content')
    <style>
        a.disabled {
            pointer-events: none;
            cursor: default;
        }
        #map {
            width: 100%;
            height: 400px;
        }
        .mapControls {
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }
        #searchMapInput {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 95%;
        }
        #searchMapInput:focus {
            border-color: #4d90fe;
        }
        .pac-container {
            z-index: 10000 !important;
        }
    </style>
    <section id="content" style="background: whitesmoke; margin-bottom: 0px;">

        <div class="content-wrap">

            <div class="container clearfix">

                <div id="processTabs" class="ui-tabs ui-corner-all ui-widget ui-widget-content">
                    <ul class="process-steps bottommargin clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="false" aria-expanded="false">
                            <a href="#ptab1" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1"><i class="icon-inbox1"></i></a>
                            <h5>Review Cart</h5>
                        </li>
                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="true" aria-expanded="true">
                            <a href="#ptab2" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2"><i class="icon-info1"></i></a>
                            <h5>Enter Shipping Info</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
                            <a href="#ptab3" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3"><i class="icon-banknote"></i></a>
                            <h5>Complete Payment</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab4" aria-labelledby="ui-id-4" aria-selected="false" aria-expanded="false">
                            <a href="#ptab4" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4"><i class="icon-check"></i></a>
                            <h5>Order Complete</h5>
                        </li>
                    </ul>
                    <div>
                        <div id="ptab2" aria-labelledby="ui-id-2" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="false" style="">
                            <div class="row">

                                <div class="col-lg-7 highlight-box">
                                    <form>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <div class="fancy-title title-bottom-border" style="padding-left: 12px; margin-top: 15px;">
                                                    <h5>Delivery Detail</h5>
                                                </div>
                                                <!-- input name -->
                                                <label for="exampleTextInput" class="label-input">Name</label>
                                                <input type="text" class="form-control no-border" id="recepient"
                                                       value="{{ $dataCus->customer_name }}" placeholder="Nama Penerima"
                                                       required>
                                                <div class="dotted"></div>
                                                <!-- input phone -->
                                                <label for="exampleTextInput" class="label-input">Phone</label>
                                                <input type="text" class="form-control no-border" id="phone"
                                                       value="{{ $dataCus->customer_phone }}" placeholder="Nomor Telepon"
                                                       required>
                                                <div class="dotted"></div>
                                                <!-- input address -->
                                                <div class="address-detail-info">
                                                    <h5>Delivery To</h5>
                                                    <a href="#" class="launch-modal" data-lightbox="inline">
                                                        <i class="icon-map-marker1 orange"></i>
                                                        <span id="locationName">Pilih Lokasi</span>
                                                    </a>
                                                    <!-- detail town -->
                                                    <ul id="locationDetailInfo"></ul>
                                                </div>
                                                <div class="address-detail-info">
                                                    <h5>Delivery Time</h5>
                                                    <a href="#myModalDate" id="timeLabel" data-lightbox="inline">Tommorow, 10:00 AM - 11:00 AM</a>
                                                    <input type="hidden" name="delevery_time" id="timeInput" value="{{ \Carbon\Carbon::now()->addDays(2)->format('Y-m-d') }} 10:00:00">
                                                </div>
                                                <div class="form-group" style="padding-left: 12px;">
                                                    <label for="exampleFormControlTextarea1">Detail Alamat Pengiriman </label>
                                                    <textarea class="form-control" id="addr" rows="3" required></textarea>
                                                </div>
                                                <!-- detal address -->
                                                <div class="form-group" style="padding-left: 12px;">
                                                    <label for="exampleFormControlTextarea1">Noted Detail</label>
                                                    <textarea class="form-control" name="notes" id="notes" rows="3" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-5">
                                    <div class="highlight-box">
                                        <div class="fancy-title title-bottom-border" style="padding-left: 12px; margin-top: 15px;">
                                            <h5>Ringkasan Belanja</h5>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table cart">
                                                <thead>
                                                <tr>
                                                    <th>Products</th>
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach(Cart::getContent() as $item)
                                                    <tr>
                                                        <td>
                                                            <a href="{{ route('product.detail', [$item->attributes->alias]) }}">
                                                                {{ $item->name }}
                                                                <strong> × {{ $item->quantity }}</strong>
                                                            </a>
                                                        </td>
                                                        <td>Rp {{ $item->getPriceSum() }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td>Sub Total</td>
                                                    <td><strong>Rp {{ Cart::getSubTotal() }}</strong></td>
                                                </tr>
                                                @foreach(Cart::getConditions() as $key)
                                                    <tr>
                                                        <td>{{ $key->getName() }}</td>
                                                        <td>Rp {{ price_format($key->getValue()) }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td><strong>Total Order</strong></td>
                                                    <td><strong>Rp {{ Cart::getTotal() }}</strong></td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="fancy-title title-bottom-border" style="padding-left: 12px; margin-top: 15px;">
                                            <h5>Punya Voucher ?
                                            </h5>
                                            <form action="#" method="post" class=" d-block d-md-flex">
                                                <input class="form-control" type="text" id="cartVoucher"
                                                       placeholder="Masukan kode voucher" style="margin-top: 7px;" required/>
                                                <button type="button" class="button button-circle"
                                                        id="checkVoucher"
                                                        data-url="{{ route('customer.cart.voucher') }}">Apply
                                                    Coupon
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear bottommargin-lg"></div>
                            <a href="{{ route('customer.cart') }}" class="button button-circle nomargin tab-linker" rel="1">Sebelumnya</a>
                            <button type="button" id="toPayment" class="button button-circle fright"
                                    data-url="{{ route('customer.cart.payment') }}">Pembayaran
                            </button>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

            </div>

        </div>

    </section>

    <!-- ModalDate -->
    <div class="modalDate mfp-hide" id="myModalDate">
        <div class="block divcenter" style="background-color: #FFF; max-width: 600px; border-radius: 20px;">
            <div class="date-food">
                <div class="c-title">When to deliver your food?</div>
                <ul class="days">
                    <li class="time-selector select-date date-active"
                        data-date="{{ \Carbon\Carbon::now()->addDay()->format('Y-m-d') }}">Tommorow</li>
                    <li class="time-selector select-date {{ (\Carbon\Carbon::now()->addDays(2)->format('D') === 'Sun' ? 'disable' : '') }}"
                        data-date="{{ \Carbon\Carbon::now()->addDays(2)->format('Y-m-d') }}">
                        {{ \Carbon\Carbon::now()->addDays(2)->format('D d') }}
                    </li>
                    <li class="time-selector select-date {{ (\Carbon\Carbon::now()->addDays(3)->format('D') == 'Sun' ? 'disable' : '') }}"
                        data-date="{{ \Carbon\Carbon::now()->addDays(3)->format('Y-m-d') }}">
                        {{ \Carbon\Carbon::now()->addDays(3)->format('D d') }}
                    </li>
                    <li class="time-selector select-date {{ (\Carbon\Carbon::now()->addDays(4)->format('D') == 'Sun' ? 'disable' : '') }}"
                        data-date="{{ \Carbon\Carbon::now()->addDays(4)->format('Y-m-d') }}">
                        {{ \Carbon\Carbon::now()->addDays(4)->format('D d') }}
                    </li>
                    <li class="time-selector select-date {{ (\Carbon\Carbon::now()->addDays(5)->format('D') == 'Sun' ? 'disable' : '') }}"
                        data-date="{{ \Carbon\Carbon::now()->addDays(5)->format('Y-m-d') }}">
                        {{ \Carbon\Carbon::now()->addDays(5)->format('D d') }}
                    </li>
                    <li class="time-selector select-date {{ (\Carbon\Carbon::now()->addDays(6)->format('D') == 'Sun' ? 'disable' : '') }}"
                        data-date="{{ \Carbon\Carbon::now()->addDays(6)->format('Y-m-d') }}">
                        {{ \Carbon\Carbon::now()->addDays(6)->format('D d') }}
                    </li>
                </ul>
                <div class="c-title">At what time?</div>
                <ul class="time">
                    <li class="time-selector select-time date-active"
                        data-time="10:00:00">10:00 AM - 11:00 AM</li>
                </ul>
            </div>

            <a href="javascript:;" class="button button-circle btn-block" id="confirmDate" style="margin: 0">Confirm</a>
        </div>
    </div>

    <div class="bs-example">
        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input id="searchMapInput" class="mapControls" type="text" placeholder="Enter a location">
                                <div id="map"></div>
                                <input name="addr_lat" id="addr_lat" type="hidden" value=""/>
                                <input name="addr_lng" id="addr_lng" type="hidden" value=""/>
                                <input id="addr_name" type="hidden" value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#"
                           id="btnMapConfirm"
                           class="button button-small button-circle bg-orange disable"
                           disabled=""
                           data-lightbox="inline" >
                            Confirm Location
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalAddress mfp-hide" id="myModalAddress">
        <div class="block divcenter" style="background-color: #FFF; max-width: 500px; border-radius: 20px;">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <h3 class="pad-sm">Detail Address</h3>
                    <div class="address-detail-info">
                        <a href="#myModalMAP" data-lightbox="inline"><i class="icon-map-marker1 orange"></i> <span class="modal-info-address"></span></a>
                    </div>
                </div>
                <div style="padding: 0 75px;">
                    <h3>Select the building type for your address</h3>
                    <div class="tabs clearfix ui-tabs ui-corner-all ui-widget ui-widget-content" id="tab-3">

                        <ul id="tabMapAddr" class="tab-nav tab-nav2 clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" style="margin-bottom: 0;"
                            role="tablist">
                            <li role="tab" tabindex="0"
                                class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"
                                aria-controls="tabs-9" aria-labelledby="ui-id-21" aria-selected="true" aria-expanded="true"><a
                                        href="#tabs-9" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-21">Gedung</a></li>
                            <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
                                aria-controls="tabs-10" aria-labelledby="ui-id-22" aria-selected="false" aria-expanded="false"><a
                                        href="#tabs-10" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-22">Rumah</a></li>
                        </ul>

                        <div class="tab-container">
                            <h4>Confirm address details</h4>
                            <div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-9"
                                 aria-labelledby="ui-id-21" role="tabpanel" aria-hidden="false">
                                <form>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationDefault03">Gedung</label>
                                            <input type="text" class="form-control gedung-nama" id="validationDefault03" required>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label for="validationDefault04">Lantai</label>
                                            <input type="text" class="form-control gedung-lantai" id="validationDefault04" required>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label for="validationDefault05">Block</label>
                                            <input type="text" class="form-control gedung-block" id="validationDefault05" required>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-10"
                                 aria-labelledby="ui-id-22" role="tabpanel" aria-hidden="true" style="display: none;">
                                <form>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationDefault03">Nomer Rumah</label>
                                            <input type="text" class="form-control rumah-no" id="validationDefault03" required>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label for="validationDefault04">Jalan</label>
                                            <input type="text" class="form-control rumah-jalan" id="validationDefault04" required>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <a href="#" class="btn add-cart-btn btn-block" onclick="set_location()">Confirm</a>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"rel="stylesheet"/>
    {{--<link rel="stylesheet" href="{{ asset_url('/backend/assets/map/jquery-gmaps-latlon-picker.css') }}">--}}

    <script>
        var _token = '{{ csrf_token() }}'
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/jquery.preloaders.min.js') }}"></script>
    <script src="{{ asset_url('/frontend/js/cart.js?v=?1607') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.launch-modal').click(function(){
                $('#myModal').modal({
                    backdrop: 'static'
                });
            });

            $(document).on("click", ".select-date:not('.disable')", function () {
                 $(this).prevAll('.select-date.date-active').removeClass('date-active');
                 $(this).nextAll('.select-date.date-active').removeClass('date-active');

                 $(this).addClass('date-active');
            });

            $(document).on("click", "#confirmDate", function () {
                 var date = $(".select-date.date-active").data('date');
                 var time = $(".select-time.date-active").data('time');
                 var label = $(".select-date.date-active").text();
                 var timeLabel = $(".select-time.date-active").text();

                 $("#timeInput").val(date+' '+time);
                 $("#timeLabel").html(label+', '+timeLabel);

                 $(".mfp-close").click();
            });

            $(document).on("click", '#btnMapConfirm:not(".disable")', function () {
                $("#myModal").modal("hide");
                $(".modal-info-address").html($("#addr_name").val());
            });
        });

        function set_location() {
            var type = $("#tabMapAddr .ui-state-active a").text();

            if(type == 'Gedung') {
                var nama    = $(".gedung-nama").val();
                var lantai  = $(".gedung-lantai").val();
                var block   = $(".gedung-block").val();

                var locationDetail = '<li>Gedung '+nama+'</li>';
                    locationDetail += '<li>Lantai '+lantai+'</li>';
                    locationDetail += '<li>Block '+block+'</li>';

                var locationFull = $("#addr_name").val()+', Gedung '+nama+', Lantai '+lantai+', Block '+block;

                $("#addr").html(locationFull);
                $("#locationDetailInfo").html(locationDetail);
            }
            else {
                var nomor  = $(".rumah-no").val();
                var jalan  = $(".rumah-jalan").val();

                var locationDetail = '<li>Nomor '+nomor+'</li>';
                    locationDetail += '<li>Jalan '+jalan+'</li>';

                var locationFull = $("#addr_name").val()+', Gedung '+nama+', Lantai '+lantai+', Block '+block;

                $("#locationDetailInfo").html(locationDetail);
            }

            $("#addr").html(locationFull);
            $("#locationName").html($("#addr_name").val());

            $(".mfp-close").click();
        }

        function initMap() {
            var myLatLng = {lat: -6.920315284273143, lng: 107.61946507177731};

            var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 14,
                disableDefaultUI: true
            });

            var input = document.getElementById('searchMapInput');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                draggable: true
            });

            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();

                /* If the place has a geometry, then present it on a map. */
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                /* Location details */
                $('#addr_name').val(place.formatted_address);
                $('#addr_lat').val(place.geometry.location.lat());
                $('#addr_lng').val(place.geometry.location.lng());

                $("#btnMapConfirm").removeClass('disable');
                $("#btnMapConfirm").removeAttr('disabled');
                $("#btnMapConfirm").attr('href', '#myModalAddress');
            });

            google.maps.event.addListener(marker, 'dragend', function(marker) {
                var latLng = marker.latLng;

                var geocoder = new google.maps.Geocoder();

                geocoder.geocode({
                    latLng: latLng
                }, function(responses) {
                    if (responses && responses.length > 0) {
                        $('#addr_name').val(responses[0].formatted_address);
                    } else {
                        //updateMarkerAddress('Cannot determine address at this location.');
                    }
                });

                $('#addr_lat').val(latLng.lat());
                $('#addr_lng').val(latLng.lng());

                $("#btnMapConfirm").removeClass('disable');
                $("#btnMapConfirm").removeAttr('disabled');
                $("#btnMapConfirm").attr('href', '#myModalAddress');
            });
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}&libraries=places&callback=initMap" async defer></script>
@endsection