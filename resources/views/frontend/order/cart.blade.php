@extends('frontend.layouts.app')
@section('title', 'Keranjang Belanja')
@section('content')
    <style>
        a.disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>
    <section id="content" style="background: whitesmoke">

        <div class="content-wrap">

            <div class="container clearfix">

                <div id="processTabs">
                    <ul class="process-steps bottommargin clearfix">
                        <li>
                            <a href="#ptab1" class="i-circled i-bordered i-alt divcenter disabled" disabled=""><i
                                        class="icon-inbox1"></i></a>
                            <h5>Review Cart</h5>
                        </li>
                        <li>
                            <a href="#ptab2" class="i-circled i-bordered i-alt divcenter disabled"><i
                                        class="icon-info1"></i></a>
                            <h5>Enter Shipping Info</h5>
                        </li>
                        <li>
                            <a href="#ptab3" class="i-circled i-bordered i-alt divcenter disabled"><i
                                        class="icon-banknote"></i></a>
                            <h5>Complete Payment</h5>
                        </li>
                        <li>
                            <a href="#ptab4" class="i-circled i-bordered i-alt divcenter disabled"><i
                                        class="icon-check"></i></a>
                            <h5>Order Complete</h5>
                        </li>
                    </ul>
                    <div>
                        <div id="ptab1">
                            <div class="fancy-title title-bottom-border">
                                <h5><span>TODAY,</span> 04/07/19</h5>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="highlight-box">
                                        <h5>Cart Detail</h5>
                                        <div class="table-responsive">
                                            <table class="table cart">
                                                <tbody>
                                                @foreach(Cart::getContent() as $item)
                                                    <tr class="cart_item cart-list" data-id="{{ $item->id }}">
                                                        <td class="cart-product-remove">
                                                            <a href="#" class="remove act-item-remove" title="Remove this item"><i
                                                                        class="icon-trash2"></i></a>
                                                        </td>
                                                        <td class="cart-product-thumbnail">
                                                            <a href="{{ route('product.detail', [$item->attributes->alias]) }}"
                                                               target="_blank">
                                                                <img width="64" height="64"
                                                                             src="{{ get_image_product($item->attributes->image) }}"
                                                                     alt="{{ $item->name }}"></a>
                                                        </td>

                                                        <td class="cart-product-name">
                                                            <a href="{{ route('product.detail', [$item->attributes->alias]) }}"
                                                               target="_blank">{{ $item->name  }}</a>
                                                        </td>

                                                        <td class="cart-product-price">
                                                            <span class="amount">Rp. {{ price_format($item->price) }}</span>
                                                        </td>

                                                        <td class="cart-product-quantity">
                                                            <div class="quantity clearfix">
                                                                <input type="button" value="-" class="cart-minus">
                                                                <input type="text" name="quantity" value="{{ $item->quantity  }}" class="qty">
                                                                <input type="button" value="+" class="cart-plus">
                                                            </div>
                                                        </td>

                                                        <td class="cart-product-subtotal">
                                                            <span class="amount item-subtotal">Rp. {{ $item->getPriceSum() }}</span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>

                                            </table>
                                        </div>

                                        <!-- subtotal -->
                                        <div class="table-responsive">
                                            <table class="table cart">
                                                <tbody>
                                                <tr class="cart_item">
                                                    <td class="cart-product-name">
                                                        Cart Subtotal
                                                    </td>

                                                    <td class="cart-product-name tright">
                                                        <span class="amount cart-subtotal">Rp {{ Cart::getSubTotal() }}</span>
                                                    </td>
                                                </tr>
                                                @foreach(Cart::getConditions() as $key)
                                                    <tr>
                                                        <td class="cart-product-name">{{ $key->getName() }}</td>
                                                        <td class="cart-product-name tright">Rp {{ price_format($key->getValue()) }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr class="cart_item">
                                                    <td class="cart-product-name">
                                                        <strong>Total</strong>
                                                    </td>

                                                    <td class="cart-product-name tright">
																<span class="amount color lead"><strong class="cart-total">
																		Rp {{ Cart::getTotal() }}</strong></span>
                                                    </td>
                                                </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">

                                    <div class="accordion highlight-box clearfix">

                                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i
                                                    class="acc-open icon-remove-circle"></i>Add Drink
                                        </div>

                                        <div class="acc_content clearfix" style="display: none;">
                                        @foreach($suggestDrink as $item)
                                            <div class="team team-list add-space clearfix">
                                                <div class="drink-add">
                                                    <img src="{{ asset_url('/images/products/'.$item->product->product_image) }}" alt="John Doe">
                                                </div>
                                                <div class="team-desc">
                                                    <div class="team-title">
                                                        <h5 class="no-mar">{{ $item->product->product_name }}</h5>
                                                    </div>
                                                    <div class="team-content no-mar">
                                                        {{ $item->product->product_sortdesc }}
                                                    </div>
                                                    <div class="quantity clearfix" style="margin-top: 10px;">
                                                        <input type="button" value="+"
                                                               class="add-plus add-suggest"
                                                               data-url="{{ route('customer.cart.add', [$item->product->product_alias]) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </div>

                                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i
                                                    class="acc-open icon-remove-circle"></i>Add
                                            Desserts</div>
                                        <div class="acc_content clearfix" style="display: none;">
                                        @foreach($suggestDessert as $item)
                                            <div class="team team-list add-space clearfix">
                                                <div class="drink-add">
                                                    <img src="{{ asset_url('/images/products/'.$item->product->product_image) }}" alt="John Doe">
                                                </div>
                                                <div class="team-desc">
                                                    <div class="team-title">
                                                        <h5 class="no-mar">{{ $item->product->product_name }}</h5>
                                                    </div>
                                                    <div class="team-content no-mar">
                                                        {{ $item->product->product_sortdesc }}
                                                    </div>
                                                    <div class="quantity clearfix" style="margin-top: 10px;">
                                                        <input type="button" value="+"
                                                               class="add-plus add-suggest"
                                                               data-url="{{ route('customer.cart.add', [$item->product->product_alias]) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-lg-12">
                                    <a href="{{ route('customer.cart.checkout') }}"
                                       class="button button-circle nomargin fright" rel="2">Set Address
                                        &rArr;</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
            </div>
        </div>
    </section>

    <script>
        $(function () {
            $("#processTabs").tabs({show: {effect: "fade", duration: 400}});
            $(".tab-linker").click(function () {
                $("#processTabs").tabs("option", "active", $(this).attr('rel') - 1);
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        var _token = '{{ csrf_token() }}';
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/js/jquery.preloaders.min.js') }}"></script>
    <script src="{{ asset_url('/frontend/js/cart.js?v=1607') }}"></script>

    <script>
        $(function () {
            $(".add-suggest").on("click", function () {
                $.preloader.start({
                    modal: true,
                    src: 'sprites.png'
                });

                $.ajax({
                    url: $(this).data('url'),
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: { orderQty: 1 },
                    dataType: 'json',
                    success: function (res) {
                        $.preloader.stop();

                        if (res.success) {
                            location.reload();
                        }
                        else {
                            location.href = res.url;
                        }
                    }
                });
            });
        });
    </script>
@endsection