@extends('frontend.layouts.app')
@section('title', 'Data Pembayaran')
@section('content')

    <style>
        a.disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>

    <section id="content" style="background: whitesmoke; margin-bottom: 0px;">

        <div class="content-wrap">

            <div class="container clearfix">

                <div id="processTabs" class="ui-tabs ui-corner-all ui-widget ui-widget-content">
                    <ul class="process-steps bottommargin clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="false" aria-expanded="false">
                            <a href="#ptab1" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1"><i class="icon-inbox1"></i></a>
                            <h5>Review Cart</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
                            <a href="#ptab2" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2"><i class="icon-info1"></i></a>
                            <h5>Enter Shipping Info</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
                            <a href="#ptab3" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3"><i class="icon-banknote"></i></a>
                            <h5>Complete Payment</h5>
                        </li>
                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="ptab4" aria-labelledby="ui-id-4" aria-selected="true" aria-expanded="true">
                            <a href="#ptab4" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4"><i class="icon-check"></i></a>
                            <h5>Order Complete</h5>
                        </li>
                    </ul>
                </div>

                <div class="clear"></div>

            </div>

        </div>

    </section>
    <main class="main topmargin-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="alert alert-light" style="background: #fff2db; color: #555" role="alert">
                        <strong>Selalu waspada kepada pihak yang tidak bertanggungjawab</strong>
                        <ul class="mt-1 ml-2">
                            <li>&mdash; Pastikan untuk menyelesaikan pembayaran sesuai dengan informasi yang tertera pada halaman ini.</li>
                            <li>&mdash; Jangan melakukan pembayaran dengan nominal yang berbeda dengan yang tertera pada tagihan.</li>
                            <li>&mdash; Jangan melakukan transfer di luar nomor rekening yang tertera pada halaman ini.</li>
                        </ul>
                    </div>

                    <div class="card">
                        <div class="card-header" style="padding: 10px">
                            Pembayaran Via Transfer Bank
                        </div><!-- End .card-header -->

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-center">Jumlah Tagihan</div>
                                <div class="col-md-12 text-center mt-1">
                                    <strong class="popover-show"
                                        data-content="Transfer tepat hingga 3 digit terakhir agar tidak menghambat proses verifikasi">
                                        <h3>Rp {{ price_format($dataOrder->order_total) }}</h3>
                                    </strong>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 100px">
                                <div class="col-md-12 text-center">Nomor Tagihan</div>
                                <div class="col-md-12 text-center mt-1">
                                    <h3 style="color: #e2595f">{{ $dataOrder->order_number }}</h3>
                                </div>
                            </div>
                        </div><!-- End .card-body -->
                    </div>

                    <div class="card" style="margin-top: 10px">
                        <div class="card-body text-center">
                            Lakukan pembayaran sebelum <strong>{{ date('g:i A, d F Y', strtotime($expDate)) }}</strong> ke rekening berikut

                            <div class="row mt-3">
                                <div class="col-md-12 pr-5 pl-5">
                                    <div class="row mb-1">
                                        <div class="col-md-6 text-left">Bank</div>
                                        <div class="col-md-6 text-right"><h5>{{ $dataOrder->order_bank_to }}</h5></div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-md-6 text-left">Nomor Rekening</div>
                                        <div class="col-md-6 text-right"><h5>{{ $dataOrder->order_bank_to_number }}</h5></div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-md-6 text-left">Nama Rekening</div>
                                        <div class="col-md-6 text-right"><h5>{{ $dataOrder->order_bank_to_account }}</h5></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="{{ route('customers.order.detail', [$dataOrder->order_number]) }}" class="button button-circle fright">LIHAT DETAIL PESANAN</a>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </main>

    <script>
        $(function () {
            $('.popover-show').popover({
                placement: 'bottom',
                template: '<div class="popover" role="tooltip" style="font-size: 13px;color: #fff"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body text-center" style="background: #ffa500;color: #fff"></div></div>'
            }).popover('show');
        });
    </script>
@endsection