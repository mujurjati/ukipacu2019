@extends('frontend.layouts.app')
@section('title', 'Pembayaran')
@section('content')

    <style>
        a.disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>

    <section id="content" style="background: whitesmoke; margin-bottom: 0px;">

        <div class="content-wrap">

            <div class="container clearfix">

                <div id="processTabs" class="ui-tabs ui-corner-all ui-widget ui-widget-content">
                    <ul class="process-steps bottommargin clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="false" aria-expanded="false">
                            <a href="#ptab1" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1"><i class="icon-inbox1"></i></a>
                            <h5>Review Cart</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
                            <a href="#ptab2" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2"><i class="icon-info1"></i></a>
                            <h5>Enter Shipping Info</h5>
                        </li>
                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="true" aria-expanded="true">
                            <a href="#ptab3" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3"><i class="icon-banknote"></i></a>
                            <h5>Complete Payment</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab4" aria-labelledby="ui-id-4" aria-selected="false" aria-expanded="false">
                            <a href="#ptab4" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4"><i class="icon-check"></i></a>
                            <h5>Order Complete</h5>
                        </li>
                    </ul>
                    <div>
                        <div id="ptab3" aria-labelledby="ui-id-3" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="false" style="">
                            <div class="row justify-content-center">
                                <div class="col-lg-5 highlight-box">
                                    <div class="fancy-title title-bottom-border text-center">
                                        <h4>Thanks For Order</h4>
                                    </div>
                                    <div class=" fbox-plain text-center">
                                        <img src="{{ asset_url('frontend/images/logo.png') }}" alt="" style="width: 250px;">
                                        <h4>Silahkan Pilih Metode Pembayaran</h4>
                                    </div>
                                    <div class=" fbox-plain text-center">
                                        <div class="row">
                                            @foreach($dataBank as $item)
                                                <div class="col-md-6 mb-3">
                                                    <div class="custom-control custom-radio custom-control-inline" style="margin: 0">
                                                        <input type="radio" name="paymentMethod" value="{{ $item->bank_id }}" id="bank-{{ $item->bank_id }}" class="custom-control-input act-select-bank" />
                                                        <label class="custom-control-label" for="bank-{{ $item->bank_id }}">
                                                            <img src="{{ get_image_bank($item->bank_image) }}" style="height: 30px" alt="{{ $item->bank_name }}">
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table cart">
                                            <tbody>
                                            <tr class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong>Total</strong>
                                                </td>

                                                <td class="cart-product-name tright">
																<span class="amount color lead"><strong>Rp {{ Cart::getTotal() }}</strong></span>
                                                </td>
                                            </tr>
                                            </tbody>

                                        </table>
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table cart">
                                            <tbody>
                                            <tr class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong>Ketentuan Pembayaran :</strong>
                                                    <ul>
                                                        <li>Pembayaran dapat dilakukan ke rekening yang Anda pilih di atas.</li>
                                                        <li>Total belanja belum termasuk kode pembayaran untuk keperluan proses verifikasi otomatis.</li>
                                                        <li>Mohon segera transfer tepat sampai 3 digit terakhir.</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clear bottommargin-lg"></div>
                            <a href="{{ route('customer.cart.checkout') }}" class="button button-circle nomargin tab-linker" rel="2">Sebelumnya</a>
                            <button type="button" id="savePayment" class="button button-circle nomargin fright" disabled="" data-url="{{ route('customer.cart.create_order') }}">Bayar</button>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

            </div>

        </div>

    </section>

    <script type="text/javascript">
        var _token = '{{ csrf_token() }}';
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/assets/js/jquery.preloaders.min.js') }}"></script>
    <script src="{{ asset_url('/frontend/assets/js/cart.js') }}"></script>
@endsection