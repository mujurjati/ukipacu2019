@extends('frontend.layouts.app')
@section('title', 'Checkout')
@section('content')
    <style>
        a.disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>

    <section id="content" style="background: whitesmoke; margin-bottom: 0px;">
        <div class="content-wrap">
            <div class="container clearfix">
                <div id="processTabs">
                    <ul class="process-steps bottommargin clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="false" aria-expanded="false">
                            <a href="#ptab1" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1"><i class="icon-inbox1"></i></a>
                            <h5>Review Cart</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
                            <a href="#ptab2" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2"><i class="icon-info1"></i></a>
                            <h5>Enter Shipping Info</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
                            <a href="#ptab3" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3"><i class="icon-banknote"></i></a>
                            <h5>Complete Payment</h5>
                        </li>
                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="ptab4" aria-labelledby="ui-id-4" aria-selected="true" aria-expanded="true">
                            <a href="#ptab4" class="i-circled i-bordered i-alt divcenter disabled ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4"><i class="icon-check"></i></a>
                            <h5>Order Complete</h5>
                        </li>
                    </ul>
                    <div>
                        <div id="ptab4" aria-labelledby="ui-id-2" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="false" style="">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <ul class="dangers">
                                        <li>
                                            <h5>Selalu Waspada Terhadap Pihak Yang Tidak Bertanggung Jawab</h5>
                                        </li>
                                        <li>Pastikan Selalu melakukan pembayaran sesuai dengan informasi yang tertera di bawah ini.</li>
                                        <li>Jangan lakukan pembayaran dengan nominal yang berbeda dari informasi yang tertera pada tagihan.</li>
                                        <li>Jangan lakukan pembayaran kepada nomer rekening tang tidak tertera pada halaman ini.</li>
                                    </ul>

                                    <div class="transfer-bank">
                                        <h5>Pembayaran Via Transfer Bank</h5>
                                        <div class="text-center box">
                                            <p class="red">Silahkan lakukan pembayaran sebelum <strong>{{ date('g:i A, d F Y', strtotime($expDate)) }}</strong></p>
                                            <div id="countdown-ex1" class="countdown coming-soon divcenter bottommargin" style="max-width:200px;"></div>
                                            <p class="no-mar">Jumlah Tagihan</p>
                                            <h1>Rp {{ price_format($dataOrder->order_total) }}</h1>
                                            <div class="alert alert-info">
                                                Harap Transfer sampai dengan 3 digit kode unik terakhir
                                            </div>
                                            <p class="no-mar">Nomor Tagihan</p>
                                            <h2>{{ $dataOrder->order_number }}</h2>
                                        </div>
                                    </div>


                                    <div class="table-responsive box-detail">
                                        <table class="table cart">
                                            <tbody>
                                            <tr class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong>Nama Bank</strong>
                                                </td>

                                                <td class="cart-product-name tright">
                                                    <span class="amount">{{ $dataOrder->order_bank_to }}</span>
                                                </td>
                                            </tr>


                                            <tr class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong>Nomor Rekening</strong>
                                                </td>

                                                <td class="cart-product-name tright">
                                                    <span class="amount color lead"><strong>{{ $dataOrder->order_bank_to_number }}</strong></span>
                                                </td>
                                            </tr>
                                            <tr class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong>Nama Rekening</strong>
                                                </td>

                                                <td class="cart-product-name tright">
                                                    <span class="amount color lead"><strong>{{ $dataOrder->order_bank_to_account }}</strong></span>
                                                </td>
                                            </tr>
                                            </tbody>

                                        </table>
                                        <div class="text-center">
                                            <span class="red">*Order akan otomatis di cancel jika belum menyelesaikan pembayaran sampai {{ date('g:i A, d F Y', strtotime($expDate)) }}*</span>
                                        </div>
                                        <div class="text-center" style="margin-top: 20px">
                                            <a href="{{ route('customers.order.detail', [$dataOrder->order_number]) }}" class="button button-circle">LIHAT DETAIL PESANAN</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

            </div>

        </div>

    </section><!-- #content end -->

    <script>
        jQuery(document).ready( function($){
            var newDate = new Date('{{ $expDate }}');

            $('#countdown-ex1').countdown({until: newDate});
        });
    </script>

@endsection