<style>
    .custom-control {
        padding-left: 0;
        margin-top: 0;
        margin-bottom: 0;
        min-height: 2.5rem;
    }
</style>

<p>&nbsp;</p>

<h2 class="step-title">Shipping Address</h2>

<div class="shipping-step-addresses">
@foreach($dataAddr as $item)
    <div class="shipping-address-box {{ (Session::has('select-addr') && Session::get('select-addr')->c_address_id == $item->c_address_id ? 'active' : '' )}}">
        <address style="min-height: 230px">
            <strong>{{ $item->c_address_type }}</strong> <br>
            {{ $item->c_address_name }} <br>
            {{ $item->c_address_address }},
            {{ $item->subdistrict->s_subdistrict_name }},
            {{ $item->subdistrict->city->s_city_name }},
            {{ $item->subdistrict->city->province->s_province_name }},
            {{ $item->subdistrict->city->s_city_postcode }} <br>
            {{ $item->c_address_phone }} <br>
        </address>

        <div class="address-box-action clearfix">
            <a href="javascript:" data-url="{{ route('ajax.select-address', [$item->c_address_id]) }}" class="btn btn-sm btn-outline-warning float-right act-select-addr">Pilih</a>
        </div>
    </div>
@endforeach
</div>
<a href="#cusAddr" class="btn btn-sm btn-outline-secondary btn-new-address" data-toggle="modal">+ Tambah Alamat</a>

@if(Session::has('select-addr'))
    <p>&nbsp;</p>

    <div class="checkout-step-shipping">
        <div class="row step-title">
            <div class="col-md-8">
                <h2 style="font-weight: 300; font-size: 2.2rem">Kurir Pengiriman</h2>
            </div>
            <div class="col-md-4 text-right">
                {{
                    Form::select('', array_merge(['' => '- Pilih Kurir -'], $dataCourier), '', [
                        'class'     => 'form-control-sm',
                        'id'        => 'selectCourier',
                        'data-url'  => route("ajax.get-shipping")
                    ])
                }}
            </div>
        </div>

        <table class="table table-step-shipping" style="max-width: 100%">
            <tbody id="wrapShipping" data-url="{{ route("ajax.sel-shipping") }}">
                <tr>
                    <td>
                        <div class="alert alert-danger text-center">Silahkan pilih Kurir Pengiriman</div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div><!-- End .checkout-step-shipping -->

    <div class="row mt-3">
        <div class="col-lg-12">
            <div class="checkout-steps-action">
                <a href="{{ route('customer.cart') }}" class="btn btn-sm btn-outline-dark">Halaman Sebelumnya</a>
            </div><!-- End .checkout-steps-action -->
        </div><!-- End .col-lg-8 -->
    </div><!-- End .row -->
@endif

@component('backend.layouts.components.modal', [
    'id'    => 'cusAddr',
    'title' => 'Alamat Pengiriman'
])

    {{ Form::open(['id' => 'formAddAddr']) }}
    <div class="row">
        <div class="col-md-12">
            <div class="form-group row" style="max-width: 100%">
                {{ Form::label('type', 'Nama Alamat', ['class' => 'col-sm-12 col-form-label text-left']) }}
                <div class="col-sm-12">
                    {{ Form::text('type', '', ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh: Alamat Rumah, Kantor, Apartemen, Dropship', 'style' => 'max-width: 100%']) }}
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row" style="max-width: 100%">
                        {{ Form::label('name', 'Nama Penerima', ['class' => 'col-sm-12 col-form-label text-left']) }}
                        <div class="col-sm-12">
                            {{ Form::text('name', $dataCus->customer_name, ['class' => 'form-control form-control-sm']) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row" style="max-width: 100%">
                        {{ Form::label('phone', 'Nomor HP', ['class' => 'col-sm-12 col-form-label text-left']) }}
                        <div class="col-sm-12">
                            {{ Form::text('phone', $dataCus->customer_phone, ['class' => 'form-control form-control-sm', 'placeholder' => 'Contoh: 081234567890']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row text-left" style="max-width: 100%">
                {{ Form::label('city', 'Kota atau Kecamatan', ['class' => 'col-sm-12 col-form-label text-left']) }}
                <div class="col-sm-12">
                    <select name="city" id="selectAddress" data-url="{{ route('ajax.get-address') }}">
                        <option value=""></option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row" style="max-width: 100%">
                {{ Form::label('type', 'Alamat', ['class' => 'col-sm-12 col-form-label text-left']) }}
                <div class="col-sm-12">
                    {{ Form::textarea('address', '', ['class' => 'form-control form-control-sm', 'placeholder' => 'Alamat Lengkap', 'style' => 'max-width: 100%']) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    @slot('btn')
        <button type="button" id="addAddress" class="btn btn-sm btn-primary waves-effect waves-light" data-url="{{ route("ajax.add-address") }}">
            Tambah Alamat
        </button>
    @endslot
@endcomponent