@extends('frontend.layouts.app')
@section('title', ucwords($data->product_name))
@section('content')
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="single-product">

                    <div class="product">

                        <div class="col_two_fifth">

                            <!-- Product Single - Gallery
                            ============================================= -->

                            <div class="product clearfix">
                                <div class="product-image" style="border-radius: 20px !important;">
                                    <a href="#"><img src="{{asset_url('/images/products/'.$data->product_image)}}"
                                                     alt="{{ $data->product_name }}"></a>
                                    <a href="#">
                                        @if(!empty($data->product_image2))
                                            <img src="{{asset_url('/images/products/'.$data->product_image2)}}"
                                                 alt="{{ $data->product_name }}">
                                        @else
                                            <img src="{{asset_url('/images/products/'.$data->product_image)}}"
                                                 alt="{{ $data->product_name }}">
                                        @endif
                                        {{--<img src="menu/images_11.jpeg" alt="Light Blue Denim Dress">--}}
                                        {{--<iframe width="450" height="507"
                                                src="https://www.youtube.com/embed/4qNHr0W6F0o"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>--}}
                                    </a>
                                </div>
                            </div>

                        </div>

                        <div class="col_three_fifth col_last product-desc">

                            <!-- Product Single - Price
                            ============================================= -->

                            <h3 class="no-mar">{{$data->product_name}}</h3>
                            <!-- <div class="product-price"><span
                                        style="font-size: 24px"> Rp {{ price_format($data->product_price) }}</span>
                            </div> -->
                            <!-- Product Single - Price End -->

                            <!-- Product Single - Rating
                            ============================================= -->
                            <!-- <div class="product-rating">
                                {{rating(intval($data->avgRating))}}
                            </div> -->
                            <!-- Product Single - Rating End -->

                            <!-- <div class="clear"></div>
                            <div class="line"></div> -->

                            <!-- Product Single - Quantity & Cart Button
                            ============================================= -->
                            <!-- {{ Form::open(['route' => ['customer.cart.order', $data->product_alias], 'method' => 'post', 'id' => 'formAddCart']) }}
                            <div class="quantity clearfix" style="margin-top: 6px;">
                                <input type="button" value="-" class="minus">
                                <input type="text" name="orderQty" title="Qty" value="1"
                                       class="qty" size="4"/>
                                <input type="button" value="+" class="plus">
                            </div>
                            <a href="#" data-toggle="tooltip" class="add-to-cart button button-circle nomargin"
                               id="actAddCart" title="Masukan Keranjang Belanja">
                                <i class="icon-cart"></i> Add to cart
                            </a>
                            <a href="#" onclick="$('#formAddCart').submit()" data-toggle="tooltip" title="Beli Sekarang"
                               class="add-to-cart button button-circle nomargin">
                                Beli sekarang
                            </a>
                            {{ Form::close() }} -->
                        <!-- Product Single - Quantity & Cart Button End -->

                            <!-- <div class="clear"></div>
                            <div class="line"></div> -->

                            <!-- Product Single - Short Description
                            ============================================= -->
                            <!-- <p>{!! $data->product_desc !!}</p> -->
                            <!-- Product Single - Short Description End -->


                            <!-- Product Single - Share
                            ============================================= -->
                            <div class="si-share noborder clearfix">
                                <span>Share:</span>
                                <div>
                                    <a href="#" class="social-icon si-borderless si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-instagram">
                                        <i class="icon-instagram"></i>
                                        <i class="icon-instagram"></i>
                                    </a>
                                </div>
                            </div><!-- Product Single - Share End -->


                            <div class="col_full nobottommargin">

                                <div class="tabs clearfix nobottommargin" id="tab-1">

                                    <ul class="tab-nav nav-single clearfix" id="">
                                    <!-- @if($data->ingredients->count() > 0)
                                        <li><a href="#tabs-1"><i class="icon-align-justify2"></i><span
                                                        class="d-none d-md-inline-block"> Ingredients</span></a></li>
                                    @endif -->
                                    @if(!empty($data->product_nutritional))
                                        <li><a href="#tabs-2"><i class="icon-info-sign"></i><span
                                                        class="d-none d-md-inline-block">Deskripsi</span></a>
                                        </li>
                                    @endif
                                        <li><a href="#tabs-3"><i class="icon-star3"></i><span
                                                        class="d-none d-md-inline-block"> Reviews ({{count($komentar)}})</span></a></li>
                                    </ul>

                                    <div class="tab-container">
                                    <!-- @if($data->ingredients->count() > 0)
                                        <div class="tab-content clearfix" id="tabs-1">
                                        @foreach($data->ingredients as $item)
                                            <div class="ingredient-view__image-list">
                                                <div class="ingredient-view__item">
                                                    <img alt="ingredient" src="{{ asset_url('/images/products/ingredient/'.$item->p_ingredients_image) }}"
                                                         class="ingredient-view__item-image">
                                                    <p class="ingredient-view__item-name no-mar">{{ $item->p_ingredients_name }}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                        </div>
                                    @endif -->
                                    @if(!empty($data->product_nutritional))
                                        <div class="tab-content clearfix" id="tabs-2">
                                            <p class="no-mar">{{ $data->product_nutritional }}</p>

                                        @if(!empty($data->product_nutritional_image1))
                                            <div class="ingredient-view__image-list">
                                                <div class="ingredient-view__item">
                                                    <img alt="ingredient" src="{{asset_url('/images/products/nutritional/'.$data->product_nutritional_image1)}}"
                                                         class="ingredient-view__item-image">
                                                </div>
                                            </div>
                                        @endif
                                        @if(!empty($data->product_nutritional_image2))
                                            <div class="ingredient-view__image-list">
                                                <div class="ingredient-view__item">
                                                    <img alt="ingredient" src="{{asset_url('/images/products/nutritional/'.$data->product_nutritional_image2)}}"
                                                         class="ingredient-view__item-image">
                                                </div>
                                            </div>
                                        @endif
                                        @if(!empty($data->product_nutritional_image3))
                                            <div class="ingredient-view__image-list">
                                                <div class="ingredient-view__item">
                                                    <img alt="ingredient" src="{{asset_url('/images/products/nutritional/'.$data->product_nutritional_image3)}}"
                                                         class="ingredient-view__item-image">
                                                </div>
                                            </div>
                                        @endif
                                        </div>
                                    @endif
                                        <div class="tab-content clearfix" id="tabs-3">

                                            <div id="reviews" class="clearfix">
                                                <div class="product-reviews-content">
                                                    @if($cekRating == 0 && auth('member')->check() && $cekPembelian > 0)
                                                        <div class="add-product-review">
                                                            <h5 class="text-uppercase heading-text-color font-weight-semibold">
                                                                Tuliskan
                                                                penilaian anda</h5>
                                                            <p>Bagaimana tentang produk ini? *</p>

                                                            {{ Form::open(['route' => ['product.review', $data->product_id, $data->product_alias], 'method' => 'post']) }}
                                                            {{ Form::token() }}
                                                            <table class="table cart">
                                                                <thead>
                                                                <tr>
                                                                    <th>&nbsp;</th>
                                                                    <th>1 star</th>
                                                                    <th>2 stars</th>
                                                                    <th>3 stars</th>
                                                                    <th>4 stars</th>
                                                                    <th>5 stars</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>Review</td>
                                                                    <td>
                                                                        <div class="custom-control custom-radio custom-control-inline" style="margin: 0">
                                                                            <input type="radio" name="ratings" value="1" id="1" class="custom-control-input act-select-bank" />
                                                                            <label class="custom-control-label" for="1"></label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="custom-control custom-radio custom-control-inline" style="margin: 0">
                                                                            <input type="radio" name="ratings" value="2" id="2" class="custom-control-input act-select-bank" />
                                                                            <label class="custom-control-label" for="2"></label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="custom-control custom-radio custom-control-inline" style="margin: 0">
                                                                            <input type="radio" name="ratings" value="3" id="3" class="custom-control-input act-select-bank" />
                                                                            <label class="custom-control-label" for="3"></label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="custom-control custom-radio custom-control-inline" style="margin: 0">
                                                                            <input type="radio" name="ratings" value="4" id="4" class="custom-control-input act-select-bank" />
                                                                            <label class="custom-control-label" for="4"></label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="custom-control custom-radio custom-control-inline" style="margin: 0">
                                                                            <input type="radio" name="ratings" value="5" id="5" class="custom-control-input act-select-bank" />
                                                                            <label class="custom-control-label" for="5"></label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="form-group mb-2">
                                                                <label>Komentar <span class="required">*</span></label>
                                                                <textarea cols="5" rows="6" name="rating_desc"
                                                                          class="form-control form-control-sm"></textarea>
                                                            </div><!-- End .form-group -->

                                                            <button type="submit" class="button button-circle">Submit Review</button>
                                                            {{ Form::close() }}
                                                        </div>
                                                    @endif
                                                    <div class="mt-3"></div>
                                                    <h3><i class="icon-user"></i> Review</h3>

                                                            <div id="reviews" class="clearfix">

                                                                <ol class="commentlist clearfix">
                                                                    @foreach($komentar as $item)
                                                                    <li class="comment even thread-even depth-1" id="li-comment-1">
                                                                        <div id="comment-1" class="comment-wrap clearfix">

                                                                            <div class="comment-meta">
                                                                                <div class="comment-author vcard">
																		<span class="comment-avatar clearfix">
																			<img alt=''
                                                                                 src="{{asset_url('/images/customer/'.$item->cusRating->customer_image)}}"
                                                                                 height='60' width='60'/></span>
                                                                                </div>
                                                                            </div>

                                                                            <div class="comment-content clearfix">
                                                                                <div class="comment-author">{{$item->cusRating->customer_name}}</div>
                                                                                <p>{{$item->p_rating_desc}}</p>
                                                                                <div class="review-comment-ratings">
                                                                                    {{rating($item->p_rating_value)}}
                                                                                </div>
                                                                            </div>

                                                                            <div class="clear"></div>

                                                                        </div>
                                                                    </li>
                                                                    @endforeach
                                                                </ol>
                                                            </div>
                                                <!-- End .add-product-review -->
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                </div>
            </div>

        </div>

    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset_url('/frontend/js/jquery.preloaders.min.js') }}"></script>
    <script>
        $(function () {
            $("#actAddCart").on("click", function () {
                $.preloader.start({
                    modal: true,
                    src: 'sprites.png'
                });

                $.ajax({
                    url: '{{ route('customer.cart.add', [$data->product_alias]) }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: $("#formAddCart").serializeArray(),
                    dataType: 'json',
                    success: function (res) {
                        $.preloader.stop();

                        if (res.success) {
                            $("#headCartCount").html(eval(res.item));
                            $("#headCartTotal").html('Rp '+res.total);
                            $("#headCartBtn").show();

                            var html = '';

                            $.each(res.data, function (index, item) {
                                html += '<div class="top-cart-item clearfix">';
                                html += '    <div class="top-cart-item-image">';
                                html += '       <a href="#"><img src="{{ get_image_product('') }}/'+item.attributes.image+'" alt="'+item.name+'"/></a>';
                                html += '    </div>';
                                html += '    <div class="top-cart-item-desc">';
                                html += '       <a href="#">'+item.name+'</a>';
                                html += '       <div class="quantity clearfix" style="margin-top: 10px;">';
                                html += '           <input readonly type="text" step="1" min="1" name="quantity" value="'+item.quantity+'" title="Qty" class="qty" size="4">';
                                html += '       </div>';
                                html += '       <span class="top-cart-item-price">Rp '+item.price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')+'</span>';
                                html += '    </div>';
                                html += '</div>';
                            });

                            $("#headCartItem").html(html);

                            swal("Berhasil !", "Produk telah ditambahkan di keranjang belanja", "success");
                        }
                        else {
                            location.href = res.url;
                        }
                    }
                });
            });

            $("#actAddWishlist").on("click", function () {
                $.preloader.start({
                    modal: true,
                    src: 'sprites.png'
                });

                $.ajax({
                    url: '{{ route('product.wishlist.act', [$data->product_alias]) }}',
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: {alias: '{{ $data->product_alias }}'},
                    dataType: 'json',
                    success: function (res) {
                        $.preloader.stop();

                        if (res.success) {
                            swal("Berhasil !", res.msg, "success")
                                .then(function () {
                                    location.reload();
                                });
                        }
                        else {
                            location.href = res.url;
                        }
                    }
                });
            });
        });
    </script>

@endsection

@section('script')

@endsection