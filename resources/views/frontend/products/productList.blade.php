@extends('frontend.layouts.app')
@section('title', 'Menu Bisnis')
@section('content')
    <section>
        <div class="container-fluid clearfix">
            <img src="" alt="">
            <!-- <img src="{{asset_url('/frontend/menu/hero-img-2.png?v=3')}}" alt=""> -->
        </div>
    </section>
    <!-- Content
    ============================================= -->
    <section id="content">

        <div>

            <div class="container clearfix">

                <div class="clear"></div>

                <div class="tabs topmargin-lg clearfix" id="tab-3">
                    <div class="row justify-content-center">
                        <ul class="tab-nav clearfix">
                            <li><a href="#tabs-9"><img class="icon-img" src="{{asset_url('/frontend/icon/icon_lunch-box.svg')}}" alt="icon">
                                    Paket 1</a></li>
                            <li class="load-lunchBox" data-id="Lunch Box" data-url="{{route('cek.data.lunchbox')}}"><a href="#tabs-10">
                                    <img class="icon-img" src="{{asset_url('/frontend/icon/icon_lunch-box.svg')}}" alt="icon">
                                    Paket 2</a></li>
                            <li class="load-lunchBox" data-id="SharedMeals" data-url="{{route('cek.data.lunchbox')}}"><a href="#tabs-11"><img class="icon-img" src="{{asset_url('/frontend/icon/icon_lunch-box.svg')}}"
                                                        alt="icon"> Paket 3</a></li>
                            <!-- <li class="load-lunchBox" data-id="Drinks" data-url="{{route('cek.data.lunchbox')}}"><a href="#tabs-12"><img class="icon-img" src="{{asset_url('/frontend/icon/icon_drinks.svg')}}" alt="icon">
                                    Drinks</a></li> -->
                        </ul>
                    </div>

                    <div class="tab-container">

                        <div class="tab-content clearfix" id="tabs-9">

                            <div id="shop" class="shop clearfix">
                                @foreach($dataHighlight as $item)
                                <div class="product clearfix">
                                    <div class="product-image">
                                        <a href="#"><img src="{{ asset_url('/images/products/'.$item->product_image)}}" alt="Checked Short Dress"></a>
                                    @if(!empty($item->product_image2))
                                        <a href="#"><img src="{{ asset_url('/images/products/'.$item->product_image2)}}" alt="Checked Short Dress"></a>
                                    @endif

                                        <div class="sale-flash">{{$item->category->parent->p_cat_name}}</div>
                                        <div class="product-overlay">
                                            <a href="{{route('product.detail', ['alias' => $item->product_alias])}}"><span>Lihat Detail Menu</span></a>
                                        </div>
                                    </div>
                                    <div class="product-desc">
                                        <div class="product-title">
                                            <h4><a href="{{route('product.detail', ['alias' => $item->product_alias])}}">{{$item->product_name}}</a></h4>
                                        </div>
                                        <div class="product-price"><span style="font-size: 20px"> Rp {{ price_format($item->product_price) }}</span></div>
                                        <div class="product-rating">
                                            {{rating(intval($item->avgRating))}}
                                        </div>
                                        <a href="{{route('product.detail', ['alias' => $item->product_alias])}}" class="btn add-cart-btn btn-block">
                                            Detail
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            <ul class="pagination" style="float: right; margin-top: 30px">
                                {{ $dataHighlight->links("pagination::bootstrap-4") }}
                            </ul>
                        </div>

                        <div class="tab-content clearfix Lunch-box" id="tabs-10">

                        </div>

                        <div class="tab-content clearfix Sharedmeals" id="tabs-11">

                        </div>

                        <div class="tab-content clearfix Drinks" id="tabs-12">

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <script>
        var _token = '{{ csrf_token() }}';
        var _submenu = '{{ $subMenu }}';
    </script>
    <script src="{{asset_url('frontend/js/loadType.js?v=1807')}}"></script>
@endsection