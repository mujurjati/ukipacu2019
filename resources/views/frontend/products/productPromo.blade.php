@extends('frontend.layouts.app')
@section('title', 'Menu Promo')
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-promo">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-wrap-product text-center">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area end -->

    <!-- page main wrapper start -->
    <div class="shop-main-wrapper pt-60 pb-60">
        <div class="container">
            <div class="row">
                <!-- sidebar area start -->
                <div class="col-lg-3 order-2 order-lg-1">
                    <div class="sidebar-wrapper mt-md-60 mt-sm-60">
                        <!-- single sidebar start -->
                        {{--<div class="sidebar-single">--}}
                            {{--<div class="sidebar-title">--}}
                                {{--<h3>{{(count($proSliderCat) > 0 ? $proSliderCat[0]->p_cat_name : '')}}</h3>--}}
                            {{--</div>--}}
                            {{--<div class="sidebar-body">--}}
                                {{--<ul class="sidebar-category">--}}
                                    {{--@if(count($proSliderCat) > 0)--}}
                                        {{--@foreach($proSliderCat as $key => $cat)--}}
                                            {{--@if($key !== 0)--}}
                                                {{--<li>--}}
                                                    {{--<a href="{{route('product.cat', ['alias'=>$cat->p_cat_alias])}}">{{$cat->p_cat_name}}</a>--}}
                                                {{--</li>--}}
                                            {{--@endif--}}
                                        {{--@endforeach--}}
                                    {{--@endif--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <!-- single sidebar end -->

                        <!-- single sidebar start -->
                        <div class="sidebar-single">
                            <div class="sidebar-banner">
                                <a href="#">
                                    <img src="{{asset_url('frontend/assets/img/banner/banner_left.jpg')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- single sidebar end -->
                    </div>
                </div>
                <!-- sidebar area end -->
                <!-- shop main wrapper start -->
                <div class="col-lg-9 order-1 order-lg-2">
                    <div class="shop-product-wrapper">
                        <!-- shop product top wrap start -->
                        <div class="shop-top-bar">
                            <div class="row">
                                <div class="col-xl-5 col-lg-4 col-md-3 order-2 order-md-1">
                                    <div class="top-bar-left">
                                        <div class="product-view-mode">
                                            <a class="active" href="#" data-target="grid-view"><i class="fa fa-th"></i></a>
                                            <a href="#" data-target="list-view"><i class="fa fa-list"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-7 col-lg-8 col-md-9 order-1 order-md-2">
                                    <div class="top-bar-right">
                                        <div class="product-short">
                                            <p>Sort By : </p>
                                            {{ Form::open(['id' => 'formSort', 'method' => 'get']) }}
                                            <div class="select-custom">
                                                {{
                                                    Form::select('sort', [
                                                        'terbaru'   => 'Terbaru',
                                                        'az'        => 'A-Z',
                                                        'za'        => 'Z-A',
                                                        'termurah'  => 'Termurah',
                                                        'termahal'  => 'Termahal',
                                                        'terlaris'  => 'Terlaris'
                                                    ],
                                                    Request::input('sort'),
                                                    [
                                                        'id'    => 'productSort',
                                                        'class' => 'nice-select'
                                                    ])
                                                }}
                                            </div><!-- End .select-custom -->

                                            {{ Form::hidden('min', $minPrice) }}
                                            {{ Form::hidden('max', $maxPrice) }}
                                            {{ Form::close() }}
                                        </div>
                                        <div class="product-amount">
                                            <p>Showing {{$data->currentPage()}}–{{$data->perPage()}} of {{$data->total()}}
                                                results</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- shop product top wrap start -->

                        <!-- product item list start -->

                        <div class="shop-product-wrap grid-view row">
                            @foreach($data as $key => $item)
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <!-- product grid item start -->
                                    <div class="product-item mb-{{($key % 2 == 0 ? '50' : '30')}}">
                                        <div class="product-thumb">
                                            <a href="{{route('product.detail', ['alias' => $item->product_alias])}}">
                                                @if(!empty($item->product_image))
                                                    <img src="{{ asset_url('/images/products/'.$item->product_image)}}"
                                                         alt="product"
                                                         style="width: 100%; height: 200px; object-fit: cover">
                                                @else
                                                    <img src="{{ asset_url('/images/products/default-black.png') }}"
                                                         alt="product">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="product-content text-center">
                                            <div class="ratings">
                                                <span><i class="ion-android-star"></i></span>
                                                <span><i class="ion-android-star"></i></span>
                                                <span><i class="ion-android-star"></i></span>
                                                <span><i class="ion-android-star"></i></span>
                                                <span><i class="ion-android-star"></i></span>
                                            </div>
                                            <div class="product-name">
                                                <h4 class="h5">
                                                    <a href="{{route('product.detail', ['alias' => $item->product_alias])}}">{{$item->product_name}}</a>
                                                </h4>
                                            </div>
                                            <div class="price-box">
                                                @if($item->product_discount > 0)
                                                    <span class="old-price"><del>Rp. {{ price_format($item->product_price) }}</del></span>
                                                @endif
                                                <span class="regular-price">Rp. {{price_format($item->product_price-$item->product_discount)}}</span>
                                            </div>
                                            <div class="product-action-link">
                                                <a href="#" data-toggle="tooltip" title="Wishlist"><i
                                                            class="ion-android-favorite-outline"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add to cart"><i
                                                            class="ion-bag"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i
                                                            class="ion-ios-shuffle"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- product grid item end -->
                                    <!-- product list item start -->
                                    <div class="product-list-item mb-{{($key % 2 == 0 ? '30' : '50')}}">
                                        <div class="product-thumb">
                                            <a href="{{route('product.detail', ['alias' => $item->product_alias])}}">
                                                @if(!empty($item->product_image))
                                                    <img src="{{ asset_url('/images/products/'.$item->product_image)}}"
                                                         alt="product"
                                                         style="width: 50%; height: 200px; object-fit: cover">
                                                @else
                                                    <img src="{{ asset_url('/images/products/default-black.png') }}"
                                                         alt="product">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="product-content-list">
                                            <div class="ratings">
                                                <span><i class="ion-android-star"></i></span>
                                                <span><i class="ion-android-star"></i></span>
                                                <span><i class="ion-android-star"></i></span>
                                                <span><i class="ion-android-star"></i></span>
                                                <span><i class="ion-android-star"></i></span>
                                            </div>
                                            <div class="product-name">
                                                <h4>
                                                    <a href="{{route('product.detail', ['alias' => $item->product_alias])}}">{{$item->product_name}}</a>
                                                </h4>
                                            </div>
                                            <div class="price-box">
                                                @if($item->product_discount > 0)
                                                    <span class="old-price"><del>Rp. {{ price_format($item->product_price) }}</del></span>
                                                @endif
                                                <span class="regular-price">Rp. {{price_format($item->product_price-$item->product_discount)}}</span>
                                            </div>
                                            <p>{{$item->product_sortdesc}}</p>
                                            <div class="action-link">
                                                <a href="#" data-toggle="tooltip" title="Add to cart" class="add-to-cart">add
                                                    to cart</a>
                                                <a href="#" data-toggle="tooltip" title="Wishlist"><i
                                                            class="ion-android-favorite-outline"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i
                                                            class="ion-ios-shuffle"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- product list item start -->
                                </div>
                            @endforeach
                        </div>

                        <!-- product item list end -->

                        <!-- start pagination area -->
                    {{ $data->appends(['keyword' => Request::input('keyword'), 'cat' => Request::input('cat')])->links("pagination::bootstrap-4") }}
                    <!-- end pagination area -->
                    </div>
                </div>
                <!-- shop main wrapper end -->
            </div>
        </div>
    </div>
    <!-- page main wrapper end -->
    <script>
        !function (e) {
            jQuery(document).ready(function () {
                var o = document.getElementById("price-input");

                null != o && (noUiSlider.create(o, {
                    start: [{{ $minPrice }}, {{ $maxPrice }}],
                    connect: true,
                    step: 100,
                    margin: 100,
                    range: {min: 0, max: {{ $limitPrice }} }
                }), o.noUiSlider.on("update", function (o) {
                    var min = o[0].split('.');
                    var max = o[1].split('.');

                    $('#minPrice').val(min[0]);
                    $('#maxPrice').val(max[0]);

                    o = o.map(function (e) {
                        return "Rp" + formatNumber(e)
                    });

                    e("#filter-price-range").text(o.join(" - "))
                }));

                $("#productSort").on("change", function () {
                    console.log('s');
                    $("#formSort").submit();
                });
            });

        }(jQuery);
    </script>
@endsection