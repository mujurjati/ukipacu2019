@extends('frontend.layouts.app')
@section('title', 'Berita & Kegiatan')
@section('content')
    <section id="page-title" class="page-title-parallax page-title-dark page-title-right" style="padding: 250px 0; background-image: url({{ asset_url('frontend/menu/blog_2.jpg') }}); background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 440px;" data-top-bottom="background-position:0px -500px;">
        <div class="container clearfix center">
            <img src="{{ asset_url('frontend/images/logoheader.png') }}" alt="">
        </div>
    </section><!-- #page-title end -->

    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <!-- Posts
                ============================================= -->
                <div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">
                @foreach($dataBlog as $item)
                    <div class="entry clearfix">
                        <div class="entry-image">
                            <a href="{{ route('blog.detail', [$item->content_alias]) }}" data-lightbox="image">
                                <img class="image_fade" src="{{ get_image_content($item->content_image) }}" alt="{{$item->content_name}}">
                            </a>
                        </div>
                        <div class="entry-title">
                            <h2>
                                <a href="{{ route('blog.detail', [$item->content_alias]) }}">{{$item->content_name}}</a>
                            </h2>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><i class="icon-calendar3"></i> {{ date('d F Y', strtotime($item->content_publish_date)) }}</li>
                            <li><i class="icon-user"></i> <span>{{$item->user->user_name}}</span></li>
                        </ul>
                        <div class="entry-content">
                            <p>{{$item->content_sortdesc}}</p>
                            <a href="{{ route('blog.detail', [$item->content_alias]) }}" class="more-link">Read More</a>
                        </div>
                    </div>
                @endforeach
                </div><!-- #posts end -->

                <!-- Pagination
                ============================================= -->
                {{ $dataBlog->links("pagination::bootstrap-4") }}

            </div>

        </div>

    </section><!-- #content end -->
@endsection