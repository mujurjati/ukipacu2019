@extends('frontend.layouts.app')
@section('title', $dataBlog->content_name)
@section('content')
    <section id="page-title" class="page-title-parallax page-title-dark page-title-right" style="padding: 250px 0; background-image: url({{ asset_url('frontend/menu/blog_1.jpg') }}); background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 440px;" data-top-bottom="background-position:0px -500px;">
        <div class="container clearfix center">
            <img src="{{ asset_url('frontend/images/logo.png') }}" alt="">
            <h2>{{$dataBlog->content_name}}</h2>
        </div>
    </section><!-- #page-title end -->

    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <!-- Post Content
                ============================================= -->
                <div class="postcontent nobottommargin clearfix">

                    <div class="single-post nobottommargin">

                        <!-- Single Post
                        ============================================= -->
                        <div class="entry clearfix">

                            <!-- Entry Title
                            ============================================= -->
                            <div class="entry-title">
                                <h2>{{$dataBlog->content_name}}</h2>
                            </div><!-- .entry-title end -->

                            <!-- Entry Meta
                            ============================================= -->
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> {{ date('d F Y', strtotime($dataBlog->content_publish_date)) }}</li>
                                <li><a href="#"><i class="icon-user"></i> {{$dataBlog->user->user_name}}</a></li>
                            </ul><!-- .entry-meta end -->

                            <!-- Entry Image
                            ============================================= -->
                            <div class="entry-image">
                                <a href="#"><img src="{{get_image_content($dataBlog->content_image)}}" alt="{{$dataBlog->content_name}}"></a>
                            </div><!-- .entry-image end -->

                            <!-- Entry Content
                            ============================================= -->
                            <div class="entry-content notopmargin">
                                {!! $dataBlog->content_desc !!}

                                <div class="clear"></div>

                                <!-- Post Single - Share
                                ============================================= -->
                                <div class="si-share noborder clearfix">
                                    <span>Share this Post:</span>
                                    <div>
                                        <a href="http://www.facebook.com/sharer.php?u={{ route('blog.detail', [$dataBlog->content_alias]) }}" class="social-icon si-borderless si-facebook">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="https://twitter.com/share?url={{ route('blog.detail', [$dataBlog->content_alias]) }}&amp;text={{ $dataBlog->content_name }}&amp;hashtags=caterin" class="social-icon si-borderless si-twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="https://plus.google.com/share?url={{ route('blog.detail', [$dataBlog->content_alias]) }}" class="social-icon si-borderless si-gplus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>
                                    </div>
                                </div><!-- Post Single - Share End -->

                            </div>
                        </div><!-- .entry end -->

                    </div>

                </div><!-- .postcontent end -->

                <!-- Sidebar
                ============================================= -->
                <div class="sidebar nobottommargin col_last clearfix">
                    <div class="sidebar-widgets-wrap">

                        <div class="widget clearfix">

                            <div class="tabs nobottommargin clearfix" id="sidebar-tabs">

                                <h4>Terbaru</h4>

                                <div id="recent-post-list-sidebar">
                                @foreach($dataTerbaru as $item)
                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href="{{ route('blog.detail', [$item->content_alias]) }}" class="nobg"><img class="rounded-circle" src="{{get_image_content($item->content_image)}}" alt="{{$item->content_name}}"></a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="{{ route('blog.detail', [$item->content_alias]) }}">{{$item->content_name}}</a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li>{{ date('d F Y', strtotime($item->content_publish_date)) }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                                </div>

                            </div>

                        </div>


                        <div class="widget clearfix">

                            <h4>Tags</h4>
                            <div class="tagcloud">
                            @foreach($dataCat as $item)
                                <a href="{{ route('blog.cat', [$item->c_cat_alias]) }}">{{  $item->c_cat_name }}</a>
                            @endforeach
                            </div>

                        </div>

                    </div>

                </div><!-- .sidebar end -->

            </div>

        </div>

    </section><!-- #content end -->
@endsection