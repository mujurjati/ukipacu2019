@extends('frontend.layouts.app')
@section('title', 'Tentang Kami')
@section('content')

    <div class="login-register-wrapper topmargin-lg">
        <div class="container">
            <div class="member-area-from-wrap">
                <div class="row">
                    <!-- Login Content Start -->
                    <div class="col-lg-12">
                        <div class="login-reg-form-wrap  pr-lg-50">
                            <h3>AKTA</h3>
                                <div class="form-group">
                                    <!-- <iframe id="ebook-read" src="{{ asset_url('/file/PT._USAHA_KREATIF_INDONESIA_PACU.pdf')}}" width='100%' height='700' allowfullscreen webkitallowfullscreen>
                                    </iframe> -->
                                    <a href="{{ asset_url('/file/PT._USAHA_KREATIF_INDONESIA_PACU.pdf')}}" target="_blank">Open!</a>
                                </div>
                        </div>
                    </div>
                    <!-- Login Content End -->
                    <div class="col-lg-1">
                    </div>

                    <!-- Register Content Start -->
                    <div class="col-lg-12">
                        <div class="login-reg-form-wrap mt-md-60 mt-sm-60">
                            <h3>IZIN USAHA</h3>
                            <div class="form-group">
                                <!-- <iframe id="ebook-read" src="{{ asset_url('/file/IZIN_USAHA_PT_USAHA_KREATIF_INDONESIA_PACU.pdf') }}" width='100%' height='700' allowfullscreen webkitallowfullscreen>
                                </iframe> -->
                                <a href="{{ asset_url('/file/IZIN_USAHA_PT_USAHA_KREATIF_INDONESIA_PACU.pdf') }}" target="_blank">Open!</a>
                            </div>
                        </div>
                    </div>
                    <!-- Register Content End -->
                    <div class="col-lg-12">
                        <div class="login-reg-form-wrap mt-md-60 mt-sm-60">
                            <h3>NIB</h3>
                            <div class="form-group">
                                <!-- <iframe id="ebook-read" src="{{ asset_url('/file/NIB_PT._USAHA_KREATIF_INDONESIA_PACU.pdf')}}" width='100%' height='700' allowfullscreen webkitallowfullscreen>
                                </iframe> -->
                                <a href="{{ asset_url('/file/NIB_PT._USAHA_KREATIF_INDONESIA_PACU.pdf')}}" target="_blank">Open!</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="login-reg-form-wrap mt-md-60 mt-sm-60">
                            <h3>SK KEMENKUMHAM</h3>
                            <div class="form-group">
                                <!-- <iframe id="ebook-read" src="{{ asset_url('/file/SK_Kemenkumham.pdf?1')}}" width='100%' height='700'>
                                </iframe> -->
                                <a href="{{ asset_url('/file/SK_Kemenkumham.pdf')}}" target="_blank">Open!</a>
                                <!-- <embed type="application/pdf" src="{{ asset_url('/file/SK_Kemenkumham.pdf')}}" width="600" height="400"></embed> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
