@extends('backend.layouts.login-app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="p-3">
                <div class="float-right text-right"><h4 class="font-18 mt-3 m-b-5">Welcome Back !</h4>
                    <p class="text-muted">Sign in to continue.</p></div>
                <a href="{{url('/')}}" class="logo-admin"><img
                            src="{{ asset_url('/frontend/images/logoheader.png') }}"
                            height="50"
                            alt="logo"></a></div>
            <div class="p-3">
                {{Form::open(['route' => 'login', 'method' => 'POST', 'class' => 'form-horizontal m-t-10'])}}
                    <div class="form-group {{ $errors->has('user_email') || $errors->has('user_username') ? ' has-error' : '' }}">
                        <label for="user_username">Username or Email</label>
                        <input type="text" class="form-control" id="user_username" name="user_username"
                               placeholder="Enter username">
                        @if ($errors->has('user_username'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('user_username') }}</strong>
                                    </span>
                        @endif

                        @if ($errors->has('user_email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="userpassword">Password</label>
                        <input type="password" class="form-control" id="userpassword" name="password"
                               placeholder="Enter password">
                    </div>
                    <div class="form-group row m-t-30">
                        <div class="col-sm-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="remember"
                                       {{ old('remember') ? 'checked' : '' }} class="custom-control-input"
                                       id="customControlInline">
                                <label class="custom-control-label" for="customControlInline">Remember me</label></div>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>
                    <div class="form-group m-t-30 mb-0 row">
                        <div class="col-12 text-center">
                            <a href="{{ route('password.request') }}" class="text-muted">
                                <i class="mdi mdi-lock"></i> Forgot your password?</a></div>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
