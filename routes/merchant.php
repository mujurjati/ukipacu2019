<?php

$app->get('/', 'HomeController@index')->middleware('merchant')->name('merchant.home');
$app->get('/login', 'LoginController@index')->name('merchant.login');
$app->post('/login', 'LoginController@login')->name('merchant.login.act');
$app->get('/logout', 'LoginController@logoutMerchant')->name('merchant.logout');

$app->group(['prefix' => 'transaksi', 'middleware' => 'merchant'], function ($app) {
    $app->get('/list/{status}', 'TransaksiController@index')->name('trx.list');
    $app->get('/detailTrx/{id}/{status}', 'TransaksiController@detail')->name('trx.detail');
});

$app->group(['prefix' => 'pelanggan', 'middleware' => 'merchant'], function ($app) {
    $app->get('/list', 'CustomerController@index')->name('cus.list');
    $app->get('/add', 'CustomerController@add')->name('cus.add');
    $app->post('/add', 'CustomerController@add')->name('cus.add.act');
    $app->get('/detail/{id}', 'CustomerController@detail')->name('cus.detail');
    $app->post('/getCusname', 'CustomerController@ajax_getCustomer')->name('ajax.get-customer');
    // $app->get('/detailTrx/{id}/{status}', 'TransaksiController@detail')->name('trx.detail');
});
