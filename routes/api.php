<?php

Route::get('/', function () { return ''; });
Route::post('/', function () { return ''; });


Route::group(['prefix' => 'customer', 'namespace' => 'ApiCustomer'], function ($app) {
    require_once(dirname(__FILE__).'/api-customer.php');
});

Route::group(['prefix' => 'driver', 'namespace' => 'ApiDriver'], function ($app) {
    require_once(dirname(__FILE__).'/api-driver.php');
});

Route::group(['namespace' => 'Api'], function ($app) {
$app->get('/', function () { return 'a'; });
$app->post('/Detailcostumber', 'APICostumberController@detail')->middleware('apii');
$app->post('/DetailMerchant', 'APIMerchanController@detail')->middleware('apii');
$app->post('/Listcostumber', 'APICostumberController@index')->middleware('apii');
$app->post('/Addcostumber', 'APICostumberController@insert')->middleware('apii');
$app->post("/login", "APIAuthController@login");
$app->post("/login2", "APIAuthController@login2");
$app->post("/login3", "APIAuthController@login3");
$app->post("/logout", "APIAuthController@logout");
$app->post("/cekcostumer", "APIAuthController@cekLoginUser");
$app->post("/cekmerchan", "APIAuthController@cekLoginMerchan");
$app->post("/city", "APICostumberController@getcity")->middleware('apii');
});