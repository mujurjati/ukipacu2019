<?php

$app->get('/comingsoon', 'HomeController@comingsoon')->name('home.comingsoon');
$app->get('/', 'HomeController@index');
$app->get('/login', 'CustomersController@index')->name('customers.login');
$app->post('/login', 'CustomersController@loginCus')->name('customers.login.act');
$app->get('/register', 'CustomersController@registerCus')->name('customers.register');
$app->post('/register-add', 'CustomersController@registerCus')->name('customers.register.act');
$app->get('/verification/{email}', 'CustomersController@verificationCus')->name('customers.verification');
$app->post('/reset-pass', 'CustomersController@resetCus')->name('customers.resetPass');
$app->get('/reset/{email}', 'CustomersController@directReset')->name('customers.reset');
$app->post('/reset-act/{token}', 'CustomersController@actReset')->name('customers.reset.act');
$app->get('/clear-session', 'CustomersController@clearSessionSosmed')->name('clear.session.sosmed');

$app->get('auth/{provider}', 'CustomersController@redirectToProvider')->name('customer.sosial');
$app->get('auth/{provider}/callback', 'CustomersController@handleProviderCallback')->name('customer.sosial.callback');

$app->group(['prefix' => 'customers', 'middleware' => 'member'], function ($app) {
    $app->post('/activation', 'CustomersController@verify')->name('customers.activation');
    $app->get('/logout', 'CustomersController@logoutCus')->name('customers.logout');
    $app->get('/dashboard', 'CustomersController@dashboardCus')->name('customers.dashboard');

    /*profile*/
    $app->get('/profil', 'CustomersController@profil')->name('customers.profil');
    $app->post('/save-profil', 'CustomersController@saveProfile')->name('customers.profil.save');

    /*address*/
    $app->get('/address', 'CustomersController@address')->name('customers.address');
    $app->post('/address-save', 'CustomersController@addressSave')->name('customers.addressSave');
    $app->post('/address-update-primary', 'CustomersController@addressUpdatePrimary');
    $app->post('/address-update/{id}', 'CustomersController@addressUpdate')->name('customers.addressUpdate');
    $app->post('/address-del', 'CustomersController@addressDel')->name('customers.addressDel');

    /*bank*/
    $app->get('/bank', 'BankController@bankList')->name('customers.bank');
    $app->post('/bankAdd', 'BankController@bankAdd')->name('customers.bankAdd');
    $app->post('/bank-update/{id}', 'BankController@bankUpdate')->name('customers.bankUpdate');
    $app->post('/bank-del', 'BankController@bankDel');

    /*ebook subscribe*/
    $app->get('/ebook-subscribe', 'EbookController@ebookSubscribe')->name('customers.ebook');
    $app->post('/cancel-subscribe', 'EbookController@ebookCancelSubscribe');
    $app->get('/ebook-saya', 'EbookController@myEbook')->name('customers.myebook');
    $app->get('/ebook-saya/detail/{id}', 'EbookController@myEbookDetail')->name('customers.myebook.detail');

    /*wishlist*/
    $app->group(['prefix' => 'wishlist'], function ($app) {
        $app->get('/', 'CustomersController@wishlist')->name('customers.wishlist');
        $app->post('/delete', 'CustomersController@wishlist_delete')->name('customers.wishlist.delete');
    });

    /*order*/
    $app->group(['prefix' => 'order'], function ($app) {
        $app->get('/{status?}', 'OrderController@order_list')->name('customers.order');
        $app->get('/detail/{number}', 'OrderController@order_detail')->name('customers.order.detail');
        $app->post('/cancel', 'OrderController@cancel_order')->name('customers.order.ajax.cancel');
        $app->post('/receive', 'OrderController@receive_order')->name('customers.order.ajax.receive');
    });
});

$app->group(['prefix' => 'products'], function ($app) {
    $app->get('/detail/{alias}', 'ProductController@detail')->name('product.detail');
    $app->get('/category/{alias}', 'ProductController@category')->name('product.cat');
    $app->get('/categories', 'ProductController@searchFromHeader')->name('product.catSearch');
    $app->post('/review/{pro_id}/alias/{alias}', 'ProductController@review')->name('product.review');
    $app->post('/wishlist/act/{alias}', 'ProductController@wishlist')->name('product.wishlist.act');

    $app->get('/promo', 'ProductController@productPromo')->name('product.product_promo');
    $app->get('/bisnis', 'ProductController@productBisnis')->name('product.product_bisnis');
    $app->get('/spesial', 'ProductController@productSpesial')->name('product.product_spesial');
    $app->get('/promo_hari_ini', 'ProductController@productHIPromo')->name('product.product_promo_hi');
    $app->get('/paket_hemat', 'ProductController@productHemat')->name('product.product_hemat');
    $app->get('/paket_diet', 'ProductController@productDiet')->name('product.product_diet');

    $app->get('/list/{sub?}', 'ProductController@productList')->name('product.product_list');
    $app->post('/list-lunchbox', 'ProductController@productListLunchbox')->name('cek.data.lunchbox');
});

$app->group(['prefix' => 'cart'], function ($app) {
    $app->get('/', 'CartController@list_cart')->name('customer.cart');
    $app->post('/order/{alias}', 'CartController@order_cart')->name('customer.cart.order');
    $app->get('/checkout', 'CartController@checkout')->name('customer.cart.checkout');
    $app->get('/payment', 'CartController@payment')->name('customer.cart.payment');
    $app->post('/payment/check', 'CartController@payment_check')->name('customer.cart.payment.check');
    $app->get('/confirm', 'CartController@confirm')->name('customer.cart.confirm');
});

$app->group(['prefix' => 'blog'], function ($app) {
    $app->get('/', 'BlogController@index')->name('blog.list');
    $app->get('/detail/{alias}', 'BlogController@detail')->name('blog.detail');
    $app->get('/category/{alias}', 'BlogController@category')->name('blog.cat');
    $app->get('/archive/{bulan}/{tahun}', 'BlogController@archive')->name('blog.archive');
});

$app->group(['prefix' => 'about'], function ($app) {
    $app->get('/', 'AboutController@index')->name('about.about');
});

$app->group(['prefix' => 'contact'], function ($app) {
    $app->get('/', 'ContactController@index')->name('contact.contact');
    $app->post('/add', 'ContactController@add')->name('contact.add');
});

$app->group(['prefix' => 'storecorner'], function ($app) {
    $app->get('/', 'StorecornerController@index')->name('storecorner');
});

$app->group(['prefix' => 'ajax'], function($app) {
    $app->group(['prefix' => 'cart'], function($app) {
        $app->post('/get', 'AddressController@ajax_merchant')->name('ajax.get-merchant');
        $app->post('/edit', 'CartController@edit_cart')->name('customer.cart.edit');
        $app->post('/add/{alias}', 'CartController@add_cart')->name('customer.cart.add');
        $app->post('/voucher/check', 'CartController@check_voucher')->name('customer.cart.voucher');
        $app->post('/create_order', 'CartController@create_order')->name('customer.cart.create_order');
    });

    $app->group(['prefix' => 'address'], function($app) {
        $app->post('/get', 'AddressController@ajax_address')->name('ajax.get-address');
//            $app->post('/get', 'AddressController@ajax_merchant')->name('ajax.get-merchant');
        $app->post('/add', 'AddressController@ajax_address_add')->name('ajax.add-address');
        $app->post('/select/{id}', 'AddressController@ajax_address_select')->name('ajax.select-address');
        $app->post('/shipping', 'AddressController@ajax_shipping')->name('ajax.get-shipping');
        $app->post('/shipping/select', 'AddressController@ajax_shipping_select')->name('ajax.sel-shipping');
    });
});

$app->group(['prefix' => 'ebook'], function ($app) {
    $app->get('/paket/{filter?}', 'EbookController@listEbook')->name('ebook.list');
    $app->get('/detail/{id}', 'EbookController@detail')->name('ebook.detail');
    $app->get('/payment', 'EbookController@payment')->name('ebook.payment');
    $app->post('/payment/subscribe', 'EbookController@subscribe')->name('ebook.create_order');
    $app->get('/payment/confirm', 'EbookController@confirm')->name('ebook.confirm');
});

$app->group(['prefix' => 'bantuan'], function ($app) {
    $app->get('/term', 'BantuanController@terms')->name('bantuan.terms');
    $app->get('/kebijakanprivasi', 'BantuanController@kebijakanprivasi')->name('bantuan.kebijakanprivasi');
    $app->get('/panduankeamanan', 'BantuanController@panduankeamanan')->name('bantuan.panduankeamanan');
});

$app->group(['prefix' => 'video'], function ($app) {
    $app->get('/', 'VideoController@index')->name('video.list');
});