<?php

Route::get('/', function () { return ''; });
Route::post('/', function () { return ''; });

Route::post('/login', 'DriverController@login');

Route::group(['middleware' => 'auth:driver-api'], function ($app) {
    $app->group(['prefix' => 'account'], function ($app) {
        $app->post('/detail', 'DriverController@detail');
    });

    $app->group(['prefix' => 'order'], function ($app) {
        $app->post('/list', 'OrderController@order_list');
        $app->post('/detail', 'OrderController@order_detail');

        $app->post('/update', 'OrderController@order_update');
    });

    $app->post('/push_location', 'DriverController@push_location');
});