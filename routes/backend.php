<?php

Auth::routes();

$app->get('/', 'HomeController@index')->name('admin.home');

$app->group(['middleware' => 'admin'], function($app) {
    $app->get('/multiple', function(){
        return view('backend.libs.image.multiple');
    });
});

$app->group(['prefix' => 'users','middleware' => 'admin'], function ($app) {
    $app->get('/', 'UsersController@index')->name('admin.user');
    $app->get('/add', 'UsersController@add')->name('admin.user.add');
    $app->post('/add', 'UsersController@add')->name('admin.user.add.save');
    $app->get('/edit/{id}','UsersController@edit')->name('admin.user.edit');
    $app->post('/update/{id}','UsersController@update')->name('admin.user.update');
    $app->post('/hapus/{id}', 'UsersController@hapus')->name('admin.user.hapus');

    /*Level*/
    $app->get('/level', 'UsersController@level')->name('admin.user.level');
    $app->get('/editlevel/{id}', 'UsersController@editlevel')->name('admin.user.editlevel');
    $app->post('/updatelevel/{id}', 'UsersController@updatelevel')->name('admin.user.updatelevel');
});

$app->group(['prefix' => 'contact', 'middleware' => 'admin'], function ($app) {
    $app->get('/', 'ContactController@index')->name('admin.contact');
});

$app->group(['prefix' => 'customers', 'middleware' => 'admin'], function ($app) {
    $app->get('/', 'CustomersController@index')->name('admin.customer');
    $app->get('/detail/{id}', 'CustomersController@detail')->name('admin.customer.detail');
    $app->get('/edit/{id}', 'CustomersController@edit')->name('admin.customer.edit');
    $app->post('/edit/{id}', 'CustomersController@edit')->name('admin.customer.edit.save');
    $app->post('/block/{id}', 'CustomersController@block')->name('admin.customer.block');
    $app->post('/open/{id}', 'CustomersController@open')->name('admin.customer.open');
    $app->get('/create', 'CustomersController@create')->name('admin.customer.create');
    $app->post('/create', 'CustomersController@create')->name('admin.customer.create.save');
});
$app->group(['prefix' => 'video', 'middleware' => 'admin'], function ($app) {
    $app->get('/', 'VideoController@index')->name('admin.video');
    $app->get('/create', 'VideoController@create')->name('admin.video.create');
    $app->post('/create', 'VideoController@create')->name('admin.video.create.save');
    $app->get('/edit/{id}','VideoController@edit')->name('admin.video.edit');
    $app->post('/edit/{id}','VideoController@update')->name('admin.video.update');
});

$app->group(['prefix' => 'marketing', 'middleware' => 'admin'], function ($app) {
    $app->get('/', 'MerchantController@index')->name('admin.merchant');
    $app->get('/detail/{id}', 'MerchantController@detail')->name('admin.merchant.detail');
    $app->post('/block/{id}', 'MerchantController@block')->name('admin.merchant.block');
    $app->post('/open/{id}', 'MerchantController@open')->name('admin.merchant.open');
    $app->get('/create', 'MerchantController@create')->name('admin.merchant.create');
    $app->post('/create', 'MerchantController@create')->name('admin.merchant.create.save');
    $app->get('/edit/{id}','MerchantController@edit')->name('admin.merchant.edit');
    $app->post('/edit/{id}','MerchantController@update')->name('admin.merchant.update');
});

$app->group(['prefix' => 'reward', 'middleware' => 'admin'], function ($app) {
    $app->get('/', 'RewardController@index')->name('admin.reward');
    $app->get('/detail/{id}', 'RewardController@detail')->name('admin.reward.detail');
    $app->get('/create', 'RewardController@create')->name('admin.reward.create');
    $app->post('/create', 'RewardController@create')->name('admin.reward.create.save');
    $app->get('/edit/{id}','RewardController@edit')->name('admin.reward.edit');
    $app->post('/edit/{id}','RewardController@edit')->name('admin.reward.update');
    $app->post('/approved/{id}/{param}','RewardController@approve')->name('admin.reward.approve');
});

$app->group(['prefix' => 'paket', 'middleware' => 'admin'], function ($app) {
    $app->get('/', 'PaketController@index')->name('admin.paket');
    $app->get('/detail/{id}', 'PaketController@detail')->name('admin.paket.detail');
    $app->post('/create', 'PaketController@create')->name('admin.paket.create.save');
    $app->post('/delete/{id}/{param?}','PaketController@delete')->name('admin.paket.delete');
    $app->post('/get-produk', 'PaketController@ajax_produk')->name('ajax.get-produk');
    $app->post('/add-produk/{id}', 'PaketController@add_produk')->name('admin.paket.add.produk');
});

$app->group(['prefix' => 'driver', 'middleware' => 'admin'], function ($app) {
    $app->get('/', 'DriverController@index')->name('admin.driver');
    $app->get('/detail/{id}', 'DriverController@detail')->name('admin.driver.detail');
    $app->post('/block/{id}', 'DriverController@block')->name('admin.driver.block');
    $app->post('/open/{id}', 'DriverController@open')->name('admin.driver.open');
    $app->get('/create', 'DriverController@create')->name('admin.driver.create');
    $app->post('/create', 'DriverController@create')->name('admin.driver.create.save');
    $app->get('/edit/{id}','DriverController@edit')->name('admin.driver.edit');
    $app->post('/edit/{id}','DriverController@update')->name('admin.driver.update');
});

$app->group(['prefix' => 'product_cat','middleware' => 'admin'], function ($app) {
    $app->get('/', 'ProductCatController@list_cat')->name('admin.product-cat');
    $app->get('/add', 'ProductCatController@add_cat')->name('admin.product-cat.add');
    $app->post('/add', 'ProductCatController@add_cat')->name('admin.product-cat.add.save');
    $app->post('/delete/{id}', 'ProductCatController@delete_cat')->name('admin.product-cat.add.delete');
    $app->get('/edit/{section}/{id}/{attr_id?}', 'ProductCatController@edit_cat')->name('admin.product-cat.edit');
    $app->post('/edit/detail/{id}', 'ProductCatController@edit_detail')->name('admin.product-cat.edit.detail');
    $app->post('/edit/attribute-add/{id}', 'ProductAttrController@add_attr')->name('admin.product-attr.add');
    $app->post('/edit/attribute-edit/{attr_id}', 'ProductAttrController@edit_attr')->name('admin.product-attr.edit');
    $app->post('/edit/attribute-delete/{attr_id}', 'ProductAttrController@delete_attr')->name('admin.product-attr.delete');
    $app->post('/edit/attribute-table/{attr_id}', 'ProductAttrController@table_attr')->name('admin.product-attr.table');
});

$app->group(['prefix' => 'product', 'middleware' => 'admin'], function ($app) {
    $app->get('/','ProductController@index')->name('admin.product-list');
    $app->get('/create','ProductController@create_product')->name('admin.product.create');
    $app->post('/store','ProductController@create_product')->name('admin.product.store');
    $app->get('/getChildCat/{p_cat_parent}','ProductController@getSubCategory')->name('admin.product.getCat');
    $app->get('/getLastCat/{last_cat}','ProductController@getLastCategory')->name('admin.product.getLastCat');
    $app->get('/getAttribut/{id}','ProductController@getAttribute')->name('admin.product.getAttribute');
    $app->post('/uploadimage', 'ProductController@UploadImage')->name('pictures.store');
    $app->get('/edit/{id}','ProductController@edit')->name('admin.product.edit');
    $app->post('/edit/{id}','ProductController@edit')->name('admin.product.update');
    $app->get('/show/{id}','ProductController@show')->name('admin.product.show');
    $app->post('/delete/{id}', 'ProductController@destroy')->name('admin.product.delete');
    $app->post('/deleteStok', 'ProductController@destroyStok')->name('admin.product.delStok');
});

$app->group(['prefix' => 'order', 'middleware' => 'admin'], function($app){
    $app->get('/', 'OrderController@index')->name('admin.order');
    $app->get('/add', 'OrderController@addOrder')->name('admin.order.add');
    $app->get('/detail/{id}', 'OrderController@detail')->name('admin.order.detail');
    $app->post('/change-status/{id}', 'OrderController@updateStatus')->name('admin.order.updatestatus');
    $app->post('/change-driver', 'OrderController@updateDriver')->name('admin.order.updateDriver');
    $app->post('/setType_eceran', 'OrderController@setType_eceran')->name('admin.order.setType-eceran');
    $app->post('/setType_paket', 'OrderController@setType_paket')->name('admin.order.setType-paket');
    $app->post('/pilihMember', 'OrderController@pilihMember')->name('admin.order.pilihMember');
    $app->post('/add-row', 'OrderController@addRow')->name('admin.order.addRow');
});

$app->group(['prefix' => 'content', 'middleware' => 'admin'], function($app){
    $app->get('/', 'ContentController@index')->name('admin.content');
    $app->get('/create', 'ContentController@create')->name('admin.content.create');
    $app->post('/store', 'ContentController@store')->name('admin.content.store');
    $app->get('/contentCatChild/{c_cat_parent}','ContentController@getSubCategory')->name('admin.content.getCat');
    $app->get('/ContentCat/{c_cat_parent}','ContentController@getContentCat')->name('admin.content.category');
    $app->get('/edit/{id}','ContentController@edit')->name('admin.content.edit');
    $app->post('/update/{id}', 'ContentController@update')->name('admin.content.update');
    $app->post('/delete/{id}', 'ContentController@destroy')->name('admin.content.delete');
    $app->get('/detail/{id}', 'ContentController@detail')->name('admin.content.detail');


    $app->get('/category', 'CatContentController@index')->name('admin.content.cat');
    $app->post('/category/add', 'CatContentController@catAdd')->name('admin.content.catAdd');
    $app->post('/category/edit/{id}', 'CatContentController@catEdit')->name('admin.content.catEdit');
    $app->post('/category/delete/{id}', 'CatContentController@destroy')->name('admin.content.catDelete');

    $app->get('/broadcat', 'ContentController@broadcastEmail')->name('admin.content.broadcast');
    $app->post('/broadcat/send', 'ContentController@broadcastEmail')->name('admin.broadcast.act');
});


$app->group(['prefix' => 'storecorner', 'middleware' => 'admin'], function($app){
    $app->get('/', 'StorecornerController@index')->name('admin.storecorner');
    $app->get('/create', 'StorecornerController@create')->name('admin.storecorner.create');
    $app->post('/create', 'StorecornerController@create')->name('admin.storecorner.create.save');
    $app->get('/edit/{id}','StorecornerController@edit')->name('admin.storecorner.edit');
    $app->post('/update/{id}','StorecornerController@update')->name('admin.storecorner.update');
    $app->get('/detail/{id}','StorecornerController@detail')->name('admin.storecorner.detail');
    $app->post('/hapus/{id}', 'StorecornerController@hapus')->name('admin.storecorner.hapus');
});

$app->group(['prefix' => 'setting', 'middleware' => 'admin'], function($app){
    /*UMUM*/
    $app->get('/', 'SettingController@index')->name('admin.setting');
    $app->post('/update', 'SettingController@update')->name('admin.setting.update');

    /*SHIPPING*/
    $app->get('/alamatpengiriman', 'SettingController@alamatpengiriman')->name('admin.setting.alamatpengiriman');
    $app->post('/alamatpengirimanUpdate', 'SettingController@alamatpengirimanUpdate')->name('admin.setting.alamatpengirimanUpdate');
    $app->get('/kurirpengiriman', 'SettingController@kurirpengiriman')->name('admin.setting.kurirpengiriman');
    $app->get('/editkurir/{id}', 'SettingController@editkurir')->name('admin.setting.editkurir');
    $app->post('/updatekurir/{id}', 'SettingController@updatekurir')->name('admin.setting.updatekurir');

    /*BANK*/
    $app->get('/bank', 'SettingController@bank')->name('admin.setting.bank');
    $app->get('/bankdetail/{id}', 'SettingController@bankdetail')->name('admin.setting.bankdetail');
    $app->get('/bankedit/{id}', 'SettingController@bankedit')->name('admin.setting.bankedit');
    $app->post('/bankupdate/{id}', 'SettingController@bankupdate')->name('admin.setting.bankupdate');

    /*BANTUAN*/
    $app->get('/kebijakanprivasi', 'SettingController@kebijakanprivasi')->name('admin.setting.kebijakanprivasi');
    $app->post('/kebijakanprivasiUpdate', 'SettingController@kebijakanprivasiUpdate')->name('admin.setting.kebijakanprivasiUpdate');
    $app->get('/panduankeamanan', 'SettingController@panduankeamanan')->name('admin.setting.panduankeamanan');
    $app->post('/panduankeamananUpdate', 'SettingController@panduankeamananUpdate')->name('admin.setting.panduankeamananUpdate');
    $app->get('/terms', 'SettingController@terms')->name('admin.setting.terms');
    $app->post('/termsUpdate', 'SettingController@termsUpdate')->name('admin.setting.termsUpdate');
    $app->get('/about', 'SettingController@about')->name('admin.setting.about');
    $app->post('/aboutUpdate', 'SettingController@aboutUpdate')->name('admin.setting.aboutUpdate');
});

$app->group(['prefix' => 'ebook-package', 'middleware' => 'admin'], function($app){
    $app->get('/', 'EbookPackageController@index')->name('admin.ebookpackage');
    $app->get('/create', 'EbookPackageController@create')->name('admin.ebookpackage.create');
    $app->post('/store', 'EbookPackageController@store')->name('admin.ebookpackage.store');
    $app->get('/edit/{id}', 'EbookPackageController@edit')->name('admin.ebookpackage.edit');
    $app->post('/update/{id}', 'EbookPackageController@update')->name('admin.ebookpackage.update');
    $app->post('/delete/{id}', 'EbookPackageController@destroy')->name('admin.ebookpackage.delete');
    $app->get('/subscribe/{id}', 'EbookPackageController@subscribe')->name('admin.ebookpackage.subscribe');
    $app->get('/subscribe/edit/{id}', 'EbookPackageController@editSubscribe')->name('admin.ebookpackage.subscribe.edit');
    $app->post('/subscribe/update/{id}', 'EbookPackageController@editSubscribe')->name('admin.ebookpackage.subscribe.update');
    $app->get('/detail/{id}', 'EbookPackageController@detail')->name('admin.ebookpackage.detail');
});

$app->group(['prefix' => 'ebook-product', 'middleware' => 'admin'], function($app){
    $app->get('/', 'EbookProductController@index')->name('admin.ebookproduct');
    $app->get('/create', 'EbookProductController@create')->name('admin.ebookproduct.create');
    $app->post('/store', 'EbookProductController@store')->name('admin.ebook.product.store');
    $app->get('/edit/{id}', 'EbookProductController@edit')->name('admin.ebook.product.edit');
    $app->post('/update/{id}', 'EbookProductController@update')->name('admin.ebook.product.update');
    $app->post('/delete/{id}', 'EbookProductController@destroy')->name('admin.ebook.product.delete');
    $app->get('/detail/{id}', 'EbookProductController@detail')->name('admin.ebook.product.detail');
});

$app->group(['prefix' => 'ebook-trx', 'middleware' => 'admin'], function($app){
    $app->get('/', 'EbookSubscribeController@index')->name('admin.ebooktrx');
    $app->get('/detail/{id}', 'EbookSubscribeController@detail')->name('admin.ebooktrx.detail');
    $app->post('/update/{id}', 'EbookSubscribeController@update')->name('admin.ebooktrx.update');
});

$app->group(['prefix' => 'payment', 'middleware' => 'admin'], function($app){
    $app->get('/', 'EbookProductController@index')->name('admin.payment');
});

$app->group(['prefix' => 'promo', 'middleware' => 'admin'], function($app){
    $app->get('/', 'PromoController@index')->name('admin.promo');
    $app->get('/create', 'PromoController@create')->name('admin.promo.create');
    $app->post('/store', 'PromoController@store')->name('admin.promo.store');
    $app->get('/edit/{id}', 'PromoController@edit')->name('admin.promo.edit');
    $app->post('/update/{id}', 'PromoController@update')->name('admin.promo.update');
    $app->post('/delete/{id}', 'PromoController@destroy')->name('admin.promo.delete');
    $app->get('/detail/{id}', 'PromoController@detail')->name('admin.promo.detail');
    $app->get('/listUsed/{id}', 'PromoController@listUsed')->name('admin.promo.used');
});

$app->group(['prefix' => 'report', 'middleware' => 'admin'], function($app){
    $app->get('/', 'ReportController@index')->name('admin.report');
});
