<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*DASHBOARD*/
Route::group(['prefix' => 'uki-panel', 'namespace' => 'Admin'], function ($app) {
    require_once(dirname(__FILE__).'/backend.php');
});

/*FRONT*/
Route::group(['namespace' => 'Front'], function ($app) {
    require_once(dirname(__FILE__).'/front.php');
});

Route::group(['prefix' => 'marketing', 'namespace' => 'Merchant'], function ($app) {
    require_once(dirname(__FILE__).'/merchant.php');
});

Route::any('{query}',
    function() { return redirect('/'); })
    ->where('query', '.*');
