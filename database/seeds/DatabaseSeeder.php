<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ShippingCourierTableSeeder::class);
        $this->call(BankTableSeeder::class);
        $this->call(OrderTableSeeder::class);
        $this->call(OrderDetailTableSeeder::class);
        $this->call(PromoTableSeeder::class);
        $this->call(PromoUsedTableSeeder::class);
        $this->call(EbookPackageTableSeeder::class);
        $this->call(EbookProductTableSeeder::class);
        $this->call(EbookSubscribeTableSeeder::class);
        $this->call(ContentTableSeeder::class);
    }
}
