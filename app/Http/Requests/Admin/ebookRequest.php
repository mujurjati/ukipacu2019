<?php

namespace App\Http\Requests\Admin;
use Illuminate\Foundation\Http\FormRequest;

class ebookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'e_package_name'  => 'required',
            'e_package_shortdesc'  => 'required',
            'e_package_desc'  => 'required',
            'e_package_time'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'e_package_name.required' => 'Nama ebook harus di isi',
            'e_package_shortdesc.required' => 'Shordesch harus di isi',
            'e_package_desc.required' => 'Deskripsi harus Di isi',
            'e_package_time.required' => 'Time harus di isi',
        ];
    }

}
