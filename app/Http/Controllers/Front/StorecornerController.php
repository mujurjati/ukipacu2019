<?php
/**
 * Created by PhpStorm.
 * User: Mukail
 * Date: 08/07/2019
 * Time: 22:04 AM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\StoreCorner;
use App\Models\ShippingSubdistrict;
use Illuminate\Http\Request;

class StorecornerController extends Controller
{
    public function index()
    {

        $data = StoreCorner::with('subdistrict.city.province')
            ->where('s_corner_status', '=', 'active')
            ->orderBy('s_corner_create_date' ,'desc')
            ->paginate(12);

        return view('frontend.storecorner.storecorner', ['data' => $data]);
    }

}