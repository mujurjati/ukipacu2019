<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 16-Jan-19
 * Time: 00:40
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Order;
use App\Models\OrderDetail;

class OrderController extends Controller
{
    public function order_list($status = '', Request $request)
    {
        $dataOrder = Order::with('detail.detail_product.product')
            ->where('customer_id', '=', auth('member')->user()->customer_id)
            ->orderBy('order_create_date', 'desc');

        if(!empty($status))
            $dataOrder = $dataOrder->where('order_status', '=', $status);

        return view('frontend.customers.order.list', [
            'dataOrder'     => $dataOrder,
            'selectStatus'  => $status
        ]);
    }

    public function order_detail($number) {
        $dataOrder = Order::with("detail.detail_product", "detail.merchant", "detail.driver")
            ->where('order_number', $number)
            ->first();

        return view('frontend.customers.order.detail', ['dataOrder' => $dataOrder]);
    }

    public function cancel_order(Request $request) {
        $number = $request->input('order_number');

        $order = Order::where('order_number', '=', $number)->first();

        $order->order_status        = 'cancel';
        $order->order_cancel_date   = date('Y-m-d H:i:s');
        $order->order_cancel_note   = 'Pesanan dibatalkan oleh member';
        $order->order_cancel_by     = 'customer';

        if($order->save()) {
            $res['success'] = true;
            $res['message'] = 'Pembatalan pesanan berhasil';
        }
        else {
            $res['success'] = false;
            $res['message'] = 'Pembatalan pesanan gagal';
        }

        return response($res);
    }

    public function receive_order(Request $request) {
        $number = $request->input('order_number');

        $order = Order::where('order_number', '=', $number)->first();

        $order->order_status            = 'received';
        $order->order_received_date     = date('Y-m-d H:i:s');
        $order->order_received_note     = 'Pesanan telah diterima oleh pelanggan';

        if($order->save()) {
            $res['success'] = true;
            $res['message'] = 'Pesanan telah diterima';
        }
        else {
            $res['success'] = false;
            $res['message'] = 'Gagal update data';
        }

        return response($res);
    }
}