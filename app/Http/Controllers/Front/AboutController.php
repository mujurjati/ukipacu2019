<?php
/**
 * Created by PhpStorm.
 * User: Mukail
 * Date: 08/07/2019
 * Time: 22:04 AM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {

        $data = Content::with('category')
            ->with('user')
            ->where('content_type', '=', 'page')->first();

        return view('frontend.about.about', ['data' => $data]);
    }

}