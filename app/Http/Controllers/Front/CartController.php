<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:47
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Customer;
use App\Models\Product;
use App\Models\Promo;
use App\Models\PromoUsed;
use App\Models\Bank;
use App\Models\Merchant;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderDetailProduct;
use App\Models\ProductSuggest;
use App\Models\Setting;

use Auth;
use Cart;
use RajaOngkir;
use Carbon\Carbon;

class CartController extends Controller
{
    public function list_cart() {
        if(auth('member')->check() && Cart::getContent()->count() == 0)
            return redirect()->to('/');

        $drink      = ProductSuggest::with('product')->where('p_suggest_type', '=', 'drink')->get();
        $dessert    = ProductSuggest::with('product')->where('p_suggest_type', '=', 'dessert')->get();

        return view('frontend.order.cart', [
            'suggestDrink'      => $drink,
            'suggestDessert'    => $dessert
        ]);
    }

    public function add_cart($alias, Request $request) {
        if(!auth('member')->check()) {
            $request->session()->put('login-redirect', route('product.detail', $alias));

            $res['success'] = false;
            $res['message'] = 'Login terlebih dahulu.';
            $res['url']     = route('customers.login');

            return response($res);
        }

        if(Cart::getCondition('Diskon') == null) {
            $discount =  new \Darryldecode\Cart\CartCondition([
                'name' => 'Diskon',
                'type' => 'voucher',
                'target' => 'total',
                'value' => '0',
                'attributes' => [
                    'name' => '',
                    'code' => ''
                ],
                'order' => 1
            ]);

            Cart::condition($discount);
        }

        $qty = $request->input('orderQty');

        $dataProduct = Product::where('product_alias', '=', $alias)->first();

        $price = $dataProduct->product_price-$dataProduct->product_discount;

        Cart::add($dataProduct->product_id, $dataProduct->product_name, $price, $qty, [
            'image'     => $dataProduct->product_image,
            'alias'     => $dataProduct->product_alias,
            'hpp'       => $dataProduct->product_hpp,
            'option'    => ''
        ]);

        $res['success'] = true;
        $res['message'] = 'Produk telah ditambahkan di keranjang belanja';
        $res['item']    = Cart::getContent()->count();
        $res['total']   = json_decode(json_encode(Cart::getTotal()), true);
        $res['data']    = Cart::getContent();

        return response($res);
    }

    public function order_cart($alias, Request $request) {
        if(!auth('member')->check()) {
            $request->session()->put('login-redirect', route('product.detail', $alias));

            return redirect()->route('customers.login');
        }

        if(Cart::getCondition('Diskon') == null) {
            $discount =  new \Darryldecode\Cart\CartCondition([
                'name' => 'Diskon',
                'type' => 'voucher',
                'target' => 'total',
                'value' => '0',
                'attributes' => [
                    'name' => '',
                    'code' => ''
                ],
                'order' => 1
            ]);

            Cart::condition($discount);
        }

        $qty = $request->input('orderQty');

        $dataProduct = Product::where('product_alias', '=', $alias)->first();

        $price = $dataProduct->product_price-$dataProduct->product_discount;

        Cart::add($dataProduct->product_id, $dataProduct->product_name, $price, $qty, [
            'image'     => $dataProduct->product_image,
            'alias'     => $dataProduct->product_alias,
            'hpp'       => $dataProduct->product_hpp,
            'option'    => ''
        ]);

        return redirect()->route('customer.cart');
    }

    public function edit_cart(Request $request) {
        $res = [];

        $type = $request->input('type');

        if($type == 'remove') {
            $id = $request->input('id');

            Cart::remove($id);

            $res['success'] = true;
            $res['type']    = $type;
            $res['id']      = $id;
            $res['cart']    = [
                'cart-subtotal'  => 'Rp '.Cart::getSubTotal(),
                'cart-total'     => 'Rp '.Cart::getTotal()
            ];
        }
        elseif($type == 'qty') {
            $id     = $request->input('id');
            $qty    = $request->input('qty');

            Cart::update($id, [
                'quantity' => ['relative' => false, 'value' => $qty]
            ]);

            $res['success']     = true;
            $res['type']        = $type;
            $res['id']          = $id;
            $res['subtotal']    = 'Rp '.Cart::get($id)->getPriceSum();
            $res['cart']        = [
                'cart-subtotal'  => 'Rp '.Cart::getSubTotal(),
                'cart-total'     => 'Rp '.Cart::getTotal()
            ];
        }
        elseif($type == 'clear') {
            Cart::clearCartConditions();
            Cart::clear();

            $res['success'] = true;
            $res['type']    = $type;
        }
        else {
            $res['success'] = false;
        }

        return response($res);
    }

    public function checkout(Request $request) {
        if(Cart::getContent()->count() == 0)
            return redirect()->to('/');

        $dataCustomer = Customer::where('customer_id', '=', auth('member')->user()->customer_id)->first();

        $setting = new Setting();
        $setting = $setting->select_data();

        $shipping =  new \Darryldecode\Cart\CartCondition([
            'name' => 'Shipping',
            'type' => 'shipping',
            'target' => 'total',
            'value' => $setting['General']['biaya_kirim'],
            'attributes' => [
                'id'        => '',
                'courier'   => '',
                'service'   => '',
                'etd'       => ''
            ],
            'order' => 1
        ]);

        Cart::condition($shipping);

        return view('frontend.order.checkout', ['dataCus' => $dataCustomer]);
    }

    public function check_voucher(Request $request) {
        $voucher = $request->input('voucher');

        $data = Promo::where('promo_code', '=', $voucher)
            ->where('promo_start_date', '<=', date('Y-m-d H:i:s'))
            ->where('promo_end_date', '>=', date('Y-m-d H:i:s'))
            ->where('promo_status', '=', 'active')
            ->whereRaw("promo_qty > promo_used");

        if($data->count() > 0) {
            $dataPromo = $data->first();

            $subtotal  = str_replace('.', '', Cart::getSubTotal());

            if($dataPromo->promo_min_payment > 0 && $subtotal < $dataPromo->promo_min_payment) {
                $res['success'] = false;
                $res['message'] = 'Keranjang belanja Anda tidak memenuhi syarat untuk menggunakan voucher ini';
            }
            else {
                if($dataPromo->promo_type == 'price') {
                    $voucherNominal = $dataPromo->promo_value;
                }
                elseif($dataPromo->promo_type == 'percent') {
                    $voucherNominal = $subtotal*$dataPromo->promo_value/100;

                    if($voucherNominal > $dataPromo->promo_max_discount)
                        $voucherNominal = $dataPromo->promo_max_discount;
                }

                if(isset($voucherNominal)) {
                    $discount =  new \Darryldecode\Cart\CartCondition([
                        'name' => 'Diskon',
                        'type' => 'voucher',
                        'target' => 'total',
                        'value' => -$voucherNominal,
                        'attributes' => [
                            'name' => $dataPromo->promo_name,
                            'code' => $dataPromo->promo_code
                        ],
                        'order' => 1
                    ]);

                    Cart::condition($discount);

                    $res['success']     = true;
                    $res['message']     = "Sekarang Anda berhemat Rp ".price_format($voucherNominal);
                    $res['discount']    = 'Rp -'.price_format($voucherNominal);
                    $res['total']       = 'Rp '.Cart::getTotal();
                }
                else {
                    $res['success'] = false;
                    $res['message'] = 'Voucher yang Anda masukan salah atau sudah kadaluarsa';
                }
            }
        }
        else {
            $res['success'] = false;
            $res['message'] = 'Voucher yang Anda masukan salah atau sudah kadaluarsa';
        }

        return response($res);
    }

    public function payment(Request $request) {
        if(Cart::getContent()->count() == 0)
            return redirect()->to('/');

        $subtotal   = Cart::getSubTotal(false)+Cart::getCondition('Diskon')->getValue();
        $shipping   = Cart::getCondition('Shipping');

        $orderAddr  = $request->session()->get('order-addr');

        $dataBank   = Bank::where('bank_status', '=', 'active')
            ->orderBy('bank_order')
            ->get();

        return view('frontend.order.payment', [
            'subtotal'  => $subtotal,
            'shipping'  => $shipping->getValue(),
            'courier'   => $shipping->getAttributes(),
            'orderAddr' => $orderAddr,
            'dataBank'  => $dataBank
        ]);
    }

    public function payment_check(Request $request) {
        if(
            empty($request->addr_lat) ||
            empty($request->addr_lng) ||
            empty($request->addr) ||
            empty($request->recepient) ||
            empty($request->date) ||
            empty($request->phone) ||
            empty($request->notes)
        ) {
            $error = 'Anda harus melengkapi detail pemesanan';
        }
        else {
            $orderAddr = [
                'lat'   => $request->addr_lat,
                'lng'   => $request->addr_lng,
                'addr'  => $request->addr,
                'name'  => $request->recepient,
                'date'  => $request->date,
                'phone' => $request->phone,
                'notes' => $request->notes,
            ];

            $request->session()->put('order-addr', $orderAddr);
        }

        if(empty($error)) {
            $res['success'] = true;
            $res['message'] = 'Berhasil';
        }
        else {
            $res['success'] = false;
            $res['message'] = $error;
        }

        return response($res);
    }

    public function create_order(Request $request) {
        $dataShipping   = $request->session()->get('order-addr');
        $dataBank       = Bank::where('bank_id', '=', $request->input('bank_id'))->first();

        $add = new Order();

        $total  = str_replace('.', '', Cart::getTotal());
        $unique = $this->generate_unique($total);

        if(Cart::getCondition('Diskon') != null && !empty(Cart::getCondition('Diskon')->getAttributes()['code'])) {
            $dataPromo = Promo::where('promo_code', '=',Cart::getCondition('Diskon')->getAttributes()['code']);

            if($dataPromo->count() > 0) {
                $dataPromo = $dataPromo->first();

                $add->promo_id              = $dataPromo->promo_id;
                $add->order_discount_price  = abs(intval(Cart::getCondition('Diskon')->getValue()));
            }
        }

        $add->order_number                  = $this->generate_code();
        $add->customer_id                   = auth('member')->user()->customer_id;
        $add->order_subtotal                = intval(Cart::getSubtotal(false));
        $add->order_shipment_price          = intval(Cart::getCondition('Shipping')->getValue());
        $add->order_unique_code             = $unique;
        $add->order_total                   = $total+$unique;
        $add->order_bank_to                 = $dataBank->bank_name;
        $add->order_bank_to_account         = $dataBank->bank_account;
        $add->order_bank_to_number          = $dataBank->bank_number;
        $add->order_bank_to_branch          = $dataBank->bank_branch;
        $add->order_shipment_date           = $dataShipping['date'];
        $add->order_shipment_lat            = $dataShipping['lat'];
        $add->order_shipment_lng            = $dataShipping['lng'];
        $add->order_shipment_recipient      = $dataShipping['name'];
        $add->order_shipment_address        = $dataShipping['addr'];
        $add->order_shipment_phone          = $dataShipping['phone'];
        $add->order_note                    = $dataShipping['notes'];
        $add->order_status                  = 'pending_payment';
        $add->order_pending_payment_date    = date('Y-m-d H:i:s');
        $add->order_create_date             = date('Y-m-d H:i:s');

        if($add->save()) {
            $arrIn = [];

            foreach(Cart::getContent() as $item) {
                $product = Product::where('product_id', '=', $item->id)->first();

                $arrIn[$product->merchant_id][] = [
                    'id'        => $item->id,
                    'name'      => $item->name,
                    'image'     => $item->attributes->image,
                    'price'     => $item->price,
                    'hpp'       => $item->attributes->hpp,
                    'qty'       => $item->quantity,
                    'subtotal'  => $item->price*$item->quantity,
                    'subtotal_hpp'  => $item->attributes->hpp*$item->quantity
                ];
            }

            foreach($arrIn as $merchant_id => $item) {
                $subtotal       = 0;
                $subtotalHpp    = 0;

                foreach ($item as $key) {
                    $subtotal       += $key['subtotal'];
                    $subtotalHpp    += $key['subtotal_hpp'];
                }

                $merchant = Merchant::where('merchant_id', '=', $merchant_id)->first();

                $oDetail = new OrderDetail();

                $oDetail->order_id                      = $add->order_id;
                $oDetail->merchant_id                   = $merchant_id;
                $oDetail->o_detail_store_lat            = $merchant->merchant_lat;
                $oDetail->o_detail_store_lng            = $merchant->merchant_lng;
                $oDetail->o_detail_store_address        = $merchant->merchant_address;
                $oDetail->o_detail_subtotal_hpp         = $subtotalHpp;
                $oDetail->o_detail_subtotal             = $subtotal;
                $oDetail->o_detail_shipping_distance    = 1;
                $oDetail->o_detail_shipping_price       = 0;
                $oDetail->o_detail_total                = $subtotal;
                $oDetail->o_detail_status               = 'new';
                $oDetail->o_detail_create_date          = date('Y-m-d H:i:s');

                if($oDetail->save()) {
                    foreach ($item as $key) {
                        $odProduct = new OrderDetailProduct();

                        $odProduct->o_detail_id         = $oDetail->o_detail_id;
                        $odProduct->product_id          = $key['id'];
                        $odProduct->od_product_name     = $key['name'];
                        $odProduct->od_product_image    = $key['image'];
                        $odProduct->od_product_price    = $key['price'];
                        $odProduct->od_product_hpp      = $key['hpp'];
                        $odProduct->od_product_qty      = $key['qty'];
                        $odProduct->od_product_subtotal = $key['subtotal'];

                        $odProduct->save();
                    }
                }
            }

            if(!empty($add->promo_id)) {
                $dataPromo->promo_used = $dataPromo->promo_used+1;

                if($dataPromo->save()) {
                    $promoUsed = new PromoUsed();

                    $promoUsed->promo_id                  = $add->promo_id;
                    $promoUsed->customer_id               = $add->customer_id;
                    $promoUsed->order_id                  = $add->order_id;
                    $promoUsed->order_total               = $add->order_total;
                    $promoUsed->promo_nominal             = $add->order_discount_price;
                    $promoUsed->promo_used_create_date    = date('Y-m-d H:i:s');
                    $promoUsed->promo_used_status         = '1';

                    $promoUsed->save();
                }
            }

            Cart::clearCartConditions();
            Cart::clear();

            $request->session()->forget('order-addr');

            $request->session()->put('order-confirm', $add->order_number);

            $res['success']     = true;
            $res['message']     = 'Pembelian Anda berhasil. Silahkan selesaikan proses pembayaran transaksi Anda.';
            $res['code']        = $add->order_number;
            $res['order_id']    = $add->order_id;
            $res['url']         = route('customer.cart.confirm');
        }
        else {
            $res['success']     = false;
            $res['message']     = 'Terjadi kesalahan pada saat proses pembayaran. Silahkan ulangi beberapa saat lagi.';
        }

        return response($res);
    }

    public function confirm(Request $request) {
        if(!$request->session()->has('order-confirm'))
            return redirect()->to('/');

        $orderNumber = $request->session()->get('order-confirm');

        $dataOrder = Order::with('detail')
            ->where('order_number', '=', $orderNumber)
            ->where('order_status', '=', 'pending_payment');

        if($dataOrder->count() == 0)
            return redirect()->to('/');

        $dataOrder = $dataOrder->first();

        $expDate = date('Y-m-d H:i:s', strtotime('+1 Day', strtotime($dataOrder->order_create_date)));

        return view('frontend.order.confirm', [
            'dataOrder' => $dataOrder,
            'expDate'   => $expDate
        ]);
    }

    function generate_code() {
        $first  = 'CT';
        $day    = substr(date('D'),0, 1);
        $month  = substr(date('F'),0, 1);
        $year   = substr(date('Y'),-1);

        do {
            $code = $first.$month.$day.$year.rand(10000000, 99999999);

            $count = Order::where('order_number', '=', $code)->count();
        }
        while($count > 0);

        return $code;
    }

    function generate_unique($nominal) {
        do {
            $unique = rand(100, 999);
            $total  = $nominal+$unique;

            $order = Order::where('order_total', '=', $total)->count();

            $count = $order;
        }
        while($count > 0);

        return $unique;
    }
}