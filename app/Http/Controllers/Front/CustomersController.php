<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 12/18/2018
 * Time: 8:31 PM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\CustomerWishlist;
use App\Models\Product;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\Customer;
use App\Models\CustomerPenunjang;
use App\Models\CustomerBank;
use App\Models\CustomerAddress;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Lib\ImageController;
use Laravel\Socialite\Facades\Socialite;
use File;
use Carbon\Carbon;

class CustomersController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function guard()
    {
        return Auth::guard('member');
    }

    public function index()
    {
        if(auth('member')->check())
            return redirect()->route('customers.profil');

        return view('frontend.customers.loginRegister');
    }

    public function registerCus(Request $request)
    {
        if($request->has('addCusBySelf')) {
            $cus = new Customer();
            $this->validate($request, [
                'customer_name'          => 'required',
                'customer_email'         => 'required|email|unique:_customer,customer_email',
                'customer_no_ktp'     => 'required|unique:_customer,customer_nik',
                'customer_password'      => 'required',
                'customer_bank_name' => 'required',
                'customer_bank_nasabah' => 'required',
                'customer_bank_rek' => 'required',
            ]);

            $ceknik = Customer::where('customer_nik', '=', $request->input('customer_no_ktp'))->get();

            if(count($ceknik) > 0) {
                return redirect()->route('customers.register')->with('error', 'NIK member sudah terdaftar');
            } else {

            $cus->customer_name = $request->input('customer_name');
            $cus->customer_reg_id = 0;
            $cus->customer_count =0;
            $cus->merchant_id = 00;
            $cus->customer_email = $request->input('customer_email');
            $cus->customer_npwp = $request->input('customer_npwp');
            $cus->customer_nik = $request->input('customer_no_ktp');
            $cus->customer_city = $request->input('city');
            $cus->customer_phone = $request->input('customer_phone');
            $cus->customer_password = bcrypt($request->input('customer_password'));
            $cus->customer_address = $request->input('customer_address');
            $cus->customer_birth_date = $request->input('customer_tlg');
            $cus->customer_status = 'new';
            $cus->customer_create_date = Carbon::now();

            if ($cus->save()) {
                if($request->input('customer_image') != null) {
                    if(!empty($cus->customer_image) && file_exists(public_path().'/images/customer/'.$cus->customer_image))
                        File::delete(public_path().'/images/customer/'.$cus->customer_image);
    
                    $fileImage  = $request->input('customer_image');
                    $filename   = "customer-".$cus->customer_name;
    
                    $path       = public_path().'/images/customer/';
    
                    $Image      = new ImageController();
                    $fileUpload = $Image->filepond_upload($fileImage, $filename, $path, [300, 300]);
                    $cus->customer_image  = $fileUpload->filename;
                }

                if($cus->save()) {
                    $cus_pen = new CustomerPenunjang();

                    $cus_pen->customer_id = $cus->customer_id;
                    $cus_pen->c_pendidikan = $request->input('customer_pendidikan');
                    $cus_pen->c_pekerjaan = $request->input('customer_perkerjaan');
                    $cus_pen->c_agama = $request->input('customer_agama');
                    $cus_pen->c_ibu_kandung = $request->input('customer_ibu');
                    $cus_pen->c_ahli_waris = $request->input('customer_waris');
                    $cus_pen->c_hub_waris = $request->input('customer_hub_waris');
    
                    if($cus_pen->save()) {
                        $cus_bank = new CustomerBank();
    
                        $cus_bank->customer_id = $cus->customer_id;
                        $cus_bank->s_bank_name = $request->input('customer_bank_name');
                        $cus_bank->c_bank_cabang = $request->input('customer_bank_cabang');
                        $cus_bank->c_bank_nasabah = $request->input('customer_bank_nasabah');
                        $cus_bank->c_bank_rek = $request->input('customer_bank_rek');
        
                        $cus_bank->save();
                    } 
                }                               

                return redirect()->route('customers.register')->with('success', 'Data Member berhasil ditambahkan. Silahkan lakukan transaksi agar akun ada aktif');
            }

            }
        }
        
        return view('frontend.customers.loginRegister');
        // $validator = Validator::make($request->all(), [
        //     'customerType'          => 'required',
        //     'registerName'          => 'required|string|max:255',
        //     'registerEmail'         => 'required|email|unique:_customer,customer_email',
        //     'registerPassword'      => 'required|min:6',
        //     'login_con_password'    => 'required|same:registerPassword',
        // ]);

        // if ($validator->fails()) {
        //     return  redirect()->route('customers.login')->withErrors($validator->messages()->first());
        // }
        // else {
        //     $customer = new Customer();
        //     $customer->customer_type = $request->customerType;
        //     $customer->customer_name = $request->registerName;
        //     $customer->customer_email = $request->registerEmail;

        //     if ($request->session()->has('data_sos') && !empty($request->session()->get('data_sos')))
        //         $customer->customer_status = 'active';

        //     $customer->customer_password = bcrypt($request->registerPassword);

        //     if ($customer->save()) {
        //         $email = $customer->customer_email;
        //         $link = route('customers.verification', md5(md5($email)));
        //         $varMail = array(
        //             'name'  => $customer->customer_name,
        //             'type'  => $customer->customer_type,
        //             'email' => $email,
        //             'link'  => $link
        //         );
        //         if (!$request->session()->has('data_sos') || empty($request->session()->get('data_sos'))) {
        //             Mail::send('emails.activation', $varMail, function($message) use($email) {
        //                 $message->to($email)->subject('Konfirmasi Pendaftaran Akun');
        //             });
        //         }
        //         if ($request->session()->has('data_sos') && !empty($request->session()->get('data_sos'))) {
        //             Auth::guard('member')->login($customer);
        //             return  redirect()->route('customers.dashboard');
        //         } else {
        //             return  redirect()->route('customers.login')->with('success', 'Akun '.$customer->customer_name.' telah terdaftar, silahkan cek email untuk melakukan konfirmasi');
        //         }
        //     }
        // }
    }

    public function verificationCus($email) {
        $dataCus = DB::select(DB::raw("
            SELECT customer_id FROM _customer 
              WHERE MD5(MD5(customer_email)) = '".$email."'
                AND customer_status = 'new'
        "));

        if (count($dataCus) > 0) {
            $update = Customer::find($dataCus[0]->customer_id);
            $update->customer_status = 'active';

            if ($update->save()) {
                return redirect()->route('customers.login')->with('success', 'Akun anda telah AKTIF, silahkan login untuk melakukan transaksi');
            }
        } else {
            return redirect()->route('customers.login')->withErrors('Terjadi kesalahan saat aktivasi, silahkan hubungi admin');
        }
    }

    public function loginCus(Request $request)
    {
        $this->validate($request, [
            'login_email'       => 'required|email',
            'login_password'    => 'required'
        ]);

        if (Auth::guard('member')->attempt(['customer_email' => $request->login_email, 'password' => $request->login_password, 'customer_status'=> 'active'])) {
            flashy()->success('Selamat datang, '.Auth::guard('member')->user()->customer_name);
            if($request->session()->has('login-redirect')) {
                $url = $request->session()->get('login-redirect');

                $request->session()->forget('login-redirect');

                return redirect()->to($url);
            }
            else {
                return redirect()->route('customers.profil');
            }
        }

        flashy()->error('Login gagal, Cek email dan password, atau pastikan akun anda telah terverifikasi!');

        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logoutCus()
    {
        Auth::guard('member')->logout();

        return redirect('/');
    }

    public function resetCus(Request $request) {
        $validator = Validator::make($request->all(), [
            'email'          => 'required'
        ]);

        if ($validator->fails()) {
            flashy()->error($validator->messages()->first());
            return  redirect()->route('customers.login');
        }
        else {
            $email      = $request->input('email');
            $data       = Customer::where('customer_email', '=', $email)->first();
            if (isset($data)) {
                $expired    = date('Y-m-d H:i:s', strtotime('+1 Day', time()));
                $token      = md5(md5($data->customer_email));
                $link       = route('customers.reset', $token);

                $varMail = array(
                    'expired'   => $expired,
                    'data'      => $data,
                    'link'      => $link
                );
                Mail::send('emails.reset', $varMail, function($message) use($email) {
                    $message->to($email)->subject('Reset Password');
                });

                return  redirect()->route('customers.login')->with('success', 'Silahkan cek '.$email.' untuk reset password');
            } else {
                return  redirect()->route('customers.login')->withErrors('Email : '.$email.' tidak terdaftar di sistem kami');
            }

        }
    }

    public function directReset($email) {
        $dataCus = DB::select(DB::raw("
            SELECT customer_id FROM _customer 
              WHERE MD5(MD5(customer_email)) = '".$email."'
        "));

        if (count($dataCus) > 0) {
            return view('frontend.customers.reset', ['data' => $email]);
        } else {
            return  redirect()->route('customers.login')->withErrors('Terjadi kesalahan reset password');
        }
    }

    public function actReset($token, Request $request) {
        $dataCus = DB::select(DB::raw("
            SELECT customer_id FROM _customer 
              WHERE MD5(MD5(customer_email)) = '".$token."'
        "));

        if (count($dataCus) > 0) {
            $update = Customer::find($dataCus[0]->customer_id);
            $update->customer_password = bcrypt($request->input('password'));

            if ($update->save()) {
                flashy()->success('Selamat, password akun '.$update->customer_email.' sudah berhasil diganti');
                return  redirect()->route('customers.login');
            }
        } else {
            flashy()->error('Terjadi kesalahan saat reset password');
        }
    }

    public function dashboardCus()
    {
        $data = Order::select(DB::raw('DISTINCT(order_status) AS status, COUNT(order_status) AS total'))
            ->where('customer_id', '=' , auth('member')->user()->customer_id)
            ->groupBy('order_status')
            ->having('total', '>', 0)
            ->get();

        $arr = array(
            'pending_payment' => 0,
            'paid' => 0,
            'process' => 0,
            'finish' => 0,
            'cancel' => 0,
            'refund' => 0
        );

        foreach ($data as $item) {
            foreach ($arr as $key => $nilai) {
                if ($item->status == $key) {
                    $arr[$key] = $item->total;
                }
            }
        }

        return view('frontend/customers/dashboard', ['data' => $arr]);
    }

    public function profil()
    {
        $dataCus = Customer::where('customer_id', '=', auth('member')->user()->customer_id)->first();
        return view('frontend/customers/profil', ['dataCus' => $dataCus]);
    }

    public function saveProfile(Request $request) {
        $cus = Customer::where('customer_id', '=', auth('member')->user()->customer_id)->first();
        $validator = Validator::make($request->all(), [
            'customer_name'         => 'required',
            'gender'                => 'required',
        ]);

        if ($validator->fails()) {
            flashy()->error($validator->messages()->first());
            return redirect()->route('customers.profil');
        } else {
            $cus->customer_name         = $request->input('customer_name');
            $cus->customer_birth_place  = $request->input('customer_birth_place');
            $cus->customer_birth_date   = $request->input('customer_birth_date');
            $cus->customer_gender       = $request->input('gender');

            if(!empty($request->input('gambar'))) {
                $fileImage = $request->input('gambar');
                $filename   = "customer-1";
                $path       = public_path().'/images/customer/';

                $Image = new ImageController();

                $fileUpload = $Image->filepond_upload($fileImage, $filename, $path);

                $cus->customer_image      = $fileUpload->filename;
            }

            if ($cus->save()) {
                flashy()->success('Perubahan Profil Berhasil');
                return redirect()->route('customers.profil');
            }
        }
    }

    public function address()
    {
        $dataAdd = CustomerAddress::with('subdistrict.city.province')
            ->where('customer_id', '=', auth('member')->user()->customer_id)
            ->orderBy('c_address_id', 'DESC')
            ->get();

        $dataCus = Customer::where('customer_id', '=', auth('member')->user()->customer_id)->first();
        return view('frontend/customers/address', [
            'dataAdd' => $dataAdd,
            'dataCus' => $dataCus
        ]);
    }

    public function addressSave(Request $request) {
        $addR = new CustomerAddress;
        $validator = Validator::make($request->all(), [
            'alamat'            => 'required',
            'name'              => 'required',
            'phone'             => 'required',
            'city'              => 'required',
            'alamat_lengkap'    => 'required',
        ]);

        $addR->customer_id              = auth('member')->user()->customer_id;
        $addR->c_address_type           = $request->input('alamat');
        $addR->s_subdistrict_id         = $request->input('city');
        $addR->c_address_name           = $request->input('name');
        $addR->c_address_phone          = $request->input('phone');
        $addR->c_address_address        = $request->input('alamat_lengkap');
        $addR->c_address_primary        = '0';
        $addR->c_address_create_date    = date('Y-m-d H:i:s');

        if ($validator->fails()) {
            flashy()->error($validator->messages()->first());
        } else {
            flashy()->success('Alamat pengiriman berhasil ditambahkan');
            $addR->save();
        }
        return redirect()->route('customers.address');
    }

    public function  addressUpdatePrimary(Request $request) {
        $addr = CustomerAddress::where('customer_id', '=', auth('member')->user()->customer_id)->get();

        foreach ($addr as $item) {
            if ($item->c_address_id == $request->input('id')) {
                $input = [
                    'c_address_primary' => '1'
                ];
                CustomerAddress::where('c_address_id', '=', $item->c_address_id)->update($input);
            }
            else {
                $input = [
                    'c_address_primary' => '0'
                ];
                CustomerAddress::where('c_address_id', '=', $item->c_address_id)->update($input);
            }
        }

        $res['success'] = true;
        return response($res);
    }

    public function  addressUpdate($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'alamat'            => 'required',
            'name'              => 'required',
            'phone'             => 'required',
            'city'              => 'required',
            'alamat_lengkap'    => 'required',
        ]);

        $input = [
            'c_address_type'        => $request->input('alamat'),
            's_subdistrict_id'      => $request->input('city'),
            'c_address_name'        => $request->input('name'),
            'c_address_phone'       => $request->input('phone'),
            'c_address_address'     => $request->input('alamat_lengkap'),
            'c_address_primary'     => $request->input('c_address_primary')
        ];

        if ($validator->fails()) {
            flashy()->error($validator->messages()->first());
        } else {
            CustomerAddress::where('c_address_id', '=', $id)->update($input);
            flashy()->success('Perubahan alamat pengiriman berhasil diperbaharui');
        }
        return redirect()->route('customers.address');
    }

    public function addressDel(Request $request) {
        CustomerAddress::where('c_address_id', '=', $request->input('id'))->delete();
        $res['success'] = true;
        $res['id'] = $request->input('id');
        return response($res);
    }

    public function wishlist() {
        $dataWishlist = CustomerWishlist::where('customer_id', '=', auth('member')->user()->customer_id)
            ->where('c_wishlist_status', '=', '1')
            ->orderBy('c_wishlist_create_date', 'DESC')
            ->paginate(16);

        return view('frontend/customers/wishlist', [
            'dataWishlist' => $dataWishlist
        ]);
    }

    public function wishlist_delete(Request $request) {
        $alias = $request->input('alias');

        $dataProduct = Product::where('product_alias', '=', $alias)->first();

        $up = CustomerWishlist::where('customer_id', '=', auth('member')->user()->customer_id)
            ->where('product_id', '=', $dataProduct->product_id)
            ->first();

        $up->c_wishlist_status = '0';

        if($up->save())
            $res['success'] = true;
        else
            $res['success'] = false;

        return $res;
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($social, Request $request)
    {
        $userSocial = Socialite::driver($social)->user();
        $customer = Customer::where(['customer_email' => $userSocial->getEmail()])->first();
        if ($customer) {
            Auth::guard('member')->login($customer);
            flashy()->success('Selamat datang, '.$userSocial->getName());
            return redirect()->route('customers.dashboard');
        }else{
            $dataLogin = array('name_sos' => $userSocial->getName(), 'email_sos' => $userSocial->getEmail());
            $request->session()->put('data_sos', $dataLogin);
            return redirect()->route('customers.login');
        }
    }

    public function clearSessionSosmed(Request $request) {
        $request->session()->forget('data_sos');
        return redirect()->route('customers.login');
    }


}
