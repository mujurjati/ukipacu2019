<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\Content;
use App\Models\Product;
use App\Models\Paket;
use App\Models\Video;
use App\Post;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        /*DATA SLIDER*/
        $dataSlider = Content::where('content_type', '=', 'slider')
            ->where('content_status', '=', 'publish')
            ->orderBy('content_publish_date', 'DESC')
            ->limit(1)
            ->first();

        /*PRODUK TERBARU*/
        $dataProMinggu = Product::where('product_status', '=', 'publish')
            ->whereRaw("FIND_IN_SET('minggu_ini', product_tags) > 0")
            ->orderBy('product_create_date', 'DESC')
            ->limit(5)
            ->get();

        $dataProTerbaru = Product::where('product_status', '=', 'publish')
            ->where("product_tags", '=', 'promo')
            ->orderBy('product_create_date', 'DESC')
            ->limit(5)
            ->get();

        $paket = Paket::get();

        return view('frontend/layouts/front', [
            'slider' => $dataSlider,
            'proTerbaru' => $dataProTerbaru,
            'proMinggu' => $dataProMinggu,
            'paket'     =>$paket
        ]);
    }

    public function comingsoon()
    {
        return view('frontend.comingsoon.index');
    }
}
