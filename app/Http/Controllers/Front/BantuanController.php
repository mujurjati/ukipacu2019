<?php
/**
 * Created by PhpStorm.
 * User: Mukail
 * Date: 08/07/2019
 * Time: 22:04 AM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class BantuanController extends Controller
{
    public function terms()
    {
        return view('frontend.bantuan.terms');
    }

    public function kebijakanprivasi()
    {
        return view('frontend.bantuan.kebijakanprivasi');
    }

    public function panduankeamanan()
    {
        return view('frontend.bantuan.panduankeamanan');
    }

}