<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 1/11/2019
 * Time: 10:35 PM
 */

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\EbookPackage;
use App\Models\EbookProduct;
use App\Models\EbookSubscribe;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Order;

class EbookController extends Controller
{
    public function listEbook($type="") {
        $ebookPackage = EbookPackage::where('e_package_status', '=', 'active')
            ->orderBy('e_package_id', 'DESC');
        if ($type != "") {
            $ebookPackage->where('e_package_type', '=', $type);
        }

        $ebookPackageTerbaru = EbookPackage::where('e_package_status', '=', 'active')
            ->orderBy('e_package_id', 'DESC')
            ->limit(3)
            ->get();
        
        return view('frontend.ebook.list', [
            'dataEbook' => $ebookPackage->get(),
            'ebookTerbaru' => $ebookPackageTerbaru
        ]);
    }

    public function detail($id) {
        $ebookPackage = EbookPackage::where('e_package_id', '=', $id)->first();
        Session::put('dataEbook', $ebookPackage);

        $ebookPackageTerbaru = EbookPackage::where('e_package_status', '=', 'active')
            ->orderBy('e_package_id', 'DESC')
            ->limit(3)
            ->get();

        return view('frontend/ebook/detail', ['dataEbook' => $ebookPackage, 'ebookTerbaru' => $ebookPackageTerbaru]);
    }

    public function payment() {
        if (auth('member')->check()) {
            $dataBank   = Bank::where('bank_status', '=', 'active')
                ->orderBy('bank_order')
                ->get();

            return view('frontend.ebook.payment', ['dataBank' => $dataBank]);
        }
        else {
            Session::put('login-redirect', route('ebook.detail', session()->get('dataEbook')->e_package_id));

            return redirect()->route('customers.login');
        }
    }

    public function subscribe(Request $request) {
        $bank_id = $request->input('id_bank');
        $data_bank = Bank::where('bank_id', '=', $bank_id)->first();

        $subscribe = new EbookSubscribe;
        $nominal = session()->get('dataEbook')->e_package_price;
        $unique  = $this->generate_unique($nominal);
        if ($data_bank->count() > 0 && session()->has('dataEbook')) {
            $subscribe->e_package_id            = session()->get('dataEbook')->e_package_id;
            $subscribe->customer_id             = auth('member')->user()->customer_id;
            $subscribe->e_subscribe_type        = session()->get('dataEbook')->e_package_type;
            $subscribe->e_subscribe_time        = session()->get('dataEbook')->e_package_time;
            $subscribe->e_subscribe_price       = $nominal+$unique;
            $subscribe->e_subscribe_active_date = '';
            $subscribe->e_subscribe_expired_date= '';
            $subscribe->e_subscribe_status      = 'pending';
            $subscribe->e_bank_to               = $data_bank->bank_name;
            $subscribe->e_bank_to_account       = $data_bank->bank_account;
            $subscribe->e_bank_to_number        = $data_bank->bank_number;
            $subscribe->e_bank_to_branch        = $data_bank->bank_branch;
            $subscribe->e_subscribe_create_date = date('Y-m-d H:i:s');

            Session::put('dataEbookConfirm', $subscribe);
            if ($subscribe->save()) {
                $email = auth('member')->user()->customer_email;
                $expDate = date('Y-m-d H:i:s', strtotime('+1 Day', strtotime($subscribe->e_subscribe_create_date)));
                $varMail = array(
                    'data'      => $subscribe,
                    'expDate'   => $expDate
                );

                Mail::send('emails.subscribe', $varMail, function($message) use($email) {
                    $message->to($email)->subject('Ebook Langganan');
                });
            }

            $res['success'] = true;
            $res['url'] = route('ebook.confirm');
            $res['message'] = 'Berhasil';
        }
        else {
            $res['success'] = false;
            $res['url'] = url('/');
            $res['message'] = 'Terjadi Kesalahan';
        }

        return response($res);
    }

    public function confirm() {
        if (session()->has('dataEbook'))
            $dataConfirm = session()->get('dataEbookConfirm');

        $nominal = $dataConfirm->e_subscribe_price;
        $unique = $this->generate_unique($dataConfirm->e_subscribe_price);
        $expDate = date('Y-m-d H:i:s', strtotime('+1 Day', strtotime($dataConfirm->e_subscribe_create_date)));
        return view('frontend.ebook.confirm', [
            'dataConfirm' => $dataConfirm,
            'expDate' => $expDate,
            'nominal' => $unique+$nominal
        ]);
    }

    public function ebookSubscribe() {
        $dataEbook = EbookSubscribe::with('package')
            ->where('customer_id', '=', auth('member')->user()->customer_id)
            ->orderBy('e_subscribe_id', 'DESC')
            ->get();

        return view('frontend.customers.ebookSubscribe', ['dataEbook' => $dataEbook]);
    }

    public function myEbook() {
        $dataFull = EbookSubscribe::select('_ebook_product.*')
            ->join('_ebook_product', function($join) {
                $join->on('_ebook_product.e_product_publish_date', '>=', '_ebook_subscribe.e_subscribe_active_date')
                    ->where('_ebook_product.e_product_publish_date', '<=', '_ebook_subscribe.e_subscribe_expired_date');
            })
            ->where('_ebook_subscribe.customer_id', '=', auth('member')->user()->customer_id)
            ->where('_ebook_subscribe.e_subscribe_type', '=', 'full')
            ->where('_ebook_subscribe.e_subscribe_status', '=', 'success')
            ->where('_ebook_product.e_product_status', '=', 'active');

        $dataGanjilGenap = EbookSubscribe::select('_ebook_product.*')
            ->join('_ebook_product', '_ebook_subscribe.e_subscribe_type', '=', '_ebook_product.e_product_type')
            ->where('_ebook_subscribe.customer_id', '=', auth('member')->user()->customer_id)
            ->where('_ebook_subscribe.e_subscribe_type', '!=', 'full')
            ->where('_ebook_subscribe.e_subscribe_status', '=', 'success')
            ->where('_ebook_product.e_product_status', '=', 'active')
            ->where('_ebook_product.e_product_status', '=', 'active');

        $allData = $dataFull->unionAll($dataGanjilGenap)->get();

        return view('frontend.customers.myEbook', ['myEbook' => $allData]);
    }

    public function myEbookDetail($id) {
        $data = EbookProduct::where('e_product_id', '=', $id)->first();
        return view('frontend.customers.myEbookDetail', ['myEbookDetail' => $data]);
    }

    public function ebookCancelSubscribe(Request $request) {
        $updateCancel = EbookSubscribe::find($request->input('id'));
        $updateCancel->e_subscribe_status = 'cancel';

        if ($updateCancel->save()) {
            $res['success'] = true;
            $res['url']     = route('customers.ebook');
        }

       return response($res);
    }

    function generate_unique($nominal) {
        do {
            $unique = rand(100, 999);
            $total  = $nominal+$unique;

            $order = Order::where('order_total', '=', $total)->count();
            $subsc = EbookSubscribe::where('e_subscribe_price', '=', $total)->count();

            $count = $order+$subsc;
        }
        while($count > 0);

        return $unique;
    }
}