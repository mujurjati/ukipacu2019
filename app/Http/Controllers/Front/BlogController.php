<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 12/29/2018
 * Time: 11:04 AM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $cat = new Content();

        $qry = Content::with('category')
            ->with('user')
            ->where('content_type', '=', 'blog');

        if($request->has('s') && !empty($request->input('s'))) {
            $s = $request->input('s');

            $qry->where('content_name', 'LIKE', "%".$s."%")
                ->orWhere('content_sortdesc', 'LIKE', "%".$s."%")
                ->orWhere('content_tags', 'LIKE', "%".$s."%");
        }

        $dataBlog = $qry->orderBy('content_publish_date', 'DESC')
            ->paginate(15);

        return view('frontend.blog.blogList', [
            'dataBlog'      => $dataBlog,
            'dataCat'       => $cat->listCat(),
            'dataTerbaru'   => $cat->contentTerbaru(),
            'dataArchive'   => $cat->contentArsip()
        ]);
    }

    public function category($alias, Request $request) {
        $cat = new Content();

        $qry = Content::with('category')
            ->with('user')
            ->whereHas('category', function ($qry) use($alias) {
                $qry->where('c_cat_alias', '=', $alias);
            });

        if($request->has('s') && !empty($request->input('s'))) {
            $s = $request->input('s');

            $qry->where('content_name', 'LIKE', "%".$s."%")
                ->orWhere('content_sortdesc', 'LIKE', "%".$s."%")
                ->orWhere('content_tags', 'LIKE', "%".$s."%");
        }

        $dataBlog = $qry->orderBy('content_publish_date', 'DESC')
            ->paginate(15);

        return view('frontend.blog.blogList', [
            'dataBlog'      => $dataBlog,
            'dataCat'       => $cat->listCat(),
            'dataTerbaru'   => $cat->contentTerbaru(),
            'dataArchive'   => $cat->contentArsip()
        ]);
    }

    public function archive($bulan, $tahun, Request $request) {
        $cat = new Content();

        $qry = Content::with('category')
            ->with('user')
            ->whereMonth('content_publish_date', '=', $bulan)
            ->whereYear('content_publish_date', '=', $tahun);

        if($request->has('s') && !empty($request->input('s'))) {
            $s = $request->input('s');

            $qry->where('content_name', 'LIKE', "%".$s."%")
                ->orWhere('content_sortdesc', 'LIKE', "%".$s."%")
                ->orWhere('content_tags', 'LIKE', "%".$s."%");
        }

        $dataBlog = $qry->orderBy('content_publish_date', 'DESC')
            ->paginate(15);

        return view('frontend.blog.blogList', [
            'dataBlog'      => $dataBlog,
            'dataCat'       => $cat->listCat(),
            'dataTerbaru'   => $cat->contentTerbaru(),
            'dataArchive'   => $cat->contentArsip()
        ]);
    }

    public function detail($alias) {
        $cat = new Content();

        $detailBlog = Content::with('category')
            ->with('user')
            ->where('content_type', '=', 'blog')
            ->where('content_alias', '=', $alias)
            ->first();

        $dataBlog = Content::with('category')
            ->with('user')
            ->where('content_type', '=', 'blog')
            ->where('content_alias', '!=', $alias)
            ->where('c_cat_id', '!=', $detailBlog->c_cat_id)
            ->get();

        return view('frontend.blog.blogDetail', [
            'dataBlog'      => $detailBlog,
            'dataBlogList'  => $dataBlog,
            'dataCat'       => $cat->listCat(),
            'dataTerbaru'   => $cat->contentTerbaru()
        ]);
    }
}