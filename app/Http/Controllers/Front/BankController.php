<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 1/10/2019
 * Time: 2:25 AM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\CustomerBank;
use App\Models\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class BankController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function bankList() {
        $dataBank = CustomerBank::with('bank')
            ->where('customer_id', '=', Auth::guard('member')->user()->customer_id)
            ->orderBy('c_bank_id', 'DESC')
            ->get();

        $dataFromBank = Bank::select('bank_name')->get();

        return view('frontend/customers/bank', [
            'dataBank' => $dataBank,
            'dataFromBank' => $dataFromBank
        ]);
    }

    public function bankAdd(Request $request) {
        $addBank = new CustomerBank;
        $validator = Validator::make($request->all(), [
            'c_bank_name'            => 'required',
            'c_bank_branch'          => 'required',
            'c_bank_acc'             => 'required',
            'c_bank_number'          => 'required'
        ]);

        $addBank->customer_id        = auth('member')->user()->customer_id;
        $addBank->c_bank_name        = $request->input('c_bank_name');
        $addBank->c_bank_acc         = $request->input('c_bank_acc');
        $addBank->c_bank_number      = $request->input('c_bank_number');
        $addBank->c_bank_branch      = $request->input('c_bank_branch');
        $addBank->c_bank_create_date = date('Y-m-d H:i:s');

        if ($validator->fails()) {
            flashy()->error($validator->messages()->first());
        } else {
            flashy()->success('Bank Akun berhasil ditambahkan');
            $addBank->save();
        }
        return redirect()->route('customers.bank');
    }

    public function  bankUpdate($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'c_bank_name'            => 'required',
            'c_bank_branch'          => 'required',
            'c_bank_acc'             => 'required',
            'c_bank_number'          => 'required'
        ]);

        $input = [
            'c_bank_name'        => $request->input('c_bank_name'),
            'c_bank_branch'      => $request->input('c_bank_branch'),
            'c_bank_acc'         => $request->input('c_bank_acc'),
            'c_bank_number'      => $request->input('c_bank_number')
        ];

        if ($validator->fails()) {
            flashy()->error($validator->messages()->first());
        } else {
            CustomerBank::where('c_bank_id', '=', $id)->update($input);
            flashy()->success('Bank berhasil diperbaharui');
        }
        return redirect()->route('customers.bank');
    }

    public function bankDel(Request $request) {
        CustomerBank::where('c_bank_id', '=', $request->input('id'))->delete();
        $res['success'] = true;
        $res['id'] = $request->input('id');
        return response($res);
    }
}