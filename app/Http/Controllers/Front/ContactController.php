<?php
/**
 * Created by PhpStorm.
 * User: Mukail
 * Date: 08/07/2019
 * Time: 22:04 AM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
class ContactController extends Controller
{
    public function index()
    {
        return view('frontend.contact.contact');
    }

    public function add(Request $request)
    {
        $contact = new Contact;

        if ($request->has('send')) {

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
                'subject' => 'required',
                'message' => 'required'
            ]);

            $contact->guest_name      = $request->input('name');
            $contact->guest_email     = $request->input('email');
            $contact->guest_subject  = $request->input('subject');
            $contact->guest_message = $request->input('message');
            $contact->guest_created_date = date('Y-m-d H:i:s');

            if ($contact->save()) {
                return redirect()->route('contact.contact')->with('success', 'Terima kasih telah menghubungi kami');
            }
        }
    }

}