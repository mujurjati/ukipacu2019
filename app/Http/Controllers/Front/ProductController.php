<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 12/27/2018
 * Time: 8:44 PM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\OrderDetail;
use App\Models\ProductRating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\CustomerWishlist;

class ProductController extends Controller
{

    public function productList($sub = '', Request $request)
    {
        $list = Product::with('avgRating')
            ->with('category.parent')
            ->where('product_status', '=', 'publish')
            ->where('product_highlight', '=', '1')
            ->orderBy('product_create_date', 'DESC');

        if(!empty($sub))
            $list->whereRaw("FIND_IN_SET('".$sub."', product_tags) > 0");

        return view('frontend.products.productList', [
            'dataHighlight' => $list->paginate('10'),
            'subMenu' => $sub
        ]);
    }

    public function productListLunchbox(Request $request)
    {
        $output = '';
        $lunchs = Product::with('avgRating')
            ->with('category.parent')
            ->where('product_type', '=', $request->id)
            ->where('product_status', '=', 'publish')
            ->orderBy('product_create_date', 'DESC')
            ->limit(10);

        if($request->has('sub') && !empty($request->input('sub')))
            $lunchs->whereRaw("FIND_IN_SET('".$request->input('sub')."', product_tags) > 0");

        $lunchs = $lunchs->get();

        if (!$lunchs->isEmpty()) {
            $output .= '
                    <div id="shop" class="shop clearfix '.generate_alias($request->id).'">';
            foreach ($lunchs as $item) {
                $output .= '
                        <div class="product clearfix">
                            <div class="product-image">
                                <a href="#"><img src="' . asset_url('/images/products/' . $item->product_image) . '" alt="Checked Short Dress"></a>
                                <a href="#"><img src="' . asset_url('/images/products/' . (!empty($item->product_image2) ? $item->product_image2 : $item->product_image)) . '" alt="Checked Short Dress"></a>
                                <div class="sale-flash">' . $item->category->parent->p_cat_name . '</div>
                                <div class="product-overlay">
                                    <a href="' . route('product.detail', ['alias' => $item->product_alias]) . '"><span>Lihat Detail Menu</span></a>
                                </div>
                            </div>
                            <div class="product-desc">
                                <div class="product-title">
                                    <h4><a href="' . route('product.detail', ['alias' => $item->product_alias]) . '">' . $item->product_name . '</a></h4>
                                </div>
                                <div class="product-price"><span style="font-size: 20px"> Rp ' . price_format($item->product_price) . '</span></div>
                                <div class="product-rating">
                                    '.$this->rating(intval($item->avgRating)).'
                                </div>
                                <a href="' . route('product.detail', ['alias' => $item->product_alias]) . '" class="btn add-cart-btn btn-block">
                                    Detail
                                </a>
                            </div>
                        </div>
                ';
            }

            $output .= '</div>';

            $res['status'] = true;
            $res['type'] = generate_alias($request->id);
            $res['data'] = $output;

            return response($res);
        }
    }

    public function detail($alias, Request $request)
    {
        $proDetail = Product::with('avgRating')
            ->with('category', 'ingredients')
            ->where('product_alias', '=', $alias)
            ->where('product_status', '=', 'publish')
            ->first();

        $dataWishlist = [];
        $countWishlist = 0;

        $arrCat = [];

        if (!empty($proDetail->category->p_cat_root)) {
            $expCat = explode(',', $proDetail->category->p_cat_root);

            foreach ($expCat as $val) {
                $arrCat[] = $this->get_name($val);
            }
        }

        $proTerkait = Product::with('avgRating')
            ->where('product_status', '=', 'publish')
            ->where('p_cat_id', '=', $proDetail->p_cat_id)
            ->where('product_id', '!=', $proDetail->product_id)
            ->limit(6)
            ->get();

        $produkTerbaru = Product::orderBy('product_create_date', 'DESC')
            ->where('product_status', '=', 'publish')
            ->limit(3)
            ->get();

        $produkTerlaris = Product::orderBy('product_create_date', 'DESC')
            ->where('product_status', '=', 'publish')
            ->limit(3)
            ->get();

        $cekPembelian = array();
        $cekRating = array();

        if (auth('member')->check()) {
            $customer_id = auth('member')->user()->customer_id;

            // $cekPembelian = OrderDetail::with(['order' => function ($q) use ($customer_id) {
            //     $q->where('customer_id', '=', $customer_id);
            // }])
            //     ->whereHas('detail_product', function ($q) use ($proDetail) {
            //         $q->where('product_id', '=', $proDetail->product_id);
            //     })->count();

            $cekRating = ProductRating::where('customer_id', '=', $customer_id)
                ->where('product_id', '=', $proDetail->product_id)->count();
        }

        $komentar = ProductRating::with('cusRating')
            ->where('product_id', '=', $proDetail->product_id);
        if (auth('member')->check()) {
            $komentar->orderByRaw("FIELD(customer_id , $customer_id) DESC");
        } else {
            $komentar->orderBy('p_rating_id', 'DESC');
        }

        /*dd($proDetail);*/
        return view('frontend.products.detail', [
            'data' => $proDetail,
            'dataWishlist' => $dataWishlist,
            'countWishlist' => $countWishlist,
            'proTerkait' => $proTerkait,
            'arrCat' => $arrCat,
            'proTerbaru' => $produkTerbaru,
            'proTerlaris' => $produkTerlaris,
            'cekPembelian' => $cekPembelian,
            'cekRating' => $cekRating,
            'komentar' => $komentar->get()
        ]);
    }

    public function category($alias, Request $request)
    {
        $cat = ProductCategory::select('p_cat_id', 'p_cat_root')
            ->where('p_cat_alias', '=', $alias)->first();

        $expCat = explode(',', $cat->p_cat_root);

        $arrCat = [];

        foreach ($expCat as $val) {
            $arrCat[] = $this->get_name($val);
        }

        $qry = Product::with('avgRating')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('" . $cat->p_cat_id . "', p_cat_root) > 0")
            ->where('product_status', '=', 'publish');

        if ($request->has('min') && !empty($request->input('min'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) >= " . $request->input('min'));
        }

        if ($request->has('max') && !empty($request->input('max'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) <= " . $request->input('max'));
        }

        if ($request->has('sort') && !empty($request->input('sort'))) {
            $sort = $request->input('sort');

            if ($sort == 'terbaru') {
                $qry->orderBy('product_create_date', 'DESC');
            } elseif ($sort == 'az') {
                $qry->orderBy('product_name', 'ASC');
            } elseif ($sort == 'za') {
                $qry->orderBy('product_name', 'DESC');
            } elseif ($sort == 'termurah') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'ASC');
            } elseif ($sort == 'termahal') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'DESC');
            }
        } else {
            $qry->orderBy('product_create_date', 'DESC');
        }

        $proByCat = $qry->paginate(15);

        $proSliderCat = ProductCategory::where('p_cat_parent', '=', $cat->p_cat_id)
            ->orWhere('p_cat_id', '=', $cat->p_cat_id)
            ->get();

        $limitPrice = Product::selectRaw('(product_price-COALESCE(product_discount, 0)) as p_price')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('" . $cat->p_cat_id . "', p_cat_root) > 0")
            ->where('product_status', '=', 'publish')
            ->orderBy('p_price', 'DESC')
            ->first();

        $priceLimit = (!empty($limitPrice->p_price)) ? $limitPrice->p_price : 0;

        $minPrice = ($request->has('min')) ? $request->input('min') : 0;
        $maxPrice = ($request->has('max')) ? $request->input('max') : $priceLimit;

        return view('frontend.products.productByCat', [
            'data' => $proByCat,
            'proSliderCat' => $proSliderCat,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'limitPrice' => $priceLimit,
            'arrCat' => $arrCat
        ]);
    }

    public function searchFromHeader(Request $request)
    {
        $cat = (empty($request->input('cat'))) ? 0 : $request->input('cat');

        $proByCat = Product::with('avgRating');

        if ($request->input('cat') > 0) {
            $proByCat->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
                ->whereRaw("FIND_IN_SET('" . $cat . "', p_cat_root) > 0");
        }

        $proByCat->where('product_name', 'like', '%' . $request->input('keyword') . '%')
            ->where('product_status', '=', 'publish');

        if ($request->has('min') && !empty($request->input('min'))) {
            $proByCat->whereRaw("(product_price-COALESCE(product_discount, 0)) >= " . $request->input('min'));
        }

        if ($request->has('max') && !empty($request->input('max'))) {
            $proByCat->whereRaw("(product_price-COALESCE(product_discount, 0)) <= " . $request->input('max'));
        }

        if ($request->has('sort') && !empty($request->input('sort'))) {
            $sort = $request->input('sort');

            if ($sort == 'terbaru') {
                $proByCat->orderBy('product_create_date', 'DESC');
            } elseif ($sort == 'az') {
                $proByCat->orderBy('product_name', 'ASC');
            } elseif ($sort == 'za') {
                $proByCat->orderBy('product_name', 'DESC');
            } elseif ($sort == 'termurah') {
                $proByCat->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'ASC');
            } elseif ($sort == 'termahal') {
                $proByCat->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'DESC');
            }
        } else {
            $proByCat->orderBy('product_create_date', 'DESC');
        }

        $proSliderCat = ProductCategory::where('p_cat_parent', '=', $cat)
            ->orWhere('p_cat_id', '=', $cat)
            ->get();

        $limitPrice = Product::selectRaw('(product_price-COALESCE(product_discount, 0)) as p_price');

        if ($cat > 0) {
            $limitPrice->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
                ->whereRaw("FIND_IN_SET('" . $cat . "', p_cat_root) > 0");
        }

        $limitPrice = $limitPrice->where('product_name', 'like', '%' . $request->input('keyword') . '%')
            ->where('product_status', '=', 'publish')
            ->orderBy('p_price', 'DESC')
            ->first();

        $priceLimit = (!empty($limitPrice->p_price)) ? $limitPrice->p_price : 0;

        $minPrice = ($request->has('min')) ? $request->input('min') : 0;
        $maxPrice = ($request->has('max')) ? $request->input('max') : $priceLimit;

        return view('frontend.products.productByCat', [
            'data' => $proByCat->paginate('12'),
            'proSliderCat' => $proSliderCat,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'limitPrice' => $priceLimit
        ]);
    }

    public function wishlist($alias, Request $request)
    {
        $res = [];

        if (!auth('member')->check()) {
            $request->session()->put('login-redirect', route('product.detail', $alias));

            $res['success'] = false;
            $res['message'] = 'Login terlebih dahulu.';
            $res['url'] = route('customers.login');

            return response($res);
        } else {
            $customerId = auth('member')->user()->customer_id;

            $dataProduct = Product::where('product_alias', '=', $alias)->first();

            $wishlist = CustomerWishlist::where('product_id', '=', $dataProduct->product_id)
                ->where('customer_id', '=', $customerId);

            if ($wishlist->count() > 0) {
                $dataWishlist = $wishlist->first();

                if ($dataWishlist->c_wishlist_status == '1') {
                    $dataWishlist->c_wishlist_status = '0';

                    $res['msg'] = 'Dihapus dari "Produk Favorit"';
                } else {
                    $dataWishlist->c_wishlist_status = '1';

                    $res['msg'] = 'Produk berhasil disimpan';
                }

                $res['success'] = true;

                $dataWishlist->save();
            } else {
                $add = new CustomerWishlist();

                $add->customer_id = $customerId;
                $add->product_id = $dataProduct->product_id;
                $add->c_wishlist_status = '1';
                $add->c_wishlist_create_date = date('Y-m-d H:i:s');

                if ($add->save()) {
                    $res['success'] = true;
                    $res['msg'] = 'Produk berhasil disimpan';
                }
            }
        }

        return $res;
    }

    function get_name($id)
    {
        $cat = ProductCategory::select('p_cat_name', 'p_cat_alias')
            ->where('p_cat_id', '=', $id);

        if ($cat->count() > 0) {
            $data = $cat->first();

            return ['alias' => $data->p_cat_alias, 'name' => $data->p_cat_name];
        } else {
            return '';
        }
    }

    public function review($id, $alias, Request $request)
    {
        $rating = new ProductRating;
        $rating->customer_id = auth('member')->user()->customer_id;
        $rating->product_id = $id;
        $rating->p_rating_value = $request->input('ratings');
        $rating->p_rating_desc = $request->input('rating_desc');
        $rating->p_rating_create_date = date('Y-m-d H:i:s');

        if ($rating->save()) {
            return redirect()->route('product.detail', $alias)->with('success', 'Terima kasih atas penilaian anda');
        }
    }

    public function productPromo(Request $request)
    {
        $cat = ProductCategory::select('p_cat_id', 'p_cat_root')
            ->first();

        $expCat = explode(',', $cat->p_cat_root);

        $arrCat = [];

        foreach ($expCat as $val) {
            $arrCat[] = $this->get_name($val);
        }

        $qry = Product::with('avgRating')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('promo', product_tags) > 0")
            ->where('product_status', '=', 'publish');

        if ($request->has('min') && !empty($request->input('min'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) >= " . $request->input('min'));
        }

        if ($request->has('max') && !empty($request->input('max'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) <= " . $request->input('max'));
        }

        if ($request->has('sort') && !empty($request->input('sort'))) {
            $sort = $request->input('sort');

            if ($sort == 'terbaru') {
                $qry->orderBy('product_create_date', 'DESC');
            } elseif ($sort == 'az') {
                $qry->orderBy('product_name', 'ASC');
            } elseif ($sort == 'za') {
                $qry->orderBy('product_name', 'DESC');
            } elseif ($sort == 'termurah') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'ASC');
            } elseif ($sort == 'termahal') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'DESC');
            }
        } else {
            $qry->orderBy('product_create_date', 'DESC');
        }

        $proByCat = $qry->paginate(15);

        $proSliderCat = ProductCategory::where('p_cat_parent', '=', $cat->p_cat_id)
            ->orWhere('p_cat_id', '=', $cat->p_cat_id)
            ->get();

        $limitPrice = Product::selectRaw('(product_price-COALESCE(product_discount, 0)) as p_price')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('" . $cat->p_cat_id . "', p_cat_root) > 0")
            ->where('product_status', '=', 'publish')
            ->orderBy('p_price', 'DESC')
            ->first();

        $priceLimit = (!empty($limitPrice->p_price)) ? $limitPrice->p_price : 0;

        $minPrice = ($request->has('min')) ? $request->input('min') : 0;
        $maxPrice = ($request->has('max')) ? $request->input('max') : $priceLimit;

        return view('frontend.products.productPromo', [
            'data' => $proByCat,
            'proSliderCat' => $proSliderCat,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'limitPrice' => $priceLimit,
            'arrCat' => $arrCat
        ]);
    }

    public function productBisnis(Request $request)
    {
        $cat = ProductCategory::select('p_cat_id', 'p_cat_root')
            ->first();

        $expCat = explode(',', $cat->p_cat_root);

        $arrCat = [];

        foreach ($expCat as $val) {
            $arrCat[] = $this->get_name($val);
        }

        $qry = Product::with('avgRating')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('bisnis', product_tags) > 0")
            ->where('product_status', '=', 'publish');

        if ($request->has('min') && !empty($request->input('min'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) >= " . $request->input('min'));
        }

        if ($request->has('max') && !empty($request->input('max'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) <= " . $request->input('max'));
        }

        if ($request->has('sort') && !empty($request->input('sort'))) {
            $sort = $request->input('sort');

            if ($sort == 'terbaru') {
                $qry->orderBy('product_create_date', 'DESC');
            } elseif ($sort == 'az') {
                $qry->orderBy('product_name', 'ASC');
            } elseif ($sort == 'za') {
                $qry->orderBy('product_name', 'DESC');
            } elseif ($sort == 'termurah') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'ASC');
            } elseif ($sort == 'termahal') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'DESC');
            }
        } else {
            $qry->orderBy('product_create_date', 'DESC');
        }

        $proByCat = $qry->paginate(15);

        $proSliderCat = ProductCategory::where('p_cat_parent', '=', $cat->p_cat_id)
            ->orWhere('p_cat_id', '=', $cat->p_cat_id)
            ->get();

        $limitPrice = Product::selectRaw('(product_price-COALESCE(product_discount, 0)) as p_price')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('" . $cat->p_cat_id . "', p_cat_root) > 0")
            ->where('product_status', '=', 'publish')
            ->orderBy('p_price', 'DESC')
            ->first();

        $priceLimit = (!empty($limitPrice->p_price)) ? $limitPrice->p_price : 0;

        $minPrice = ($request->has('min')) ? $request->input('min') : 0;
        $maxPrice = ($request->has('max')) ? $request->input('max') : $priceLimit;

        return view('frontend.products.productBisnis', [
            'data' => $proByCat,
            'proSliderCat' => $proSliderCat,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'limitPrice' => $priceLimit,
            'arrCat' => $arrCat
        ]);
    }

    public function productSpesial(Request $request)
    {
        $cat = ProductCategory::select('p_cat_id', 'p_cat_root')
            ->first();

        $expCat = explode(',', $cat->p_cat_root);

        $arrCat = [];

        foreach ($expCat as $val) {
            $arrCat[] = $this->get_name($val);
        }

        $qry = Product::with('avgRating')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('hari_ini', product_tags) > 0")
            ->whereRaw("FIND_IN_SET('spesial', product_tags) > 0")
            ->where('product_status', '=', 'publish');

        if ($request->has('min') && !empty($request->input('min'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) >= " . $request->input('min'));
        }

        if ($request->has('max') && !empty($request->input('max'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) <= " . $request->input('max'));
        }

        if ($request->has('sort') && !empty($request->input('sort'))) {
            $sort = $request->input('sort');

            if ($sort == 'terbaru') {
                $qry->orderBy('product_create_date', 'DESC');
            } elseif ($sort == 'az') {
                $qry->orderBy('product_name', 'ASC');
            } elseif ($sort == 'za') {
                $qry->orderBy('product_name', 'DESC');
            } elseif ($sort == 'termurah') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'ASC');
            } elseif ($sort == 'termahal') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'DESC');
            }
        } else {
            $qry->orderBy('product_create_date', 'DESC');
        }

        $proByCat = $qry->paginate(15);

        $proSliderCat = ProductCategory::where('p_cat_parent', '=', $cat->p_cat_id)
            ->orWhere('p_cat_id', '=', $cat->p_cat_id)
            ->get();

        $limitPrice = Product::selectRaw('(product_price-COALESCE(product_discount, 0)) as p_price')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('" . $cat->p_cat_id . "', p_cat_root) > 0")
            ->where('product_status', '=', 'publish')
            ->orderBy('p_price', 'DESC')
            ->first();

        $priceLimit = (!empty($limitPrice->p_price)) ? $limitPrice->p_price : 0;

        $minPrice = ($request->has('min')) ? $request->input('min') : 0;
        $maxPrice = ($request->has('max')) ? $request->input('max') : $priceLimit;

        return view('frontend.products.productSpesial', [
            'data' => $proByCat,
            'proSliderCat' => $proSliderCat,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'limitPrice' => $priceLimit,
            'arrCat' => $arrCat
        ]);
    }

    public function productHIPromo(Request $request)
    {
        $cat = ProductCategory::select('p_cat_id', 'p_cat_root')
            ->first();

        $expCat = explode(',', $cat->p_cat_root);

        $arrCat = [];

        foreach ($expCat as $val) {
            $arrCat[] = $this->get_name($val);
        }

        $qry = Product::with('avgRating')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('hari_ini', product_tags) > 0")
            ->whereRaw("FIND_IN_SET('promo', product_tags) > 0")
            ->where('product_status', '=', 'publish');

        if ($request->has('min') && !empty($request->input('min'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) >= " . $request->input('min'));
        }

        if ($request->has('max') && !empty($request->input('max'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) <= " . $request->input('max'));
        }

        if ($request->has('sort') && !empty($request->input('sort'))) {
            $sort = $request->input('sort');

            if ($sort == 'terbaru') {
                $qry->orderBy('product_create_date', 'DESC');
            } elseif ($sort == 'az') {
                $qry->orderBy('product_name', 'ASC');
            } elseif ($sort == 'za') {
                $qry->orderBy('product_name', 'DESC');
            } elseif ($sort == 'termurah') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'ASC');
            } elseif ($sort == 'termahal') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'DESC');
            }
        } else {
            $qry->orderBy('product_create_date', 'DESC');
        }

        $proByCat = $qry->paginate(15);

        $proSliderCat = ProductCategory::where('p_cat_parent', '=', $cat->p_cat_id)
            ->orWhere('p_cat_id', '=', $cat->p_cat_id)
            ->get();

        $limitPrice = Product::selectRaw('(product_price-COALESCE(product_discount, 0)) as p_price')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('" . $cat->p_cat_id . "', p_cat_root) > 0")
            ->where('product_status', '=', 'publish')
            ->orderBy('p_price', 'DESC')
            ->first();

        $priceLimit = (!empty($limitPrice->p_price)) ? $limitPrice->p_price : 0;

        $minPrice = ($request->has('min')) ? $request->input('min') : 0;
        $maxPrice = ($request->has('max')) ? $request->input('max') : $priceLimit;

        return view('frontend.products.productHIPromo', [
            'data' => $proByCat,
            'proSliderCat' => $proSliderCat,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'limitPrice' => $priceLimit,
            'arrCat' => $arrCat
        ]);
    }

    public function productHemat(Request $request)
    {
        $cat = ProductCategory::select('p_cat_id', 'p_cat_root')
            ->first();

        $expCat = explode(',', $cat->p_cat_root);

        $arrCat = [];

        foreach ($expCat as $val) {
            $arrCat[] = $this->get_name($val);
        }

        $qry = Product::with('avgRating')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('hari_ini', product_tags) > 0")
            ->whereRaw("FIND_IN_SET('paket_hemat', product_tags) > 0")
            ->where('product_status', '=', 'publish');

        if ($request->has('min') && !empty($request->input('min'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) >= " . $request->input('min'));
        }

        if ($request->has('max') && !empty($request->input('max'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) <= " . $request->input('max'));
        }

        if ($request->has('sort') && !empty($request->input('sort'))) {
            $sort = $request->input('sort');

            if ($sort == 'terbaru') {
                $qry->orderBy('product_create_date', 'DESC');
            } elseif ($sort == 'az') {
                $qry->orderBy('product_name', 'ASC');
            } elseif ($sort == 'za') {
                $qry->orderBy('product_name', 'DESC');
            } elseif ($sort == 'termurah') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'ASC');
            } elseif ($sort == 'termahal') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'DESC');
            }
        } else {
            $qry->orderBy('product_create_date', 'DESC');
        }

        $proByCat = $qry->paginate(15);

        $proSliderCat = ProductCategory::where('p_cat_parent', '=', $cat->p_cat_id)
            ->orWhere('p_cat_id', '=', $cat->p_cat_id)
            ->get();

        $limitPrice = Product::selectRaw('(product_price-COALESCE(product_discount, 0)) as p_price')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('" . $cat->p_cat_id . "', p_cat_root) > 0")
            ->where('product_status', '=', 'publish')
            ->orderBy('p_price', 'DESC')
            ->first();

        $priceLimit = (!empty($limitPrice->p_price)) ? $limitPrice->p_price : 0;

        $minPrice = ($request->has('min')) ? $request->input('min') : 0;
        $maxPrice = ($request->has('max')) ? $request->input('max') : $priceLimit;

        return view('frontend.products.productHemat', [
            'data' => $proByCat,
            'proSliderCat' => $proSliderCat,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'limitPrice' => $priceLimit,
            'arrCat' => $arrCat
        ]);
    }

    public function productDiet(Request $request)
    {
        $cat = ProductCategory::select('p_cat_id', 'p_cat_root')
            ->first();

        $expCat = explode(',', $cat->p_cat_root);

        $arrCat = [];

        foreach ($expCat as $val) {
            $arrCat[] = $this->get_name($val);
        }

        $qry = Product::with('avgRating')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('hari_ini', product_tags) > 0")
            ->whereRaw("FIND_IN_SET('diet', product_tags) > 0")
            ->where('product_status', '=', 'publish');

        if ($request->has('min') && !empty($request->input('min'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) >= " . $request->input('min'));
        }

        if ($request->has('max') && !empty($request->input('max'))) {
            $qry->whereRaw("(product_price-COALESCE(product_discount, 0)) <= " . $request->input('max'));
        }

        if ($request->has('sort') && !empty($request->input('sort'))) {
            $sort = $request->input('sort');

            if ($sort == 'terbaru') {
                $qry->orderBy('product_create_date', 'DESC');
            } elseif ($sort == 'az') {
                $qry->orderBy('product_name', 'ASC');
            } elseif ($sort == 'za') {
                $qry->orderBy('product_name', 'DESC');
            } elseif ($sort == 'termurah') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'ASC');
            } elseif ($sort == 'termahal') {
                $qry->selectRaw('*, (product_price-COALESCE(product_discount, 0)) as p_price')
                    ->orderBy('p_price', 'DESC');
            }
        } else {
            $qry->orderBy('product_create_date', 'DESC');
        }

        $proByCat = $qry->paginate(15);

        $proSliderCat = ProductCategory::where('p_cat_parent', '=', $cat->p_cat_id)
            ->orWhere('p_cat_id', '=', $cat->p_cat_id)
            ->get();

        $limitPrice = Product::selectRaw('(product_price-COALESCE(product_discount, 0)) as p_price')
            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            ->whereRaw("FIND_IN_SET('" . $cat->p_cat_id . "', p_cat_root) > 0")
            ->where('product_status', '=', 'publish')
            ->orderBy('p_price', 'DESC')
            ->first();

        $priceLimit = (!empty($limitPrice->p_price)) ? $limitPrice->p_price : 0;

        $minPrice = ($request->has('min')) ? $request->input('min') : 0;
        $maxPrice = ($request->has('max')) ? $request->input('max') : $priceLimit;

        return view('frontend.products.productDiet', [
            'data' => $proByCat,
            'proSliderCat' => $proSliderCat,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'limitPrice' => $priceLimit,
            'arrCat' => $arrCat
        ]);
    }

    function rating($x) {
        $label = "";
        for ($y=0; $y<$x; $y++) {
            $label .= '<i class="icon-star3"></i>';
        }

        for ($j=$x; $j<=4; $j++) {
            $label .= '<i class="icon-star-empty"></i>';
        }

        return $label;
    }
}