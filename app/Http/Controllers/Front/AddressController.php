<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 09-Jan-19
 * Time: 03:38
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\CustomerAddress;
use App\Models\ShippingSubdistrict;
use App\Models\ShippingCourier;
use App\Models\Setting;
use App\Models\Merchant;

use RajaOngkir;
use Cart;

class AddressController extends Controller
{
    public function ajax_address(Request $request) {
        $src = $request->input('q');

        $data = ShippingSubdistrict::selectRaw("
                s_subdistrict_id as id,
                CONCAT(s_province_name, ', ', s_city_name, ', ', s_subdistrict_name, ', ', s_city_postcode) as text
            ")
            ->join('_shipping_city', '_shipping_subdistrict.s_city_id', '=', '_shipping_city.s_city_id')
            ->join('_shipping_province', '_shipping_city.s_province_id', '=', '_shipping_province.s_province_id')
            ->where('s_province_name', 'like', '%'.$src.'%')
            ->orWhere('s_city_name', 'like', '%'.$src.'%')
            ->orWhere('s_subdistrict_name', 'like', '%'.$src.'%')
            ->orderBy('s_province_name')
            ->orderBy('s_city_name')
            ->orderBy('s_subdistrict_name')
            ->paginate(10);

        return response($data);
    }
    public function ajax_merchant(Request $request) {
        $src = $request->input('q');

        $data = Merchant::selectRaw("
                merchant_id as id,
                merchant_name as text
            ")
            ->where('merchant_name', 'like', '%'.$src.'%')
            ->orWhere('merchant_email', 'like', '%'.$src.'%')
            ->orderBy('merchant_name')
            ->paginate(10);

        return response($data);
    }

    public function ajax_address_add(Request $request) {
        $check = CustomerAddress::where('customer_id', '=', auth('member')->user()->customer_id)->count();

        $add = new CustomerAddress();

        $add->customer_id           = auth('member')->user()->customer_id;
        $add->s_subdistrict_id      = $request->input('city');
        $add->c_address_type        = $request->input('type');
        $add->c_address_name        = $request->input('name');
        $add->c_address_phone       = $request->input('phone');
        $add->c_address_address     = $request->input('address');
        $add->c_address_primary     = ($check > 0) ? '0' : '1';
        $add->c_address_create_date = date('Y-m-d H:i:s');

        if($add->save()) {
            $request->session()->put('select-addr', $add);

            $res['success'] = true;
            $res['message'] = 'Berhasil';

            return response($res);
        }
    }

    public function ajax_address_select($id, Request $request) {
        $data = CustomerAddress::where('c_address_id', '=', $id)->first();

        $request->session()->put('select-addr', $data);

        $res['success'] = true;
        $res['message'] = 'Berhasil';

        return response($res);
    }

    public function ajax_shipping(Request $request) {
        $courier    = $request->input('courier');
        $address_id = $request->session()->get('select-addr')->c_address_id;

        $setting = new Setting();
        $setting = $setting->select_data();

        $weight = 0;
        foreach(Cart::getContent() as $item)
            $weight += $item->attributes->weight;

        $dataAddr = CustomerAddress::where('c_address_id', '=', $address_id)->first();

        $data = RajaOngkir::cost($setting['General']['alamat_pengiriman'], 'subdistrict', $dataAddr->s_subdistrict_id, 'subdistrict', $weight, $courier);

        return response($data);
    }

    public function ajax_shipping_select(Request $request) {
        $courier    = $request->input('courier');
        $service    = $request->input('service');
        $etd        = $request->input('etd');
        $price      = $request->input('price');

        $dataCourier = ShippingCourier::where('s_courier_code', '=', $courier)->first();

        $discount =  new \Darryldecode\Cart\CartCondition([
            'name' => 'Shipping',
            'type' => 'shipping',
            'target' => 'total',
            'value' => $price,
            'attributes' => [
                'id'        => $dataCourier->s_courier_id,
                'courier'   => $courier,
                'service'   => $service,
                'etd'       => $etd
            ],
            'order' => 1
        ]);

        Cart::condition($discount);

        $res['success']     = true;
        $res['shipping']    = price_format($price);
        $res['total']       = Cart::getTotal();

        return response($res);
    }
}
