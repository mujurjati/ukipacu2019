<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 5/30/2019
 * Time: 11:33 PM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Video;

class VideoController extends Controller
{
    public function index() {
        $dataList = Video::where('video_status', '=', 'publish')
            ->orderBy('video_create_date', 'DESC')
            ->paginate('2');

        return view('frontend.video.index', [
            'video' => $dataList
        ]);
    }
}