<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 18-Jun-19
 * Time: 15:02
 */

namespace App\Http\Controllers\ApiDriver;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\Driver;

class DriverController extends Controller
{
    public function login(Request $request) {
        $this->validate($request, [
           'driver_email'       => 'required|email',
           'driver_password'    => 'required'
        ]);

        $data = Driver::where('driver_email', $request->driver_email)->first();

        if($data != null && Hash::check($request->driver_password, $data->getAuthPassword())) {
            $data->api_token = str_random(60);

            $data->save();

            $result = [
                'success'   => true,
                'rc'        => 200,
                'result'    => "Autentication Success",
                'token'     => $data->api_token,
                'data'      => $data,
            ];

            return $result;
        }

        $result = [
            'success'   => false,
            'rc'        => 401,
            'result'    => "Autentication Failed",
        ];

        return $result;
    }

    public function detail() {
        $result = [
            'success'   => true,
            'rc'        => 200,
            'result'    => "Data found",
            'data'      => auth()->user()
        ];

        return $result;
    }

    public function push_location(Request $request) {
        $this->validate($request, [
            'driver_location_lat'   => 'required',
            'driver_location_lng'   => 'required'
        ]);

        Driver::where('driver_id', '=', auth()->user()->driver_id)
            ->update([
                'driver_location_lat' => $request->driver_location_lat,
                'driver_location_lng' => $request->driver_location_lng
            ]);

        $result = [
            'success'   => true,
            'rc'        => 200,
            'result'    => "Success"
        ];

        return $result;
    }
}