<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Jun-19
 * Time: 10:08
 */

namespace App\Http\Controllers\ApiDriver;

use App\Http\Controllers\Controller;

use function foo\func;
use Illuminate\Http\Request;

use App\Models\Order;
use App\Models\OrderDetail;

class OrderController extends Controller
{
    public function order_list(Request $request) {
        $data = Order::with('customer')
            ->whereHas('detail', function($detail) {
                $detail->where('driver_id', '=', auth()->user()->driver_id);
            });

        if($request->has('status') && !empty($request->input('status')))
            $data->where('order_status', '=', $request->input('status'));

        if($request->has('keyword') && !empty($request->input('keyword')))
            $data->where('order_number', '=', $request->input('keyword'));

        $data = $data->orderBy('order_create_date', 'desc')
            ->paginate(10);

        if($data != null) {
            $result = [
                'success'   => true,
                'rc'        => 200,
                'result'    => "Data found",
                'data'      => $data
            ];
        }
        else {
            $result = [
                'success'   => false,
                'rc'        => 200,
                'result'    => "Data not found"
            ];
        }

        return $result;
    }

    public function order_detail(Request $request) {
        $this->validate($request, [
            'order_id' => 'required'
        ]);

        $driverId = auth()->user()->driver_id;

        $data = Order::with(['customer', 'detail' => function($detail) use($driverId) {
                $detail->where('driver_id', '=', auth()->user()->driver_id);
            }, 'detail.merchant', 'detail.detail_product'])
            ->has('detail')
            ->where('order_id', '=', $request->order_id);

        if($data != null) {
            $result = [
                'success'   => true,
                'rc'        => 200,
                'result'    => "Data found",
                'data'      => $data->first()
            ];
        }
        else {
            $result = [
                'success'   => false,
                'rc'        => 200,
                'result'    => "Data not found"
            ];
        }

        return $result;
    }

    public function order_update(Request $request) {
        $o_detail_id    = $request->input('o_detail_id');
        $step           = $request->input('step');
        $note           = $request->input('note');

        $detail = OrderDetail::where('o_detail_id', '=', $o_detail_id)->first();
        $detail->o_detail_status = $step;

        if($step == 'pickup') {
            $detail->o_detail_pickup_date = date('Y-m-d H:i:s');
            $detail->o_detail_pickup_note = $note;

            $msg = 'Pesanan telah di ambil oleh driver.';
        }
        elseif($step == 'shipment') {
            $detail->o_detail_shipment_date = date('Y-m-d H:i:s');
            $detail->o_detail_shipment_note = $note;

            $msg = 'Pesanan dalam proses pengiriman oleh driver.';
        }
        elseif($step == 'received') {
            $detail->o_detail_received_date = date('Y-m-d H:i:s');
            $detail->o_detail_reveived_note = $note;

            $msg = 'Pesanan telah diterima Customer.';
        }

        $detail->save();

        $result = [
            'success'   => true,
            'rc'        => 200,
            'result'    => $msg
        ];

        return $result;
    }
}