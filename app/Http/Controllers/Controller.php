<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\ProductCategory;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        //its just a dummy data object.
        $set = new Setting();

        $setting = $set->select_data();

        $dataCatHeader = ProductCategory::where('p_cat_level', '=', 1)->get();

        $dataCat = ProductCategory::with('grandchildren')
            ->where('p_cat_level', '=', '1')
            ->orderBy('p_cat_order')
            ->get();

        // Sharing is caring
        View::share('setting', $setting);
        View::share('catHeader', $dataCatHeader);
        View::share('catSidebar', $dataCatHeader);
    }
}

