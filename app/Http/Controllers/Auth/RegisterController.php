<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:_user',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'u_level_id' => '1', 
            'user_code' => '123', 
            'user_name' => $data['name'], 
            'email' => $data['email'], 
            'user_username' => $data['name'], 
            'password' => bcrypt($data['password']),
            'user_desc' => 'coba',
            'user_photo' => 'coba.png',
            'user_birth' => '2018-12-08',
            'user_address' => 'jogja',
            'user_city' => 'jogja',
            'user_state' => 'jogja',
            'user_province' => 'DIY',
            'user_postcode' => '12345',
            'user_phone' => '0987654321',
            'user_status' => 'active',
            'user_create_date' => '2018-12-08',
        ]);
    }
}
