<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promo;
use App\Models\PromoUsed;
use App\Http\Requests\Admin\PromoRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PromoController extends Controller
{
    public function index(){
    	$promo = Promo::orderBy('promo_id', 'DESC')->get();
    	return view('backend.promo.index', ['promo' => $promo]);
    }

    public function create(){
    	return view('backend.promo.create');
    }

    public function store(PromoRequest $request){
    	$promo = new Promo();
    	$nilai_price = str_replace('.', '', $request->input('promo_value_price'));
    	$promo->promo_name = $request->input('promo_name');
    	$promo->promo_alias = generate_alias($request->input('promo_name'));
    	$promo->promo_desc = $request->input('promo_desc');
    	$promo->promo_min_payment = str_replace('.', '', $request->input('promo_min_payment'));
    	$promo->promo_max_discount = str_replace('.', '', $request->input('promo_max_discount'));
    	$promo->promo_type = $request->input('promo_type');
    	$promo->promo_value = ($nilai_price == '' ? $request->input('promo_value_percent') : $nilai_price);
    	$promo->promo_code = $request->input('promo_code');
    	$promo->promo_qty = $request->input('promo_qty');
    	$promo->promo_start_date = $request->input('promo_start_date');
    	$promo->promo_end_date = $request->input('promo_end_date');
    	$promo->promo_status = $request->input('promo_status');
    	$promo->promo_create_by = Auth::id();
    	$promo->promo_create_date = Carbon::now();

    	$promo->save();
    	return redirect()->route('admin.promo')->with('success', 'Data Promo berhasil ditambahkan');
    }

    public function edit($id){
    	$promo = Promo::find($id);
    	return view('backend.promo.edit', ['promo' => $promo, 'id' => $id]);
    }

    public function update($id, PromoRequest $request){
    	$promo = Promo::find($id);
    	if ($request->input('promo_type') == 'price') {
    	    $promo_value = str_replace('.', '', $request->input('promo_value_price'));
    	    $promo_max_discount = 0;
        } else {
            $promo_value = $request->input('promo_value_percent');
            $promo_max_discount = str_replace('.', '', $request->input('promo_max_discount'));
        }
    	$promo->promo_name = $request->input('promo_name');
    	$promo->promo_alias = generate_alias($request->input('promo_name'));
    	$promo->promo_desc = $request->input('promo_desc');
        $promo->promo_min_payment = str_replace('.', '', $request->input('promo_min_payment'));
        $promo->promo_max_discount = $promo_max_discount;
    	$promo->promo_type = $request->input('promo_type');
    	$promo->promo_value = $promo_value;
    	$promo->promo_code = $request->input('promo_code');
    	$promo->promo_qty = $request->input('promo_qty');
        $promo->promo_start_date = $request->input('promo_start_date');
        $promo->promo_end_date = $request->input('promo_end_date');
    	$promo->promo_status = $request->input('promo_status');

    	$promo->save();
    	return redirect()->route('admin.promo')->with('success', 'Data Promo berhasil diEdit');
    }

    public function destroy($id){
        $promo = Promo::find($id);
        $promo->delete();

        return redirect()->route('admin.promo')->with('success', 'Data Produk berhasil di hapus');
    }

    public function detail($id){
    	$promo = Promo::find($id);

    	return view('backend.promo.detail', ['promo' => $promo]);
    }

    public function listUsed($id){
    	$promo = PromoUsed::where('promo_id', $id)->get();
    	return view('backend.promo.listUsed', ['promo' => $promo]);
    }
}
