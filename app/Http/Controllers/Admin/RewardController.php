<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Reward;
use App\Models\RewardHistory;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Lib\ImageController;
use File;


class RewardController extends Controller
{
    public function index() {
        $data = Reward::orderBy('reward_id', 'desc')
            ->paginate(10);

        return view('backend.reward.index', ['rewards' => $data]);
    }

    public function create(Request $request)
    {

        if($request->has('addReward')) {
            $this->validate($request, [
                'reward_position'      => 'required',
                'reward_member'     => 'required',
                'reward'      => 'required',
                'reward_status'  => 'required',
            ]);

            $add = new Reward();

            $add->reward_position   = $request->input('reward_position');
            $add->reward_member     = $request->input('reward_member');
            $add->reward            = $request->input('reward');
            $add->reward_status     = $request->input('reward_status');

            $add->reward_created_date  = date('Y-m-d H:i:s');

            $add->save();

            return  redirect()->route('admin.reward')->with('success', 'Tambah Reward Berhasil');
        }

        return view('backend.reward.create');
    }

    public function edit($id, Request $request) {
        $reward = Reward::find($id);
        if($request->has('editReward')) {
            $this->validate($request, [
                'reward_position'      => 'required',
                'reward_member'     => 'required',
                'reward'      => 'required',
                'reward_status'  => 'required',
            ]);

            $reward->reward_position   = $request->input('reward_position');
            $reward->reward_member     = $request->input('reward_member');
            $reward->reward            = $request->input('reward');
            $reward->reward_status     = $request->input('reward_status');

            $reward->reward_created_date  = date('Y-m-d H:i:s');

            if($reward->save()) {
                return  redirect()->route('admin.reward')->with('success', 'Update Reward Berhasil');
            }

        }
        return view('backend.reward.edit', ['reward' => $reward]);
    }

    public function detail($id) {
        $rewards = Reward::find($id);
        $customers = Customer::where('customer_count', '>=', $rewards->reward_member)->get();
        $rh_cek = RewardHistory::select('customer_id')->get();

        $temp_rh = array();

        foreach($rh_cek as $rhc) {
            $temp_rh[] = $rhc->customer_id;
        }

        if (count($customers) > 0) {
            $rh = new RewardHistory();
            foreach ($customers as $cus) {
                if (!in_array($cus->customer_id, $temp_rh)) {
                    $rh->customer_id = $cus->customer_id;
                    $rh->reward_id = $id;
                    $rh->rh_status = 'pending';
                    $rh->rh_created_date = date('Y-m-d H:i:s');
                    $rh->save();   
                }                 
            }
        }

        $rewardHistory = RewardHistory::with('rewardHistory')->get();

        return view('backend.reward.detail', [
            'reward' => $rewards,
            'rewardHistory' => $rewardHistory
            ]);
    }

    public function approve($id, $param) {
        $up_rh = RewardHistory::find($id);

        $up_rh->rh_status = 'approved';
        if($up_rh->save()) {
            return  redirect()->route('admin.reward.detail', $param)->with('success', 'Update Berhasil');
        }
    }
}
