<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 23-Dec-18
 * Time: 06:04
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Admin\Lib\ImageController;
use App\Models\ProductCategory;
use App\Models\ProductAttribute;

class ProductCatController extends Controller
{
    private $attributes = [
        'tipe'      => 'Tipe kategori',
        'sub'       => 'Sub kategori',
        'gambar'    => 'Gambar kategori',
        'nama'      => 'Nama kategori',
        'alias'     => 'Kategori',
        'deskripsi' => 'Deskripsi kategori',
        'status'    => 'Status kategori',
    ];

    private $messages = [
        'required' => ':attribute wajib di isi.',
        'unique'    => ':attribute sudah digunakan.',
    ];

    private $rules = [
        'nama'      => 'required',
        'deskripsi' => 'required',
        'status'    => 'required',
    ];

    public function list_cat() {
        $data = ProductCategory::with('grandchildren')
            ->where('p_cat_level', '=', '1')
            ->orderBy('p_cat_order')
            ->get();

        return view('backend.product_cat.list', ['data' => $data]);
    }

    public function add_cat(Request $request) {
        if($request->has('add')) {
            $sub = $request->input('sub');

            if(!empty($sub)) {
                $dParent = ProductCategory::where('p_cat_id', '=', $sub)->first();

                $p_cat_alias   = $dParent->p_cat_alias.'-'.str_slug($request->input('nama'));
            }
            else {
                $p_cat_alias   = str_slug($request->input('nama'));
            }

            $request->request->add(['alias' => $p_cat_alias]);

            if($request->input('tipe') == 'sub')
                $this->rules['sub'] = 'required';

            $this->rules['nama']    = 'required';
            $this->rules['alias']   = 'required|unique:_product_category,p_cat_alias';
            $this->rules['tipe']    = 'required';
            $this->rules['gambar']  = 'required';

            $this->validate($request, $this->rules, $this->messages, $this->attributes);

            $cat = new ProductCategory();

            $cat->p_cat_name    = $request->input('nama');
            $cat->p_cat_alias   = $p_cat_alias;
            $cat->p_cat_desc    = $request->input('deskripsi');
            $cat->p_cat_parent  = (!empty($sub)) ? $sub : '0';
            $cat->p_cat_level   = $request->input('level')+1;
            $cat->p_cat_root    = $request->input('root');
            $cat->p_cat_order   = $this->get_last_order()+1;
            $cat->p_cat_status  = $request->input('status');

            if($cat->save()) {
                $fileImage = $request->input('gambar');

                $filename   = "pcat-".$cat->p_cat_id;
                $path       = public_path().'/images/product_cat/';

                $Image = new ImageController();

                $fileUpload = $Image->filepond_upload($fileImage, $filename, $path);

                $cat->p_cat_image = $fileUpload->filename;

                if(!empty($cat->p_cat_root))
                    $cat->p_cat_root = $cat->p_cat_root.','.$cat->p_cat_id;
                else
                    $cat->p_cat_root = $cat->p_cat_id;

                $cat->save();
            }

            return  redirect()->route('admin.product-cat')->with('success', 'Data Produk Kategori berhasil ditambahkan');
        }

        $data = ProductCategory::where('p_cat_level', '=', '1')
            ->orderBy('p_cat_order')
            ->get();

        return view('backend.product_cat.add', ['data' => $data]);
    }

    public function edit_cat($section, $id, $attr_id = '', Request $request) {
        $data = ProductCategory::with('attribute')->where('p_cat_id', '=', $id)->first();

        $countCreateAttr = ProductAttribute::where('p_cat_id', '=', $id)
            ->where('p_attribute_status', '=', 'active')
            ->where('p_attribute_create', '=', '1')
            ->count();

        $countUpdateAttr = ProductAttribute::where('p_cat_id', '=', $id)
            ->where('p_attribute_status', '=', 'active')
            ->where('p_attribute_create', '=', '0')
            ->count();

        if(!empty($attr_id)) {
            $dataAttr = ProductAttribute::where('p_attribute_id', '=', $attr_id)->first();
        }
        else {
            $dataAttr = [];
        }

        return view('backend.product_cat.edit', ['section' => $section, 'data' => $data, 'dataAttr' => $dataAttr, 'countCreate' => $countCreateAttr, 'countUpdate' => $countUpdateAttr]);
    }

    public function edit_detail($id, Request $request) {
        $cat = ProductCategory::where('p_cat_id', '=', $id)->first();

        if($cat->p_cat_name != $request->input('nama'))
            $this->rules['nama'] = 'required|unique:_product_category,p_cat_name';

        $this->validate($request, $this->rules, $this->messages, $this->attributes);

        $cat->p_cat_name    = $request->input('nama');
        $cat->p_cat_desc    = $request->input('deskripsi');
        $cat->p_cat_status  = $request->input('status');

        if(!empty($request->input('gambar'))) {
            $fileImage = $request->input('gambar');

            $filename   = "pcat-" . $cat->p_cat_id;
            $path       = public_path() . '/images/product_cat/';

            $Image = new ImageController();

            $fileUpload = $Image->filepond_upload($fileImage, $filename, $path);

            $cat->p_cat_image = $fileUpload->filename;
        }

        $cat->save();

        return  redirect()->route('admin.product-cat')->with('success', 'Data Produk Kategori berhasil diupdate');
    }

    public function delete_cat($id) {
        ProductCategory::whereRaw("FIND_IN_SET('".$id."', p_cat_root) > 0")
            ->delete();

        return  redirect()->route('admin.product-cat')->with('success', 'Data Produk Kategori berhasil dihapus');
    }

    public function get_last_order() {
        $data = ProductCategory::orderBy('p_cat_order', 'DESC');

        if($data->count() > 0) {
            $first = $data->first();

            return $first->p_cat_order;
        }
        else {
            return '0';
        }
    }
}