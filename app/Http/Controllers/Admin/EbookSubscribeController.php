<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 2/8/2019
 * Time: 2:17 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\EbookSubscribe;
use Illuminate\Http\Request;

class EbookSubscribeController extends Controller
{
    public function index() {
        $data = EbookSubscribe::with('customer')
                ->with('package')
                ->orderBy('e_subscribe_id', 'DESC')
                ->paginate('10');

        return view('backend.ebookTrx.index', ['data' => $data]);
    }

    public function detail($id) {
        $data = EbookSubscribe::with('customer')
            ->with('package')
            ->where('e_subscribe_id', '=', $id)
            ->first();

        return view('backend.ebookTrx.detail', ['data' => $data]);
    }

    public function update($id, Request $request) {
        $status = $request->input('status');
        $data = EbookSubscribe::find($id);
        $data->e_subscribe_status = $status;
        if ($status == 'success') {
            $data->e_subscribe_active_date  = $request->input('start');
            $data->e_subscribe_expired_date = $request->input('end');
        }

        if ($data->save()) {
            return redirect()->route('admin.ebooktrx')->with('success', 'Data Transaksi Paket berhasil dirubah');
        }
    }
}