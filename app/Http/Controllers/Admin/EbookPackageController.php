<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\EbookPackage;
use App\Http\Requests\Admin\ebookRequest;
use App\Http\Controllers\Admin\Lib\ImageController;
use App\Models\EbookSubscribe;
use Illuminate\Http\Request;

class EbookPackageController extends Controller
{
    public function index(){
    	$ebook = EbookPackage::orderBy('e_package_id', 'desc')->paginate('10');
    	return view('backend.ebookPackage.index', ['ebook' => $ebook]);
    }

    public function create(){
    	return view('backend.ebookPackage.create');
    }

    public function store(ebookRequest $request){

    	$ebook = new EbookPackage();
    	$ebook->e_package_name = $request->input('e_package_name');
    	$ebook->e_package_shortdesc = $request->input('e_package_shortdesc');
    	$ebook->e_package_desc = $request->input('e_package_desc');
    	$ebook->e_package_time = $request->input('e_package_time');
    	$ebook->e_package_type = $request->input('e_package_type');
    	$ebook->e_package_price = str_replace('.', '',$request->input('e_package_price'));
    	$ebook->e_package_status = $request->input('e_package_status');
    	$ebook->e_package_create_date = date('Y-m-d H:i:s');

    	if($request->input('ebookImage') != null) {
                $filename   = "users-".$ebook->e_package_name;
                $path       = public_path().'/images/ebook_package/';

                $Image      = new ImageController();

                $fileUpload = $Image->filepond_upload($request->input('ebookImage'), $filename, $path);

                $ebook->e_package_image = $fileUpload->filename;
            }

        $ebook->save();

    	return redirect()->route('admin.ebookpackage')->with('success', 'Data Produk berhasil ditambahkan');
    }

    public function edit($id){
    	$ebook = EbookPackage::where('e_package_id', $id)->first();
    	return view('backend.ebookPackage.edit', ['ebook' => $ebook, 'id' => $id]);
    }

    public function update($id,ebookRequest $request){
    	$ebook = EbookPackage::where('e_package_id', $id)->first();
    	$ebook->e_package_name = $request->input('e_package_name');
    	$ebook->e_package_shortdesc = $request->input('e_package_shortdesc');
    	$ebook->e_package_desc = $request->input('e_package_desc');
    	$ebook->e_package_time = $request->input('e_package_time');
    	$ebook->e_package_type = $request->input('e_package_type');
    	$ebook->e_package_price = str_replace('.', '', $request->input('e_package_price'));
    	$ebook->e_package_status = $request->input('e_package_status');

    	if($request->input('ebookImage') != null) {
                $filename   = "users-".$ebook->e_package_name;
                $path       = public_path().'/images/ebook_package/';

                $Image      = new ImageController();

                $fileUpload = $Image->filepond_upload($request->input('ebookImage'), $filename, $path);

                $ebook->e_package_image = $fileUpload->filename;
            }
        $ebook->save();

    	return redirect()->route('admin.ebookpackage')->with('success', 'Data Produk berhasil Diedit');
    }

    public function destroy($id){
    	$ebook = EbookPackage::find($id);
        $ebook->delete();

        return redirect()->route('admin.ebookpackage')->with('success', 'Data Produk berhasil dihapuss');
    }

    public function subscribe($id){
        $ebookSubs = EbookSubscribe::where('e_package_id', $id)->get();

        return view('backend.ebookPackage.subscribe', ['subs' => $ebookSubs]);
    }

    public function editSubscribe($id, Request $request){
        $ebookSubs = EbookSubscribe::with('package')->where('e_subscribe_id', $id)->first();
        if($request->has('editSubscribe')){
            $ebookSubs->e_subscribe_expired_date = $request->input('e_subscribe_expired_date');
            $ebookSubs->e_subscribe_status = $request->input('e_subscribe_status');

            $ebookSubs->save();
            return redirect()->route('admin.ebookpackage.subscribe', ['id' => $ebookSubs->e_package_id])->with('success', 'Data Produk berhasil Diubah');
        }
        return view('backend.ebookPackage.editsubscribe', ['subs' => $ebookSubs, 'id' => $id]);
    }

    public function detail($id){
        $ebook = EbookPackage::where('e_package_id', $id)->first();
        return view('backend.ebookPackage.show', ['ebook' => $ebook]);
    }
}
