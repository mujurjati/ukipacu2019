<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalProduk        = Product::whereIn('product_status', ['publish', 'draft'])->count();
        $totalCustomer      = Customer::whereIn('customer_status', ['active', 'new'])->count();
        $totalMerchant      = Merchant::whereIn('merchant_status', ['active', 'new'])->count();
        $totalTrxSuccess    = Order::whereIn('order_status', ['finish'])->count();

        $startDate      = date('Y-m-d');
        $endDate        = date('Y-m-d');

        $thisMonth  = date('Y-m');
        $lastDay    = date('t',strtotime($thisMonth));
        $startDate  = date('Y-m-01');
        $endDate    = date('Y-m-').$lastDay;
        $arrDay    = array();
        for($i=1;$i<=$lastDay;$i++){
            $day = substr("0".$i,-2);
            $arrDay[date('Y-m-').$day] = "0";
        }

        $dataNilaiTrxBulanIni = DB::select(DB::raw("
            SELECT DATE(order_create_date) AS DATE, 
                SUM(order_total) AS TOTAL_TRX 
                FROM _order 
                WHERE order_status = 'finish' 
                    AND DATE(order_create_date) >= '".date('Y-m-d',strtotime($startDate))."' 
                    AND DATE(order_create_date) <= '".date('Y-m-d',strtotime($endDate))."'
                GROUP BY DATE
        "));

        $chartDataTotTRXSekarang	= array_merge($arrDay,array_column($dataNilaiTrxBulanIni, 'TOTAL_TRX', 'DATE'));
        
        $dataDayTotTRXBulanIni = [];
        $dataValTotTRXBulanIni = [];
        foreach($chartDataTotTRXSekarang as $key => $val){
            if($key != '') {
                $dataDayTotTRXBulanIni[] = date('d', strtotime($key));
                $dataValTotTRXBulanIni[] = $val;
            }
        }

        $dataJLHTrxStatus = DB::select(DB::raw("
            SELECT order_status, 
                SUM(order_total) AS JLH_TRX_STATUS 
                FROM _order 
                GROUP BY order_status
        "));

        $dataKeyStatus = [];
        $dataTotStatus = [];
        foreach($dataJLHTrxStatus as $val){
            $dataKeyStatus[] = str_replace('_', ' ', $val->order_status);
            $dataTotStatus[] = $val->JLH_TRX_STATUS;
        }

        $dataPro = DB::select(DB::raw("
        SELECT product_name, 
            COUNT(_order.order_id) AS JLH_PRO
            FROM _order 
            JOIN _order_detail ON _order.order_id = _order_detail.order_id
            JOIN _product ON _order_detail.product_id = _product.product_id
            WHERE order_status = 'finish'
            GROUP BY _order_detail.product_id
        "));

        $dataKeyPro = [];
        $dataTotPro = [];
        foreach($dataPro as $val){
                $dataKeyPro[] = $val->product_name;
                $dataTotPro[] = $val->JLH_PRO;
        }

        $bulanSebelumnya = date("Y-m", strtotime( date( "Y-m-d", strtotime( date("Y-m") ) ) . "-1 month" ) );
        $bulanSekarang = date("Y-m", strtotime( date( "Y-m-d", strtotime( date("Y-m") ) ) . "" ) );

        return view('backend.dashboard', [
            'totalProduk'   => $totalProduk,
            'totalCustomer' => $totalCustomer,
            'totalMerchant' => $totalMerchant,
            'totalTrxSuccess'   => $totalTrxSuccess,
            'dataDayTotTRXBulanIni'     => $dataDayTotTRXBulanIni,
            'dataValTotTRXBulanIni'     => $dataValTotTRXBulanIni,
            'dataKeyPro'     => $dataKeyPro,
            'dataTotPro'     => $dataTotPro,
            'dataKeyStatus'     => $dataKeyStatus,
            'dataTotStatus'     => $dataTotStatus,
            'bulanSebelumnya'     => date_indo($bulanSebelumnya),
            'bulanSekarang'     => date_indo($bulanSekarang)
        ]);
    }
}
