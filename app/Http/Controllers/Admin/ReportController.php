<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Order;

class ReportController extends Controller
{
    public function index(Request $request) {
        $data = Order::with('customer')->with('detailIndex')->where('order_status', 'finish');       
        $totalAll = 0;
        if(!empty($request->has('filter_date_awal')) && !empty($request->input('filter_date_akhir'))) {
            $data->whereRaw("DATE(order_finish_date) >= '".date("Y-m-d", strtotime($request->input('filter_date_awal')))."'");
            $data->whereRaw("DATE(order_finish_date) <= '".date("Y-m-d", strtotime($request->input('filter_date_akhir')))."'");
            $totalAll = Order::whereRaw("DATE(order_finish_date) >= '".date("Y-m-d", strtotime($request->input('filter_date_awal')))."'")
            ->whereRaw("DATE(order_finish_date) <= '".date("Y-m-d", strtotime($request->input('filter_date_akhir')))."'")
            ->where('order_status', 'finish')->sum('order_total');
        } else {
            $totalAll = Order::where('order_status', 'finish')->sum('order_total');
        }

        // if($request->has('keyword') && !empty($request->input('keyword'))) {
        //     $data->where('order_number','like', '%'. $request->input('keyword'). '%');
        // }

        $data = $data->orderBy('order_create_date', 'desc')
            ->paginate(50);

        $hitungUntung = count($data)*100000;

        $data->appends([
            'filter_date_awal'    => $request->input('filter_date_awal'),
            'filter_date_akhir'    => $request->input('filter_date_akhir'),
            // 'keyword'   => $request->input('keyword')
        ]);

        

        return view('backend.report.index', [
            'reports' => $data,
            'total' => $totalAll-$hitungUntung
            ]);
    }
}