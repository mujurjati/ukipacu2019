<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 23-Dec-18
 * Time: 06:04
 */

namespace App\Http\Controllers\Admin;

use App\Models\ProductIngredient;
use Illuminate\Support\Facades\Schema;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Merchant;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductAttribute;
use App\Models\ProductStock;
use Illuminate\Http\Request;
use Carbon\Carbon;
use File;
use FileUpload;
use Croppa;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Admin\Lib\ImageController;

class ProductController extends Controller
{
	public $folder = '/images/products/';
	
	public function index(Request $request)
	{
	    $category = ProductCategory::where('p_cat_level','=','1')
            ->orderBy('p_cat_name')
            ->pluck('p_cat_name', 'p_cat_id')
            ->toArray();

        $product = Product::where('product_status','!=','delete');
        // ::with("merchant")
//            ->with("category")
//            ->join('_product_category', '_product.p_cat_id', '=', '_product_category.p_cat_id')
            // ->where('product_status','!=','delete');

        if($request->has('status') && !empty($request->input('status')))
            $product->where('product_status', '=', $request->input('status'));

        if($request->has('category') && !empty($request->input('category')))
            $product->whereRaw("FIND_IN_SET('".$request->input('category')."', p_cat_root) > 0");

        if($request->has('keyword') && !empty($request->input('keyword'))) {
            $product->where('product_name', 'like', '%'. $request->input('keyword'). '%')
                ->orWhere('product_number','=',  $request->input('keyword'));
        }

        $product = $product->orderBy('product_create_date', 'desc')
            ->paginate(20);

        $product->appends([
            'status'    => $request->input('status'),
            'keyword'   => $request->input('keyword')
        ]);
        
		return view('backend.products.index', ['products' => $product, 'category' => $category]);
	}

	public function create_product(Request $request)
	{
		if($request->has('addProduct')){
			$produt = new Product();

            $this->validate($request, [
               'product_price'     => 'required',
                'product_name'      => 'required',
                'product_image'     => 'required',
            ]);

			$produt->p_cat_id         = 0;
            $produt->product_name     = $request->input('product_name');
            $produt->product_type     = 'SharedMeals';
			$produt->product_alias    = str_slug($request->input('product_name'));
			$produt->product_sortdesc = $request->input('product_sortdesc');
			$produt->product_desc     = $request->input('product_desc');
			$produt->product_price    = str_replace('.', '', $request->input('product_price'));
            $produt->product_hpp      = 55.000;
            $produt->product_discount = 200;
			$produt->product_stock    = $request->input('product_stock');
            $produt->product_highlight = ($request->has('product_highlight') && $request->input('product_highlight') == 'on') ? '1' : '0';
			$produt->product_nutritional   = $request->input('product_nutritional');
			$produt->product_status   = $request->input('product_status');
			$produt->product_create_date = Carbon::now();
            $produt->product_number   = $this->generate_code();

            if (empty($request->product_tags)){
                $produt->product_tags = '';
            }else {
                $produt->product_tags     = implode(',',$request->product_tags);
            }

			$produt->save();

			$produt->product_alias = $produt->product_alias.'-'.$produt->product_id;

            if(!empty($request->input('product_image'))) {
                $fileImage  = $request->input('product_image');
                $filename   = "product-".$produt->product_alias;

                $path       = public_path().'/images/products/';

                $Image      = new ImageController();

                $fileUpload = $Image->upload_crop($fileImage, $filename, $path, [300, 300]);
                $produt->product_image  = $fileUpload->filename;
            }

            if(!empty($request->input('product_image2'))) {
                $fileImage  = $request->input('product_image2');
                $filename   = "product-".$produt->product_alias."-2";

                $path       = public_path().'/images/products/';

                $Image      = new ImageController();

                $fileUpload = $Image->upload_crop($fileImage, $filename, $path, [300, 300]);
                $produt->product_image2  = $fileUpload->filename;
            }

            $produt->save();

		    return redirect()->route('admin.product-list')->with('success', 'Data Produk berhasil ditambahkan');
		}

		$productCat = ProductCategory::where('p_cat_parent','=','0')->pluck('p_cat_name', 'p_cat_id')->all();

		return view('backend.products.create', ['productCat' => $productCat]);
	}

    public function edit($id, Request $request){
        $product = Product::where('product_id', $id)->first();

        if($request->has('editProduct')){
            $product = Product::where('product_id',$id)->first();

            $product->product_name  = $request->input('product_name');
            $product->product_type   = 'SharedMeals';
            $product->product_desc  = $request->input('product_desc');
            $product->product_price = str_replace('.', '', $request->input('product_price'));
            $product->product_stock = $request->input('product_stock');
            $product->product_status = $request->input('product_status');

            if(!empty($request->input('product_image'))) {
                $fileImage  = $request->input('product_image');
                $filename   = "product-".$product->product_alias;

                $path       = public_path().'/images/products/';

                $Image      = new ImageController();

                $fileUpload = $Image->filepond_upload($fileImage, $filename, $path, [300, 300]);
                $product->product_image  = $fileUpload->filename;
            }

            if(!empty($request->input('product_image2'))) {
                $fileImage  = $request->input('product_image2');
                $filename   = "product-".$product->product_alias."-2";

                $path       = public_path().'/images/products/';

                $Image      = new ImageController();

                $fileUpload = $Image->filepond_upload($fileImage, $filename, $path, [300, 300]);
                $product->product_image2  = $fileUpload->filename;
            }

            if (empty($request->product_tags)){
                $product->product_tags = '';
            }else {
                $product->product_tags     = implode(',',$request->product_tags);
            }

            $product->save();

            return redirect()->route('admin.product-list')->with('success', 'Data Produk berhasil Di Update');
        }

        return view('backend.products.edit', ['product' =>  $product,'id' => $id]);
    }

    public function getSubCategory($p_cat_parent)
    {
        $productCat = ProductCategory::where('p_cat_parent', '=', $p_cat_parent)->get();

        return response([
            'data' => $productCat
        ]);
    }

    public function getLastCategory($last_cat)
    {
        $productCat = ProductCategory::where('p_cat_parent', '=', $last_cat)->get();
        return response([
            'data' => $productCat
        ]);
    }

    public function getAttribute($id)
    {
        $productAttribute = ProductAttribute::with('category')->where('p_cat_id', '=', $id);
        dd($productAttribute->get());
        if($productAttribute->count() > 0) {
            $data = array();
            foreach ($productAttribute as $key => $value) {
                $data = $value->category->p_cat_alias;
            }

            $dataTable = DB::table('_product_attr_'.$data.'')->get();
        }
        else {
            $dataTable = [];
        }

        return response([
            'data' => $productAttribute->get(),
            'cat' => $dataTable
        ]);
    }

    public function show($id){
        $product = Product::where('product_id',$id)->first();

        return view('backend.products.show', ['product' =>  $product]);
    }

    public function destroy($id){
        $product = Product::find($id);

        $product->product_status = 'delete';

        $product->save();

        return redirect()->route('admin.product-list')->with('success', 'Data Produk berhasil di hapus');
    }

	public function UploadImage(Request $request)
    {
        // create upload path if it does not exist
        $path = public_path($this->folder);
        if(!File::exists($path)) {
            File::makeDirectory($path);
        };

        // Simple validation (max file size 2MB and only two allowed mime types)
        $validator = new FileUpload\Validator\Simple('5M', ['image/png', 'image/jpg', 'image/jpeg']);

        // Simple path resolver, where uploads will be put
        $pathresolver = new FileUpload\PathResolver\Simple($path);

        // The machine's filesystem
        $filesystem = new FileUpload\FileSystem\Simple();

        // FileUploader itself
        $fileupload = new FileUpload\FileUpload($_FILES['files'], $_SERVER);
        $slugGenerator = new FileUpload\FileNameGenerator\Slug();

        // Adding it all together. Note that you can use multiple validators or none at all
        $fileupload->setPathResolver($pathresolver);
        $fileupload->setFileSystem($filesystem);
        $fileupload->addValidator($validator);
        $fileupload->setFileNameGenerator($slugGenerator);
        
        // Doing the deed
        list($files, $headers) = $fileupload->processAll();

        // Outputting it, for example like this
        foreach($headers as $header => $value) {
            header($header . ': ' . $value);
        }

        foreach($files as $file){
            //Remember to check if the upload was completed
            if ($file->completed) {

                // set some data
                $filename = $file->getFilename();
                $url = $this->folder . $filename;
                
                // save data
                $productImage = new productImage();
                $productImage->p_image_name = $filename;
                $productImage->p_image_primary = '1';
                $productImage->p_image_order = 123123;
                $productImage->product_id = $request->input('idProduct');
                $productImage->p_image_create_date = date('Y-m-d H:i:s');
                $productImage->save();
                // prepare response
                $data[] = [
                    'size' => $file->size,
                    'name' => $filename,
                    'url' => $url,
                    'thumbnailUrl' => $url,
                    'deleteType' => 'DELETE',
                    'image_id' => $productImage->p_image_id,
                ];

                // output uploaded file response
                return response()->json(['files' => $data]);
            }
        }
        // errors, no uploaded file
        return response()->json(['files' => $files]);
    }

    public function destroyStok(Request $request) {
        $productStok = ProductStock::find($request->input('id'));
        $productStok->delete();
        $res['success'] = true;
        $res['id'] = $request->input('id');
        return response($res);
    }

    function generate_code() {
        $first  = 'C';
        $day    = substr(date('D'),0, 1);
        $month  = substr(date('F'),0, 1);
        $year   = substr(date('Y'),-1);

        do {
            $code = $first.$month.$day.$year.rand(10000, 99999);

            $count = Product::where('product_number', '=', $code)->count();
        }
        while($count > 0);

        return $code;
    }



}