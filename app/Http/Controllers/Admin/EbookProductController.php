<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EbookProduct;
use App\Http\Requests\Admin\EbookProductRequest;
use App\Http\Controllers\Admin\Lib\ImageController;
use File;

class EbookProductController extends Controller
{
    public function index(){
    	$ebookproduct = EbookProduct::orderBy('e_product_id', 'DESC')->paginate('10');
    	return view('backend.ebookproduct.index', ['ebook' => $ebookproduct]);
    }

    public function create(){
    	return view('backend.ebookproduct.create');
    }

    public function store(EbookProductRequest $request){
    	$ebook = new EbookProduct();

    	$ebook->e_product_type = $request->input('e_product_type');
    	$ebook->e_product_name = $request->input('e_product_name');
    	$ebook->e_product_desc = $request->input('e_product_desc');
        $ebook->e_product_shortdesc = $request->input('e_product_shortdesc');
    	$ebook->e_product_edition = $request->input('e_product_edition');
    	$ebook->e_product_status = $request->input('e_product_status');
    	$ebook->e_product_publish_date = $request->input('e_product_publish_date');
    	$ebook->e_product_create_date = date('Y-m-d H:i:s');

    	if($request->input('ebookProductImage') != null) {
            $filename   = "ebookPackage-".$ebook->e_product_name;
            $path       = public_path().'/images/ebook_product/';

            $Image      = new ImageController();

            $fileUpload = $Image->filepond_upload($request->input('ebookProductImage'), $filename, $path);

            $ebook->e_product_image = $fileUpload->filename;
        }

        if($request->file('e_product_file') != null) {
            $fileex = $request->file('e_product_file')->getClientOriginalExtension();

            $filename = "ebookproduct-File".$ebook->e_product_name.".".$fileex;
            $destinationPath = public_path().'/document/';

            $copyFile = File::copy($request->file('e_product_file'), $destinationPath.$filename);

            $ebook->e_product_file = $filename;
        }

        $ebook->save();

        return redirect()->route('admin.ebookproduct')->with('success', 'Data Produk berhasil ditambahkan');
    }

    public function edit($id){
        $ebook = EbookProduct::find($id);
        return view('backend.ebookproduct.edit', ['ebook' => $ebook, 'id' => $id]);
    }

    public function update($id, EbookProductRequest $request){

        $ebook = EbookProduct::find($id);
        $ebook->e_product_type = $request->input('e_product_type');
        $ebook->e_product_name = $request->input('e_product_name');
        $ebook->e_product_desc = $request->input('e_product_desc');
        $ebook->e_product_shortdesc = $request->input('e_product_shortdesc');
        $ebook->e_product_edition = $request->input('e_product_edition');
        $ebook->e_product_status = $request->input('e_product_status');
        $ebook->e_product_publish_date = $request->input('e_product_publish_date');
        $ebook->e_product_create_date = date('Y-m-d H:i:s');

        if($request->input('ebookProductImage') != null) {
            $deleteImage = File::delete(public_path().'/images/ebook_product/'.$ebook->e_product_image);
            $filename   = "ebookPackage-".$ebook->e_product_name;
            $path       = public_path().'/images/ebook_product/';

            $Image      = new ImageController();

            $fileUpload = $Image->filepond_upload($request->input('ebookProductImage'), $filename, $path);

            $ebook->e_product_image = $fileUpload->filename;
        }

        if($request->file('e_product_file') != null) {
            $deleteImage = File::delete(public_path().'/images/ebook_product/file/'.$ebook->e_product_file);
            $fileex = $request->file('e_product_file')->getClientOriginalExtension();

            $filename = "ebookproduct-File".$ebook->e_product_name.".".$fileex;
            $destinationPath = public_path().'/images/ebook_product/file/';
            $copyFile = File::copy($request->file('e_product_file'), $destinationPath.$filename);

            $ebook->e_product_file = $filename;
        }

        $ebook->save();

        return redirect()->route('admin.ebookproduct')->with('success', 'Data Ebook Produk berhasil ditambahkan');
    }

    public function destroy($id){
        EbookProduct::where('e_product_id', $id)->delete();
        
        return redirect()->route('admin.ebookproduct')->with('success', 'Data Ebook Produk berhasil diHapus');
    }

    public function detail($id){
        $ebook = EbookProduct::where('e_product_id', $id)->first();

        return view('backend.ebookproduct.show', ['ebook' => $ebook]);
    }
}
