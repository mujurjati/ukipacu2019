<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use App\Http\Controllers\Controller;

use App\Models\ProductAttribute;

class ProductAttrController extends Controller
{
    private $attributes = [
        'attrName'      => 'Nama atribut',
        'attrType'      => 'Tipe atribut',
        'attrStatus'    => 'Status atribut',
    ];

    private $messages = [
        'required'  => ':attribute wajib di isi.',
        'unique'    => ':attribute sudah digunakan.',
    ];

    private $rules = [
        'attrName'      => 'required',
        'attrType'      => 'required',
        'attrStatus'    => 'required',
    ];

    public function add_attr($id, Request $request) {
        $this->rules['attrName'] = 'required|unique:_product_attribute,p_attribute_name';

        $this->validate($request, $this->rules, $this->messages, $this->attributes);

        $add = new ProductAttribute();

        $add->p_cat_id                  = $id;
        $add->p_attribute_name          = $request->input('attrName');
        $add->p_attribute_alias         = str_slug($request->input('attrName'));
        $add->p_attribute_form_type     = $request->input('attrType');
        $add->p_attribute_value         = '';
        $add->p_attribute_default_value = '';
        $add->p_attribute_order         = $this->get_last_order($id);
        $add->p_attribute_filter        = ($request->has('attrOption') && $request->input('attrOption') == 'on') ? '1' : '0';
        $add->p_attribute_status        = $request->input('attrStatus');
        $add->p_attribute_create        = '0';

        $add->save();

        return  redirect()->route('admin.product-cat.edit', ['attribute', $id])->with('success', 'Data Atribut Kategori berhasil ditambahkan');
    }

    public function edit_attr($id, Request $request) {
        $edit = ProductAttribute::where('p_attribute_id', '=', $id)->first();

        if($request->input('attrName') != $edit->p_attribute_name)
            $this->rules['attrName'] = 'required|unique:_product_attribute,p_attribute_name';

        $this->validate($request, $this->rules, $this->messages, $this->attributes);

        $edit->p_attribute_name          = $request->input('attrName');
        $edit->p_attribute_alias         = str_slug($request->input('attrName'));
        $edit->p_attribute_form_type     = $request->input('attrType');
        $edit->p_attribute_filter        = ($request->has('attrOption') && $request->input('attrOption') == 'on') ? '1' : '0';
        $edit->p_attribute_status        = $request->input('attrStatus');
        $edit->p_attribute_create        = '0';

        $edit->save();

        return  redirect()->route('admin.product-cat.edit', ['attribute', $edit->p_cat_id])->with('success', 'Data Atribut Kategori berhasil diperbaharui');
    }

    public function delete_attr($id, Request $request) {
        ProductAttribute::where('p_attribute_id', '=', $id)->delete();

        return  redirect()->route('admin.product-cat.edit', ['attribute', $request->input('catId')])->with('success', 'Data Atribut Kategori berhasil dihapus');
    }

    public function table_attr($id, Request $request) {
        $data = ProductAttribute::where('p_cat_id', '=', $id)
            ->where('p_attribute_status', '=', 'active')
            ->where('p_attribute_create', '=', '0');

        if($request->has('createTable')) {
            Schema::create($request->input('name'), function($table) use ($data)
            {
                $table->integer('product_id');

                foreach($data->get() as $item) {
                    if($item->p_attribute_form_type == 'text')
                        $table->string($item->p_attribute_alias, 200)->nullable();
                    elseif($item->p_attribute_form_type == 'textarea')
                        $table->text($item->p_attribute_alias)->nullable();
                    elseif($item->p_attribute_form_type == 'number')
                        $table->double($item->p_attribute_alias)->nullable();
                }
            });

            $data->update(['p_attribute_create' => '1']);

            return  redirect()->route('admin.product-cat.edit', ['attribute', $id])->with('success', 'Table '.$request->input('name').' berhasil dibuat.');
        }
        elseif($request->has('updateTable')) {
            Schema::table($request->input('name'), function($table) use ($data) {
                foreach($data->get() as $item) {
                    if($item->p_attribute_form_type == 'text')
                        $table->string($item->p_attribute_alias, 200)->nullable();
                    elseif($item->p_attribute_form_type == 'textarea')
                        $table->text($item->p_attribute_alias)->nullable();
                    elseif($item->p_attribute_form_type == 'number')
                        $table->double($item->p_attribute_alias)->nullable();
                }
            });

            $data->update(['p_attribute_create' => '1']);

            return  redirect()->route('admin.product-cat.edit', ['attribute', $id])->with('success', 'Table '.$request->input('name').' berhasil diperbaharui.');
        }

        return  redirect()->route('admin.product-cat.edit', ['attribute', $id]);
    }

    public function get_last_order($id) {
        $data = ProductAttribute::where('p_cat_id', '=', $id)
            ->orderBy('p_attribute_order', 'DESC');

        if($data->count() > 0) {
            $first = $data->first();

            return $first->p_attribute_order;
        }
        else {
            return '0';
        }
    }
}