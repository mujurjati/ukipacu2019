<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index() {
        $data = Contact::orderBy('guest_created_date', 'desc')
            ->paginate(20);
        return view('backend.contact.index', ['contact' => $data]);
    }

}