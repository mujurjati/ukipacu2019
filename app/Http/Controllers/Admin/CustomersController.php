<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\CustomerBank;
use App\Models\CustomerPenunjang;
use Carbon\Carbon;
use App\Http\Controllers\Admin\Lib\ImageController;
use File;
use Illuminate\Support\Facades\DB;

class CustomersController extends Controller
{
    public function index(Request $request) {
        $data = Customer::with('bank')->with('penunjang');

        if($request->has('status') && !empty($request->input('status')))
            $data->where('customer_status', '=', $request->input('status'));

        if($request->has('keyword') && !empty($request->input('keyword'))) {
            $data->where('customer_name', 'like', '%'. $request->input('keyword'). '%')
                ->orWhere('customer_email','like', '%'. $request->input('keyword'). '%')
                ->orWhere('customer_phone','like', '%'. $request->input('keyword'). '%');
        }

        $data = $data->orderBy('customer_create_date', 'desc')
            ->paginate(50);

        $data->appends([
            'status'    => $request->input('status'),
            'keyword'   => $request->input('keyword')
        ]);

        return view('backend.customers.listCustomer', ['customers' => $data]);
    }

    public function detail($id, Request $request)
    {
        $customer = Customer::with('bank')->with('penunjang')->find($id);

        $trx_bawahan = Customer::selectRaw('_order.order_id, _order.order_status, _order.order_number, _customer.customer_id, _customer.customer_name')
            ->leftJoin('_order', function($join) {
                $join->on('_customer.customer_id', '=', '_order.customer_id')
                    ->whereMonth('_order.order_finish_date', date('m'));
              })
            ->where('_customer.customer_path_reg', 'like', '%'. $id. '%')
            ->get();
            
        $current_month = Carbon::now();

        return view('backend.customers.detailCustomer', [
            'customer' => $customer,
            'bawahan' => $trx_bawahan,
            'bulan'     =>$current_month->format('F')
            ]);
    }

    public function block($id, Request $request)
    {
        $customer = Customer::find($id);

        if($request->has('updateblock')) {
            $customer->customer_status        = 'block';
            $dataPath      = json_decode($customer->customer_path_reg);
            foreach ($dataPath as $value) {
                $dataPathUpdate = Customer::find($value);
                $dataPathUpdate->customer_count -=1;
                $dataPathUpdate->save();
            }

            $customer->customer_reg_id = 0;
            $customer->customer_path_reg = 0;

            $customer->save();

            return  redirect()->route('admin.customer')->with('success', 'Blok member berhasil');
        }
        return view('backend.customers.listCustomer', ['customers' => $customer]);
    }

    public function open($id, Request $request)
    {
        $customer = Customer::find($id);
        if($request->has('updateopen')) {
            $customer->customer_status        = 'active';
            $customer->save();

            return  redirect()->route('admin.customer')->with('success', 'You have just updated one item');
        }
        return view('backend.customers.listCustomer', ['customers' => $customer]);
    }

    public function create(Request $request) {
        if($request->has('addCusByAdm')){
            $cus = new Customer();

            $this->validate($request, [
                'customer_name'          => 'required',
                'customer_tlg'       => 'required',
                'customer_nik'     => 'required|unique:_customer,customer_nik',
                'customer_password'      => 'required',
                'customer_bank_name' => 'required',
                'customer_bank_nasabah' => 'required',
                'customer_bank_rek' => 'required',
            ]);

            $ceknik = Customer::where('customer_nik', '=', $request->input('customer_nik'))->get();

            if(count($ceknik) > 0) {
                return redirect()->route('admin.customer.create')->with('error', 'NIK member sudah terdaftar atau akun member diblok');
            } else {
                $cus->customer_name = $request->input('customer_name');
                $cus->customer_reg_id = 0;
                $cus->customer_count =0;
                $cus->merchant_id = 0;
                $cus->customer_email = $request->input('customer_email');
                $cus->customer_npwp = $request->input('customer_npwp');
                $cus->customer_nik = $request->input('customer_nik');
                $cus->customer_city = $request->input('city');
                $cus->customer_phone = $request->input('customer_phone');
                $cus->customer_password = bcrypt($request->input('customer_password'));
                $cus->customer_address = $request->input('customer_address');
                $cus->customer_birth_date = $request->input('customer_tlg');
                $cus->customer_status = 'new';
                $cus->customer_create_date = Carbon::now();

                if ($cus->save()) {

                    if($request->input('customer_image') != null) {
                        if(!empty($cus->customer_image) && file_exists(public_path().'/images/customer/'.$cus->customer_image))
                            File::delete(public_path().'/images/customer/'.$cus->customer_image);
        
                        $fileImage  = $request->input('customer_image');
                        $filename   = "customer-".$cus->customer_name;
        
                        $path       = public_path().'/images/customer/';
        
                        $Image      = new ImageController();
        
                        $fileUpload = $Image->upload_crop($fileImage, $filename, $path, [300, 300]);
                        $cus->customer_image  = $fileUpload->filename;
                    }

                    if($cus->save()) {
                        $cus_pen = new CustomerPenunjang();

                        $cus_pen->customer_id = $cus->customer_id;
                        $cus_pen->c_pendidikan = $request->input('customer_pendidikan');
                        $cus_pen->c_pekerjaan = $request->input('customer_perkerjaan');
                        $cus_pen->c_agama = $request->input('customer_agama');
                        $cus_pen->c_ibu_kandung = $request->input('customer_ibu');
                        $cus_pen->c_ahli_waris = $request->input('customer_waris');
                        $cus_pen->c_hub_waris = $request->input('customer_hub_waris');
                        if($cus_pen->save()) {
                            $cus_bank = new CustomerBank();
                            $cus_bank->customer_id = $cus->customer_id;
                            $cus_bank->s_bank_name = $request->input('customer_bank_name');
                            $cus_bank->c_bank_cabang = $request->input('customer_bank_cabang');
                            $cus_bank->c_bank_nasabah = $request->input('customer_bank_nasabah');
                            $cus_bank->c_bank_rek = $request->input('customer_bank_rek');
        
                            $cus_bank->save();
                        }
                    }   

                    return redirect()->route('admin.customer')->with('success', 'Data Member berhasil ditambahkan');
                }
            }

        }
        return view('backend.customers.create');
    }

    public function edit($id, Request $request) {
        $edit = Customer::with('bank')->with('penunjang')->where('customer_id', $id)->first();
        if($request->has('editCusByAdm')){
            $cus = Customer::find($id);
            $this->validate($request, [
                'customer_name'          => 'required',
                'customer_tlg'       => 'required',
                'customer_bank_name' => 'required',
                'customer_bank_nasabah' => 'required',
                'customer_bank_rek' => 'required',
            ]);

                $cus->customer_name = $request->input('customer_name');
                $cus->customer_email = $request->input('customer_email');
                $cus->customer_npwp = $request->input('customer_npwp');
                $cus->customer_nik = $request->input('customer_nik');
                $cus->customer_phone = $request->input('customer_phone');
                $cus->customer_address = $request->input('customer_address');
                $cus->customer_birth_date = $request->input('customer_tlg');
                if($request->input('userPassword') != null) {
                    $cus->customer_password          = bcrypt($request->input('userPassword'));
                }
                $cus->customer_create_date = Carbon::now();

                if ($cus->save()) {

                    if($request->input('customer_image') != null) {
                        if(!empty($cus->customer_image) && file_exists(public_path().'/images/customer/'.$cus->customer_image))
                            File::delete(public_path().'/images/customer/'.$cus->customer_image);
        
                        $fileImage  = $request->input('customer_image');
                        $filename   = "customer-".$cus->customer_name;
        
                        $path       = public_path().'/images/customer/';
        
                        $Image      = new ImageController();
        
                        $fileUpload = $Image->upload_crop($fileImage, $filename, $path, [300, 300]);
                        $cus->customer_image  = $fileUpload->filename;
                    }

                    if($cus->save()) {
                        $id_pen = $edit->penunjang->c_pen_id;
                        $pen = CustomerPenunjang::where('c_pen_id', $id_pen)->first();
                        $pen->c_pendidikan = $request->input('customer_pendidikan');
                        $pen->c_pekerjaan = $request->input('customer_perkerjaan');
                        $pen->c_agama = $request->input('customer_agama');
                        $pen->c_ibu_kandung = $request->input('customer_ibu');
                        $pen->c_ahli_waris = $request->input('customer_waris');
                        $pen->c_hub_waris = $request->input('customer_hub_waris');
                        if($pen->save()) {
                            $id_bank = $edit->bank->c_bank_id;
                            $cus_bank = CustomerBank::where('c_bank_id', $id_bank)->first();
                            $cus_bank->s_bank_name = $request->input('customer_bank_name');
                            $cus_bank->c_bank_cabang = $request->input('customer_bank_cabang');
                            $cus_bank->c_bank_nasabah = $request->input('customer_bank_nasabah');
                            $cus_bank->c_bank_rek = $request->input('customer_bank_rek');
                            $cus_bank->save();
                        }                        
                    }   

                    return redirect()->route('admin.customer')->with('success', 'Data Member berhasil di update');
                }
        }

        return view('backend.customers.edit', [
            'cus' => $edit
        ]);
    }
}
