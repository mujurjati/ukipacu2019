<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserLevel;
use Illuminate\Http\Request;

use App\Models\User;
use App\Http\Controllers\Admin\Lib\ImageController;
use Auth;
use File;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::with('level')
            ->orderBy('user_create_date', 'desc')
            ->paginate(20);

        return view('backend.users.listUser', ['users' => $data]);
    }

    public function add(Request $request)
    {
        $level =UserLevel::where('u_level_status','=', 'active')->get();
        if($request->has('addUser')) {
            $userImage = $request->input('userImage');

            $this->validate($request, [
                'userName'      => 'required',
                'userEmail'     => 'required|email|unique:_user,user_email',
                'userPassword'  => 'required|min:8',
                'userLevel'     => 'required',
            ]);

            $name   = $request->input('userName');
            $code   = generate_code();
            $email  = $request->input('userEmail');
            $phone  = $request->input('userPhone');
            $pass   = $request->input('userPassword');
            $level  = $request->input('userLevel');
            $address  = $request->input('userAddress');
            $status = $request->input('userStatus');

            $add = new User();

            $add->u_level_id        = $level;
            $add->user_name         = $name;
            $add->user_code         = $code;
            $add->user_phone        = $phone;
            $add->user_email        = $email;
            $add->user_username     = $email;
            $add->user_status       = $status;
            $add->user_address      = $address;
            $add->password          = bcrypt($pass);
            $add->user_create_date  = date('Y-m-d H:i:s');

            $add->save();

            if($userImage != null) {
                $filename   = "users-".$add->user_id;
                $path       = public_path().'/images/';

                $Image      = new ImageController();

                $fileUpload = $Image->filepond_upload($userImage, $filename, $path);

                $add->user_photo = $fileUpload->filename;
            }

            $add->save();

            return  redirect()->route('admin.user')->with('success', 'You have just created one User');
        }

        return view('backend.users.addUser', ['level' => $level]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('backend.users.editUser', ['user' => $user]);
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);

        if($request->has('editUser')) {
            $userImage = $request->input('userImage');
            $this->validate($request, [
                'userName'      => 'required',
                'userEmail'     => 'required|email|unique:_user,user_email,'.$user->user_id.',user_id',
                'userPassword'  => 'sometimes|nullable|min:8',
                'userLevel'     => 'required',
            ]);

            $name   = $request->input('userName');
            $email  = $request->input('userEmail');
            $pass   = $request->input('userPassword');
            $level  = $request->input('userLevel');

            $user->u_level_id        = $level;
            $user->user_name         = $name;
            $user->user_email        = $email;
            $user->user_username     = $email;

            if($pass != null){
            $user->password          = bcrypt($pass);
            }

            if($userImage != null) {

                $deleteImage = File::delete(public_path().'/images/'.$user->user_photo);
                $filename   = "users-".$user->user_id;
                $path       = public_path().'/images/';

                $Image      = new ImageController();

                $fileUpload = $Image->filepond_upload($userImage, $filename, $path);

                $user->user_photo = $fileUpload->filename;
            }

            $user->save();

            return  redirect()->route('admin.user')->with('success', 'You have just updated one item');
        }

        return view('backend.users.editUser', ['user' => $user]);
    }

    public function hapus($id)
    {
        User::where('user_id', $id)->delete();
        
        return redirect()->route('admin.user')->with('success', 'Data Produk berhasil dihapuss');
    }

    public function level(Request $request)
    {
        $level=UserLevel::get();
        return view('backend.users.level', ['level' => $level]);
    }
    public function editlevel($id)
    {
        $level = UserLevel::find($id);
        return view('backend.users.editLevel', ['level' => $level]);
    }
    public function updatelevel($id, Request $request)
    {
        $level = UserLevel::find($id);
        if($request->has('editLevel')) {

            $level->u_level_status       = $request->input('levelStatus');

            $level->save();

            return  redirect()->route('admin.user.level')->with('success', 'You have just updated one item');
        }
        return view('backend.users.editLevel', ['level' => $level]);
    }

}
