<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Merchant;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Controllers\Admin\Lib\ImageController;
use File;
class MerchantController extends Controller
{
    public function index(Request $request) {
        $data = Merchant::where('merchant_status', '!=', 'delete');

        if($request->has('status') && !empty($request->input('status')))
            $data->where('merchant_status', '=', $request->input('status'));

        if($request->has('keyword') && !empty($request->input('keyword'))) {
            $data->where('merchant_name', '=', $request->input('keyword'))
                ->orWhere('merchant_email','like', '%'. $request->input('keyword'). '%')
                ->orWhere('merchant_phone','like', '%'. $request->input('keyword'). '%');
        }

        $data = $data->orderBy('merchant_create_date', 'desc')
            ->paginate(20);

        $data->appends([
            'status'    => $request->input('status'),
            'keyword'   => $request->input('keyword')
        ]);

        return view('backend.merchant.listMerchant', ['merchants' => $data]);
    }

    public function create(Request $request)
    {

        if($request->has('addMerchant')) {
            $this->validate($request, [
                'merchant_name'      => 'required',
                'merchant_email'     => 'required|email|unique:_merchant,merchant_email',
                // 'merchant_npwp'      => 'required',
                'merchant_password'  => 'required|min:8',
                'merchant_status'    => 'required',
            ]);

            $add = new Merchant();

            $add->merchant_name      = $request->input('merchant_name');
            $add->merchant_npwp      = $request->input('merchant_npwp');
            $add->merchant_email     = $request->input('merchant_email');
            $add->merchant_phone     = $request->input('merchant_phone');
            $add->merchant_password  = bcrypt($request->input('merchant_password'));
            $add->merchant_gender    ='m';
            $add->merchant_birth_place ='Indonesia';
            $add->merchant_address   = $request->input('merchant_address');
            $add->merchant_status    = $request->input('merchant_status');

            $add->merchant_lat      = $request->input('merchant_lat');
            $add->merchant_lng      = $request->input('merchant_lng');

            $add->merchant_create_date  = date('Y-m-d H:i:s');

            $add->save();
            if(!empty($request->input('merchant_image'))) {
                $fileImage  = $request->input('merchant_image');
                $filename   = "merchant-".$add->merchant_name;

                $path       = public_path().'/images/merchant/';

                $Image      = new ImageController();

                $fileUpload = $Image->upload_crop($fileImage, $filename, $path, [300, 300]);
                $add->merchant_image  = $fileUpload->filename;
            }
            $add->save();

            return  redirect()->route('admin.merchant')->with('success', 'You have just created one  merchant');
        }

        return view('backend.merchant.create');
    }
    public function edit($id, Request $request){
        $merchant = Merchant::find($id);
        return view('backend.merchant.edit', ['merchant' => $merchant]);
    }
    public function update($id, Request $request){
        $merchant = Merchant::find($id);


        if($request->has('editMerchant')){
            $merchant_image = $request->input('merchant_image');
            $this->validate($request, [
                'merchant_name'      => 'required',
                'merchant_npwp'      => 'required',
                'merchant_email'     => 'required|email|unique:_merchant,merchant_email,'.$merchant->merchant_id.',merchant_id',
                'merchant_password'  => 'sometimes|nullable|min:8',
                'merchant_status'    => 'required',
            ]);
            $merchant_password  = $request->input('userPassword');

            $merchant->merchant_name      = $request->input('merchant_name');
            $merchant->merchant_npwp      = $request->input('merchant_npwp');
            $merchant->merchant_email     = $request->input('merchant_email');
            $merchant->merchant_phone     = $request->input('merchant_phone');
            $merchant->merchant_address   = $request->input('merchant_address');
            $merchant->merchant_status    = $request->input('merchant_status');

            $merchant->merchant_lat      = $request->input('merchant_lat');
            $merchant->merchant_lng      = $request->input('merchant_lng');


            if($merchant_password != null){
                $merchant->merchant_password          = bcrypt($merchant_password);
            }
            if($merchant_image != null) {
                if(!empty($merchant->merchant_image) && file_exists(public_path().'/images/merchant/'.$merchant->merchant_image))
                    File::delete(public_path().'/images/merchant/'.$merchant->merchant_image);

                $fileImage  = $request->input('merchant_image');
                $filename   = "merchant-".$merchant->merchant_name;

                $path       = public_path().'/images/merchant/';

                $Image      = new ImageController();

                $fileUpload = $Image->upload_crop($fileImage, $filename, $path, [300, 300]);
                $merchant->merchant_image  = $fileUpload->filename;
            }
            $merchant->save();

            return redirect()->route('admin.merchant')->with('success', 'Data merchant berhasil Di Update');
        }

        return view('backend.merchant.edit', ['merchant' =>  $merchant,'id' => $id]);
    }

    public function detail($id, Request $request)
    {
        $merchant = Merchant::find($id);

        return view('backend.merchant.detailMerchant', ['merchant' => $merchant]);
    }

    public function block($id, Request $request)
    {
        $merchant = Merchant::find($id);

        if($request->has('updateblock')) {
            $merchant->merchant_status        = 'block';
            $merchant->save();

            return  redirect()->route('admin.merchant')->with('success', 'You have just updated one item');
        }
        return view('backend.merchant.listMerchant', ['merchant' => $merchant]);
    }
    public function open($id, Request $request)
    {
        $merchant = Merchant::find($id);
        if($request->has('updateopen')) {
            $merchant->merchant_status        = 'active';
            $merchant->save();

            return  redirect()->route('admin.merchant')->with('success', 'You have just updated one item');
        }
        return view('backend.merchant.listMerchant', ['merchant' => $merchant]);
    }
}
