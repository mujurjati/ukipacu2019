<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Video;
use File;

class VideoController extends Controller
{
    public function index(Request $request) {
        $data = Video::orderBy('video_create_date','desc');

        if($request->has('status') && !empty($request->input('status')))
            $data->where('video_status', '=', $request->input('status'));

        if($request->has('keyword') && !empty($request->input('keyword')))
            $data->where('video_name','like', '%'. $request->input('keyword'). '%');

        return view('backend.video.listVideo', ['video' => $data->paginate('12')]);
    }

    public function create(Request $request)
    {

        if($request->has('addVideo')) {
            $this->validate($request, [
                'video_name'      => 'required',
                'video_status'    => 'required',
            ]);

            $video = new Video();

            $video->video_name            = $request->input('video_name');
            $video->video_sortdesc        = $request->input('video_sortdesc');
            $video->video_desc            = $request->input('video_desc');
            $video->video_status          = $request->input('video_status');
            $video->video_create_date     = date('Y-m-d H:i:s');

            $video->save();

            if($request->file('video_file') != null){
                $fileex = $request->file('video_file')->getClientOriginalExtension();

                $filename = "Video-".$video->video_name.".".$fileex;
                $destinationPath = public_path().'/videos/';
                $copyFile = File::copy($request->file('video_file'), $destinationPath.$filename);

                $video->video_file = $filename;
            }
            $video->save();

            return  redirect()->route('admin.video')->with('success', 'You have just created one  video');
        }

        return view('backend.video.create');
    }

    public function edit($id, Request $request){
        $video = Video::find($id);
        return view('backend.video.edit', ['video' => $video]);
    }
    public function update($id, Request $request){
        $video = Video::find($id);

        if($request->has('editVideo')){
            $this->validate($request, [
                'video_name'      => 'required',
                'video_status'    => 'required',
                'video_sortdesc'    => 'required',
                'video_desc'    => 'required',
            ]);


            $video->video_name            = $request->input('video_name');
            $video->video_sortdesc        = $request->input('video_sortdesc');
            $video->video_desc            = $request->input('video_desc');
            $video->video_status          = $request->input('video_status');

            $video->save();

            if($request->file('video_file') != null){
                $fileex = $request->file('video_file')->getClientOriginalExtension();

                $filename = "Video-".$video->audio_title.".".$fileex;
                $destinationPath = public_path().'/videos/';
                $copyFile = File::copy($request->file('video_file'), $destinationPath.$filename);

                $video->video_file = $filename;
            }
            $video->save();

            return redirect()->route('admin.video')->with('success', 'Data Video berhasil Di Update');
        }

        return view('backend.video.edit', ['video' =>  $video,'id' => $id]);
    }



}