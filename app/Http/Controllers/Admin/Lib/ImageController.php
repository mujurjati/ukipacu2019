<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 24-Dec-18
 * Time: 11:19
 */

namespace App\Http\Controllers\Admin\Lib;

use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use File;
use FileUpload;
use Croppa;
use App\Models\ProductImage;
use Carbon\Carbon;

class ImageController extends Controller
{

    public $folder = '/uploads/';

    public function filepond_upload($data, $name, $path, $resize = []) {
        $file = json_decode($data);

        $exp    = explode('.', $file->name);
        $ext    = end($exp);

        $filename   = $name.".".$ext;
        $uploadpath = $path.$filename;

        $exp_fd     = explode("base64,", $file->data);
        $baseImage  = (!empty($exp_fd[1])) ? $exp_fd[1] : $file->data;

        $filedata   = 'data:text/plain;base64,'.$baseImage;

        $create = Image::make(file_get_contents($filedata));

        if(!empty($resize))
            $create->resize($resize[0], $resize[1]);

        $create->save($uploadpath);

        $result = (object)[
            'filename'  => $filename,
            'fullpath'  => $uploadpath,
            'origin'    => $file->name,
            'data'      => $filedata
        ];

        return $result;
    }

    public function upload_crop($data, $name, $path, $resize = [], $thumb = false, $width = null, $height = null) {
        $exp    = explode(';base64', $data);
        $exp    = explode('/', $exp[0]);
        $ext    = end($exp);

        $filename   = $name.".".$ext;
        $uploadpath = $path.$filename;
        $thumbpath  = '';

        $img = Image::make(file_get_contents($data));

        if(!empty($resize))
            $img->resize($resize[0], $resize[1]);

        $img->save($uploadpath);

        if($thumb) {
            $thumbpath = $path.'thumb/'.$filename;

            $img->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbpath);
        }

        $result = (object)[
            'filename'  => $filename,
            'fullpath'  => $uploadpath,
            'thumbpath' => $thumbpath
        ];

        return $result;
    }

    public function filepond_upload_2($data, $name, $path) {
        $file = json_decode($data);

        $exp    = explode('.', $file->name);
        $ext    = end($exp);

        $filename   = $name.".".$ext;
        $uploadpath = $path.$filename;

        $filedata   = 'data:text/plain;base64,'.$file->data;

        Image::make(file_get_contents($filedata))->save($uploadpath);

        $result = (object)[
            'filename'  => $filename,
            'fullpath'  => $uploadpath,
            'origin'    => $file->name,
            'data'      => $filedata
        ];

        return $result;
    }
}