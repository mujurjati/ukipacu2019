<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bank;
use App\Models\ShippingCourier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\ShippingSubdistrict;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public function index(Request $request){
        $set = new Setting();

        $setting = $set->select_data();
        return view('backend.setting.index', ['setting' => $setting]);
    }

    public function update(Request $request)
    {
        if($request->has('updateSetting')) {
            foreach($request->input('form') as $key => $val) {
                Setting::where('setting_type', '=', 'General')
                    ->where('setting_name', '=', $key)
                    ->update(['setting_value' => $val]);
            }

            return  redirect()->route('admin.setting')->with('success', 'Berhasil perbaharui setting');
        }
        return view('backend.setting.index');
    }

    /*Alamat Pengiriman*/
    public function alamatpengiriman(){
        $alamat = Setting::with('subdistrict.city.province')
            ->where('setting_type', '=','General')
            ->where('setting_name', '=','alamat_pengiriman')
            ->first();

        return view('backend.setting.alamatpengiriman', [
            'alamat' => $alamat
        ]);
    }


    public function alamatpengirimanUpdate(Request $request)
    {
        if($request->has('alamatpengirimanUpdate')) {
            foreach($request->input('form') as $key => $val) {
                if(in_array($key, ['biaya_kirim']))
                    $value = str_replace('.', '', $val);
                else
                    $value = $val;

                Setting::where('setting_type', '=', 'General')
                    ->where('setting_name', '=', $key)
                    ->update(['setting_value' => $value]);
            }

            return  redirect()->route('admin.setting.alamatpengiriman')->with('success', 'Berhasil perbaharui alamat pengiriman');
        }
        return view('backend.setting.alamatpengiriman');
    }

    public function kurirpengiriman(){
        $kurir = ShippingCourier::orderBy('s_courier_name','=','asc')->get();
        return view('backend.setting.kurirpengiriman', [
            'kurir' => $kurir
        ]);
    }
    public function editkurir($id){
        $kurir = ShippingCourier::find($id);
        return view('backend.setting.editkurir', [
            'kurir' => $kurir
        ]);
    }
    public function updatekurir($id, Request $request)
    {
        $kurir = ShippingCourier::find($id);
        if($request->has('editKurir')) {

            $kurir->s_courier_status       = $request->input('kurirStatus');

            $kurir->save();

            return  redirect()->route('admin.setting.kurirpengiriman')->with('success', 'Berhasil perbaharui one item');
        }
        return view('backend.setting.editkurir', ['kurir' => $kurir]);
    }

    /*About*/
    public function about(){
        $set = new Setting();
        $about = $set->select_data();

        return view('backend.setting.about', ['about' => $about]);
    }

    public function aboutUpdate(Request $request)
    {
        if($request->has('aboutUpdate')) {
            foreach($request->input('form') as $key => $val) {
                Setting::where('setting_type', '=', 'General')
                    ->where('setting_name', '=', $key)
                    ->update(['setting_value' => $val]);
            }

            return  redirect()->route('admin.setting.about')->with('success', 'Berhasil perbaharui tentang kami');
        }
        return view('backend.setting.about');
    }


    /*Syarat dan ketentuan*/
    public function terms(){
        $set = new Setting();
        $terms = $set->select_data();

        return view('backend.setting.terms', ['terms' => $terms]);
    }

    public function termsUpdate(Request $request)
    {
        if($request->has('termsUpdate')) {
            foreach($request->input('form') as $key => $val) {
                Setting::where('setting_type', '=', 'General')
                    ->where('setting_name', '=', $key)
                    ->update(['setting_value' => $val]);
            }

            return  redirect()->route('admin.setting.terms')->with('success', 'Berhasil perbaharui Syarat & Ketentuan');
        }
        return view('backend.setting.terms');
    }

    /*BANK*/
    public function bank(){
        $bank = Bank::orderBy('bank_order','=','asc')->get();
        return view('backend.setting.bank', ['bank' => $bank]);
    }
    public function bankdetail($id){
        $bank = Bank::find($id);
        return view('backend.setting.bankdetail', ['bank' => $bank]);
    }
    public function bankedit($id){
        $bank = Bank::find($id);
        return view('backend.setting.bankedit', ['bank' => $bank]);
    }

    public function bankupdate($id, Request $request)
    {

        $bank = Bank::find($id);

        if($request->has('editbank')) {
            $this->validate($request, [
                'bankAccount'      => 'required',
                'bankNumber'       => 'required',
                'bankBranch'       => 'required',
            ]);

            $bank->bank_account         = $request->input('bankAccount');
            $bank->bank_number          = $request->input('bankNumber');
            $bank->bank_branch          = $request->input('bankBranch');
            $bank->bank_status          = $request->input('bankStatus');

            $bank->save();

            return  redirect()->route('admin.setting.bank')->with('success', 'Berhasil perbaharui one item');
        }

        return view('backend.setting.bankedit', ['bank' => $bank]);
    }


    /*Kebijakan Privasi*/
    public function kebijakanprivasi(){
        $set = new Setting();
        $kebijakanprivasi = $set->select_data();

        return view('backend.setting.kebijakanprivasi', ['kebijakanprivasi' => $kebijakanprivasi]);
    }

    public function kebijakanprivasiUpdate(Request $request)
    {
        if($request->has('kebijakanprivasiUpdate')) {
            foreach($request->input('form') as $key => $val) {
                Setting::where('setting_type', '=', 'General')
                    ->where('setting_name', '=', $key)
                    ->update(['setting_value' => $val]);
            }

            return  redirect()->route('admin.setting.kebijakanprivasi')->with('success', 'Berhasil perbaharui Kebijakan Privasi');
        }
        return view('backend.setting.kebijakanprivasi');
    }

    /*Panduan Keamanan*/
    public function panduankeamanan(){
        $set = new Setting();
        $panduankeamanan = $set->select_data();

        return view('backend.setting.panduankeamanan', ['panduankeamanan' => $panduankeamanan]);
    }

    public function panduankeamananUpdate(Request $request)
    {
        if($request->has('panduankeamananUpdate')) {
            foreach($request->input('form') as $key => $val) {
                Setting::where('setting_type', '=', 'General')
                    ->where('setting_name', '=', $key)
                    ->update(['setting_value' => $val]);
            }

            return  redirect()->route('admin.setting.panduankeamanan')->with('success', 'Berhasil perbaharui Panduan Keamanan');
        }
        return view('backend.setting.panduankeamanan');
    }

}
