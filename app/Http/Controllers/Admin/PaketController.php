<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Paket;
use App\Models\Product;
use App\Models\PaketDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Lib\ImageController;

class PaketController extends Controller
{
    public function index() {
        $data = Paket::orderBy('paket_id', 'desc')
            ->paginate(10);

        return view('backend.paket.index', ['pakets' => $data]);
    }

    public function create(Request $request)
    {

        if($request->has('savePaket')) {
            $this->validate($request, [
                'paket_name'      => 'required'
            ]);

            $add = new Paket();

            $add->paket_name   = $request->input('paket_name');

            $add->paket_created_date  = date('Y-m-d H:i:s');
            
            if($request->input('paket_image') != null) {
                if(!empty($add->paket_image) && file_exists(public_path().'/images/paket/'.$add->paket_image))
                    File::delete(public_path().'/images/paket/'.$add->paket_image);

                $fileImage  = $request->input('paket_image');
                $filename   = "paket-".$add->paket_name;

                $path       = public_path().'/images/paket/';

                $Image      = new ImageController();
                $fileUpload = $Image->filepond_upload($fileImage, $filename, $path);
                $add->paket_image  = $fileUpload->filename;
                $add->save();
            }

            return  redirect()->route('admin.paket')->with('success', 'Tambah Paket Berhasil');
        }

        return view('backend.paket');
    }

    public function detail($id) {
        $pakets = Paket::find($id);
        $produks = PaketDetail::with('paketDetailProduk')
            ->where('paket_id', '=', $id)
            ->get();
// dd($produks);
        return view('backend.paket.detail', [
            'paket' => $pakets,
            'produks' => $produks,
            ]);
    }

    public function delete($id, $param)
    {
        PaketDetail::where('pd_id', $id)->delete();
        return  redirect()->route('admin.paket.detail', ['id'=>$param])->with('success', 'Hapus produk paket berhasil');

    }

    public function ajax_produk(Request $request) {
        $src = $request->input('q');
        $data = Product::selectRaw("
                product_id as id,
                product_name as text
            ")
            ->where('product_name', 'like', '%'.$src.'%')
            ->orWhere('product_alias', 'like', '%'.$src.'%')
            ->orderBy('product_name')
            ->paginate(10);

        return response($data);
    }

    public function add_produk($id, Request $request) {
        $data = new PaketDetail();

        $data->paket_id         = $id;
        $data->product_id       = $request->input('product_id');
        $data->pd_created_date  = date('Y-m-d H:i:s');

        if ($data->save()) {
            return  redirect()->route('admin.paket.detail', ['id'=>$id])->with('success', 'Tambah Produk Paket Berhasil');
        }
    }   
}
