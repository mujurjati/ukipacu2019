<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Controllers\Admin\Lib\ImageController;
use File;
class DriverController extends Controller
{
    public function index(Request $request) {
        $data = Driver::orderBy('driver_create_date', 'desc');

        if($request->has('status') && !empty($request->input('status')))
            $data->where('driver_status', '=', $request->input('status'));

        if($request->has('keyword') && !empty($request->input('keyword'))) {
            $data->where('driver_name', '=', $request->input('keyword'))
                ->orWhere('driver_email','like', '%'. $request->input('keyword'). '%')
                ->orWhere('driver_phone','like', '%'. $request->input('keyword'). '%');
        }

        $data = $data->paginate(20);

        $data->appends([
            'status'    => $request->input('status'),
            'keyword'   => $request->input('keyword')
        ]);

        return view('backend.driver.listDriver', ['drivers' => $data]);
    }

    public function create(Request $request)
    {

        if($request->has('addDriver')) {
            $this->validate($request, [
                'driver_name'  => 'required',
                'driver_email'     => 'required|email|unique:_driver,driver_email',
                'driver_password'  => 'required|min:8',
                'driver_status'   => 'required',
            ]);

            $add = new Driver();

            $add->driver_name      = $request->input('driver_name');
            $add->driver_email     = $request->input('driver_email');
            $add->driver_phone     = $request->input('driver_phone');
            $add->driver_password  = bcrypt($request->input('driver_password'));
//            $add->driver_gender    = $request->input('driver_gender');
            $add->driver_gender    ='m';
            $add->driver_status    = $request->input('driver_status');

            $add->driver_create_date  = date('Y-m-d H:i:s');

            $add->save();
            if(!empty($request->input('driver_image'))) {
                $fileImage  = $request->input('driver_image');
                $filename   = "driver-".$add->driver_name;

                $path       = public_path().'/images/driver/';

                $Image      = new ImageController();

                $fileUpload = $Image->upload_crop($fileImage, $filename, $path, [300, 300]);
                $add->driver_image  = $fileUpload->filename;
            }
            $add->save();

            return  redirect()->route('admin.driver')->with('success', 'You have just created one  Driver');
        }

        return view('backend.driver.create');
    }
    public function edit($id, Request $request){
        $driver = Driver::find($id);
        return view('backend.driver.edit', ['driver' => $driver]);
    }
    public function update($id, Request $request){
        $driver = Driver::find($id);


        if($request->has('editDriver')){
            $driver_image = $request->input('driver_image');
            $this->validate($request, [
                'driver_name'      => 'required',
                'driver_email'     => 'required|email|unique:_driver,driver_email,'.$driver->driver_id.',driver_id',
                'driver_password'  => 'sometimes|nullable|min:8',
                'driver_status'    => 'required',
            ]);
            $driver_password  = $request->input('userPassword');

            $driver->driver_name      = $request->input('driver_name');
            $driver->driver_email     = $request->input('driver_email');
            $driver->driver_phone     = $request->input('driver_phone');
//            $driver->driver_gender    = $request->input('driver_gender');
            $driver->driver_status    = $request->input('driver_status');


            if($driver_password != null){
                $driver->driver_password          = bcrypt($driver_password);
            }
            if($driver_image != null) {

                $deleteImage = File::delete(public_path().'/images/driver/'.$driver->driver_image);
                $fileImage  = $request->input('driver_image');
                $filename   = "driver-".$driver->driver_name;

                $path       = public_path().'/images/driver/';

                $Image      = new ImageController();

                $fileUpload = $Image->upload_crop($fileImage, $filename, $path, [300, 300]);
                $driver->driver_image  = $fileUpload->filename;
            }
            $driver->save();

            return redirect()->route('admin.driver')->with('success', 'Data Driver berhasil Di Update');
        }

        return view('backend.driver.edit', ['driver' =>  $driver,'id' => $id]);
    }

    public function detail($id, Request $request)
    {
        $driver = Driver::with('address_primary.subdistrict.city.province')->find($id);

        return view('backend.driver.detailDriver', ['driver' => $driver]);
    }

    public function block($id, Request $request)
    {
        $driver = Driver::find($id);

        if($request->has('updateblock')) {
            $driver->driver_status        = 'block';
            $driver->save();

            return  redirect()->route('admin.driver')->with('success', 'You have just updated one item');
        }
        return view('backend.driver.listDriver', ['driver' => $driver]);
    }
    public function open($id, Request $request)
    {
        $driver = Driver::find($id);
        if($request->has('updateopen')) {
            $driver->driver_status        = 'active';
            $driver->save();

            return  redirect()->route('admin.driver')->with('success', 'You have just updated one item');
        }
        return view('backend.driver.listDriver', ['driver' => $driver]);
    }
}