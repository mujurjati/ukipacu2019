<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StoreCorner;
use App\Models\ShippingSubdistrict;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\Lib\ImageController;

class StorecornerController extends Controller
{
    public function index(){
        $store = StoreCorner::orderBy('s_corner_create_date', 'desc')
        ->paginate(15);

        return view('backend.storecorner.index', ['store' => $store]);
    }

    public function detail($id)
    {
        $store = StoreCorner::with('subdistrict.city.province')
        ->find($id);

        return view('backend.storecorner.detail', ['store' => $store]);
    }

    public function create(Request $request)
    {

        if($request->has('addStore')) {
            $storeImage = $request->input('storeImage');

            $this->validate($request, [
                'storeCode'      => 'required',
                'storeName'      => 'required',
                'cityStore'      => 'required',
                'openStore'      => 'required',
                'closeStore'     => 'required',
                'storeStatus'    => 'required',
            ]);

            $add = new StoreCorner();

            $add->s_corner_code      = $request->input('storeCode');;
            $add->s_corner_name      = $request->input('storeName');;
            $add->s_corner_phone     = $request->input('storePhone');;
            $add->s_corner_desc      = $request->input('storeDesc');;
            $add->s_subdistrict_id   = $request->input('cityStore');;
            $add->s_corner_address   = $request->input('storeAddress');;
            $add->s_corner_open      = $request->input('openStore');;
            $add->s_corner_close     = $request->input('closeStore');;
            $add->s_corner_status    = $request->input('storeStatus');;

            $add->s_corner_create_date  = date('Y-m-d H:i:s');

            $add->save();

            if($storeImage != null) {

                $filename   = "storecorner-".$add->s_corner_id;
                $path       = public_path().'/images/storecorner/';

                $Image      = new ImageController();

                $fileUpload = $Image->filepond_upload($storeImage, $filename, $path);

                $add->s_corner_logo = $fileUpload->filename;

                $add->save();
            }
            $add->save();

            return  redirect()->route('admin.storecorner')->with('success', 'You have just created one Store Corner');
        }

        return view('backend.storecorner.create');
    }

    public function edit($id)
    {
        $store = StoreCorner::find($id);
        return view('backend.storecorner.edit', ['store' => $store]);
    }

    public function update($id, Request $request)
    {

        $store = StoreCorner::find($id);

        if($request->has('editStore')) {
            $storeImage = $request->input('storeImage');
            $this->validate($request, [
                'storeCode'      => 'required',
                'storeName'      => 'required',
                'cityStore'      => 'required',
                'openStore'      => 'required',
                'closeStore'     => 'required',
                'storeStatus'    => 'required',
            ]);


            $store->s_corner_code      = $request->input('storeCode');;
            $store->s_corner_name      = $request->input('storeName');;
            $store->s_corner_phone     = $request->input('storePhone');;
            $store->s_corner_desc      = $request->input('storeDesc');;
            $store->s_subdistrict_id   = $request->input('cityStore');;
            $store->s_corner_address   = $request->input('storeAddress');;
            $store->s_corner_open      = $request->input('openStore');;
            $store->s_corner_close     = $request->input('closeStore');;
            $store->s_corner_status    = $request->input('storeStatus');;

            $store->save();

            if($storeImage != null) {

                $deleteImage = File::delete(public_path().'/images/'.$store->s_corner_logo);
                $filename   = "storecorner-".$store->s_corner_logo;
                $path       = public_path().'/images/';

                $Image      = new ImageController();

                $fileUpload = $Image->filepond_upload($storeImage, $filename, $path);

                $store->s_corner_logo = $fileUpload->filename;
            }

            $store->save();

            return  redirect()->route('admin.storecorner')->with('success', 'You have just updated one item');
        }

        return view('backend.storecorner.edit', ['store' => $store]);
    }

    public function hapus($id, Request $request)
    {
        StoreCorner::where('s_corner_id', $id)->delete();

        return  redirect()->route('admin.storecorner')->with('success', 'You have just delete one Store Corner');
    }


}
