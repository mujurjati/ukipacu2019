<?php

namespace App\Http\Controllers\Admin;

use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\Paket;
use App\Models\PaketDetail;
use App\Models\Customer;
use App\Models\Driver;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function index(Request $request){
        $orderDetail = Order::with('customer')->with('detailIndex'); 	
        if($request->has('status') && !empty($request->input('status')))
            $orderDetail->where('order_status', '=', $request->input('status'));

        if($request->has('filter_date') && !empty($request->input('filter_date')))
            // dd($request->has('filter_date'));
            $orderDetail->whereRaw("DATE(order_create_date) = '".date("Y-m-d", strtotime($request->input('filter_date')))."'");

        if($request->has('keyword') && !empty($request->input('keyword'))) {
            $orderDetail->where('order_bank_from_account', 'like', '%'.$request->input('keyword').'%');
        }

        $orderDetail = $orderDetail->orderBy('order_create_date', 'desc')
            ->paginate(50);

        $orderDetail->appends([
            'status'    => $request->input('status'),
            'filter_date'    => $request->input('filter_date'),
            'keyword'   => $request->input('keyword')
        ]);
        
    	return view('backend.order.index', [
    	    'order'         => $orderDetail
        ]);
    }

    public function detail($id){
        $orderDetail = Order::with("detail")
            ->with('customer')
            ->where('order_id', $id)
            ->first();

        // $driver = Driver::where('driver_status', '=', 'active')
        //     ->orderBy("driver_name")
        //     ->get();
        
        return view('backend.order.detail', [
            'order'     => $orderDetail,
            // 'driver'    => $driver
        ]);
    }

    public function updateStatus($id, Request $request){
        $status = $request->input('status');

        $up = Order::where('order_id', '=', $id)->first();

        if($request->has('resi') && !empty($request->input('resi'))) {
            $up->order_shipment_resi = $request->input('resi');
        }

        $up->order_status               = $status;
        $up->{'order_'.$status.'_date'} = date('Y-m-d H:i:s');

        if($up->save()) {
            if($status == 'finish') {
                $cus = Customer::find($up->customer_id);
                if($cus->customer_status == 'new') {
                    $dataAtasan = NULL;
                    $cekCus = Customer::count();            
                    if ($cekCus >= 7) {
                        $dataAtasan = Customer::where('customer_count', '>=', 0)
                            ->where('customer_count', '<', 7)
                            ->orderBy('customer_id', 'asc')
                            ->orderByRaw('RAND()')
                            ->first();
                    }

                    $cus->customer_reg_id = ($dataAtasan == NULL ? 0 : $dataAtasan->customer_id);

                    if (isset($dataAtasan)) {
                        if ($dataAtasan->customer_path_reg == NULL) {
                            $path = array();
                            array_push($path, $dataAtasan->customer_id);
                            $cus->customer_path_reg = json_encode($path);
                            if($cus->save()) {
                                $dataAtasan->customer_count +=1;
                                $dataAtasan->save();
                            }
                        } else {
                            $dataPath = json_decode($dataAtasan->customer_path_reg);
                            array_push($dataPath, $dataAtasan->customer_id);
                                foreach ($dataPath as $value) {
                                    $dataPathUpdate = Customer::find($value);
                                    $dataPathUpdate->customer_count +=1;
                                    $dataPathUpdate->save();
                                }

                            $cus->customer_path_reg = json_encode($dataPath);
                            $cus->save();

                        }

                    }
                    $cus->customer_status = 'active';
                    $cus->save();
                }
            }
        }

        return redirect()->back();
    }

    public function updateDriver(Request $request) {
        $o_detail_id    = $request->o_detail_id;
        $driver_id      = $request->driver_id;

        $up = OrderDetail::where('o_detail_id', '=', $o_detail_id)->first();

        $up->driver_id                  = $driver_id;
        $up->o_detail_status            = 'process';
        $up->o_detail_process_date      = date('Y-m-d H:i:s');
        $up->o_detail_process_note      = 'Pesanan sedang diproses';

        if($up->save()) {
            return response([
                'success' => true,
                'message' => 'Driver berhasil dipilih',
            ]);
        }
        else {
            return response([
                'success' => false,
                'message' => 'Driver gagal dipilih'
            ]);
        }
    }

    public function pilihMember(Request $request) {
        $src = $request->input('q');
        $data = Customer::selectRaw("
        customer_id as id,
        customer_name as text
        ")
        ->where('customer_name', 'like', '%'.$src.'%')
        ->orWhere('customer_email', 'like', '%'.$src.'%')
        ->orderBy('customer_name', 'ASC')
        ->paginate(10);

        return response($data);
    }

    public function setType_paket(Request $request) {
        $src = $request->input('q');
        $data = Paket::selectRaw("
        paket_id as id,
        paket_name as text
        ")
        ->where('paket_name', 'like', '%'.$src.'%')
        ->orderBy('paket_name')
        ->paginate(10);

        return response($data);
    }

    public function addOrder(Request $request) {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.order')->withErrors($validator->messages()->first());
        } else {
            $add = new Order();
            $cus = Customer::with('bank')->where('customer_id', '=', $request->input('customer_id'))->first();
            $status = $request->input('status');
            $paket_id = $request->input('paket_id');
            if($status == 'eceran') {
                $temp_product_array = $request->input('product_id');
                $temp_total_array = $request->input('total');
                $temp_qty_array = $request->input('qty');
                $total = 0;
                foreach($temp_total_array as $tot) {
                    $removeStringTot = str_replace('.', '', $tot);
                    $total += $removeStringTot;
                }
            } else {
                $pakets = PaketDetail::where('paket_id', '=', $paket_id)->get();
            }         

            $add->order_number                  = $this->generate_code();
            $add->customer_id                   = $cus->customer_id;
            $add->order_total                   = ($status == 'paket' ? '125000' : $total);
            $add->order_bank_from               = $cus->bank->s_bank_name;
            $add->order_bank_from_account       = $cus->bank->c_bank_nasabah;
            $add->order_bank_from_number        = $cus->bank->c_bank_rek;
            $add->order_bank_from_branch        = $cus->bank->c_bank_cabang;
            $add->order_status                  = ($status == 'paket' ? 'pending_payment' : 'paid');
            $add->order_pending_payment_date    = date('Y-m-d H:i:s');
            $add->order_create_date             = date('Y-m-d H:i:s');

            if ($add->save()) {      
                if ($status == 'eceran' && count($temp_product_array) > 0) {
                    foreach ($temp_product_array as $key => $prod) {
                        $oDetail = new OrderDetail();
                        $getProduct = Product::where('product_id', '=', $prod)->first();
                        $oDetail->product_id  = $getProduct->product_id;
                        $oDetail->paket_id    = '';
                        $oDetail->order_id    = $add->order_id;
                        $oDetail->od_type     = $status;
                        $oDetail->o_detail_name     = $getProduct->product_name;
                        $oDetail->o_detail_price    = $getProduct->product_price;
                        $oDetail->o_detail_qty      = $temp_qty_array[$key];
                        if($oDetail->save()) {
                            $getProduct->product_stock -= $temp_qty_array[$key];
                            $getProduct->save();
                        }
                        
                    }
                } 
                elseif ($status == 'paket' && count($pakets) > 0) {
                    foreach ($pakets as $val) {
                        $oDetail = new OrderDetail();
                        $paket = Product::find($val->product_id);
                        $oDetail->product_id  = $paket->product_id;
                        $oDetail->paket_id    = $request->input('paket_id');
                        $oDetail->order_id    = $add->order_id;
                        $oDetail->od_type     = $status;
                        $oDetail->o_detail_name     = $paket->product_name;
                        $oDetail->o_detail_price    = $paket->product_price;
                        $oDetail->o_detail_qty      = 1;
                        if($oDetail->save()) {
                            $paket->product_stock -= 1;
                            $paket->save();
                        }
                    }
                }
                
                return  redirect()->route('admin.order')->with('success', 'Tambah Transaksi Berhasil');
                
            }
            
        }   
    }

    function generate_code() {
        $first  = 'UKI';
        $day    = substr(date('D'),0, 1);
        $month  = substr(date('F'),0, 1);
        $year   = substr(date('Y'),-1);

        do {
            $code = $first.$month.$day.$year.rand(10000000, 99999999);

            $count = Order::where('order_number', '=', $code)->count();
        }
        while($count > 0);

        return $code;
    }

    public function addRow(Request $request) {
        $dataCount = $request->input('count');
        $data = Product::selectRaw("
        product_id as id,
        product_name as text,
        product_price as harga,
        product_stock as stok
        ")
        ->where('product_status', '=', 'publish')
        ->orderBy('product_name')
        ->get();

        $row = '
        <tr>
            <td>
                <select name="product_id[]" class="form-control selectpicker-pro-'.$dataCount.'"  data-live-search="true" data-size="10" style="width:100%;">
                    <option value="">Pilih Produk</option>';
                    foreach ($data as $val) :
                        $row .='<option value="'.$val->id.'" 
                            data-subtext="'.$val->text.'"
                            data-harga="'.$val->harga.'"
                            data-stok="'.$val->stok.'"
                            >'.$val->text.'</option>';
                    endforeach; 
                    $row .='</select>
            </td>
            <td>Rp <label class="harga-'.$dataCount.'" id="harga-'.$dataCount.'">0</label></td>
            <td width="100"><input class="form-control" id="qty-'.$dataCount.'" type="number" name="qty[]" onkeyup="qtyFunction('.$dataCount.')" onchange="qtyFunction('.$dataCount.')" disabled></td>
            <td><label class="stok-'.$dataCount.'" id="stok-'.$dataCount.'">0</label></td>
            <td width="120">Rp <input class="form-control" id="total-'.$dataCount.'" type="text" name="total[]" readonly></td>
            <td><button type="button" onclick="if(confirm(\'Yakin menghapus item?\')){$(this).closest(\'tr\').remove();}" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Click To Remove"><i class="mdi mdi-delete h5 text-error"></i></button></td>
            </tr>
        <tr>
        ';
        return response($row);
    }

}
