<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmailBroadcast;
use App\Models\Content;
use App\Models\ContentCategory;
use App\Http\Requests\Admin\ContentRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\Lib\ImageController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use File;

class ContentController extends Controller
{
    public function index(Request $request){
    	$content    = Content::orderBy('content_id','=','desc');

        if($request->has('status') && !empty($request->input('status')))
            $content->where('content_status', '=', $request->input('status'));

        if($request->has('cat') && !empty($request->input('cat')))
            $content->where('c_cat_id','=', $request->input('cat'));

        if($request->has('keyword') && !empty($request->input('keyword')))
            $content->where('content_name','like', '%'. $request->input('keyword'). '%');

        $ContentCat = ContentCategory::where('c_cat_parent', '=', '0')->pluck('c_cat_name','c_cat_id')->all();

    	return view('backend.content.index', [
    	    'content' => $content->paginate(15),
            'dataCat' => $ContentCat
        ]);
    }

    public function create(Request $request)
    {
    	$ContentCat = ContentCategory::where('c_cat_parent', '=', '0')->pluck('c_cat_name','c_cat_id')->all();
    	return view('backend.content.create', ['category' => $ContentCat]);
    }

    public function store(ContentRequest $request){
            $content = new Content();
            $content->c_cat_id = $request->input('c_cat_id');
            $content->user_id  = Auth::id();
            $content->content_name  = $request->input('content_name');
            $content->content_type  = $request->input('content_type');
            

            if($request->input('content_image') != null) {
                $contentImage =$request->input('content_image');
                $filename   = "content-".generate_alias($content->content_name);
                $path       = public_path().'/images/content/';
                $Image      = new ImageController();
                $fileUpload = $Image->filepond_upload($contentImage, $filename, $path);

                $content->content_image = $fileUpload->filename;
            }

            $content->content_alias  = generate_alias($content->content_name);
            $content->content_sortdesc  = $request->input('content_sortdesc');
            $content->content_desc  = $request->input('content_desc');
            $content->content_tags  = $request->input('content_tags');
            $content->content_status  = $request->input('content_status');
            $content->content_create_date  = date('Y-m-d H:i:s');
            if($content->content_status = 'publish'){
                $content->content_publish_date = date('Y-m-d H:i:s');
            }

            $content->save();
            return  redirect()->route('admin.content')->with('success', 'Data Content Berhasil Dibuat');
    }

    public function edit($id){
        $content = Content::find($id);
        $ContentCat = ContentCategory::where('c_cat_parent', '=', '0')->pluck('c_cat_name','c_cat_id')->all();
        return view('backend.content.edit', ['content' => $content,'category' => $ContentCat, 'id' => $id]);
    }

    public function update(ContentRequest $request, $id){
        $content = Content::find($id);

        if($request->input('c_cat_id') != null ){
            $content->c_cat_id = $request->input('c_cat_id');
        }
            $content->user_id  = Auth::id();
            $content->content_name  = $request->input('content_name');
            $content->content_type  = $request->input('content_type');
            $content->content_alias  = generate_alias($request->input('content_name'));

            if($request->input('content_image') != null) {
                $deleteImage = File::delete(public_path().'/images/content/'.$content->content_image);
                $contentImage =$request->input('content_image');
                $filename   = "content-".generate_alias($content->content_name);
                $path       = public_path().'/images/content/';
                $Image      = new ImageController();
                $fileUpload = $Image->filepond_upload($contentImage, $filename, $path);

                $content->content_image = $fileUpload->filename;
            }

            $content->content_sortdesc  = $request->input('content_sortdesc');
            $content->content_desc      = $request->input('content_desc');
            $content->content_tags      = $request->input('content_tags');
            $content->content_status    = $request->input('content_status');
            $content->content_create_date  = date('Y-m-d H:i:s');
            if($content->content_status == 'publish'){
                $content->content_publish_date = date('Y-m-d H:i:s');
            }

            if ($content->save()) {
                return redirect()->route('admin.content')->with('success', 'Data Berhasil Di Ubah');
            }
    }

    public function detail($id){
        $content = Content::find($id);

        return view('backend.content.detail', ['content' => $content]);
    }

    public function destroy($id){
        $content = Content::find($id);
        $content->delete();

        return redirect()->route('admin.content')->with('success', 'Data Produk berhasil dihapuss');
    }

    public function getSubCategory($c_cat_parent)
    {
        $productCat = ContentCategory::where('c_cat_parent',$c_cat_parent)->get();
        return response([
            'data' => $productCat
        ]);
    }

    public function getContentCat($c_cat_parent)
    {
        $productCat = ContentCategory::where('c_cat_parent',$c_cat_parent)->get();
        return response([
            'data' => $productCat
        ]);
    }

    public function broadcastEmail(Request $request) {
        if($request->has('sendBroadcast')) {
            $email = new EmailBroadcast;
            $validator = Validator::make($request->all(), [
                'subject'       => 'required',
                'deskripsi'     => 'required'
            ]);

            if ($validator->fails()) {
                return redirect()->route('admin.content.broadcast')->withErrors($validator->messages()->first());
            }
            else {
                $email->user_id         = Auth::id();
                $email->eb_subject      = $request->input('subject');
                $email->eb_desc         = $request->input('deskripsi');
                $email->eb_create_date  = date('Y-m-d H:i:s');

                if ($email->save()) {
                    $customer = Customer::where('customer_status', '=', 'active')->get();
                    $subject = $email->eb_subject;
                    $varMail = array(
                        'desc'      => $email->eb_desc
                    );
                    foreach ($customer as $item) {
                        $emailCus = $item->customer_email;
                        Mail::send('emails.broadcast', $varMail, function($message) use ($emailCus, $subject) {
                            $message->to($emailCus)->subject($subject);
                        });
                    }
                    return  redirect()->route('admin.content.broadcast')->with('success', 'Email berhasil di kirim');
                }
            }
        }
        $data = EmailBroadcast::with('user')->orderBy('eb_id', 'DESC')->get();
        return view('backend.content.broadcast', ['dataTerkirim' => $data]);
    }
}
