<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 1/30/2019
 * Time: 7:42 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContentCategory;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Lib\ImageController;
use File;

class CatContentController extends Controller
{
    public function index() {
        $data = ContentCategory::orderBy('c_cat_id', 'DESC')->get();
        return view('backend.catContent.index', ['dataCat' => $data]);
    }

    public function catAdd(Request $request) {
        $cat = new ContentCategory();
        $validator = Validator::make($request->all(), [
            'nama_kategori'       => 'required',
            'deskripsi_kategori'  => 'required',
            'status_kategori'     => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.content.cat')->withErrors($validator->messages()->first());
        }
        else {
            $cat->c_cat_name          = $request->input('nama_kategori');
            $cat->c_cat_alias         = generate_alias($request->input('nama_kategori'));
            $cat->c_cat_desc          = $request->input('deskripsi_kategori');
            $cat->c_cat_status        = $request->input('status_kategori');

            $cat->save();
            if(!empty($request->input('gambar_kategori'))) {
                $fileImage  = $request->input('gambar_kategori');
                $filename   = "cat-content-".$cat->c_cat_id;
                $path       = public_path().'/images/cat_content/';
                $Image      = new ImageController();
                $fileUpload = $Image->filepond_upload($fileImage, $filename, $path);
                $cat->c_cat_image  = $fileUpload->filename;
            }

            if ($cat->save()) {
                return redirect()->route('admin.content.cat')->with('success', 'Data Kategori Konten Berhasil Ditambahkan');
            }
        }
    }

    public function catEdit($id, Request $request) {
        $cat = ContentCategory::find($id);
        $validator = Validator::make($request->all(), [
            'nama_kategori'       => 'required',
            'deskripsi_kategori'  => 'required',
            'status_kategori'     => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.content.cat')->withErrors($validator->messages()->first());
        }
        else {
            $cat->c_cat_name          = $request->input('nama_kategori');
            $cat->c_cat_alias         = generate_alias($request->input('nama_kategori'));
            $cat->c_cat_desc          = $request->input('deskripsi_kategori');
            $cat->c_cat_status        = $request->input('status_kategori');

            if(!empty($request->input('gambar_kategori'))) {
                $path                   = public_path().'/images/cat_content/';
                $fileImage              = $request->input('gambar_kategori');
                $deleteImage            = File::delete($path.$cat->c_cat_id);
                $filename               = "content-".$cat->c_cat_id;
                $Image                  = new ImageController();
                $fileUpload             = $Image->filepond_upload($fileImage, $filename, $path);
                $cat->c_cat_image  = $fileUpload->filename;
            }

            if ($cat->save()) {
                return redirect()->route('admin.content.cat')->with('success', 'Data Kategori Konten Berhasil Ditambahkan');
            }
        }
    }

    public function destroy($id) {
        $del = ContentCategory::where('c_cat_id', '=', $id)->delete();
        if ($del) {
            return redirect()->route('admin.content.cat')->with('success', 'Hapus Kategori Konten Berhasil');
        }
    }
}