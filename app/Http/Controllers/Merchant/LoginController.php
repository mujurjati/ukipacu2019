<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 5/22/2019
 * Time: 8:52 AM
 */

namespace App\Http\Controllers\Merchant;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function guard()
    {
        return Auth::guard('merchant');
    }

    public function index()
    {
        return view('merchant.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'login_email' => 'required|email',
            'login_password' => 'required'
        ]);

        if (Auth::guard('merchant')->attempt(['merchant_email' => $request->login_email, 'password' => $request->login_password, 'merchant_status' => 'active'])) {
            return redirect()->route('merchant.home');
        }

        return redirect()->back()->with('failed', 'Periksa kembali email dan password anda, terjadi kesalahan!');
    }

    public function logoutMerchant()
    {
        Auth::guard('merchant')->logout();
        return redirect()->route('merchant.login');
    }
}