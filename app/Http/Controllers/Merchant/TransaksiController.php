<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 5/27/2019
 * Time: 10:21 PM
 */

namespace App\Http\Controllers\Merchant;

use App\Models\Order;
use App\Http\Controllers\Controller;

class TransaksiController extends Controller
{
    public function index($status) {
        $merchantId = auth('merchant')->user()->merchant_id;
        $query = Order::with('customer')
            ->with(['detail' => function ($q) use($merchantId) {
                $q->with('driver')
                ->where('merchant_id', '=', $merchantId);
            }])
            ->whereNotIn('order_status', ['pending_payment', 'paid', 'cancel'])
            ->has('detail')
            ->whereHas('detail', function ($d) use($status, $merchantId) {
                $d->where('o_detail_status', '=', $status)
                ->where('merchant_id', '=', $merchantId);
            })
            ->orderBy('order_shipment_date', 'DESC')
            ->get();

        return view('merchant.transaksi.index', [
            'status' => $status,
            'dataTrx' => $query
        ]);
    }

    public function detail($id, $st) {
        $status = $st;
        $merchantId = auth('merchant')->user()->merchant_id;
        $data = Order::with(['customer' => function ($q) use($merchantId) {
                $q->with('address_primary');
            }])
            ->with(['detail' => function ($q) use($merchantId) {
                $q->with('driver')
                    ->with('detail_product')
                    ->with('merchant')
                    ->where('merchant_id', '=', $merchantId);
            }])
            ->where('order_id', '=', $id)
            ->has('detail')
            ->orderBy('order_shipment_date', 'DESC')
            ->first();


        return view('merchant.transaksi.detail', [
            'status' => $status,
            'detailTrx' => $data
        ]);
    }
}