<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 5/27/2019
 * Time: 10:21 PM
 */

namespace App\Http\Controllers\Merchant;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerBank;
use App\Models\CustomerPenunjang;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index() {
        $data = Customer::with('cusAtasan')
            ->orderBy('customer_id', 'desc')
            ->get();

        return view('merchant.customer.index', ['data' => $data]);
    }

    public function add(Request $request) {
        if($request->has('addCusByMer')){
            $cus = new Customer();

            $this->validate($request, [
                'customer_name'          => 'required',
                'customer_email'         => 'required',
                'customer_tlg'          => 'required',
                'customer_no_ktp'      => 'required|unique:_customer,customer_nik',
                'customer_address'       => 'required',
                'city'                  => 'required',
                'customer_password'      => 'required',
                'customer_ibu' => 'required',
                'customer_bank_nasabah' => 'required',
                'customer_bank_rek' => 'required',
            ]);

            $ceknik = Customer::where('customer_nik', '=', $request->input('customer_no_ktp'))->get();

            if(count($ceknik) > 0) {
                return redirect()->route('merchant.customer.add')->with('error', 'NIK member sudah terdaftar atau akun member diblok');
            } else {

                $cus->customer_name = $request->input('customer_name');
                $cus->customer_reg_id = 0;
                $cus->customer_count =0;
                $cus->merchant_id = 00;
                $cus->customer_email = $request->input('customer_email');
                $cus->customer_npwp = $request->input('customer_npwp');
                $cus->customer_nik = $request->input('customer_no_ktp');
                $cus->customer_city = $request->input('city');
                $cus->customer_phone = $request->input('customer_phone');
                $cus->customer_password = bcrypt($request->input('customer_password'));
                $cus->customer_address = $request->input('customer_address');
                $cus->customer_birth_date = $request->input('customer_tlg');
                $cus->customer_status = 'new';
                $cus->customer_create_date = Carbon::now();

                if ($cus->save()) {
                        $cus_pen = new CustomerPenunjang();

                        $cus_pen->customer_id = $cus->customer_id;
                        $cus_pen->c_pendidikan = $request->input('customer_pendidikan');
                        $cus_pen->c_pekerjaan = $request->input('customer_perkerjaan');
                        $cus_pen->c_agama = $request->input('customer_agama');
                        $cus_pen->c_ibu_kandung = $request->input('customer_ibu');
                        $cus_pen->c_ahli_waris = $request->input('customer_waris');
                        $cus_pen->c_hub_waris = $request->input('customer_hub_waris');

                        $cus_pen->save();

                        $cus_bank = new CustomerBank();

                        $cus_bank->customer_id = $cus->customer_id;
                        $cus_bank->s_bank_name = $request->input('customer_bank_name');
                        $cus_bank->c_bank_cabang = $request->input('customer_bank_cabang');
                        $cus_bank->c_bank_nasabah = $request->input('customer_bank_nasabah');
                        $cus_bank->c_bank_rek = $request->input('customer_bank_rek');

                        $cus_bank->save();

                    return redirect()->route('cus.list')->with('success', 'Data Member berhasil ditambahkan');
                }
            }

        }

//        $dataAtasan = Customer::where('customer_count', '<=', 7)->get();

        return view('merchant.customer.add');
    }

    public function ajax_getCustomer(Request $request) {
        $src = $request->input('q');

        $data =  Customer::selectRaw("
                customer_id as id,
                customer_name as text")
                ->where('customer_count', '<=', 7)
                ->where('customer_name', 'like', '%'.$src.'%')
                ->paginate(7);

        return response($data);
    }

    public function detail($id) {
        $detail = Customer::with('cusAtasan')
            ->with('bank')
            ->with('penunjang')
            ->where('customer_id', '=', $id)->first();

        return view('merchant.customer.detail', ['detail' => $detail]);
    }
}
