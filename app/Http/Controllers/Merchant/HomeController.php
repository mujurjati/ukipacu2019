<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 5/22/2019
 * Time: 9:36 AM
 */

namespace App\Http\Controllers\Merchant;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
use App\Models\OrderDetail;

class HomeController extends Controller
{
    public function index()
    {
        // $merchantID = auth('merchant')->user()->merchant_id;
        // $dataNew = Order::with(['detail' => function ($q) use ($merchantID) {
        //     $q->where('merchant_id', '=', $merchantID);
        // }])
        //     ->whereNotIn('order_status', ['pending_payment', 'paid', 'cancel'])
        //     ->has('detail')
        //     ->whereHas('detail', function ($d) use ($merchantID) {
        //         $d->where('o_detail_status', '=', 'new')
        //             ->where('merchant_id', '=', $merchantID);
        //     })->count();

        // $dataProcess = Order::with(['detail' => function ($q) use ($merchantID) {
        //     $q->where('merchant_id', '=', $merchantID);
        // }])
        //     ->whereNotIn('order_status', ['pending_payment', 'paid', 'cancel'])
        //     ->has('detail')
        //     ->whereHas('detail', function ($d) use ($merchantID) {
        //         $d->whereIn('o_detail_status', ['process', 'pickup', 'shipment'])
        //             ->where('merchant_id', '=', $merchantID);
        //     })->count();

        // $dataCancel = Order::with(['detail' => function ($q) use ($merchantID) {
        //     $q->where('merchant_id', '=', $merchantID);
        // }])
        //     ->whereNotIn('order_status', ['pending_payment', 'paid', 'cancel'])
        //     ->has('detail')
        //     ->whereHas('detail', function ($d) use ($merchantID) {
        //         $d->whereIn('o_detail_status', ['cancel', 'refund'])
        //             ->where('merchant_id', '=', $merchantID);
        //     })->count();

        // $dataFinish = Order::with(['detail' => function ($q) use ($merchantID) {
        //     $q->where('merchant_id', '=', $merchantID);
        // }])
        //     ->whereNotIn('order_status', ['pending_payment', 'paid', 'cancel'])
        //     ->has('detail')
        //     ->whereHas('detail', function ($d) use ($merchantID) {
        //         $d->where('o_detail_status', '=', 'finish')
        //             ->where('merchant_id', '=', $merchantID);
        //     })->count();


        return view('merchant.dashboard', [
            'new' => 10,
            'process' => 10,
            'cancel' => 10,
            'finish' => 10
        ]);
    }
}