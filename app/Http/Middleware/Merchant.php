<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 5/27/2019
 * Time: 11:06 PM
 */

namespace App\Http\Middleware;
use Closure;
use Auth;

class Merchant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('merchant')->check() && Auth::guard('merchant')->user()->merchant_id > 0) {
            return $next($request);
        }else{
            return redirect()->route('merchant.login');
        }
    }
}