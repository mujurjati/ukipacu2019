<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Member
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('member')->check() && Auth::guard('member')->user()->customer_id > 0) {
            return $next($request);
        }else{
            return redirect()->route('customers.login');
        }
    }
}
