<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Office
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('web')->check() && Auth::guard('web')->user()->u_level_id == '4') {
            return $next($request);
        }else{
            return redirect()->route('admin.home');
        }
    }
}
