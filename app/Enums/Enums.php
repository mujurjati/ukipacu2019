<?php

namespace App\Enums;

use IsnanIas\Enum\Enum;

class Enums extends Enum
{
	public static function userLevel()
    {
        $level = [
            '1' => 'Administrator',
            '2' => 'Customer Service',
            '3' => 'Finance',
        ];

        return $level;
    }

    public static function generalBollean()
    {
    	$data = [
    		'0' => 'Tidak',
    		'2' => 'Ya'
    	];

    	return $data;
    }

    public static function productStatus()
    {
    	$data = ['publish' => 'Publish','draft' => 'Draft'];

    	return $data;
    }

    public static function contentStatus()
    {
        $data = ['publish' => 'Publish','draft' => 'Draft'];
        
        return $data;
    }

    public static function ebookType()
    {
        $data = ['ganjil' => 'Ganjil','genap' => 'Genap','full' => 'Full'];
        return $data;
    }

    public static function ebookProType()
    {
        $data = ['ganjil' => 'Ganjil','genap' => 'Genap'];
        return $data;
    }

    public static function subscribeStatus()
    {
        $data = ['pending' => 'Pending','success' => 'Success','cancel' => 'Cancel','block' => 'Block'];
        return $data;
    }

    public static function orderStatus(){
        $data = ['pending_payment' => 'pending_payment','paid' => 'paid','process' => 'process','ready_shipment' => 'ready_shipment', 'shipping_process' => 'shipping_process','received' => 'received','complaint' => 'complaint','finish' => 'finish','cancel' => 'cancel','refund' => 'refund'];
        return $data;
    }

    public static function promoType(){
        $data = ['price' => 'Price','percent' => 'Percent'];
        return $data;
    }

    public static function contentType(){
        $data = ['blog' => 'Blog','slider' => 'Slider'];
        return $data;
    }
}