<?php

namespace App\Mail;

use App\Models\Customer;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerActivation extends Mailable
{
    use Queueable, SerializesModels;

    private $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $encryptedEmail = Crypt::encrypt($this->customer->customer_email);

        // ex: domain.com/verify?token=xxxx
        $link = route('customers.activation', ['token' => $encryptedEmail]);

        /*return $this->subject('Verify Your Email Address')
            ->with('link', $link)
            ->view('emails/activation');*/
        return $this->markdown('emails/activation', ['link' => $link]);
    }
}
