<?php

function generalData() {
    $option = \Setting::where('setting_id', '>', '0');
    return $option->value;
}

function asset_url($path, $secure = null)
{
    if(request()->server->get('SERVER_PORT') == '8000')
        return asset($path, $secure);
    else
        return asset($path, $secure);
}

function price_format($number = 0) {
    $result = number_format($number,0,',','.');

    return $result;
}

function statusTrx($status) {
    $statusSendToView = '';
    if ($status == 'new') {
        $statusSendToView = 'Baru';
    }
    if ($status == 'process') {
        $statusSendToView = 'Proses';
    }
    if ($status == 'pickup') {
        $statusSendToView = 'Proses Pickup';
    }
    if ($status == 'shipment') {
        $statusSendToView = 'Proses Pengiriman';
    }
    if ($status == 'cancel') {
        $statusSendToView = 'Pesanan Dibatalkan';
    }
    if ($status == 'refund') {
        $statusSendToView = 'Pesanan Dikembalikan';
    }

    echo $statusSendToView;
}

function label_status($status){
    $label = "";
    $status = strtolower($status);
    $statusName = $status;
    if($status=="0")
        $statusName = "Non-Active";
    if($status=="1")
        $statusName = "Active";
    if($status=="new")
        $label = '<span class="badge badge-info">'.ucwords($statusName).'</span>';
    if($status=="publish" || $status=="active" || $status=="1" || $status=="approved")
        $label = '<span class="badge badge-success">'.ucwords($statusName).'</span>';
    if($status=="non-active" || $status=="0" || $status=="block" || $status=="reject")
        $label = '<span class="badge badge-danger">'.ucwords($statusName).'</span>';
    if($status=="draft" || $status=="pending")
        $label = '<span class="badge badge-warning">'.ucwords($statusName).'</span>';

    echo $label;
}

function jenis_kelamin($status = ''){
    $label = "";

    $status = strtolower($status);

    if($status=="m")
        $label = "Laki-laki";
    if($status=="f")
        $label = "Perempuan";

    echo $label;
}
function generate_code(){
    $pattern0 = ((date('Y')-2018)*12) + (intval(date('m'))-5) + 1 ;
    $pattern1 = substr("0".(date('d')*24)+intval(date('H')),-3);
    $pattern2 = substr("00".((intval(date('i'))*60)+intval(date('s'))).substr(microtime(),2,4),-2);
    $code = $pattern0.$pattern1.$pattern2;
    return $code;
}

function get_image($val) {
    return url('/').'/images/'.$val;
}

function get_image_product($val) {
    return url('/').'/images/products/'.$val;
}

function get_image_ebook($val) {
    return url('/').'/images/ebook_package/'.$val;
}

function get_image_ebook_product($val) {
    return url('/').'/images/ebook_product/'.$val;
}

function get_image_bank($val) {
    return url('/').'/images/bank/'.$val;
}

function get_image_content($val) {
    return url('/').'/images/content/'.$val;
}

function get_image_all($val, $folder) {
    $image = '';
    if (empty($val)) {
        $image = '-';
    }
    else {
        $image = url('/').'/images/'.$folder.'/'.$val;
    }
    return $image;
}

function arrMonths($opt = "")
{
    if ($opt == "") {
        $arrMonths = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    } else if ($opt == "id") {
        $arrMonths = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    } else if ($opt == "shortId") {
        $arrMonths = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des");
    }elseif ($opt=='simple') {
        $arrMonths = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Ags", "Sep", "Oct", "Nov", "Dec");
    }
    return $arrMonths;
}

function date_indo($data,$format=""){
    if(empty($data))
        return '-';

    if(substr($data,0,10)=="0000-00-00"){
        $newDate = "-";
    }
    else{
        $newDate = "";
        $arrMonth = arrMonths("id");
        $day = substr($data,8,2);
        $month = $arrMonth[substr($data,5,2)-1];
        $year = substr($data,0,4);
        $hour = substr($data,11,2);
        $minute= substr($data,14,2);
        $second = substr($data,17,2);

        $newDate = $day." ".substr($month,0,3)." ".$year;
        if($format=="dd FF YYYY"){
            $newDate = $day." ".$month." ".$year;
        }
        elseif($format=="datetime"){
            $newDate = $day." ".substr($month,0,3)." ".$year." ".$hour.":".$minute.":".$second;
        }
    }
    return $newDate;
}

function profit($val) {
    $data = 0;
    if ($val >= 32) {
        $data = rupiah(480000);
    }
    if ($val >= 64) {
        $data = rupiah(960000);
    }
    if ($val >= 128) {
        $data = rupiah(1280000);
    }
    if ($val >= 256) {
        $data = rupiah(2560000);
    }
    if ($val >= 512) {
        $data = rupiah(3840000);
    }
    if ($val >= 1024) {
        $data = rupiah(7860000);
    }
    if ($val >= 2019) {
        $data = rupiah(10095000);
    }

    return "Rp " .$data;
}

function rupiah($angka){

    $hasil_rupiah =  number_format($angka,2,',','.');
    return $hasil_rupiah;

}

function label_status_ebook($status){
    $label = "";
    if($status=="pending") {
        $statusName = "Menunggu Pembayaran";
        $label = '<span class="badge badge-info">'.$statusName.'</span>';
    }
    if($status=="success") {
        $statusName = "Terbayar";
        $label = '<span class="badge badge-success">'.$statusName.'</span>';
    }
    if($status=="cancel") {
        $statusName = "Cancel";
        $label = '<span class="badge badge-danger">'.$statusName.'</span>';
    }
    if($status=="block") {
        $statusName = "Block";
        $label = '<span class="badge badge-danger">'.$statusName.'</span>';
    }
    echo $label;
}

function order_status($status) {
    switch($status) {
        case 'pending_payment' :
            return 'Menunggu Pembayaran';
            break;
        case 'paid' :
            return 'Terbayar';
            break;
        case 'process' :
            return 'Sedang Diproses';
            break;
        case 'ready_shipment':
            return 'Menunggu Pengiriman';
            break;
        case 'shipping_process' :
            return 'Dalam Pengiriman';
            break;
        case 'received' :
            return 'Terkirim';
            break;
        case 'complaint' :
            return 'Proses Komplain';
            break;
        case 'finish' :
            return 'Selesai';
            break;
        case 'cancel' :
            return 'Sudah Dibatalkan';
            break;
        case 'refund' :
            return 'Sudah Dibatalkan (Refund)';
            break;
    }
}

function order_status_label($status) {
    switch($status) {
        case 'belum_trx' :
            return '<span class="badge badge-danger">Belum Transaksi</span>';
            break;
        case 'pending_payment' :
            return '<span class="badge badge-purple">Menunggu Pembayaran</span>';
            break;
        case 'new' :
            return '<span class="badge badge-info">Pesanan Baru</span>';
            break;
        case 'pickup' :
            return '<span class="badge badge-dark">Diambil Driver</span>';
            break;
        case 'shipment' :
            return '<span class="badge badge-warning">Dalam Pengiriman</span>';
            break;
        case 'paid' :
            return '<span class="badge badge-info">Terbayar</span>';
            break;
        case 'process' :
            return '<span class="badge badge-warning">Sedang Diproses</span>';
            break;
        case 'received' :
            return '<span class="badge badge-secondary">Terkirim</span>';
            break;
        case 'complaint' :
            return '<span class="badge badge-primary">Proses Komplain</span>';
            break;
        case 'finish' :
            return '<span class="badge badge-success">Selesai</span>';
            break;
        case 'cancel' :
            return '<span class="badge badge-danger">Sudah Dibatalkan</span>';
            break;
        case 'refund' :
            return '<span class="badge badge-danger">Sudah Dibatalkan (Refund)</span>';
            break;
    }
}

function generate_alias($str)
{
    setlocale(LC_ALL, 'en_US.UTF8');
    $plink = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $plink = str_replace(" &amp; ", " ", $plink);
    $plink = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $plink);
    $plink = strtolower(trim($plink, '-'));
    $plink = preg_replace("/[\/_| -]+/", '-', $plink);
    return $plink;
}

function rating($x) {
    $label = "";
    for ($y=0; $y<$x; $y++) {
            $label .= '<i class="icon-star3"></i>';
    }

    for ($j=$x; $j<=4; $j++) {
        $label .= '<i class="icon-star-empty"></i>';
    }


    echo $label;
}
