<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:34
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps      = false;
    protected $table        = '_order';
    protected $primaryKey   = 'order_id';

    public function customer() {
        return $this->hasOne('App\Models\Customer', 'customer_id', 'customer_id');
    }

    public function promo_used() {
        return $this->hasOne('App\Models\PromoUsed', 'order_id', 'order_id');
    }

    public function detail() {
        return $this->hasMany('App\Models\OrderDetail', 'order_id', 'order_id');
    }

    public function detailIndex() {
        return $this->hasOne('App\Models\OrderDetail', 'order_id', 'order_id');
    }
/*    public function merchant() {
        return $this->hasMany('App\Models\Merchant', 'merchant_id', 'merchant_id');
    }*/
    /*public function driver() {
        return $this->hasMany('App\Models\Driver', 'driver_id', 'driver_id');
    }*/
}