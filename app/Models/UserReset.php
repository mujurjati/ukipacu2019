<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:43
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserReset extends Model
{
    public $timestamps      = false;
    protected $table        = '_user_reset';
    protected $primaryKey   = 'u_reset_id';

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}