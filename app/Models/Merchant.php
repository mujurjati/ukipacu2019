<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 12/18/2018
 * Time: 11:02 PM
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property string $api_token
 * @property string $customer_password
 */
class Merchant extends Authenticatable
{
    use Notifiable;
    protected $guard = 'merchant';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    protected $table = '_merchant';
    protected $primaryKey = 'merchant_id';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $fillable = [
        'merchant_reg_id',
        'merchant_name',
        'merchant_email',
        'merchant_phone',
        'merchant_password',
        'merchant_image',
        'merchant_birth_place',
        'merchant_birth_date',
        'merchant_gender',
        'merchant_status',
        'merchant_create_date',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'merchant_password', 'api_token',
    ];

    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // do nothing
    }

    public function getAuthPassword() {
        return $this->merchant_password;
    }

    public function address() {
        return $this->hasMany('App\Models\CustomerAddress', 'customer_id', 'customer_id');
    }

    public function address_primary() {
        return $this->hasOne('App\Models\CustomerAddress', 'customer_id', 'customer_id')
            ->where('c_address_primary', '=', '1');
    }
    public function product() {
        return $this->belongsTo('App\Models\Product', 'merchant_id', 'merchant_id');
    }

}



