<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 2/21/2019
 * Time: 1:18 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EmailBroadcast extends Model
{
    public $timestamps = false;
    protected $table = '_email_broadcast';
    protected $primaryKey = 'eb_id';

    public function user() {
        return $this->hasOne('App\Models\User', 'user_id', 'user_id');
    }
}