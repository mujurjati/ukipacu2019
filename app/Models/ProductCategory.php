<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:36
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductCategory extends Model
{
    public $timestamps      = false;
    protected $table        = '_product_category';
    protected $primaryKey   = 'p_cat_id';

    public function children() {
        return $this->hasMany(self::class, 'p_cat_parent', 'p_cat_id')
            ->where('p_cat_status', '=', 'active');
    }

    public function grandchildren()
    {
        return $this->children()->with('grandchildren');
    }

    public function parent() {
        return $this->hasOne(self::class, 'p_cat_id', 'p_cat_parent');
    }

    public function attribute() {
        return $this->hasMany('App\Models\ProductAttribute', 'p_cat_id', 'p_cat_id');
    }

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }

    public function productCount() {
        return DB::table('_product_category')
            ->select(DB::raw('_product_category.p_cat_name, _product_category.p_cat_image, COUNT(product_id) as jlh'))
            ->join('_product', '_product_category.p_cat_id', '=', '_product.p_cat_id')
            ->where('p_cat_status', '=', 'active')
            ->where('p_cat_level', '=', '1')
            ->groupBy('_product_category.p_cat_id')
            ->orderBy('p_cat_order')
            ->limit('4')
            ->get();
    }
}