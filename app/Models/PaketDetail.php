<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PaketDetail extends Model
{
    public $timestamps      = false;
    protected $table        = '_paket_detail';
    protected $primaryKey   = 'pd_id';


    public function paketDetailProduk()
    {
        return $this->hasOne('App\Models\Product', 'product_id', 'product_id');
    }
    
}
