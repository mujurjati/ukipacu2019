<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 12/18/2018
 * Time: 11:02 PM
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property string $api_token
 * @property string $customer_password
 */
class Customer extends Authenticatable
{
    use Notifiable;
    protected $guard = 'member';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    protected $table = '_customer';
    protected $primaryKey = 'customer_id';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $fillable = [
        'customer_reg_id',
        'customer_name',
        'customer_email',
        'customer_phone',
        'customer_password',
        'customer_image',
        'customer_birth_place',
        'customer_birth_date',
        'customer_gender',
        'customer_status',
        'customer_create_date',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'customer_password', 'api_token',
    ];

    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // do nothing
    }

    public function getAuthPassword() {
        return $this->customer_password;
    }

    public function penunjang() {
        return $this->hasOne('App\Models\CustomerPenunjang', 'customer_id', 'customer_id');
    }

    public function address_primary() {
        return $this->hasOne('App\Models\CustomerAddress', 'customer_id', 'customer_id')
            ->where('c_address_primary', '=', '1');
    }

    public function bank() {
        return $this->hasOne('App\Models\CustomerBank', 'customer_id', 'customer_id');
    }

    public function deposit() {
        return $this->hasMany('App\Models\CustomerDeposit', 'customer_id', 'customer_id');
    }

    public function reset() {
        return $this->hasMany('App\Models\CustomerReset', 'customer_id', 'customer_id');
    }

    public function wishlist() {
        return $this->hasMany('App\Models\CustomerWishlist', 'customer_id', 'customer_id');
    }

    public function withdraw() {
        return $this->hasMany('App\Models\CustomerWithdraw', 'customer_id', 'customer_id');
    }

    public function promo_used() {
        return $this->hasMany('App\Models\PromoUsed', 'customer_id', 'customer_id');
    }

    public function rating() {
        return $this->hasMany('App\Models\ProductRating', 'customer_id', 'customer_id');
    }

    public function order() {
        return $this->belongsTo('App\Models\Order');
    }

    public function subscribe() {
        return $this->hasMany('App\Models\EbookSubscribe', 'customer_id', 'customer_id');
    }

    public function ratingCus() {
        return $this->belongsTo('App\Models\ProductRating');
    }

    public function cusAtasan()
    {
        return $this->hasOne('App\Models\Customer', 'customer_id', 'customer_reg_id');
    }
    public function cusAsli()
    {
        return $this->hasMany('App\Models\Customer', 'customer_reg_id', 'customer_id');

    }
}
