<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:31
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    public $timestamps      = false;
    protected $table        = '_customer_address';
    protected $primaryKey   = 'c_address_id';

    public function secondary() {
        return $this->hasMany(self::class, 'customer_id', 'customer_id')
            ->where('c_address_primary', '=', '0');
    }

    public function subdistrict() {
        return $this->hasOne('App\Models\ShippingSubdistrict', 's_subdistrict_id', 's_subdistrict_id');
    }

    public function customer() {
        $this->belongsTo('App\Models\Customer');
    }
}
