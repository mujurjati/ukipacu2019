<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 1/11/2019
 * Time: 10:53 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EbookSubscribe extends Model
{
    public $timestamps      = false;
    protected $table        = '_ebook_subscribe';
    protected $primaryKey   = 'e_subscribe_id';
    protected $dates = ['e_subscribe_expired_date'];

    public function package() {
        return $this->belongsTo('App\Models\EbookPackage', 'e_package_id', 'e_package_id');
    }

    public function customer() {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'customer_id');
    }
}