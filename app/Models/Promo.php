<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:38
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    public $timestamps      = false;
    protected $table        = '_promo';
    protected $primaryKey   = 'promo_id';

    public function promo_used() {
        $this->hasMany('App\Models\PromoUsed', 'promo_id', 'promo_id');
    }
}