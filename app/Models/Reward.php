<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    public $timestamps      = false;
    protected $table        = '_reward';
    protected $primaryKey   = 'reward_id';

    
}
