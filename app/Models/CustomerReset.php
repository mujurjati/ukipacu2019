<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:33
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerReset extends Model
{
    public $timestamps      = false;
    protected $table        = '_customer_reset';
    protected $primaryKey   = 'c_reset_id';

    public function customer() {
        return $this->belongsTo('App\Models\Customer');
    }
}