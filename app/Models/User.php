<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $table = '_user';
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'u_level_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
         // do nothing
    }

    public function level() {
        return $this->hasOne('App\Models\UserLevel', 'u_level_id', 'u_level_id');
    }

    public function subdistrict() {
        return $this->hasOne('App\Models\ShippingSubdistrict', 's_subdistrict_id', 's_subdistrict_id');
    }

    public function reset() {
        return $this->hasMany('App\Models\UserReset', 'user_id', 'user_id');
    }

    public function content() {
        return $this->belongsTo('App\Models\Content');
    }

    public function emailBroadcast() {
        return $this->belongsTo('App\Models\EmailBroadcast');
    }
}
