<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:37
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    public $timestamps      = false;
    protected $table        = '_product_stock';
    protected $primaryKey   = 'p_stock_id';

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}