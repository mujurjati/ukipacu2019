<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTracking extends Model
{
    public $timestamps      = false;
    protected $table        = '_order_tracking';
    protected $primaryKey   = 'o_tracking_id';

    public function order() {
        return $this->belongsTo('App\Models\Order');
    }
}