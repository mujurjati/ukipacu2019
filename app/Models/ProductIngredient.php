<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductIngredient extends Model
{
    public $timestamps      = false;
    protected $table        = '_product_ingredients';
    protected $primaryKey   = 'p_ingredients_id';
}