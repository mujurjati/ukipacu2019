<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:32
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerDeposit extends Model
{
    public $timestamps      = false;
    protected $table        = '_customer_deposit';
    protected $primaryKey   = 'c_deposit_id';

    public function customer() {
        return $this->belongsTo('App\Models\Customer');
    }
}