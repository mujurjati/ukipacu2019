<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 12/18/2018
 * Time: 11:02 PM
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property string $api_token
 * @property string $customer_password
 */
class Driver extends Authenticatable
{
    use Notifiable;
    protected $guard = 'driver-api';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    protected $table = '_driver';
    protected $primaryKey = 'driver_id';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $fillable = [
        'driver_reg_id',
        'driver_name',
        'driver_email',
        'driver_phone',
        'driver_password',
        'driver_image',
        'driver_birth_place',
        'driver_birth_date',
        'driver_gender',
        'driver_status',
        'driver_create_date',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'driver_password', 'api_token',
    ];

    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // do nothing
    }

    public function getAuthPassword() {
        return $this->driver_password;
    }

    public function order_detail() {
        return $this->belongsTo('App\Models\OrderDetail', 'driver_id', 'driver_id');
    }
}



