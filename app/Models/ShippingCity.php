<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:39
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingCity extends Model
{
    public $timestamps      = false;
    protected $table        = '_shipping_city';
    protected $primaryKey   = 's_city_id';

    public function subdistrict() {
        return $this->hasMany('App\Models\ShippingSubdistrict', 's_city_id', 's_city_id');
    }

    public function province() {
        return $this->belongsTo('App\Models\ShippingProvince', 's_province_id', 's_province_id');
    }
}