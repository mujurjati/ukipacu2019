<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:40
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingCourier extends Model
{
    public $timestamps      = false;
    protected $table        = '_shipping_courier';
    protected $primaryKey   = 's_courier_id';
}