<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CustomerPenunjang extends Model
{
    public $timestamps      = false;
    protected $table        = '_customer_penunjang';
    protected $primaryKey   = 'c_pen_id';

    public function customer() {
        $this->belongsTo('App\Models\Customer');
    }
}
