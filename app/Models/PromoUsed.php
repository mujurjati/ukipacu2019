<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:38
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoUsed extends Model
{
    public $timestamps      = false;
    protected $table        = '_promo_used';
    protected $primaryKey   = 'p_used_id';

    public function promo() {
        return $this->belongsTo('App\Models\Promo', 'promo_id', 'promo_id');
    }

    public function customer() {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'customer_id');
    }

    public function order() {
        return $this->belongsTo('App\Models\Order', 'order_id', 'order_id');
    }
}