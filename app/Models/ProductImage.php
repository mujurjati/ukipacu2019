<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:37
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    public $timestamps      = false;
    protected $table        = '_product_image';
    protected $primaryKey   = 'p_image_id';

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}