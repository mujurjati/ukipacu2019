<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:41
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingProvince extends Model
{
    public $timestamps      = false;
    protected $table        = '_shipping_province';
    protected $primaryKey   = 's_province_id';

    public function city() {
        return $this->hasMany('App\Models\ShippingCity', 's_province_id', 's_province_id');
    }
}