<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 1/11/2019
 * Time: 10:39 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EbookPackage extends Model
{
    public $timestamps      = false;
    protected $table        = '_ebook_package';
    protected $primaryKey   = 'e_package_id';

    public function subscribe() {
        return $this->hasMany('App\Models\EbookSubscribe', 'e_package_id', 'e_package_id');
    }
}