<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:30
 */

namespace App\Models;


class Cart
{
    public $timestamps      = false;
    protected $table        = '_cart_tmp';
    protected $primaryKey   = 'c_tmp_id';
}