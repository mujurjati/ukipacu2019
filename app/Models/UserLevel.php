<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:43
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    public $timestamps      = false;
    protected $table        = '_user_level';
    protected $primaryKey   = 'u_level_id';

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}