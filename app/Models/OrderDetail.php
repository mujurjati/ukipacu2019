<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:34
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public $timestamps      = false;
    protected $table        = '_order_detail';
    protected $primaryKey   = 'o_detail_id';

    public function merchant() {
        return $this->hasOne('App\Models\Merchant', 'merchant_id', 'merchant_id');
    }

    public function driver() {
        return $this->hasOne('App\Models\Driver', 'driver_id', 'driver_id');
    }

    public function detail_product() {
        return $this->hasMany('App\Models\OrderDetailProduct', 'o_detail_id', 'o_detail_id');
    }

    public function order() {
        return $this->belongsTo('App\Models\Order', 'order_id', 'order_id');
    }
}