<?php
/**
 * Created by PhpStorm.
 * User: Mujurjati
 * Date: 1/11/2019
 * Time: 10:48 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EbookProduct extends Model
{
    public $timestamps      = false;
    protected $table        = '_ebook_product';
    protected $primaryKey   = 'e_product_id';
}