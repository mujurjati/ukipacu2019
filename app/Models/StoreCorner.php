<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:42
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreCorner extends Model
{
    public $timestamps      = false;
    protected $table        = '_store_corner';
    protected $primaryKey   = 's_corner_id';

    public function subdistrict() {
        return $this->hasOne('App\Models\ShippingSubdistrict', 's_subdistrict_id', 's_subdistrict_id');
    }
}