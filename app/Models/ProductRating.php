<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:37
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRating extends Model
{
    public $timestamps      = false;
    protected $table        = '_product_rating';
    protected $primaryKey   = 'p_rating_id';

    public function customer() {
        return $this->belongsTo('App\Models\Customer');
    }

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }

    public function cusRating() {
        return $this->hasOne('App\Models\Customer', 'customer_id', 'customer_id');
    }
}