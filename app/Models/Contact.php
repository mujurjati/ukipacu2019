<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:27
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $timestamps      = false;
    protected $table        = '_guest';
    protected $primaryKey   = 'guest_id';
}