<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:29
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentCategory extends Model
{
    public $timestamps      = false;
    protected $table        = '_content_category';
    protected $primaryKey   = 'c_cat_id';

    public function content() {
        return $this->belongsTo('App\Models\Content');
    }
}