<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSuggest extends Model
{
    public $timestamps      = false;
    protected $table        = '_product_suggest';
    protected $primaryKey   = 'p_suggest_id';

    public function product() {
        return $this->hasOne('App\Models\Product', 'product_id', 'product_id');
    }
}