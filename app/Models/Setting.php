<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;
    protected $table = '_setting';
    protected $primaryKey = 'setting_id';

    function select_data() {
        $data = $this->all();

        $result = [];

        foreach($data as $key) {
            $result[$key->setting_type][$key->setting_name] = $key->setting_value;
        }

        return $result;
    }
    public function subdistrict() {
        return $this->hasOne('App\Models\ShippingSubdistrict', 's_subdistrict_id', 'setting_value');
    }
}
