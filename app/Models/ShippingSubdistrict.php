<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:41
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingSubdistrict extends Model
{
    public $timestamps      = false;
    protected $table        = '_shipping_subdistrict';
    protected $primaryKey   = 's_subdistrict_id';

    public function city() {
        return $this->belongsTo('App\Models\ShippingCity', 's_city_id', 's_city_id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function store() {
        return $this->belongsTo('App\Models\StoreCorner');
    }

    public function customer_address() {
        return $this->belongsTo('App\Models\CustomerAddress');
    }
}
