<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:29
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Content extends Model
{
    public $timestamps      = false;
    protected $table        = '_content';
    protected $primaryKey   = 'content_id';

    public function category() {
        return $this->hasOne('App\Models\ContentCategory', 'c_cat_id', 'c_cat_id');
    }

    public function user() {
        return $this->hasOne('App\Models\User', 'user_id', 'user_id');
    }

    public function listCat() {
        return $this->select('_content_category.*')
            ->join('_content_category', '_content.c_cat_id', '=', '_content_category.c_cat_id')
            ->where('_content.content_type', '=', 'blog')
            ->groupBy('_content.c_cat_id')
            ->get();
    }

    public function contentTerbaru() {
        return $this->where('content_type', '=', 'blog')
            ->orderBy('content_publish_date', 'DESC')
            ->limit(3)
            ->get();
    }

    public function contentArsip() {
        return DB::table('_content')
            ->select(DB::raw('YEAR(content_publish_date) year, MONTH(content_publish_date) month, MONTHNAME(content_publish_date) month_name, COUNT(*) post_count'))
            ->where('content_type', '=', 'blog')
            ->groupBy('year')
            ->groupBy('month')
            ->orderBy('year', 'desc')
            ->orderBy('month', 'desc')
            ->get();
    }
}