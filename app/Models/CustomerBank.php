<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:27
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerBank extends Model
{
    public $timestamps      = false;
    protected $table        = '_customer_bank';
    protected $primaryKey   = 'c_bank_id';

    public function customer() {
        $this->belongsTo('App\Models\Customer');
    }
}
