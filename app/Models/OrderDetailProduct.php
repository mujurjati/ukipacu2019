<?php
/**
 * Created by PhpStorm.
 * User: macbookair
 * Date: 2019-05-24
 * Time: 01:07
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetailProduct extends Model
{
    public $timestamps      = false;
    protected $table        = '_order_detail_product';
    protected $primaryKey   = 'od_product_id';

    public function product() {
        return $this->hasOne('App\Models\Product', 'product_id', 'product_id');
    }

    public function order_detail() {
        return $this->belongsTo('App\Models\OrderDetail', 'o_detail_id', 'o_detail_id');
    }
}