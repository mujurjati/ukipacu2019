<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:33
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerWishlist extends Model
{
    public $timestamps      = false;
    protected $table        = '_customer_wishlist';
    protected $primaryKey   = 'c_wishlist_id';

    public function product() {
        return $this->hasOne('App\Models\Product', 'product_id', 'product_id');
    }

    public function customer() {
        return $this->belongsTo('App\Models\Customer');
    }
}