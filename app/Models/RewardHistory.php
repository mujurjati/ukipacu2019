<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RewardHistory extends Model
{
    public $timestamps      = false;
    protected $table        = '_reward_history';
    protected $primaryKey   = 'rh_id';

    public function rewardHistory()
    {
        return $this->hasOne('App\Models\Customer', 'customer_id', 'customer_id');

    }
}
