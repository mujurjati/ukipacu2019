<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:33
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerWithdraw extends Model
{
    public $timestamps      = false;
    protected $table        = '_customer_withdraw';
    protected $primaryKey   = 'c_withdraw_id';

    public function customer() {
        return $this->belongsTo('App\Models\Customer');
    }
}