<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:36
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    public $timestamps      = false;
    protected $table        = '_product_attribute';
    protected $primaryKey   = 'p_attribute_id';

    public function category() {
        return $this->belongsTo('App\Models\ProductCategory', 'p_cat_id', 'p_cat_id');
    }
}