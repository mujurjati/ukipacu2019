<?php
/**
 * Created by PhpStorm.
 * User: Isnan IAS
 * Date: 19-Dec-18
 * Time: 20:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps      = false;
    protected $table        = '_product';
    protected $primaryKey   = 'product_id';

    public function category() {
        return $this->hasOne('App\Models\ProductCategory', 'p_cat_id', 'p_cat_id');
    }

    public function image_primary() {
        return $this->hasOne('App\Models\ProductImage', 'product_id', 'product_id')
            ->where('p_image_primary', '=', '1');
    }

    public function stock() {
        return $this->hasMany('App\Models\ProductStock', 'product_id', 'product_id');
    }

    public function ingredients() {
        return $this->hasMany('App\Models\ProductIngredient', 'product_id', 'product_id');
    }

    public function rating() {
        return $this->hasMany('App\Models\ProductRating', 'product_id', 'product_id');
    }

    public function merchant() {
        return $this->hasOne('App\Models\Merchant', 'merchant_id', 'merchant_id');
    }

    public function avgRating()
    {
        return $this->rating()
            ->selectRaw('avg(p_rating_value) as rating, product_id')
            ->groupBy('product_id');
    }

    public function getAvgRatingAttribute()
    {
        if ( ! array_key_exists('avgRating', $this->relations)) {
            $this->load('avgRating');
        }

        $relation = $this->getRelation('avgRating')->first();

        return ($relation) ? $relation->rating : 0;
    }

    public function order_detail() {
        return $this->belongsTo('App\Models\OrderDetail');
    }

    public function wishlist() {
        return $this->belongsTo('App\Models\CustomerWishlist');
    }
}